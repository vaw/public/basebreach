# Contribution Guidelines

The core development team of BASEbreach software (at Laboratory of Hydraulics, Hydrology and Glaciology (VAW) of ETH Zurich) welcomes contributions to this project.

However, please note that BASEbreach was originally developed for research purposes and the present version was extended for application by engineering companies. This project is not in ongoing development, and the project maintainers may not be able to provide immediate assistance with the implementation of new features or review of merge requests.

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * ExceptionHandling.h
 *
 *  Created on: May 12, 2014
 *      Author: samuelpeter
 */

#ifndef EXCEPTIONHANDLING_H_
#define EXCEPTIONHANDLING_H_

#include <iostream>
#include <sstream>

#define BB_MESSAGE(msg)                                                        \
  { std::cout << msg << std::endl; }

#define BB_EXCEPTION(msg)                                                      \
  {                                                                            \
    std::cerr << msg << "\nin file: " << __FILE__ << " on line " << __LINE__   \
              << ", compiled at " << __DATE__ << ", " << __TIME__              \
              << std::endl;                                                    \
    std::ostringstream _sout;                                                  \
    _sout << msg;                                                              \
    throw std::logic_error(_sout.str());                                       \
  }

#define BB_ASSERT(cond, msg)                                                   \
  {                                                                            \
    if (!(cond)) {                                                             \
      std::ostringstream _sout;                                                \
      _sout << msg;                                                            \
      if (_sout.str().size() == 0) {                                           \
        _sout << #cond;                                                        \
      }                                                                        \
      BB_EXCEPTION(msg);                                                       \
    }                                                                          \
  }

#define BB_WARNING(msg)                                                        \
  { BB_EXCEPTION(msg); }

#define BB_TRACE(msg)                                                          \
  {                                                                            \
    std::ostringstream _sout;                                                  \
    _sout << msg;                                                              \
    _sout << "\nin file: " << __FILE__ << " on line " << __LINE__              \
          << std::endl;                                                        \
    BB_MESSAGE(_sout.str());                                                   \
  }

#endif // EXCEPTIONHANDLING_H_

/*
 * GeometryTests.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 */

#define BOOST_TEST_MODULE GeometryTests

#include <boost/test/unit_test.hpp>
#include "api/IDam.h"
#include "model/Dam.h"
#include "api/IGeometry.h"
#include "model/GeometryWithNumerics.h"
#include "math.h"

using namespace boost;

BOOST_AUTO_TEST_CASE(Initialization) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	BOOST_REQUIRE_CLOSE(geom->getLevel(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 0.0, 1e-9);

	geom->init();
	BOOST_REQUIRE_CLOSE(geom->getLevel(), 4.5, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 4.5, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 1.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.259637310505756, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -4.984698277154260, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 0.396939655430852, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 1.593325160981950, 1e-6);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.715850535212686, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCallGreaterZero) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	geom->update(4.0);
	BOOST_REQUIRE_CLOSE(geom->getLevel(), 4.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 4.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 1.259637310505756, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.412241378276592, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -8.238621616709100, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 0.335127115073184, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 1.471853390805788, 1e-6);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.738526733246079, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCloseToZero) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	geom->update(0.1);
	BOOST_REQUIRE_CLOSE(geom->getLevel(), 0.1, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 0.1, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 3.284808332450655, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.774610079321769, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -55.203995070682382, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 0.157965914127835, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 1.153737477641027, 1e-6);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.780182104816362, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCallSmallerZero) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	geom->update(-1.0);
	BOOST_REQUIRE_CLOSE(geom->getLevel(), -1.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 4.365178919464604, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.594791909554995, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -80.048125606402877, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 0.347917376790796, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 1.534152544372356, 1e-6);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.761312807387051, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCallExtremeWidth) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	geom->update(-250.0);
	BOOST_REQUIRE_CLOSE(geom->getLevel(), -250.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 2.604475170227516e+02, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.009968891754997, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -6.645564193840859e+03, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 1.260124155853147e+02, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 2.546344229379231e+02, 1e-6);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.668867350360534, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCallRectangle) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryGauLeg(4.5,1.0,0.5,dam));

	geom->update(-1000.0);
	BOOST_REQUIRE_CLOSE(geom->getLevel(), -1000.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getWidth(), 1.031779860707351e+03, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getLambda(), 1.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getVolumeErosionRate(), -2.654359925107270e+04, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcFlowArea(0.5), 5.158899303536754e+02, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->calcWettedPerimeter(0.5), 1.032779860707351e+03, 1e-2);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(), 0.666666666666667, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateNonValid) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));

	geom->init();
	BOOST_CHECK_THROW(geom->update(5.1);, std::logic_error);
}

/*
 * ErosionTests.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: samuelpeter
 */

#define BOOST_TEST_MODULE ErosionTests

#include <boost/test/unit_test.hpp>
#include <api/IDam.h>
#include <model/Dam.h>
#include <api/IReservoir.h>
#include <model/Reservoir.h>
#include <api/IGeometry.h>
#include <model/GeometryWithNumerics.h>
#include <api/IHydraulics.h>
#include <model/Hydraulics.h>
#include <api/IErosion.h>
#include <model/Erosion.h>

using namespace boost;

BOOST_AUTO_TEST_CASE(Initialization) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));
	api::IHydraulics_t hydr(new model::Hydraulics(reservoir,geom));
	api::IErosion_t erosion(new model::Erosion(3.5,-0.5,0.01,hydr));

	BOOST_REQUIRE_CLOSE(erosion->getCalibrationCoefficient(), 0.01, 1e-9);
	BOOST_REQUIRE_CLOSE(erosion->getExponentA(), 3.5, 1e-9);
	BOOST_REQUIRE_CLOSE(erosion->getExponentB(), -0.5, 1e-9);
	BOOST_REQUIRE_CLOSE(erosion->getTransportRate(), 0.0, 1e-9);

	erosion->init();
	BOOST_REQUIRE_CLOSE(erosion->getTransportRate(), 0.134105491787658, 1e-9);
}

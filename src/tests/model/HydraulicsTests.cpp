/*
 * HydraulicsTests.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#define BOOST_TEST_MODULE HydraulicsTests

#include <boost/test/unit_test.hpp>
#include "math.h"
#include "api/IDam.h"
#include "model/Dam.h"
#include "api/IReservoir.h"
#include "model/Reservoir.h"
#include "api/IGeometry.h"
#include "model/GeometryWithNumerics.h"
#include "api/IHydraulics.h"
#include "model/Hydraulics.h"

using namespace boost;

BOOST_AUTO_TEST_CASE(Initialization) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));
	api::IHydraulics_t hydr(new model::Hydraulics(reservoir,geom));

	BOOST_REQUIRE_CLOSE(hydr->getDischarge(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getCriticalWaterDepth(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowArea(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowVelocity(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getHydraulicRadius(), 0.0, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getWettedPerimeter(), 0.0, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getErodiblePerimeter(), 0.0, 1e-6);
	BOOST_CHECK_EQUAL(hydr->isSteady(), false);

	hydr->init();

	BOOST_CHECK_EQUAL(hydr->isSteady(), true);
	BOOST_REQUIRE_CLOSE(hydr->getDischarge(), 0.434897656856629, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getCriticalWaterDepth(), 0.357925267606343, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowArea(), 0.260527342089781, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowVelocity(), 1.669297561507989, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getHydraulicRadius(), 0.200836750773738, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getWettedPerimeter(), 1.297209505163181, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getErodiblePerimeter(), 1.297209505163181, 1e-6);
}

BOOST_AUTO_TEST_CASE(UpdateCall) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(4.5,1.0,0.5,dam));
	api::IHydraulics_t hydr(new model::Hydraulics(reservoir,geom));

	hydr->init();
	reservoir->update(4);
	geom->update(-10);
	hydr->update();

	BOOST_CHECK_EQUAL(hydr->isSteady(), true);
	BOOST_REQUIRE_CLOSE(hydr->getDischarge(),1.391555822382060e+02, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getCriticalWaterDepth(), 2.816997230123148, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowArea(), 28.888993848676844, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowVelocity(), 4.816906499655735, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getHydraulicRadius(), 1.951451539816991, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getWettedPerimeter(), 14.803848960239147, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getErodiblePerimeter(), 14.803848960239147, 1e-6);
}

BOOST_AUTO_TEST_CASE(UpdateCallRitter) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));
	api::IGeometry_t geom(new model::GeometryAdaptiveTrapezSimpson(1.0,1.0,0.9,dam));
	api::IHydraulics_t hydr(new model::Hydraulics(reservoir,geom));

	hydr->init();
	BOOST_CHECK_EQUAL(hydr->isSteady(), false);
	BOOST_REQUIRE_CLOSE(reservoir->getLevel(),5.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getBreachlevel(),1.0, 1e-9);
	BOOST_REQUIRE_CLOSE(geom->getAlpha(),0.675526651067459, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getDischarge(),9.931313746929771, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getCriticalWaterDepth(), 2.281681281512084, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowArea(), 2.142079598793885, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getFlowVelocity(), 4.636295379742973, 1e-9);
	BOOST_REQUIRE_CLOSE(hydr->getHydraulicRadius(), 0.400165667712344, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getWettedPerimeter(), 5.352981956297421, 1e-6);
	BOOST_REQUIRE_CLOSE(hydr->getErodiblePerimeter(), 5.352981956297421, 1e-6);
}

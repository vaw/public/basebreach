/*
 * ReservoirTests.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#define BOOST_TEST_MODULE GeometryTests

#include <boost/test/unit_test.hpp>
#include "math.h"
#include "api/IDam.h"
#include "model/Dam.h"
#include "api/IReservoir.h"
#include "model/Reservoir.h"

using namespace boost;

BOOST_AUTO_TEST_CASE(Initialization) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));

	BOOST_REQUIRE_CLOSE(reservoir->getLevel(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getVolume(), 0.0, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getWaterSurface(), 0.0, 1e-9);

	reservoir->init();
	BOOST_REQUIRE_CLOSE(reservoir->getLevel(), 5.0, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getVolume(), 1e5, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getWaterSurface(), 1e5/5.0, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateCall) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));

	reservoir->update(2.0);
	BOOST_REQUIRE_CLOSE(reservoir->getLevel(), 2.0, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getVolume(), 100000.0*2.0/5.0, 1e-9);
	BOOST_REQUIRE_CLOSE(reservoir->getWaterSurface(), 100000.0 / 5.0, 1e-9);
}

BOOST_AUTO_TEST_CASE(UpdateNonValid) {
	api::IDam_t dam(new model::Dam(5.0, 3.0, 2.5));
	api::IReservoir_t reservoir(new model::Reservoir(1e5,5.0,1.0,dam));

	reservoir->init();
	BOOST_CHECK_THROW(reservoir->update(6.0), std::logic_error);
}



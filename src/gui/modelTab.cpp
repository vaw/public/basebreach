// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/modelTab.h"

#include <QtCore>
#include <QtWidgets>

#include "ExceptionHandling.h"
#include "core.h"
#include "gui/distributionEditor.h"
#include "gui/ui_modelTab.h"
#include "model/Distribution.h"
#include "model/Parameter.h"
#include "model/modelDefinition.h"
#include "types.h"

namespace {

QString modelIdToUrlFragment(const QString &name) {
  if (name == "bbMacchione")
    return "parameters-1";
  if (name == "bbAwel")
    return "parameters-2";
  if (name == "bbPeter")
    return "parameters-3";
  if (name == "bbPeterCal")
    return "parameters-4";
  return "";
}

} // namespace

namespace widgets {

namespace modelDef = model::definition;

ModelTab::ModelTab(QWidget *parent) : QWidget(parent) {
  ui_.reset(new Ui::ModelTab());
  ui_->setupUi(this);
  // Clear the model cache and disable export button
  clearLastModelRun();
  // Hook up signals
  QObject::connect(ui_->exportModelRunBtn, &QPushButton::clicked, this,
                   &ModelTab::exportModelRun);
  QObject::connect(ui_->runBtn, &QPushButton::clicked, this,
                   &ModelTab::runCurrentModel);
  QObject::connect(ui_->hydrograph, &gui::SingleHydrograph::timeChanged,
                   ui_->breachSection, &widgets::BreachSectionPlot::setTime);
  QObject::connect(ui_->importParamsBtn, &QPushButton::clicked, this,
                   &ModelTab::importParameters);
  QObject::connect(ui_->exportParamsBtn, &QPushButton::clicked, this,
                   &ModelTab::exportParameters);
}

ModelTab::ModelTab(const modelDef::ModelType *modelType, QWidget *parent)
    : ModelTab(parent) {
  modelType_ = modelType;
  // Create GUI elements for the model type
  auto params = modelType->getModelParams();
  for (auto it = params.begin(); it != params.end(); it++) {
    const modelDef::Parameter param = modelType->getParameter(*it);
    createGuiParameter(param);
  }
}

void ModelTab::exportModelRun() {
  // Load save location suggestion
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  // Get output filepath
  QString fileName = QFileDialog::getSaveFileName(
      this, "Export Model Run",
      dir + "/" + modelType_->getName().toLower() + "_export.csv",
      "CSV Files (*.csv)");
  if (fileName.isEmpty()) {
    return;
  }
  exportCSV(fileName);
}

void ModelTab::exportCSV(QString filename) const {
  BaseBreachCore::exportModelRun(lastModelRun_, filename);
}

void ModelTab::createGuiParameter(const modelDef::Parameter param) {
  QGridLayout *grid = ui_->parametersGrid;
  int row = grid->rowCount();
  // Create label
  QLabel *label = new QLabel(param.getName());
  label->setMinimumWidth(150);
  label->setToolTip(param.getDesc());
  grid->addWidget(label, row, 1);
  // Create unit string
  QLabel *unit = new QLabel(param.getUnit());
  unit->setMaximumWidth(40);
  grid->addWidget(unit, row, 2);
  // Create line edit
  QLineEdit *edit =
      new QLineEdit(QString::number(modelType_->getDefault(param)));
  edit->setMaximumWidth(60);
  paramEdits_[param.getId()] = edit;
  grid->addWidget(edit, row, 3);
  // Create Wiki help button
  QToolButton *help = new QToolButton();
  help->setText("?");
  // NOTE: The object name is used to define the URI fragment pointed to
  help->setObjectName(modelType_->getId());
  QObject::connect(help, &QToolButton::clicked, this, &ModelTab::openWikiLink);
  grid->addWidget(help, row, 4);
}

ModelConfig_T ModelTab::parseParameters() const {
  ModelConfig_T params;
  for (auto it = paramEdits_.begin(); it != paramEdits_.end(); it++) {
    // Convert input text
    bool conversionOk;
    QString text = it.value()->text();
    double value = text.toDouble(&conversionOk);
    if (!conversionOk)
      BB_EXCEPTION("Unable to convert to double: \"" + text.toStdString() +
                   "\"");
    // Update nested model key
    setNestedKey(params, it.key(), value);
  }
  return params;
}

void ModelTab::clearLastModelRun() {
  lastModelRun_.reset();
  ui_->exportModelRunBtn->setEnabled(false);
}

void ModelTab::loadModelRun(model::FullModel_t model) {
  // Load the raw model run into cache for export
  lastModelRun_ = model;
  ui_->exportModelRunBtn->setEnabled(true);
  // Update plots
  ui_->breachSection->setModel(model);
  ui_->hydrograph->setModel(model);
}

void ModelTab::runCurrentModel() { emit modelRunRequested(modelType_); }

void ModelTab::importParameters() {
  // Load open location suggestion
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  // Get filepath
  QString fileName = QFileDialog::getOpenFileName(this, "Import parameters",
                                                  dir, "JSON Files (*.json)");
  if (fileName.isEmpty())
    return;
  // Import parameters
  ModelConfig_T config = modelDef::loadModelConfig(fileName);
  loadModelConfig(config);
}

void ModelTab::loadModelConfig(const ModelConfig_T &config) {
  // Update parameters
  for (auto it = paramEdits_.begin(); it != paramEdits_.end(); it++) {
    QLineEdit *edit = it.value();
    // Break up nested key
    if (it.key().size() == 0)
      BB_EXCEPTION("Parameter ID may not be empty");
    QStringList words = it.key().split(".");
    if (words.size() > 2)
      BB_EXCEPTION("Only one separator allowed in parameter IDs: \"" +
                   it.key().toStdString() + "\"");
    String_T block = words[0];
    String_T key = words[1];
    // Update value from input config
    double value = config[block][key].toDouble();
    edit->setText(QString::number(value));
  }
}

void ModelTab::exportParameters() {
  // Load save location suggestion
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  // Get output filepath
  QString fileName = QFileDialog::getSaveFileName(
      this, "Export parameters",
      dir + "/parameters_" + modelType_->getName().toLower() + ".json",
      "JSON Files (*.json)");
  if (fileName.isEmpty())
    return;
  // Export parameters
  ModelConfig_T config = parseParameters();
  modelDef::saveModelConfig(config, fileName);
}

void ModelTab::setNestedKey(ModelConfig_T &params, String_T const &paramId,
                            double value) const {
  if (paramId.size() == 0)
    BB_EXCEPTION("Parameter ID may not be empty");
  QStringList words = paramId.split(".");
  if (words.size() > 2)
    BB_EXCEPTION("Only one separator allowed in parameter IDs: \"" +
                 paramId.toStdString() + "\"");
  String_T block = words[0];
  String_T key = words[1];
  // Create parameter block if it doesn't exist
  if (!params.contains(block))
    params[block] = QVariantMap();
  // Set the parameter value
  params[block][key] = QVariant::fromValue<double>(value);
}
void ModelTab::openWikiLink() {
  // Get the internal object name used to identify the button that was clicked
  // QString name = this->sender()->objectName();
  // Generate URL
  QUrl url = QUrl("https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/"
                  "dam-breach-models");
  url.setFragment(modelIdToUrlFragment(modelType_->getId()));
  QDesktopServices::openUrl(url);
}

ModelTabMC::ModelTabMC(QWidget *parent) : ModelTab(parent) {
  enableMc_ = new QCheckBox("Enable Monte Carlo analysis (Advanced)");
  ui_->leftPanel->addWidget(enableMc_);
  QObject::connect(enableMc_, &QCheckBox::toggled, this,
                   &ModelTabMC::setMCEnabled);
  // Prepend MC button in footer
  runMcBtn_ = new QPushButton("Run MC Analysis");
  runMcBtn_->setEnabled(false);
  ui_->rightFooter->insertWidget(1, runMcBtn_);
  QObject::connect(runMcBtn_, &QPushButton::clicked, this,
                   &ModelTabMC::runMcAnalysis);
  // KDE plot
  kdeGroup_ = new QGroupBox();
  kdeGroup_->setTitle("KDE Plot");
  kdeGroup_->setFlat(true);
  kdePlot_ = new widgets::Kde2dPlot();
  kdeGroup_->setLayout(new QVBoxLayout());
  kdeGroup_->layout()->addWidget(kdePlot_);
  ui_->rightPanel->insertWidget(0, kdeGroup_);
  // PDF plot
  pdfGroup_ = new QGroupBox();
  pdfGroup_->setTitle("PDF Plot");
  pdfGroup_->setFlat(true);
  pdfPlot_ = new widgets::ParameterPdfPlot();
  pdfGroup_->setLayout(new QVBoxLayout());
  pdfGroup_->layout()->addWidget(pdfPlot_);
  ui_->rightPanel->insertWidget(1, pdfGroup_);
}

ModelTabMC::ModelTabMC(const modelDef::ModelType *modelType, QWidget *parent)
    : ModelTabMC(parent) {
  modelType_ = modelType;
  // Add model parameters
  isMcEnabled_ = true; // setMcEnabled() only runs if the value changed
  setMCEnabled(false);
}

void ModelTabMC::loadMcAnalysis(QList<model::FullModel_t> results) {
  if (results.size() == 0)
    return;
  // Load variable names
  stringVec fieldNames = results[0]->getOutputVariableNames();
  uint peakDischargeIndex = 1;
  uint breachWidthIndex = 6;
  // Populate KDE plot
  doubleVec qp(results.size()), wb(results.size());
  for (qsizetype i = 0; i < results.size(); i++) {
    qp[i] = results[i]->Qp();
    wb[i] = results[i]->AverageBreachWidth();
  }
  kdePlot_->setData(qp, wb, results.size());
  kdePlot_->xAxis->setLabel(
      QString::fromStdString(fieldNames[peakDischargeIndex]));
  kdePlot_->yAxis->setLabel(
      QString::fromStdString(fieldNames[breachWidthIndex]));
  kdePlot_->replot();
  // Populdate PDF plot
  doubleVec pdfData(results.size());
  for (qsizetype i = 0; i < results.size(); i++) {
    pdfData.push_back(results[i]->Qp());
  }
  pdfPlot_->setData(pdfData, results.size());
  pdfPlot_->replot();
}

void ModelTabMC::setMCEnabled(bool enabled) {
  if (isMcEnabled_ == enabled)
    return;
  isMcEnabled_ = enabled;
  // Remove all parameters
  QLayout *layout = ui_->parametersGrid->layout();
  while (auto item = layout->takeAt(0)) {
    delete item->widget();
  }
  // Update "Run MC Analysis" button availability
  runMcBtn_->setEnabled(enabled);
  ui_->runBtn->setEnabled(!enabled);
  // Recreate parameters depending on MC checkbox state
  auto params = modelType_->getModelParams();
  for (auto it = params.begin(); it != params.end(); it++) {
    modelDef::Parameter param = modelType_->getParameter(*it);
    if (enabled)
      createDistributionParameter(param);
    else
      createGuiParameter(param);
  }
  // Switch right panel widgets to KDE and PDF
  ui_->sectionPlotGroup->setHidden(enabled);
  ui_->variablePlotGroup->setHidden(enabled);
  kdeGroup_->setVisible(enabled);
  pdfGroup_->setVisible(enabled);
}

void ModelTabMC::createDistributionParameter(modelDef::Parameter param) {
  QGridLayout *grid = ui_->parametersGrid;
  int row = grid->rowCount();
  QLabel *label = new QLabel(param.getName());
  label->setMinimumWidth(150);
  grid->addWidget(label, row, 1);
  // Distribution editor
  QToolButton *edit = new QToolButton();
  edit->setText("Define…");
  edit->setObjectName(param.getId());
  api::IDistribution_t dist = std::make_shared<model::UniformDistribution>();
  distributions_[param.getId()] = dist;
  QObject::connect(edit, &QToolButton::clicked, this,
                   &ModelTabMC::openDistributionEditor);
  grid->addWidget(edit, row, 2);
  // Wiki help link
  QToolButton *help = new QToolButton();
  help->setText("?");
  help->setObjectName(modelType_->getId());
  QObject::connect(help, &QToolButton::clicked, this,
                   &ModelTabMC::openWikiLink);
  grid->addWidget(help, row, 3);
}

void ModelTabMC::distributionUpdated(String_T paramId,
                                     api::IDistribution_t newDist) {
  distributions_[paramId] = newDist;
}

void ModelTabMC::runMcAnalysis() {
  // Check all distributions and validate the distribution parameters
  for (auto it = distributions_.begin(); it != distributions_.end(); it++)
    if (!it.value()->isDefined()) {
      // Get display name of the current parameter
      String_T paramName = modelType_->getParameter(it.key()).getName();
      BB_EXCEPTION("Distribution for parameter \"" + paramName.toStdString() +
                   "\" is not sufficiently defined");
    }
  // Prompt the user about the number of runs
  int numRuns = QInputDialog::getInt(
      this, "Monte Carlo Analysis",
      "Enter the number of parameter values to sample.", 1000, 1, 100000, 1);
  // Sample parameters
  QMap<String_T, doubleVec> parameterSamples;
  for (auto it = distributions_.begin(); it != distributions_.end(); it++) {
    String_T paramId = it.key();
    api::IDistribution_t dist = it.value();
    // Use legacy parameter object to sample distribution
    api::IParameter_t modParam = std::make_shared<model::Parameter>();
    modParam->setDistribution(dist);
    BB_ASSERT(modParam->isDistributionDefined(),
              "Unable to sample MC parameters");
    parameterSamples[paramId] = modParam->getRealizations(numRuns);
  }
  // Generate model configurations
  QList<ModelConfig_T> configs(numRuns);
  QList<String_T> paramNames = modelType_->getModelParams();
  for (int i = 0; i < numRuns; i++) {
    ModelConfig_T config;
    for (auto it = paramNames.begin(); it != paramNames.end(); it++) {
      String_T paramId = *it;
      double value = parameterSamples[paramId][i];
      setNestedKey(config, paramId, value);
    }
    configs[i] = config;
  }
  // Run MC analysis to main window
  emit MCRunRequested(modelType_, configs);
}

void ModelTabMC::openDistributionEditor() {
  QString paramId = this->sender()->objectName();
  api::IDistribution_t dist = distributions_[paramId];
  auto dlg = new gui::DistributionEditor(paramId, dist);
  // Hook up accept signal
  QObject::connect(dlg, &gui::DistributionEditor::distributionUpdated, this,
                   &ModelTabMC::distributionUpdated);
  dlg->exec();
}

} // namespace widgets

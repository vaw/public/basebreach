// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_MAINWINDOW_H_
#define GUI_MAINWINDOW_H_

#include <QtCore>
#include <QtWidgets>

#include "gui/modelTab.h"
#include "model/modelDefinition.h"
#include "types.h"

namespace Ui {

// Forward declaration of GUI implementation included via AUTOUIC
class MainWindow;

} // namespace Ui

namespace gui {

namespace modelDef = model::definition;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QList<modelDef::ModelType> modelTypes,
             QList<modelDef::Parameter> scenarioParams);

  /**
   * Create a model tab for the given model type.
   *
   * If the model type ID is either "bbPeter" or "bbPeterCal", the model tab
   * will support Monte Carlo simulations as well.
   */
  widgets::ModelTab *addModelTab(const modelDef::ModelType *modelType);

public slots:
  /** Open a link to the BASEchange source code repository. */
  void openRepository();
  /** Open the link to the BASEchange wiki. */
  void openWiki();
  /** Open the GitLab Wiki help section for a given parameter. */
  void openParameterHelp();

  /** Callback for finished model runs. */
  void loadModelRun(const modelDef::ModelType *modelType,
                    model::FullModel_t model);
  /** Load the results of an MC analysis for the given model type. */
  void loadMcAnalysis(const modelDef::ModelType *modelType,
                      const QList<model::FullModel_t>);
  /** Schedule the given model type to be run with their current parameters. */
  void runModel(const modelDef::ModelType *modelType);
  /** Schedule all enabled models to run with their current parameters. */
  void runEnabledModels();
  /** Perform MC analysis on the given model type. */
  void runMcAnalysis(const modelDef::ModelType *modelType,
                     QList<ModelConfig_T> configs);
  /** Load a scenario config from disk. */
  void importScenario();
  /** Write a scenario config to disk. */
  void exportScenario();
  /** Load model parameters from a file. */
  void importParameters();
  /** Write model parameters to a file. */
  void exportParameters();
  /** Load an inflow hydrograph from disk. */
  void browseReservoirInflow();

signals:
  /**
   * Signal the main app to run the given models.
   * Finished model runs are emitted as they are completed.
   * @param configs The model configurations to run.
   * @param numThreads The number of threads to use.
   */
  void batchModelRunRequested(const QList<ModelConfig_T> configs,
                              uint numThreads);
  /**
   * Signal the main app to run the given models.
   * Finished model runs are emitted all at once when all are finished.
   * All provided model configs must be for the same model type.
   * @param configs The model configurations to run.
   * @param numthreads The number of threads to use.
   */
  void bulkModelRunRequested(const QList<ModelConfig_T> configs,
                             uint numThreads);

private:
  /** Hook up signals for the MainWindow menu bar. */
  void attachMenuBarSignals();
  /** Hook up signals for the "Overview" tab. */
  void attachOverviewTabSignals();
  /** Get the link to the BASEchange source code repository. */
  QUrl getBasebreachRepoLink();
  /**
   * Reset the UI to its default state.
   *
   * This will clear all model runs and reset the value of all paremeters to
   * their default values.
   */
  void resetUi();
  /** Enable or disable models according to the given map. */
  void setEnabledModels(QMap<String_T, bool> enabledModels);
  void disableCurrentModel();
  void enableCurrentModel();
  void setScenarioParameters(ModelConfig_T config);
  /* **** */

  /** A model was enabled or disabled via the menu bar action. */
  void menuBarModelCheckedStateChanged();
  /** Create a new project using default values and load it. */
  void newProject();
  /** Close the current project. Resets the GUI. */
  void closeProject();
  /** Open a file picker and load the selected project. */
  void openProject();
  /** Load the model and scenario parameters from the given project file. */
  void loadProjectFromFile(const QString &fileName);
  /** Save the current project in place. */
  void saveProject();
  /** Save the model and scenario parameters as a project. */
  void saveProjectAs();
  void saveProjectToFile(const QString &fileName);
  /** Parse special PDE parameters from the advanced section. */
  ModelConfig_T readAdvancedParameters(uint *numThreads);
  /** Parse model-agnostic scenario parameters from the GUI into doubles. */
  ModelConfig_T readScenarioParameters();
  /** Parse model-specific paramters rom the GUI into doubles. */
  ModelConfig_T readModelParameters(String_T modelId);
  /** Callback for updated reservoir level edit. */
  void damHeightChanged();
  /** Run the model for the current model tab. */
  void runCurrent();
  /** Run the MC analysis for the current model tab. */
  void runMcCurrent();
  /** Callback for model tab changes. */
  void updateRunCurrentAvailability(int tabIndex);
  /** Validate the given model configuration. */
  QList<modelDef::ParameterLimitMessage>
  validateModelConfig(const modelDef::ModelType *modelType,
                      const ModelConfig_T &config) const;
  /** Print the warnings and exceptions from the given list. */
  void
  reportParameterLimits(const QList<modelDef::ParameterLimitMessage> &messages,
                        bool *modelCanRun);
  /** Callback for models being enabled/disabled via the overview hydrograph. */
  void overviewModelEnabledChanged(String_T const &modelId, bool enabled);
  /** Callback for models being enabled/disabled via the model action. */
  void currentModelEnabledDisabled();

  QScopedPointer<Ui::MainWindow> ui_;
  QList<modelDef::ModelType> modelTypes_;
  /** Dynamically created tabs for each implemented model */
  QMap<String_T, widgets::ModelTab *> modelTabs_;
  /** Map of model implementations to their latest runs */
  QMap<String_T, model::FullModel_t> modelRuns_;
  /** Map of model type IDs to their current enable state */
  QMap<String_T, bool> enabledModels_;
  /** The current project file name. */
  String_T currentProject_ = "";
};

} // namespace gui

#endif // GUI_MAINWINDOW_H_

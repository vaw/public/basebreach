// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */
 
#include "gui/mainWindow.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <qcustomplot.h>

#include "ExceptionHandling.h"
#include "gui/ui_mainWindow.h"
#include "gui/widgets/singleHydrograph.h"
#include "model/PDESystemSolver.h"
#include "model/modelDefinition.h"
#include "modelTypes.h"
#include "types.h"

namespace {

/** Translate QObject names to URL fragements used in help URLs. */
QString nameToUrlFragement(const QString &name) {
  // Dam geometry
  if (name == "heightHelp")
    return "dam-height";
  if (name == "bottomHelp")
    return "bottom-level";
  if (name == "crestWidthHelp")
    return "crest-width";
  if (name == "damSlopeHelp")
    return "embankment-slope";
  // Reservoir
  if (name == "volumeHelp")
    return "reservoir-capacity";
  if (name == "basinShapeHelp")
    return "reservoir-stage-volume-curve";
  if (name == "inflowHelp")
    return "reservoir-inflow";
  // Initial conditions
  if (name == "breachLevelHelp")
    return "breach-level";
  if (name == "reservoirLevelHelp")
    return "reservoir-level";
  // Simulation
  if (name == "cutoffHelp" || name == "pdePrecisionHelp" ||
      name == "erosionLimitHelp")
    return "simulation";
  // Advanced
  if (name == "pdeSolverHelp" || name == "pdeSolverArgumentHelp")
    return "differential-equation-solver";
  if (name == "integrationPrecHelp")
    return "relative-precision";
  if (name == "numThreadsHelp")
    return "thread-count";
  return "";
}

} // namespace

namespace gui {

namespace modelDef = model::definition;

MainWindow::MainWindow(QList<modelDef::ModelType> modelTypes,
                       QList<modelDef::Parameter> scenarioParams)
    : QMainWindow(), modelTypes_(modelTypes) {
  // Setup GUI
  ui_.reset(new Ui::MainWindow());
  ui_->setupUi(this);

  // Load available model types into hydrograph table
  ui_->overviewHydrograph->setModelDefinitions(modelTypes);

  // Create a model tab for each loaded model type
  for (auto type = modelTypes_.begin(); type != modelTypes_.end(); type++) {
    addModelTab(type);
    String_T typeId = type->getId();
    // Create menu action for model selection
    QAction *action = new QAction(type->getName(), this);
    action->setData(typeId);
    action->setCheckable(true);
    action->setChecked(true);
    QObject::connect(action, &QAction::triggered, this,
                     &MainWindow::menuBarModelCheckedStateChanged);
    // Add action to menu
    ui_->menuChoose_Models->addAction(action);
    enabledModels_[typeId] = true;
  }

  // Populate PDE solver combo box
  ui_->pdeSolverDropdown->clear();
  stringVec solver_names = model::PDESolverEmpty().getImplementedSolvers();
  for (auto const &name : solver_names) {
    ui_->pdeSolverDropdown->addItem(QString::fromStdString(name));
  }

  // Reset all editable GUI elements to their default state
  resetUi();
  // Connect Qt signals to slots
  attachMenuBarSignals();
  attachOverviewTabSignals();
  QObject::connect(ui_->tabWidget, &QTabWidget::currentChanged, this,
                   &MainWindow::updateRunCurrentAvailability);
  QObject::connect(ui_->runBtn, &QPushButton::clicked, this,
                   &MainWindow::runEnabledModels);

  // Load scenario parameter tooltips

  // NOTE: This is quite verbose, it would be cleaner to use a global scenario
  // parameter definition as is done for the model parameters, so we can then
  // loop through these definitions here and set the tooltips accordingly.
  //
  // Similarly, the scenario parameter help button signals (currently hooked up
  // in attachOverviewTabSignals() could be replaced with a dynamic solution.
  //
  // This would however require a few changes to the parameter definition system
  // as scneario parameters are grouped into sections, which we do not have time
  // for at the moment. --ls
  QMap<String_T, String_T> tooltipMap;
  for (auto it = scenarioParams.begin(); it != scenarioParams.end(); it++) {
    String_T id = it->getId();
    String_T desc = it->getDesc();
    tooltipMap[id] = desc;
  }
  ui_->heightLabel->setToolTip(tooltipMap["DAM.height"]);
  ui_->bottomLabel->setToolTip(tooltipMap["DAM.bottom_level"]);
  ui_->crestWidthLabel->setToolTip(tooltipMap["DAM.crest_width"]);
  ui_->damSlopeLabel->setToolTip(tooltipMap["DAM.embankment_slope"]);
  ui_->volumeLabel->setToolTip(tooltipMap["RESERVOIR.volume"]);
  ui_->basinShapeLabel->setToolTip(tooltipMap["RESERVOIR.shape_exponent"]);
  ui_->inflowLabel->setToolTip(tooltipMap["RESERVOIR.inflow"]);
  ui_->breachLevelLabel->setToolTip(tooltipMap["BREACH.initial_level"]);
  ui_->reservoirLevelLabel->setToolTip(tooltipMap["RESERVOIR.initial_level"]);
  ui_->cutoffLabel->setToolTip(tooltipMap["SIMULATION.cutoff_time"]);
  ui_->pdePrecisionLabel->setToolTip(
      tooltipMap["SIMULATION.precision_at_qmax"]);
  ui_->erosionLimitLabel->setToolTip(tooltipMap["SIMULATION.erosion_limit"]);
}

widgets::ModelTab *
MainWindow::addModelTab(const modelDef::ModelType *modelType) {
  // Determine whether the given model type supports MC functinality
  String_T modelName = modelType->getId();
  bool supportsMC = modelName == "bbPeter" || modelName == "bbPeterCal";
  // Create the appropriate ModelTab subclass
  widgets::ModelTab *tab;
  if (supportsMC)
    tab = new widgets::ModelTabMC(modelType, nullptr);
  else
    tab = new widgets::ModelTab(modelType, nullptr);
  ui_->tabWidget->addTab(tab, modelType->getName());
  modelTabs_[modelType->getId()] = tab;
  // Connect signals
  QObject::connect(tab, &widgets::ModelTab::modelRunRequested, this,
                   &MainWindow::runModel);
  if (supportsMC)
    QObject::connect(qobject_cast<widgets::ModelTabMC *>(tab),
                     &widgets::ModelTabMC::MCRunRequested, this,
                     &MainWindow::runMcAnalysis);
  return tab;
}

void MainWindow::openRepository() {
  // Open the basebreach repository in the default browser
  QDesktopServices::openUrl(getBasebreachRepoLink());
}

void MainWindow::openWiki() {
  // Open the basebreach repository in the default browser
  QUrl url(getBasebreachRepoLink().toString() + "-/wikis/");
  QDesktopServices::openUrl(url);
}

void MainWindow::openParameterHelp() {
  // Get the internal object name used to identify the sender
  QString name = this->sender()->objectName();
  // Concatenate the appropriate Wiki help page link
  QUrl url(getBasebreachRepoLink().toString() + "-/wikis/general-parameters");
  // Set URL fragment to the ID of the parameter (i.e. <link>#<fragment>)
  url.setFragment(nameToUrlFragement(name));
  // Open the URL in the default browser
  QDesktopServices::openUrl(url);
}

void MainWindow::loadModelRun(const modelDef::ModelType *modelType,
                              model::FullModel_t model) {
  // Update status
  int newValue = ui_->progressBar->value() + 1;
  if (newValue >= ui_->progressBar->maximum()) {
    ui_->status->setText("Done");
  } else {
    ui_->status->setText("Completed model run " + QString::number(newValue) +
                         " of " + QString::number(ui_->progressBar->maximum()));
  }
  ui_->progressBar->setValue(newValue);
  // Load data
  modelTabs_[modelType->getId()]->loadModelRun(model);
  // Update overview
  ui_->overviewHydrograph->addModel(modelType, model);
}

void MainWindow::loadMcAnalysis(const modelDef::ModelType *modelType,
                                const QList<model::FullModel_t> results) {
  // Find the model tab responsible
  widgets::ModelTabMC *tab =
      dynamic_cast<widgets::ModelTabMC *>(modelTabs_[modelType->getId()]);
  if (tab == nullptr) {
    ui_->status->setText("Unable to load MC results");
    BB_EXCEPTION("Model \"" + modelType->getName().toStdString() +
                 "\" does not support Monte Carlo analysis");
  }
  // Set loading status
  ui_->status->setText("Loading MC analysis");
  ui_->progressBar->setRange(0, 0);
  ui_->progressBar->setValue(0);
  // Load data
  tab->loadMcAnalysis(results);
  // Set completion status
  ui_->status->setText("Done");
  ui_->progressBar->setRange(0, 1);
  ui_->progressBar->setValue(1);
}

void MainWindow::runModel(const modelDef::ModelType *modelType) {
  ModelConfig_T config = readScenarioParameters();
  config["PROJECT"]["model"] = modelType->getId();
  uint numThreads;
  modelDef::mergeModelConfigs(config, readAdvancedParameters(&numThreads));
  modelDef::mergeModelConfigs(config, readModelParameters(modelType->getId()));
  QList<modelDef::ParameterLimitMessage> messages =
      modelDef::validateScenario(config, modelDef::defineScenarioParameters());
  messages.append(validateModelConfig(modelType, config));
  bool modelCanRun = true;
  reportParameterLimits(messages, &modelCanRun);
  if (modelCanRun) {
    QList<ModelConfig_T> models = {config};
    ui_->status->setText("Running model \"" + modelType->getName() + "\"...");
    ui_->progressBar->setRange(0, models.size());
    ui_->progressBar->setValue(0);
    emit batchModelRunRequested(models, numThreads);
  } else {
    ui_->status->setText("Cancelled");
    ui_->tabWidget->setCurrentIndex(1); // Switch to messages tab
  }
}

void MainWindow::runEnabledModels() {
  // Validate scenario parameters
  ModelConfig_T modelBase = readScenarioParameters();
  uint numThreads;
  ModelConfig_T advancedParams = readAdvancedParameters(&numThreads);
  modelDef::mergeModelConfigs(modelBase, advancedParams);
  // Validate and load model parameters
  QList<modelDef::ParameterLimitMessage> messages = modelDef::validateScenario(
      modelBase, modelDef::defineScenarioParameters());
  QList<ModelConfig_T> modelConfigs = {};
  for (auto it = modelTypes_.begin(); it != modelTypes_.end(); it++) {
    String_T modelId = it->getId();
    if (!(enabledModels_.contains(modelId) && enabledModels_[modelId]))
      continue;
    // Load model definition
    ModelConfig_T config(modelBase);
    config["PROJECT"]["model"] = modelId;
    modelDef::mergeModelConfigs(config, readModelParameters(modelId));
    // Validate model parameters
    messages.append(validateModelConfig(it, config));
    modelConfigs.append(config);
  }
  // Load model messages
  ui_->progressBar->setRange(0, modelConfigs.size());
  ui_->progressBar->setValue(0);
  bool modelCanRun = true;
  reportParameterLimits(messages, &modelCanRun);
  if (modelCanRun) {
    ui_->status->setText("Running models...");
    emit batchModelRunRequested(modelConfigs, numThreads);
  } else {
    ui_->status->setText("Cancelled");
    ui_->tabWidget->setCurrentIndex(1); // Switch to messages tab
  }
}

void MainWindow::runMcAnalysis(const modelDef::ModelType *modelType,
                               QList<ModelConfig_T> configs) {
  // Load shared parameters
  ModelConfig_T modelBase = readScenarioParameters();
  modelBase["PROJECT"]["model"] = modelType->getId();
  uint numThreads;
  modelDef::mergeModelConfigs(modelBase, readAdvancedParameters(&numThreads));
  // Find the model tab responsible
  widgets::ModelTabMC *tab =
      qobject_cast<widgets::ModelTabMC *>(modelTabs_[modelType->getId()]);
  if (tab == nullptr)
    BB_EXCEPTION("Model \"" + modelType->getName().toStdString() +
                 "\" does not support Monte Carlo analysis");
  // Generate model configurations
  auto scenarioParams = modelDef::defineScenarioParameters();
  QList<ModelConfig_T> validConfigs;
  validConfigs.reserve(configs.size());
  for (qsizetype i = 0; i < configs.size(); i++) {
    ModelConfig_T config(modelBase);
    modelDef::mergeModelConfigs(config, configs[i]);
    // Validate generated config
    QList<modelDef::ParameterLimitMessage> messages =
        modelDef::validateScenario(config, scenarioParams);
    messages.append(validateModelConfig(modelType, config));
    bool configIsValid = true;
    for (auto it = messages.begin(); it != messages.end(); it++)
      if (it->limitType_ == modelDef::ParameterLimitType::CRITICAL)
        configIsValid = false;
    if (configIsValid)
      validConfigs.append(config);
  }
  // Warn user if not all models were valid
  if (validConfigs.size() == 0)
    BB_EXCEPTION("None of the generated model configurations were considered "
                 "valid.\n"
                 "Check the \"Messages\" tab for generic errors and review "
                 "your distribution definitions.");
  if (validConfigs.size() < configs.size())
    QMessageBox::warning(this, "Invalid model configurations",
                         "Some of the generated model configurations are "
                         "invalid and will be skipped.\n\n"
                         "Continuing MC analysis with " +
                             QString::number(validConfigs.size()) +
                             " samples.");
  ui_->status->setText("Running Monte Carlo analysis...");
  ui_->progressBar->setRange(0, 0); // "Busy" indicator
  // Schedule model runs
  emit bulkModelRunRequested(validConfigs, numThreads);
}

void MainWindow::attachMenuBarSignals() {
  // File
  QObject::connect(ui_->actionNew_project, &QAction::triggered, this,
                   &MainWindow::newProject);
  QObject::connect(ui_->actionOpen_project, &QAction::triggered, this,
                   &MainWindow::openProject);
  QObject::connect(ui_->actionClose_project, &QAction::triggered, this,
                   &MainWindow::closeProject);
  QObject::connect(ui_->actionSave, &QAction::triggered, this,
                   &MainWindow::saveProject);
  QObject::connect(ui_->actionSave_As, &QAction::triggered, this,
                   &MainWindow::saveProjectAs);
  QObject::connect(ui_->actionQuit, &QAction::triggered, this,
                   &QMainWindow::close);
  // Edit
  QObject::connect(ui_->actionImport_Parameters, &QAction::triggered, this,
                   &MainWindow::importParameters);
  QObject::connect(ui_->actionExport_Parameters, &QAction::triggered, this,
                   &MainWindow::exportParameters);
  QObject::connect(ui_->actionImport_Scenario, &QAction::triggered, this,
                   &MainWindow::importScenario);
  QObject::connect(ui_->actionExport_Scenario, &QAction::triggered, this,
                   &MainWindow::exportScenario);
  QObject::connect(ui_->actionDisable_Model, &QAction::triggered, this,
                   &MainWindow::disableCurrentModel);
  QObject::connect(ui_->actionEnable_Model, &QAction::triggered, this,
                   &MainWindow::enableCurrentModel);
  // NOTE: The "choose model" entries are populated during model tab creation
  // Run
  QObject::connect(ui_->actionRun_Current, &QAction::triggered, this,
                   &MainWindow::runCurrent);
  QObject::connect(ui_->actionRun_All, &QAction::triggered, this,
                   &MainWindow::runEnabledModels);
  QObject::connect(ui_->actionRun_MC_Analysis, &QAction::triggered, this,
                   &MainWindow::runMcCurrent);
  // Help
  QObject::connect(ui_->actionSource_Code_Repository, &QAction::triggered, this,
                   &MainWindow::openRepository);
  QObject::connect(ui_->actionBASEbreach_Wiki, &QAction::triggered, this,
                   &MainWindow::openWiki);
}

void MainWindow::attachOverviewTabSignals() {
  // Some changes in the overview tab need to be passed on to hydrogrpah to
  // update the standard breach reference model
  QObject::connect(ui_->heightEdit, &QLineEdit::textChanged, this,
                   &MainWindow::damHeightChanged);
  QObject::connect(ui_->bottomEdit, &QLineEdit::textChanged, this,
                   &MainWindow::damHeightChanged);

  // General signals for the overview tab GUI
  QObject::connect(ui_->importScenarioBtn, &QPushButton::clicked, this,
                   &MainWindow::importScenario);
  QObject::connect(ui_->exportScenarioBtn, &QPushButton::clicked, this,
                   &MainWindow::exportScenario);
  QObject::connect(ui_->overviewHydrograph,
                   &MultiHydrograph::modelEnabledChanged, this,
                   &MainWindow::overviewModelEnabledChanged);

  // Signals for the scenario parameter help buttons
  QObject::connect(ui_->heightHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->bottomHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->crestWidthHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->damSlopeHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->volumeHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->inflowHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->basinShapeHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->breachLevelHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->reservoirLevelHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->cutoffHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->erosionLimitHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->pdePrecisionHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->pdeSolverHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->pdeSolverArgumentHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->integrationPrecHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);
  QObject::connect(ui_->numThreadsHelp, &QToolButton::clicked, this,
                   &MainWindow::openParameterHelp);

  QObject::connect(ui_->browseInflow, &QPushButton::clicked, this,
                   &MainWindow::browseReservoirInflow);
}

QUrl MainWindow::getBasebreachRepoLink() {
  return QUrl("https://gitlab.ethz.ch/vaw/public/basebreach/");
}

void MainWindow::resetUi() {
  // Collapse the "advanced settings" group
  ui_->showAdvancedSettings->setChecked(false);

  // Formerly "resetGuiState"
  ui_->overviewHydrograph->clear();
  // Reset progress bar
  ui_->progressBar->setRange(0, 1);
  ui_->progressBar->setValue(0);
  // Reset status label
  ui_->status->setText("Ready");

  ui_->pdeSolverDropdown->setCurrentIndex(3); // "rk4-classic" by default
  // Default arguments
  ui_->pdeSolverArgumentEdit->setText(QString::number(1));
  ui_->pdePrecisionEdit->setText(QString::number(1e-3));
}

void MainWindow::setEnabledModels(QMap<String_T, bool> enabledModels) {
  // Update model tab availability
  for (auto const tab : modelTabs_) {
    String_T modelId = tab->getModelType()->getId();
    // Update its availability according to the "enabledModels" map
    int index = ui_->tabWidget->indexOf(tab);
    ui_->tabWidget->setTabEnabled(index, enabledModels[modelId]);
  }
  // Update model availability in the overview tab
  ui_->overviewHydrograph->setEnabledModels(enabledModels);
  // Update model availability in the "Edit -> Choose model" menu
  QList<QAction *> toggles = ui_->menuChoose_Models->actions();
  for (auto action : toggles) {
    String_T modelId = action->data().toString();
    action->setEnabled(enabledModels[modelId]);
  }
  // Store the new enabled models selection
  enabledModels_ = enabledModels;
}

void MainWindow::disableCurrentModel() {
  // Shifted by 2 to compensate for overview and messages panels
  const int modelIndex = ui_->tabWidget->currentIndex() - 2;
  if (modelIndex < 0)
    return;
  modelDef::ModelType modelType = modelTypes_[modelIndex];
  // Update model enabled list
  auto enabledModels = enabledModels_;
  enabledModels[modelType.getId()] = false;
  setEnabledModels(enabledModels);
}

void MainWindow::enableCurrentModel() {
  // Shifted by 2 to compensate for overview and messages panels
  const int modelIndex = ui_->tabWidget->currentIndex() - 2;
  if (modelIndex < 0)
    return;
  modelDef::ModelType modelType = modelTypes_[modelIndex];
  // Update model enabled list
  auto enabledModels = enabledModels_;
  enabledModels[modelType.getId()] = true;
  setEnabledModels(enabledModels);
}

void MainWindow::menuBarModelCheckedStateChanged() {
  QAction *action = qobject_cast<QAction *>(sender());
  String_T modelId = action->data().toString();
  bool enabled = action->isChecked();
  // Get model enabled state
  QMap<String_T, bool> enabledModels = enabledModels_;
  enabledModels[modelId] = enabled;
  setEnabledModels(enabledModels);
}

void MainWindow::newProject() {
  // Close the existing project (will prompt user to save if necessary)
  closeProject();
  // Get save location and store it for in-place saving
  QString fileName = QFileDialog::getSaveFileName(
      this, "Project file", QDir::homePath(), "Project files (*.json)");
  currentProject_ = fileName;
}

void MainWindow::closeProject() {
  // Check for unsaved changes in open project and ask to save
  if (currentProject_ != "") {
    int ret = QMessageBox::warning(this, "Unsaved changes",
                                   "The current project has unsaved changes.\n"
                                   "Do you want to save them before closing?",
                                   QMessageBox::Save | QMessageBox::Discard |
                                       QMessageBox::Cancel);
    if (ret == QMessageBox::Save) {
      saveProject();
    } else if (ret == QMessageBox::Cancel) {
      return;
    }
  }
  // Reset project
  ui_->titleEdit->setText("");
  ui_->dateEdit->setText("Never");
  ui_->authorEdit->setText("");
  // Reset scenario
  ui_->heightEdit->setText("");
  ui_->bottomEdit->setText("");
  ui_->crestWidthEdit->setText("");
  ui_->damSlopeEdit->setText("");
  ui_->breachLevelEdit->setText("");
  ui_->volumeEdit->setText("");
  ui_->basinShapeEdit->setText("1.5");
  ui_->inflowCheckbox->setChecked(false);
  ui_->inflowEdit->setText("0");
  ui_->breachLevelEdit->setText("");
  ui_->reservoirLevelEdit->setText("");
  ui_->pdePrecisionEdit->setText(QString::number(1e-2));
  ui_->cutoffEdit->setText(QString::number(2000));
  ui_->erosionLimitCheckbox->setChecked(false);
  ui_->erosionLimitEdit->setText("");
  ui_->pdeSolverDropdown->setCurrentIndex(3); // "rk4-classic" by default
  ui_->pdeSolverArgumentEdit->setText(QString::number(1));
  ui_->integrationPrecEdit->setText(QString::number(1e-2));
  // Reset model tabs
  for (qsizetype i = 2; i < ui_->tabWidget->count(); i++) {
    ui_->tabWidget->setTabEnabled(i, false);
  }
  ui_->tabWidget->setCurrentIndex(0);
  for (auto it = modelTabs_.begin(); it != modelTabs_.end(); it++) {
    widgets::ModelTab *tab = it.value();
    // Create a config containing default values
    auto type = tab->getModelType();
    auto params = type->getModelParams();
    ModelConfig_T config;
    for (auto pi = params.begin(); pi != params.end(); pi++) {
      auto param = type->getParameter(*pi);
      auto paramKeyList = param.getId().split(".");
      String_T block = paramKeyList[0];
      String_T key = paramKeyList[1];
      if (!config.contains(block)) {
        config[block] = ParameterMap_T();
      }
      config[block][key] = param.getDefault();
    }
    tab->loadModelConfig(config);
  }
}

void MainWindow::openProject() {
  // Get project file name
  QString filename = QFileDialog::getOpenFileName(
      this, "Open project file", QDir::homePath(), "Project files (*.json)");
  if (filename == "") {
    return;
  }
  // Close the existing project (will prompt user to save if necessary)
  closeProject();
  // Load project
  loadProjectFromFile(filename);
}

void MainWindow::loadProjectFromFile(const QString &filename) {
  // Read project file from JSON
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly))
    BB_EXCEPTION("Could not open project file \"" + filename.toStdString() +
                 "\"");
  QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
  file.close();
  if (doc.isNull())
    BB_EXCEPTION("Could not parse project file \"" + filename.toStdString() +
                 "\"");
  currentProject_ = filename;
  QJsonObject data = doc.object();
  // Load project info
  QJsonObject project = data["PROJECT"].toObject();
  ui_->titleEdit->setText(project["title"].toString());
  ui_->authorEdit->setText(project["author"].toString());
  ui_->dateEdit->setText(project["date"].toString());
  // Load scenario parameters
  ModelConfig_T scenario;
  for (auto it = data.begin(); it != data.end(); it++) {
    if (it.key() == "MODELS")
      continue;
    ParameterMap_T params = it.value().toObject().toVariantMap();
    scenario[it.key()] = params;
  }
  setScenarioParameters(scenario);
  // Load model parameters
  QMap<QString, ModelConfig_T> models;
  QJsonObject model_data = data["MODELS"].toObject();
  for (auto model_it = model_data.begin(); model_it != model_data.end();
       model_it++) {
    ModelConfig_T config;
    QJsonObject param_data = model_it.value().toObject();
    for (auto it = param_data.begin(); it != param_data.end(); it++) {
      ParameterMap_T params = it.value().toObject().toVariantMap();
      config[it.key()] = params;
    }
    models[model_it.key()] = config;
  }
  // Load model parameters
  for (auto it = modelTypes_.begin(); it != modelTypes_.end(); it++) {
    String_T modelId = it->getId();
    if (!models.contains(modelId))
      continue;
    widgets::ModelTab *tab = modelTabs_[modelId];
    tab->loadModelConfig(models[modelId]);
  }
  // Load advanced parameters
  auto numerics = data["NUMERICS"].toObject();
  ui_->pdeSolverDropdown->setCurrentText(numerics["pde_solver"].toString());
  ui_->pdeSolverArgumentEdit->setText(
      numerics["pde_solver_argument"].toVariant().toString());
  ui_->integrationPrecEdit->setText(
      numerics["integration_precision"].toVariant().toString());
  // Set enabled models
  QMap<QString, bool> enabled;
  for (auto it = modelTypes_.begin(); it != modelTypes_.end(); it++) {
    enabled[it->getId()] = model_data.contains(it->getId());
  }
  setEnabledModels(enabled);
}

void MainWindow::saveProject() {
  if (currentProject_ == "") {
    // Get project name
    QString fileName = QFileDialog::getSaveFileName(
        this, "Project file", QDir::homePath(), "Project files (*.json)");
    currentProject_ = fileName;
  }
  saveProjectToFile(currentProject_);
}

void MainWindow::saveProjectAs() {
  QString fileName = QFileDialog::getSaveFileName(
      this, "Project file", QDir::homePath(), "Project files (*.json)");
  saveProjectToFile(fileName);
}

void MainWindow::saveProjectToFile(const QString &filename) {
  // Check if the project name and author are specified
  if (ui_->titleEdit->text().isEmpty()) {
    QMessageBox::warning(
        this, "Project name missing",
        "Please specify a project name and author before saving.");
    return;
  }
  if (ui_->authorEdit->text().isEmpty()) {
    QMessageBox::warning(
        this, "Project author missing",
        "Please specify a project name and author before saving.");
    return;
  }
  QJsonObject data;
  // Save project info
  QJsonObject project;
  project["title"] = ui_->titleEdit->text();
  project["author"] = ui_->authorEdit->text();
  ui_->dateEdit->setText(
      QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
  project["date"] = ui_->dateEdit->text();
  data["PROJECT"] = project;
  // Save scenario parameters
  QJsonObject scenario;
  auto scenarioParams = readScenarioParameters();
  for (auto it = scenarioParams.begin(); it != scenarioParams.end(); it++) {
    auto param = QJsonObject::fromVariantMap(it.value());
    scenario[it.key()] = param;
  }
  // Merge scenario keys
  for (auto it = scenario.constBegin(); it != scenario.constEnd(); it++) {
    data.insert(it.key(), it.value());
  }
  // Save all enabled models
  QJsonObject models;
  for (auto it = modelTabs_.begin(); it != modelTabs_.end(); it++) {
    // Skip disabled models
    if (!enabledModels_[it.key()]) {
      continue;
    }
    widgets::ModelTab *tab = it.value();
    QJsonObject model;
    ModelConfig_T modelParams = tab->parseParameters();
    for (auto it = modelParams.begin(); it != modelParams.end(); it++) {
      auto param = QJsonObject::fromVariantMap(it.value());
      model.insert(it.key(), param);
    }
    models[tab->getModelType()->getId()] = model;
  }
  data["MODELS"] = models;
  // Advanced parameters
  uint threads;
  auto numerics = readAdvancedParameters(&threads)["NUMERICS"];
  data["NUMERICS"] = QJsonObject::fromVariantMap(numerics);
  // Save json object to file
  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly)) {
    QMessageBox::warning(this, "Error", "Could not open file for writing.");
    return;
  }
  QJsonDocument doc(data);
  file.write(doc.toJson());
  file.close();
  currentProject_ = filename;
}

ModelConfig_T MainWindow::readAdvancedParameters(uint *numThreads) {
  ParameterMap_T numerics;
  // Miscellaneous
  *numThreads = ui_->numThreads->value();
  // Numerics
  numerics["pde_solver"] = ui_->pdeSolverDropdown->currentText();
  numerics["pde_solver_argument"] =
      ui_->pdeSolverArgumentEdit->text().toDouble();
  numerics["integration_precision"] =
      ui_->integrationPrecEdit->text().toDouble();
  // Build return map
  ModelConfig_T params;
  params["NUMERICS"] = numerics;
  return params;
}

ModelConfig_T MainWindow::readModelParameters(String_T modelId) {
  widgets::ModelTab *tab = modelTabs_[modelId];
  ModelConfig_T params = tab->parseParameters();
  return params;
}

ModelConfig_T MainWindow::readScenarioParameters() {
  ParameterMap_T project;
  ParameterMap_T dam;
  ParameterMap_T breach;
  ParameterMap_T reservoir;
  ParameterMap_T simulation;
  // Project
  project["title"] = ui_->titleEdit->text();
  project["author"] = ui_->authorEdit->text();
  project["date"] = ui_->dateEdit->text();
  // Dam geometry
  dam["height"] = ui_->heightEdit->text().toDouble();
  dam["bottom_level"] = ui_->bottomEdit->text().toDouble();
  dam["crest_width"] = ui_->crestWidthEdit->text().toDouble();
  dam["embankment_slope"] = ui_->damSlopeEdit->text().toDouble();
  // Reservoir
  reservoir["volume"] = ui_->volumeEdit->text().toDouble();
  reservoir["shape_exponent"] = ui_->basinShapeEdit->text().toDouble();
  QString inflow = "0";
  if (ui_->inflowCheckbox->isChecked()) {
    inflow = ui_->inflowEdit->text();
  }
  reservoir["inflow"] = inflow;
  // Initial conditions
  breach["initial_level"] = ui_->breachLevelEdit->text().toDouble();
  reservoir["initial_level"] = ui_->reservoirLevelEdit->text().toDouble();
  // Simulation parameters
  simulation["cutoff_time"] = ui_->cutoffEdit->text().toDouble();
  simulation["precision_at_qmax"] = ui_->pdePrecisionEdit->text().toDouble();
  if (ui_->erosionLimitCheckbox->isChecked() &&
      ui_->erosionLimitEdit->text().toDouble() > 0.0) {
    simulation["erosion_limit"] = ui_->erosionLimitEdit->text().toDouble();
  } else {
    simulation["erosion_limit"] = 0;
  }
  // Build return map
  ModelConfig_T params;
  params["PROJECT"] = project;
  params["DAM"] = dam;
  params["BREACH"] = breach;
  params["RESERVOIR"] = reservoir;
  params["SIMULATION"] = simulation;
  return params;
}

void MainWindow::importScenario() {
  // Load load location suggestion
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  // Get filepath
  QString fileName = QFileDialog::getOpenFileName(
      this, "Import scenario", dir, QString("JSON Files (*.json)"));
  if (fileName.isEmpty())
    return;
  // Import parameters
  ModelConfig_T config = modelDef::loadModelConfig(fileName);
  setScenarioParameters(config);
}

void MainWindow::setScenarioParameters(ModelConfig_T config) {
  // Project info
  ui_->titleEdit->setText(config["PROJECT"]["title"].toString());
  ui_->authorEdit->setText(config["PROJECT"]["author"].toString());
  QDateTime last_saved = QDateTime::fromString(
      config["PROJECT"]["date"].toString(), "yyyy-MM-dd hh:mm:ss");
  ui_->dateEdit->setText(last_saved.toString("yyyy-MM-dd hh:mm:ss"));
  // Breach
  ui_->breachLevelEdit->setText(config["BREACH"]["initial_level"].toString());
  // Dam
  ui_->heightEdit->setText(config["DAM"]["height"].toString());
  ui_->bottomEdit->setText(config["DAM"]["bottom_level"].toString());
  ui_->crestWidthEdit->setText(config["DAM"]["crest_width"].toString());
  ui_->damSlopeEdit->setText(config["DAM"]["embankment_slope"].toString());
  // Reservoir
  ui_->volumeEdit->setText(config["RESERVOIR"]["volume"].toString());
  ui_->basinShapeEdit->setText(
      config["RESERVOIR"]["shape_exponent"].toString());
  ui_->reservoirLevelEdit->setText(
      config["RESERVOIR"]["initial_level"].toString());
  QString inflow = config["RESERVOIR"]["inflow"].toString();
  ui_->inflowEdit->setText(inflow);
  if (inflow != "0") {
    ui_->inflowCheckbox->setChecked(true);
  } else {
    ui_->inflowCheckbox->setChecked(false);
  }
  // Simulation
  ui_->cutoffEdit->setText(config["SIMULATION"]["cutoff_time"].toString());
  ui_->pdePrecisionEdit->setText(
      config["SIMULATION"]["precision_at_qmax"].toString());
  if (config["SIMULATION"].contains("erosion_limit") &&
      config["SIMULATION"]["erosion_limit"].toDouble() > 0.0) {
    ui_->erosionLimitCheckbox->setChecked(true);
    ui_->erosionLimitEdit->setText(
        config["SIMULATION"]["erosion_limit"].toString());
  } else {
    ui_->erosionLimitCheckbox->setChecked(false);
    ui_->erosionLimitEdit->setText("0");
  }
  // Load advanced params
  ui_->pdeSolverDropdown->setCurrentText(
      config["NUMERICS"]["pde_solver"].toString());
  ui_->pdeSolverArgumentEdit->setText(
      config["NUMERICS"]["pde_solver_argument"].toString());
  ui_->integrationPrecEdit->setText(
      config["NUMERICS"]["integration_precision"].toString());
}

void MainWindow::exportScenario() {
  // Load save location suggestion
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  // Get output filepath
  QString fileName = QFileDialog::getSaveFileName(
      this, "Export scenario", dir + "/scenario.json", "JSON Files (*.json)");
  if (fileName.isEmpty())
    return;
  // Export parameters
  ui_->dateEdit->setText(
      QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
  ModelConfig_T config = readScenarioParameters();
  uint numThreads;
  modelDef::mergeModelConfigs(config, readAdvancedParameters(&numThreads));
  modelDef::saveModelConfig(config, fileName);
}

void MainWindow::importParameters() {
  int index = ui_->tabWidget->currentIndex() - 2;
  if (index < 0)
    return;
  widgets::ModelTab *tab = modelTabs_[modelTypes_[index].getId()];
  tab->importParameters();
}

void MainWindow::exportParameters() {
  int index = ui_->tabWidget->currentIndex() - 2;
  if (index < 0)
    return;
  widgets::ModelTab *tab = modelTabs_[modelTypes_[index].getId()];
  tab->exportParameters();
}

void MainWindow::browseReservoirInflow() {
  QString dir = QStandardPaths::displayName(QStandardPaths::DesktopLocation);
  QString fileName = QFileDialog::getOpenFileName(
      this, "Import reservoir inflow", dir, "Text Files (*.txt)");
  if (fileName.isEmpty())
    return;
  ui_->inflowEdit->setText(fileName);
}

void MainWindow::damHeightChanged() {
  double crestHeight = ui_->heightEdit->text().toDouble();
  double bottomLevel = ui_->bottomEdit->text().toDouble();
  ui_->overviewHydrograph->parametersChanged(crestHeight - bottomLevel);
}

void MainWindow::runCurrent() {
  // Shifted by 2 to compensate for overview and messages panels
  const int modelIndex = ui_->tabWidget->currentIndex() - 2;
  if (modelIndex < 0)
    return;
  modelDef::ModelType modelType = modelTypes_[modelIndex];
  runModel(&modelType);
}

void MainWindow::runMcCurrent() {
  // Shifted by 2 to compensate for overview and messages panels
  const int modelIndex = ui_->tabWidget->currentIndex() - 2;
  if (modelIndex < 0)
    return;
  modelDef::ModelType modelType = modelTypes_[modelIndex];
  widgets::ModelTab *tab = modelTabs_[modelType.getId()];
  // See if the given model tab supports MC
  widgets::ModelTabMC *mcTab = dynamic_cast<widgets::ModelTabMC *>(tab);
  if (mcTab == nullptr) {
    // Model tab is not MC-capable
    return;
  }
  mcTab->runMcAnalysis();
}

void MainWindow::updateRunCurrentAvailability(int tabIndex) {
  // Tab indices 0 and 1 are the overview and messages panels
  bool isModelTab = tabIndex >= 2;
  ui_->actionRun_Current->setEnabled(isModelTab);
  ui_->actionImport_Parameters->setEnabled(isModelTab);
  ui_->actionExport_Parameters->setEnabled(isModelTab);
  ui_->actionEnable_Model->setEnabled(isModelTab);
  ui_->actionDisable_Model->setEnabled(isModelTab);
  // Update import/export option availability
  ui_->actionImport_Parameters->setEnabled(isModelTab);
  ui_->actionExport_Parameters->setEnabled(isModelTab);
}

QList<modelDef::ParameterLimitMessage>
MainWindow::validateModelConfig(const modelDef::ModelType *modelType,
                                const ModelConfig_T &config) const {
  QList<modelDef::ParameterLimitMessage> messages;
  // Validate model-specific model and scenario limits
  messages.append(modelType->validateConfig(config));
  return messages;
}

void MainWindow::reportParameterLimits(
    const QList<modelDef::ParameterLimitMessage> &messages, bool *modelCanRun) {
  ui_->messagesList->clear();
  bool raiseWarnings = false;
  for (auto it = messages.begin(); it != messages.end(); it++) {
    if (it->limitType_ == modelDef::ParameterLimitType::NONE) {
      continue;
    }
    // Load appropriate icon
    QIcon icon;
    switch (it->limitType_) {
    case modelDef::ParameterLimitType::CRITICAL:
      icon = style()->standardIcon(style()->SP_MessageBoxCritical);
      raiseWarnings = true;
      *modelCanRun = false;
      break;
    case modelDef::ParameterLimitType::WARNING:
      icon = style()->standardIcon(style()->SP_MessageBoxWarning);
      raiseWarnings = true;
      break;
    case modelDef::ParameterLimitType::INFORMATION:
      icon = style()->standardIcon(style()->SP_MessageBoxInformation);
      break;
    default:
      icon = style()->standardIcon(style()->SP_MessageBoxQuestion);
      break;
    }
    ui_->messagesList->addItem(new QListWidgetItem(icon, it->message_));
    if (!*modelCanRun)
      break;
  }
  if (raiseWarnings) {
    // Warn about exceeded parameter limits
    QMessageBox *box = new QMessageBox();
    box->setWindowTitle("Parameter warnings");
    box->setText("The parameter selection has generated warnings.\n\n"
                 "Visit the \"Messages\" tab to review them.");
    box->addButton(QMessageBox::StandardButton::Ok);
    box->exec();
  }
}

void MainWindow::overviewModelEnabledChanged(String_T const &modelId,
                                             bool enabled) {
  QMap<String_T, bool> enabledModels = enabledModels_;
  enabledModels[modelId] = enabled;
  setEnabledModels(enabledModels);
}

void MainWindow::currentModelEnabledDisabled() {
  QAction *action = qobject_cast<QAction *>(sender());
  bool enabled = action == ui_->actionEnable_Model;
  String_T modelId = modelTypes_[ui_->tabWidget->currentIndex() - 2].getId();
  QMap<String_T, bool> enabledModels = enabledModels_;
  enabledModels[modelId] = enabled;
  setEnabledModels(enabledModels);
}

} // namespace gui

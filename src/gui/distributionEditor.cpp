// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/distributionEditor.h"

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <algorithm>

#include "api/IDistribution.h"
#include "gui/ui_distributionEditor.h"
#include "model/Distribution.h"
#include "types.h"

namespace gui {

DistributionEditor::DistributionEditor(String_T &paramId, QDialog *parent)
    : QDialog(parent) {
  paramId_ = paramId;
  // Create dummy distribution
  dist_ = std::make_shared<model::UniformDistribution>();
  dist_->setParameters(ValuePair(0.0, 1.0));
  // Instantiate UI
  ui_.reset(new Ui::DistributionEditor());
  ui_->setupUi(this);
  // Set up QCP
  ui_->plot->setMinimumSize(350, 250);
  ui_->plot->xAxis->setLabel("x");
  ui_->plot->yAxis->setLabel("P[X=x]");
  // Hook up signals
  QObject::connect(ui_->nameEdit, &QLineEdit::textChanged, this,
                   &DistributionEditor::nameChanged);
  QObject::connect(ui_->typeDropdown, &QComboBox::currentIndexChanged, this,
                   &DistributionEditor::typeChanged);
  QObject::connect(ui_->paramsMomentsPicker, &QComboBox::currentIndexChanged,
                   this, &DistributionEditor::paramsMomentsDropdownChanged);
  QObject::connect(ui_->paramsMomentsLine1, &QLineEdit::textChanged, this,
                   &DistributionEditor::paramsMomentsValuesChanged);
  QObject::connect(ui_->paramsMomentsLine2, &QLineEdit::textChanged, this,
                   &DistributionEditor::paramsMomentsValuesChanged);
  QObject::connect(ui_->boundsLower, &QLineEdit::textChanged, this,
                   &DistributionEditor::boundsChanged);
  QObject::connect(ui_->boundsUpper, &QLineEdit::textChanged, this,
                   &DistributionEditor::boundsChanged);
  QObject::connect(ui_->bottomBar, &QDialogButtonBox::accepted, this,
                   &DistributionEditor::confirm);
}

DistributionEditor::DistributionEditor(String_T &paramId,
                                       api::IDistribution_t distribution,
                                       QDialog *parent)
    : DistributionEditor(paramId, parent) {
  dist_ = distribution;
  // Configure UI
  ui_->nameEdit->setText(QString::fromStdString(distribution->getName()));
  // Get distribution type
  auto types_vec = distribution->getValidTypes();
  auto types = QList<std::string>(types_vec.begin(), types_vec.end());
  int typeIndex = types.indexOf(distribution->getType());
  ui_->typeDropdown->setCurrentIndex(typeIndex);
  // Get distribution parameters
  auto params = distribution->getParameters();
  ui_->paramsMomentsLine1->setText(QString::number(params.first()));
  ui_->paramsMomentsLine2->setText(QString::number(params.second()));
  // Get distribution bounds
  ValuePair bounds = distribution->getBounds();
  QString lb = std::isnan(bounds.first()) ? QString("")
                                          : QString::number(bounds.first());
  ui_->boundsLower->setText(lb);
  QString ub = std::isnan(bounds.second()) ? QString("")
                                           : QString::number(bounds.second());
  ui_->boundsUpper->setText(ub);
  updateDistributionPlot();
}

void DistributionEditor::nameChanged() {
  dist_->setName(ui_->nameEdit->text().toStdString());
}

void DistributionEditor::paramsMomentsDropdownChanged() {
  if (dist_->isDefined()) {
    ValuePair tmp;
    int dropdownIndex = ui_->paramsMomentsPicker->currentIndex();
    if (dropdownIndex == 0) {
      tmp = dist_->getParameters();
    } else if (dropdownIndex == 1) {
      tmp = dist_->getMoments();
    }
    ui_->paramsMomentsLine1->setText(QString::number(tmp.first(), 'g', 9));
    if (std::isnan(tmp.second())) {
      ui_->paramsMomentsLine2->setText("");
    } else {
      ui_->paramsMomentsLine2->setText(QString::number(tmp.second(), 'g', 9));
    }
  } else {
    ui_->paramsMomentsLine1->setText("");
    ui_->paramsMomentsLine2->setText("");
  }
  updateDistributionPlot();
}

void DistributionEditor::paramsMomentsValuesChanged() {
  bool okp1, okp2;
  double p1 = ui_->paramsMomentsLine1->text().toDouble(&okp1);
  double p2 = ui_->paramsMomentsLine2->text().toDouble(&okp2);
  if (!okp1) {
    ui_->paramsMomentsLine1->selectAll();
  } else if (!okp2 && dist_->isSecondParameterNeeded()) {
    ui_->paramsMomentsLine2->selectAll();
  } else {
    int dropdownIndex = ui_->paramsMomentsPicker->currentIndex();
    if (dropdownIndex == 0) {
      dist_->setParameters(ValuePair(p1, p2));
    } else if (dropdownIndex == 1) {
      dist_->setMoments(ValuePair(p1, p2));
    }
    updateDistributionPlot();
  }
}

void DistributionEditor::typeChanged(int index) {
  if (index < 0)
    return;
  String_T name = QString::fromStdString(dist_->getName());
  ValuePair moments = dist_->getMoments();
  ValuePair bounds = dist_->getBounds();
  api::IDistribution_t newDist = dist_->createDistribution(index);
  newDist->setName(name.toStdString());
  newDist->setMoments(moments);
  newDist->setBounds(bounds);
  dist_ = newDist;
  updateDistributionPlot();
}

void DistributionEditor::boundsChanged() {
  bool okb1, okb2;
  double b1 = ui_->boundsLower->text().toDouble(&okb1);
  double b2 = ui_->boundsUpper->text().toDouble(&okb2);
  if (!okb1) {
    b1 = NAN;
  }
  if (!okb2) {
    b2 = NAN;
  }
  dist_->setBounds(ValuePair(b1, b2));
  updateDistributionPlot();
}

void DistributionEditor::confirm() {
  emit distributionUpdated(paramId_, dist_);
}

void DistributionEditor::updateDistributionPlot() {
  if (dist_->isDefined()) {
    QCustomPlot *plot = ui_->plot;
    plot->clearGraphs();
    plot->clearPlottables();
    double x1 = dist_->quantile(0.01);
    double x2 = dist_->quantile(0.99);
    double lb = dist_->getBounds().first();
    double ub = dist_->getBounds().second();
    x1 = std::isnan(lb) ? x1 : std::min(x1, lb);
    x2 = std::isnan(lb) ? x2 : std::max(x2, ub);
    uint N = 1e5;
    double dxi = (x2 - x1) / static_cast<double>(N);
    QVector<double> x, y;
    double ymax = 0.0;
    for (uint ii = 0; ii < N + 1; ii++) {
      double xi = x1 + ii * dxi;
      x.append(xi);
      double yi = dist_->pdf(xi);
      y.append(yi);
      ymax = (yi > ymax) ? yi : ymax;
    }
    plot->addGraph();
    plot->graph()->setData(x, y);
    double dx = (x2 - x1) * 0.05;
    plot->xAxis->setRange(x1 - dx, x2 + dx);
    plot->yAxis->setRange(0.0, ymax * 1.05);
    if (!std::isnan(lb)) {
      QCPCurve *line1 = new QCPCurve(plot->xAxis, plot->yAxis);
      line1->setData(QVector<double>{lb, lb}, QVector<double>{0.0, ymax});
      line1->setPen(QPen(Qt::red));
    }
    if (!std::isnan(ub)) {
      QCPCurve *line2 = new QCPCurve(plot->xAxis, plot->yAxis);
      line2->setData(QVector<double>{ub, ub}, QVector<double>{0.0, ymax});
      line2->setPen(QPen(Qt::red));
    }
    plot->replot();
  }
}

} // namespace gui

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_DISTRIBUTIONEDITOR_H_
#define GUI_DISTRIBUTIONEDITOR_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include "types.h"

namespace Ui {

// Forward declaration of GUI implementation included via AUTOUIC
class DistributionEditor;

} // namespace Ui

namespace gui {

/** Modal pop-up dialog invoked when editing MC distributions. */
class DistributionEditor : public QDialog {
  Q_OBJECT

public:
  explicit DistributionEditor(String_T &paramId, QDialog *parent = nullptr);
  explicit DistributionEditor(String_T &paramId, api::IDistribution_t dist,
                              QDialog *parent = nullptr);

  void nameChanged();
  void paramsMomentsDropdownChanged();
  void paramsMomentsValuesChanged();
  void typeChanged(int index);
  void boundsChanged();
  void confirm();

signals:
  void distributionUpdated(String_T paramId, api::IDistribution_t newDist);

private:
  void updateDistributionPlot();

  QScopedPointer<Ui::DistributionEditor> ui_;
  api::IDistribution_t dist_;
  String_T paramId_;
};

} // namespace gui

#endif // GUI_DISTRIBUTIONEDITOR_H_

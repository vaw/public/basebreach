// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/app.h"

#include <QtCore>
#include <QtWidgets>

#include "core.h"
#include "gui/mainWindow.h"
#include "types.h"

namespace gui {

BaseBreachGui::BaseBreachGui(int &argc, char **argv)
    : QApplication(argc, argv) {
  core_.reset(new BaseBreachCore());
  gui_.reset(new gui::MainWindow(core_->getModelTypes(),
                                 core_->getScenarioParameters()));
  // Connect GUI-level signals to the application slots
  QObject::connect(gui_.get(), &gui::MainWindow::batchModelRunRequested,
                   core_.get(), &BaseBreachCore::runModelBatch);
  QObject::connect(gui_.get(), &gui::MainWindow::bulkModelRunRequested,
                   core_.get(), &BaseBreachCore::runModelParallel);
  QObject::connect(core_.get(), &BaseBreachCore::modelRunFinished, gui_.get(),
                   &gui::MainWindow::loadModelRun);
  QObject::connect(core_.get(), &BaseBreachCore::modelRunFinishedBulk,
                   gui_.get(), &gui::MainWindow::loadMcAnalysis);
  gui_->show();
}

bool BaseBreachGui::notify(QObject *receiver, QEvent *event) {
  // NOTE: This function gets called for any application-level events, including
  // C++ exceptions. It mostly exists to catch and display exceptions via the
  // GUI at runtime.
  try {
    return QApplication::notify(receiver, event);
  } catch (std::exception &exc) {
    QMessageBox msgBox(QMessageBox::Critical, "BASEbreach Error", exc.what(),
                       QMessageBox::Ok, nullptr,
                       Qt::WindowTitleHint | Qt::WindowSystemMenuHint);
    msgBox.exec();
  }
  return false;
}

} // namespace gui

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_MODELTAB_H_
#define GUI_MODELTAB_H_

#include <QtCore>
#include <QtWidgets>

#include "gui/widgets/kde2dPlot.h"
#include "gui/widgets/parameterPdfPlot.h"
#include "model/modelDefinition.h"
#include "types.h"

namespace Ui {

// Forward declaration of GUI implementation included via AUTOUIC
class ModelTab;

} // namespace Ui

namespace widgets {

namespace modelDef = model::definition;

/**
 * GUI Sub-module for model tabs.
 *
 * This class is responsible for the GUI implementation of the model tab,
 * including parameter defintions, plots, and forwarding model run signals.
 *
 * This class is a friend to `MainWindow` and has access to its protected
 * members. This allows it to access the model's scenario parameter parsers.
 */
class ModelTab : public QWidget {
  Q_OBJECT

public:
  ModelTab(QWidget *parent = nullptr);
  explicit ModelTab(const modelDef::ModelType *modelType,
                    QWidget *parent = nullptr);

  /** Export the most recent model run in CSV format. */
  void exportCSV(String_T filename) const;
  /** Return the model type this tab was created for. */
  const modelDef::ModelType *getModelType() const { return modelType_; }
  /** Load the parameters for a given model. */
  ModelConfig_T parseParameters() const;

public slots:
  /**
   * Clear the most recent model run from the cache.
   * This cache is used to allow exporting the model run to CSV.
   */
  void clearLastModelRun();
  /** Load a single model run into the cache and plots. */
  void loadModelRun(model::FullModel_t model);
  /**
   * Callback for the "Run Model" button.
   * Loads scenario and model parameters and schedules the model run.
   */
  void runCurrentModel();
  /**
   * Load the model run from the cache and plots.
   */
  void loadModelConfig(const ModelConfig_T &config);
  /** Set model parameters from an external input file. */
  void importParameters();
  /** Save model parameters to an external file. */
  void exportParameters();

signals:
  /** Request the given model to be run. */
  void modelRunRequested(const modelDef::ModelType *modelType);

protected:
  /** Create the GUI elements for the given model parameter. */
  void createGuiParameter(modelDef::Parameter param);
  /** Callback for the export model run button. */
  void exportModelRun();
  /** Helper for setting nested keys in ModelConfig maps. */
  void setNestedKey(ModelConfig_T &params, String_T const &paramId,
                    double value) const;
  /** Open the Wiki entry for a given model parameter. */
  void openWikiLink();

  QScopedPointer<Ui::ModelTab> ui_;
  const modelDef::ModelType *modelType_;
  model::FullModel_t lastModelRun_;
  QMap<String_T, QLineEdit *> paramEdits_;
};

/**
 * Subclass of "ModelTab" with additional Monte Carlo support.
 */
class ModelTabMC : public ModelTab {
  Q_OBJECT

public:
  ModelTabMC(QWidget *parent = nullptr);
  explicit ModelTabMC(const modelDef::ModelType *modelType,
                      QWidget *parent = nullptr);

  /** Load a list of model runs and generate the distribution plots. */
  void loadMcAnalysis(QList<model::FullModel_t> results);
  /** Switch between MC and regular model tab modes. */
  void setMCEnabled(bool enabled);

public slots:
  /** Callback for when the distribution editor is closed via "Ok". */
  void distributionUpdated(String_T paramId, api::IDistribution_t newDist);
  /** Sample all distributions and schedule the model runs. */
  void runMcAnalysis();

signals:
  /**
   * Request the given model to be run in MC mode.
   *
   * All model configurations are of the given type, but only contain
   * model parameters.
   * The signal recipient must read the scenario and numerics parameters
   * before customising it using the provided configurations.
   * @param modelType The model type to run.
   * @param configs A list of model configurations to run.
   */
  void MCRunRequested(const modelDef::ModelType *modelType,
                      QList<ModelConfig_T> configs);

protected:
  /** Like createGuiParameter, but instead creates distribution editor. */
  void createDistributionParameter(modelDef::Parameter param);
  /** Open the distribution editor for the given MC parameter. */
  void openDistributionEditor();

  bool isMcEnabled_ = false;
  QMap<String_T, api::IDistribution_t> distributions_;
  // Extra GUI widgets not defined by the UI file
  QPushButton *runMcBtn_;
  QCheckBox *enableMc_;
  QGroupBox *kdeGroup_;
  QGroupBox *pdfGroup_;
  widgets::Kde2dPlot *kdePlot_;
  widgets::ParameterPdfPlot *pdfPlot_;
};

} // namespace widgets

#endif // GUI_MODELTAB_H_

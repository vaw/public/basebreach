// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_APP_H_
#define GUI_APP_H_

#include <QtCore>

#include "core.h"
#include "gui/mainWindow.h"
#include "modelTypes.h"
#include "types.h"

namespace gui {

namespace modelDef = model::definition;

/**
 * GUI version of the BASEbreach application.
 */
class BaseBreachGui : public QApplication {
  Q_OBJECT

public:
  BaseBreachGui(int &argc, char **argv);
  /**
   * Qt notification handler.
   *
   * This is overwritten to allow catching stray exceptions that are not handled
   * by underlying code. If no exception is encountered, it forwards the event
   * to the defautl implementation.
   */
  bool notify(QObject *receiver, QEvent *event);

private:
  QScopedPointer<BaseBreachCore> core_;
  QScopedPointer<gui::MainWindow> gui_;
};

} // namespace gui

#endif // GUI_APP_H_

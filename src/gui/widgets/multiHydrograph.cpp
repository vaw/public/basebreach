// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/widgets/multiHydrograph.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <qcustomplot.h>

#include <limits>
#include <vector>

#include "gui/widgets/ui_multiHydrograph.h"
#include "model/fullModel.h"
#include "model/modelDefinition.h"
#include "model/standardBreach.h"
#include "types.h"

namespace widgets {

namespace modelDef = model::definition;

MultiHydrographPlot::MultiHydrographPlot(QWidget *parent)
    : QCustomPlot(parent) {
  // Set up plot axes
  xAxis->grid()->setVisible(false);
  xAxis->setTicker(QSharedPointer<QCPAxisTicker>(new QCPAxisTicker));
  yAxis->grid()->setVisible(false);
  yAxis->setTicker(QSharedPointer<QCPAxisTicker>(new QCPAxisTicker));
  model::FullModel model = model::FullModel(); // Dummy model for variable names
  xAxis->setLabel(QString::fromStdString(model.getOutputVariableNames()[0]));
  // Create standard breach marker
  stdBreach_ = new model::StandardBreach();
  stdBreachMarker_ = new QCPCurve(xAxis, yAxis);
  stdBreachMarker_->setPen(QPen(Qt::red, 0));
  QCPScatterStyle style(QCPScatterStyle::ScatterShape::ssDisc, Qt::red, 10);
  stdBreachMarker_->setScatterStyle(style);
  stdBreachMarker_->setName("Standard Breach");
  // Create cursor
  cursor_ = new QCPCurve(xAxis, yAxis);
  cursor_->setPen(QPen(Qt::darkGray));
  cursor_->removeFromLegend();
  // Make scrollable
  setInteraction(QCP::iRangeDrag, true);
  setInteraction(QCP::iRangeZoom, true);
  replot();
}

const QMap<String_T, model::FullModel_t>
MultiHydrographPlot::getModels() const {
  return modelRuns_;
}

const model::StandardBreach *MultiHydrographPlot::getStandardBreach() const {
  return stdBreach_;
}

void MultiHydrographPlot::resetView() {
  // Reset zoom and center camera
  rescaleAxes();
  // Apply normal axes ranges
  recalculateAxesRanges();
  replot();
}

void MultiHydrographPlot::clear() {
  modelRuns_.clear();
  // Remove models from plot
  for (auto it = dataCurves_.begin(); it != dataCurves_.end(); it++) {
    removePlottable(it.value());
    it.value()->deleteLater();
  }
  // Reset cursor range
  QVector<double> x, y;
  x << 0.0 << 0.0;
  y << 0.0 << 1.0;
  cursor_->setData(x, y);
  replot();
}

void MultiHydrographPlot::loadModelRun(const modelDef::ModelType *modelType,
                                       model::FullModel_t model) {
  const String_T modelId = modelType->getId();
  // Show legend if necessary
  if (dataCurves_.size() == 0) {
    legend->setVisible(true);
  }
  // If there is an existing model run with the same id, remove it
  if (dataCurves_.contains(modelId)) {
    removePlottable(dataCurves_[modelId]);
  }
  // Create a new plot for this model type based on the given model run
  QCPCurve *curve = new QCPCurve(xAxis, yAxis);
  QPen pen = QPen(getPlotColour(modelType), Qt::DotLine);
  pen.setCapStyle(Qt::PenCapStyle::RoundCap);
  curve->setPen(pen);
  curve->setName(modelType->getName());
  // Load the model run itno the plot
  populateDataCurve(curve, model);
  dataCurves_[modelId] = curve;
  // Rescale the plot axes
  modelRuns_.insert(modelId, model);
  recalculateAxesRanges();
  replot();
}

void MultiHydrographPlot::setTime(double time) {
  time_ = time;
  if (modelRuns_.size() == 0) {
    return;
  }
  // Update model times
  for (auto it = modelRuns_.begin(); it != modelRuns_.end(); it++) {
    model::FullModel_t model = it.value();
    model->setTime(time);
  }
  // Redraw cursor
  QVector<double> x, y;
  x << time << time;
  y << -std::numeric_limits<double>::max()
    << std::numeric_limits<double>::max();
  cursor_->setData(x, y);
  replot();
}

void MultiHydrographPlot::setVariable(int variableIndex) {
  if (variableIndex < 1) {
    BB_EXCEPTION("Variable index must be 1 or greater");
  }
  varIndex_ = variableIndex;
  // Update model plots
  String_T field;
  for (auto it = dataCurves_.begin(); it != dataCurves_.end(); it++) {
    model::FullModel_t model = modelRuns_[it.key()];
    if (it == dataCurves_.begin()) {
      field = QString::fromStdString(
          model->getOutputVariableNames().at(variableIndex));
      yAxis->setLabel(field);
    }
    populateDataCurve(it.value(), model);
  }
  recalculateAxesRanges();
  // Update standard breach marker
  if (stdBreach_->fieldIsValid(field)) {
    QVector<double> x, y;
    x << -100000.0 << 0.0;
    y << stdBreach_->getField(field) << stdBreach_->getField(field);
    stdBreachMarker_->setData(x, y);
  }
  replot();
}

void MultiHydrographPlot::populateDataCurve(QCPCurve *curve,
                                            model::FullModel_t model) {
  const auto &timeVec = model->timeTS();
  // Never plot more than 10'000 data points
  size_t plot_step_size = timeVec.size() / 1000;
  if (plot_step_size == 0) {
    plot_step_size = 1;
  }
  // Create data vectors for the curve
  QVector<double> x, y;
  for (size_t i = 0; i < timeVec.size(); i += plot_step_size) {
    if (i > std::numeric_limits<int>::max()) {
      break;
    }
    model->setLevelsWithIndex(static_cast<int>(i));
    x << timeVec[i];
    y << model->getOutputVariableValue(varIndex_);
  }
  curve->setData(x, y);
}

void MultiHydrographPlot::recalculateAxesRanges() {
  if (modelRuns_.size() == 0) {
    return;
  }
  double minX, minY, maxX, maxY;
  minX = minY = std::numeric_limits<double>::max();
  maxX = maxY = std::numeric_limits<double>::lowest();

  // Iterate over all loaded models
  for (auto &model : modelRuns_) {
    auto &timeVec = model->timeTS();
    // Update X axis min/max
    if (timeVec.front() < minX) {
      minX = timeVec.front();
    }
    if (timeVec.back() > maxX) {
      maxX = timeVec.back();
    }
    // Update Y axis min/max
    for (auto &timestep : timeVec) {
      model->setTime(timestep);
      auto value = model->getOutputVariableValue(varIndex_);
      if (value < minY) {
        minY = value;
      }
      if (value > maxY) {
        maxY = value;
      }
    }
  }

  // Extend Y range autoscale for standard breach value
  model::FullModel_t model;
  auto fieldName = model->getOutputVariableNames()[varIndex_];
  if (stdBreach_->fieldIsValid(QString::fromStdString(fieldName))) {
    double value = stdBreach_->getField(QString::fromStdString(fieldName));
    if (value < minY) {
      minY = value;
    }
    if (value > maxY) {
      maxY = value;
    }
  }
  // Set axes ranges
  xAxis->setRange(minX - abs((maxX - minX) * 0.05), maxX); // 5% padding
  yAxis->setRange(minY, maxY * 1.05);                      // 5% padding

  emit timeRangeChanged(minX, maxX);
}

void MultiHydrographPlot::updateDefBreach(double damHeight) {
  stdBreach_->updateParameters(damHeight);
}

QColor
MultiHydrographPlot::getPlotColour(const modelDef::ModelType *modelType) const {
  const String_T modelId = modelType->getId();
  if (modelId == "bbAwel")
    return QColor(40, 60, 210, 180);
  if (modelId == "bbMacchione")
    return QColor(200, 50, 50, 180);
  if (modelId == "bbPeter")
    return QColor(100, 160, 80, 180);
  if (modelId == "bbPeterCal")
    return QColor(220, 140, 20, 180);
  return QColor(Qt::cyan);
}

} // namespace widgets

namespace gui {

MultiHydrograph::MultiHydrograph(QWidget *parent) : QWidget(parent) {
  ui_.reset(new Ui::MultiHydrograph());
  ui_->setupUi(this);
  ui_->timeSlider->setRange(0, 1);
  ui_->timeSlider->setValue(0);

  // Hook up signals
  QObject::connect(ui_->variableDropdown, &QComboBox::currentIndexChanged, this,
                   &MultiHydrograph::hydrographVariablesChanged);
  QObject::connect(ui_->timeSlider, &QSlider::valueChanged, this,
                   &MultiHydrograph::timeUpdated);
  QObject::connect(ui_->hydrograph,
                   &widgets::MultiHydrographPlot::timeRangeChanged,
                   ui_->timeSlider, &QSlider::setRange);
  QObject::connect(ui_->resetViewBtn, &QPushButton::clicked, ui_->hydrograph,
                   &widgets::MultiHydrographPlot::resetView);
}

void MultiHydrograph::setModelDefinitions(
    QList<modelDef::ModelType> modelTypes) {
  // Remove any previously added rows
  // (First two are skipped as they are the header and standard breach rows)
  for (int i = 2; i < ui_->modelInfoGrid->rowCount(); i++) {
    for (int j = 0; j < ui_->modelInfoGrid->columnCount(); j++) {
      QLayoutItem *item = ui_->modelInfoGrid->itemAtPosition(i, j);
      if (item != nullptr) {
        item->widget()->deleteLater();
        ui_->modelInfoGrid->removeWidget(item->widget());
      }
    }
  }
  // Add new rows for provided model definitions
  for (auto it = modelTypes.begin(); it != modelTypes.end(); it++) {
    int row = ui_->modelInfoGrid->rowCount();
    QLabel *label = new QLabel(it->getName());
    QCheckBox *enabled = new QCheckBox("");
    enabled->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
    enabled->setChecked(true);
    enabled->setObjectName(it->getId());
    QLabel *totalDischarge = new QLabel();
    QLabel *peak = new QLabel();
    QLabel *curr = new QLabel();
    // Add elements to grid row
    ui_->modelInfoGrid->addWidget(label, row, 0);
    ui_->modelInfoGrid->addWidget(enabled, row, 1);
    ui_->modelInfoGrid->addWidget(totalDischarge, row, 3);
    ui_->modelInfoGrid->addWidget(peak, row, 5);
    ui_->modelInfoGrid->addWidget(curr, row, 6);
    // Store references
    modelGridEnabled_[it->getId()] = enabled;
    modelGridCurrent_[it->getId()] = curr;
    modelGridPeak_[it->getId()] = peak;
    modelGridTotalDischarge_[it->getId()] = totalDischarge;
    // Connect signals
    QObject::connect(enabled, &QCheckBox::toggled, curr, &QWidget::setEnabled);
    QObject::connect(enabled, &QCheckBox::toggled, peak, &QWidget::setEnabled);
    QObject::connect(enabled, &QCheckBox::clicked, this,
                     &MultiHydrograph::modelEnabledClicked);
  }
}

void MultiHydrograph::clear() {
  ui_->hydrograph->clear();
  while (ui_->variableDropdown->count()) {
    ui_->variableDropdown->removeItem(0);
  }
}

void MultiHydrograph::addModel(const modelDef::ModelType *modelType,
                               model::FullModel_t model) {
  // Populate axis dropdown if not already populated
  if (ui_->variableDropdown->count() < 1) {
    ui_->variableDropdown->clear();
    for (const auto &field : model->getOutputVariableNames()) {
      ui_->variableDropdown->addItem(QString::fromStdString(field));
    }
    // Remove first "time" entry
    ui_->variableDropdown->removeItem(0);
  }
  ui_->hydrograph->loadModelRun(modelType, model);
  emit hydrographVariablesChanged();
  timeUpdated(time_);
}

void MultiHydrograph::hydrographVariablesChanged() {
  int y = ui_->variableDropdown->currentIndex();
  if (y < 0) {
    return;
  }
  // Offset by 1 as the first entry is always used for the horizontal axis and
  // not available in the dropdown
  ui_->hydrograph->setVariable(y + 1);
  // Update current StdBreach value
  String_T field = ui_->variableDropdown->currentText();
  const model::StandardBreach *stdBreach = ui_->hydrograph->getStandardBreach();
  if (stdBreach->fieldIsValid(field)) {
    ui_->stdBreachPeak->setText(QString::number(stdBreach->getField(field)));
    ui_->stdBreachPeak->setEnabled(true);
  } else {
    ui_->stdBreachPeak->setText("N/A");
    ui_->stdBreachPeak->setEnabled(false);
  }
  // Get maximum values and populate maxima column
  const QMap<String_T, model::FullModel_t> models =
      ui_->hydrograph->getModels();
  int varIndex = ui_->variableDropdown->currentIndex() + 1;
  for (auto it = models.begin(); it != models.end(); it++) {
    const String_T modelId = it.key();
    const model::FullModel_t model = it.value();
    double max = -std::numeric_limits<double>::max();
    for (const auto &time : model->timeTS()) {
      model->setTime(time);
      double value = model->getOutputVariableValue(varIndex);
      max = value > max ? value : max;
    }
    modelGridPeak_[modelId]->setText(QString::number(max));
  }
  // Update current display
  timeUpdated(time_);
}

void MultiHydrograph::timeUpdated(double time) {
  time_ = time;
  ui_->hydrograph->setTime(time);
  ui_->timeValue->setText(QString::number(time) + QString(" s"));

  auto modelRuns = ui_->hydrograph->getModels();
  for (auto it = modelRuns.begin(); it != modelRuns.end(); it++) {
    QLabel *field = modelGridCurrent_[it.key()];
    field->setText(QString::number(it.value()->getOutputVariableValue(
        ui_->variableDropdown->currentIndex() + 1)));

    // Calculate total discharge
    const model::FullModel_t model = it.value();
    double total = 0.0;
    for (size_t i = 0; i < time; i++) {
      if (model->timeTS().back() <= i)
        continue;
      model->setTime(i);
      total += model->getOutputVariableValue(1);
    }
    modelGridTotalDischarge_[it.key()]->setText(QString::number(total));
  }

  emit timeChanged(time);
}

void MultiHydrograph::parametersChanged(double damHeight) {
  ui_->hydrograph->updateDefBreach(damHeight);
  emit hydrographVariablesChanged();
}

void MultiHydrograph::setEnabledModels(QMap<String_T, bool> enabledModels) {
  for (auto it = modelGridEnabled_.begin(); it != modelGridEnabled_.end();
       it++) {
    it.value()->setChecked(enabledModels[it.key()]);
  }
}

void MultiHydrograph::modelEnabledClicked() {
  QCheckBox *checkBox = qobject_cast<QCheckBox *>(sender());
  if (checkBox == nullptr)
    return;
  String_T modelId = checkBox->objectName();
  emit modelEnabledChanged(modelId, checkBox->isChecked());
}

} // namespace gui

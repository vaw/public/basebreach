// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_WIDGETBASE_H_
#define GUI_WIDGETS_WIDGETBASE_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include "types.h"

namespace widgets {

/**
 * Most basic form of QCustomPlot subclass used for BASEbreach widgets.
 */
class CustomWidgetBase : public QCustomPlot {
  Q_OBJECT

public:
  CustomWidgetBase(QWidget *parent = nullptr);

public slots:
  void clear();

protected:
  virtual void setupGraph();
  virtual void redrawGraph();
};

/**
 * Sub class for custom BASEbreach widgets requiring a model reference.
 */
class ModelWidgetBase : public CustomWidgetBase {
  Q_OBJECT

public:
  ModelWidgetBase(QWidget *parent = nullptr);
  ModelWidgetBase(model::FullModel_t model, QWidget *parent = nullptr);

signals:
  void modelChanged(const model::FullModel_t model);

public slots:
  void clear();
  void setModel(model::FullModel_t model);

protected:
  void setupGraph() override;
  void redrawGraph() override;
  model::FullModel_t model_ = nullptr;
};

/**
 * Sub class for custom BASEbreach widgets requiring both a model
 * reference and a time for which to display a data sample.
 */
class TimeModelWidgetBase : public ModelWidgetBase {
  Q_OBJECT

public:
  TimeModelWidgetBase(QWidget *parent = nullptr);
  TimeModelWidgetBase(model::FullModel_t model, QWidget *parent = nullptr);
  TimeModelWidgetBase(model::FullModel_t model, double time,
                      QWidget *parent = nullptr);

signals:
  void timeChanged(const double time);

public slots:
  void clear();
  void setTime(double time);

protected:
  double time_ = -1.0;
};

/**
 * Sub class for custom BASEbreach widgets related to parameter spaces.
 */
class ParameterWidgetBase : public CustomWidgetBase {
  Q_OBJECT

public:
  ParameterWidgetBase(QWidget *parent = nullptr);

protected:
  void setupGraph() override;
  void redrawGraph() override;
};

} // namespace widgets

#endif // GUI_WIDGETS_WIDGETBASE_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/widgets/kde2dPlot.h"

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <kde.hpp>

#include "ExceptionHandling.h"
#include "types.h"

namespace widgets {

Kde2dPlot::Kde2dPlot(QWidget *parent) : ParameterWidgetBase(parent) {
  setupGraph();
}

int Kde2dPlot::getMostProbable2d(const doubleVec &x, const doubleVec &y) {
  size_t imax = -1;
  if (x.size() == y.size()) {
    double pmax = 0.0;
    for (size_t ii = 0; ii < x.size(); ii++) {
      double pi = kde_->pdf(x.at(ii), y.at(ii));
      if (pi > pmax) {
        pmax = pi;
        imax = ii;
      }
    }
  }
  BB_ASSERT(imax > INT_MAX, "max probability index overflows integer");
  return static_cast<int>(imax);
}

void Kde2dPlot::setData(const doubleVec &data1, const doubleVec &data2, int N) {
  if (data1.size() != data2.size()) {
    BB_EXCEPTION("Input data sets are not of the same size");
  }
  size_t Np = data1.size();
  if (Np > 0) {
    // Add data to KDE
    calcKDE(data1, data2);
    double xmin = kde_->get_min(0);
    double xmax = kde_->get_max(0);
    double ymin = kde_->get_min(1);
    double ymax = kde_->get_max(01);
    double dx = (xmax - xmin) / (N - 1.0);
    double dy = (ymax - ymin) / (N - 1.0);
    // Delete old data
    colormap_->data()->clear();
    // Recreate heat map
    colormap_->data()->setRange(QCPRange(xmin, xmax), QCPRange(ymin, ymax));
    colormap_->data()->setSize(N, N);
    // now we assign some data, by accessing the QCPColorMapData instance of the
    // color map
    for (int ii = 0; ii < N; ii++) {
      for (int jj = 0; jj < N; jj++) {
        double z = kde_->pdf(xmin + dx * ii, ymin + dy * jj);
        colormap_->data()->setCell(ii, jj, z);
      }
    }
    // rescale the data dimension (color) such that all data points lie in the
    // span visualized by the color gradient:
    colormap_->rescaleDataRange(true);
    // rescale the key (x) and value (y) axes so the whole color map is visible:
    rescaleAxes();
  }
}

void Kde2dPlot::clickedGraph(QMouseEvent *e) {
  QPoint p = e->pos();
  QCPAxisRect *plotPos = axisRect();
  double xrel = static_cast<double>(p.rx() - plotPos->left()) /
                static_cast<double>(plotPos->right() - plotPos->left());
  double yrel =
      1.0 - static_cast<double>(p.ry() - plotPos->top()) /
                static_cast<double>(plotPos->bottom() - plotPos->top());
  emit clickedRelativeCoordinates(xrel, yrel);
}

void Kde2dPlot::updateMarker(double x, double y) {
  graph_->data()->clear();
  graph_->addData(x, y);
  replot();
}

void Kde2dPlot::redrawGraph() {}

void Kde2dPlot::setupGraph() {
  setInteraction(QCP::iRangeDrag, true);
  setInteraction(QCP::iRangeZoom, true);
  // Set up axis
  QSharedPointer<QCPAxisTicker> xTicker(new QCPAxisTicker);
  xAxis->setTicker(xTicker);
  QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
  yAxis->setTicker(yTicker);
  // Set up the QCPColorMap
  colormap_ = new QCPColorMap(xAxis, yAxis);
  // Set the color gradient of the color map to one of the presets:
  QCPColorGradient gradient = QCPColorGradient::gpCold;
  colormap_->setGradient(gradient.inverted());
  // The graph representing the current data point
  graph_ = addGraph();
  graph_->setPen(QPen(Qt::red));
  graph_->setLineStyle(QCPGraph::lsNone);
  graph_->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));
  // To get the coordinates at the clicked location
  connect(this, SIGNAL(mouseDoubleClick(QMouseEvent *)), this,
          SLOT(clickedGraph(QMouseEvent *)));
}

void Kde2dPlot::calcKDE(const doubleVec &x, const doubleVec &y) {
  kde_.reset(new KDE());
  for (uint ii = 0; ii < x.size(); ii++) {
    kde_->add_data(x.at(ii), y.at(ii));
  }
}

} // namespace widgets

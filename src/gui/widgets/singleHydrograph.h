// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_SINGLEHYDROGRAPH_H_
#define GUI_WIDGETS_SINGLEHYDROGRAPH_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include "gui/widgets/widgetBase.h"
#include "model/fullModel.h"
#include "types.h"

namespace Ui {

// Forward declaration of GUI implementation included via AUTOUIC
class SingleHydrograph;

} // namespace Ui

namespace widgets {

/**
 * QCustomPlot subclass used for single model hydrograph plots.
 *
 * This includes hooks for changing the X and Y axis variable, but only allows
 * display of one model at a time.
 */
class SingleHydrographPlot : public TimeModelWidgetBase {
  Q_OBJECT

public:
  SingleHydrographPlot(QWidget *parent = nullptr);

public slots:
  /** Remove the position line and plot curve for the widget. */
  void clear();
  /** Set the model to use for the plot. This recreates the plot curve. */
  void setModel(model::FullModel_t model);
  /** Set the time to display. This only updates the position line. */
  void setTime(double time = -1.0);
  /**
   * Set the variables to display in the plot.
   *
   * Use getVariableNames() to query the current model for available names and
   * their indices.
   */
  void setVariable(int indexV, int indexXY);

private:
  void setupGraph();
  void redrawGraph();
  void getData(int outputIndex, QVector<double> &data, double &min,
               double &max);
  QCPCurve *curve_ = nullptr;
  QCPCurve *cursor_ = nullptr;
  uint varIndexX_ = -1;
  uint varIndexY_ = -1;
};

} // namespace widgets

namespace gui {

/**
 * A large QCP widget showing a single model run.
 *
 * This also allows specifying the X and Y axis variables to plot against one
 * another.
 */
class SingleHydrograph : public QWidget {
  Q_OBJECT

public:
  SingleHydrograph(QWidget *parent = nullptr);

signals:
  void timeChanged(double time);

public slots:
  /** Reset the graph, also resets the time slider and dropdowns. */
  void clear();
  /** Set the model to use for the plot. */
  void setModel(model::FullModel_t model);

protected:
  model::FullModel_t model_ = nullptr;
  QScopedPointer<Ui::SingleHydrograph> ui_;
  /** Update the main hydrograph to use the currently selected variables. */
  void hydrographVariablesChanged();
  /** Update the time indicator for value display and the shared hydrograph. */
  void timeUpdated(double time);
};

} // namespace gui

#endif // GUI_WIDGETS_SINGLEHYDROGRAPH_H_

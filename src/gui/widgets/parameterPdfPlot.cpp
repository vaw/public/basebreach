// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/widgets/parameterPdfPlot.h"

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <algorithm>
#define _USE_MATH_DEFINES
#include <cmath>

#include <kde.hpp>

#include "ExceptionHandling.h"
#include "types.h"

namespace widgets {

ParameterPdfPlot::ParameterPdfPlot(QWidget *parent)
    : ParameterWidgetBase(parent) {
  setupGraph();
}

void ParameterPdfPlot::redrawGraph() {
  double p = kde_->pdf(xPos_);
  QVector<double> xi = {xPos_, xPos_};
  QVector<double> yi = {0.0, p};
  positionLine_->setData(xi, yi);
  replot();
}

void ParameterPdfPlot::setData(const doubleVec &data, uint Np) {
  calcPdf(data);
  QVector<double> xi(Np), pi(Np);
  double xmin = kde_->get_min(0);
  double xmax = kde_->get_max(0);
  double dx = (xmax - xmin) / (Np - 1.0);
  for (uint ii = 0; ii < Np; ii++) {
    double val = xmin + ii * dx;
    xi.push_back(val);
    pi.push_back(kde_->pdf(val));
  }
  graph_->setData(xi, pi);
  xAxis->setRange(xmin, xmax);
  std::sort(pi.begin(), pi.end());
  yAxis->setRange(0.0, pi.back());
}

void ParameterPdfPlot::setValue(double value) {
  xPos_ = value;
  redrawGraph();
}

void ParameterPdfPlot::calcPdf(const doubleVec &data) {
  size_t Np = data.size();
  if (Np < 2) {
    BB_EXCEPTION("At least two values required for PDF calculation");
  }
  kde_.reset(new KDE());
  for (size_t ii = 0; ii < Np; ii++) {
    kde_->add_data(data.at(ii));
  }
}

void ParameterPdfPlot::setupGraph() {
  graph_ = addGraph();
  graph_->setPen(QPen(Qt::black));
  positionLine_ = new QCPCurve(xAxis, yAxis);
  positionLine_->setPen(QPen(Qt::red));
  // Add axis labels
  xAxis->grid()->setVisible(false);
  xAxis->setTicks(true);
  yAxis->setLabel("probability densitiy [-]");
  yAxis->grid()->setVisible(false);
  yAxis->setTicks(false);
  QSharedPointer<QCPAxisTicker> xTicker(new QCPAxisTicker);
  xAxis->setTicker(xTicker);
  QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
  yAxis->setTicker(yTicker);
}

} // namespace widgets

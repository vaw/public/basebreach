// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_KDE2DPLOT_H_
#define GUI_WIDGETS_KDE2DPLOT_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <kde.hpp>

#include "gui/widgets/widgetBase.h"
#include "types.h"

namespace widgets {

/**
 * A plot widget displaying a 2D KDE graph.
 */
class Kde2dPlot : public ParameterWidgetBase {
  Q_OBJECT

public:
  Kde2dPlot(QWidget *parent = nullptr);

  int getMostProbable2d(const doubleVec &x, const doubleVec &y);
  QCPColorMap *getColorMap() { return colormap_; }
  void setData(const doubleVec &data1, const doubleVec &data2, int N);

signals:
  void clickedRelativeCoordinates(double, double);

public slots:
  void clickedGraph(QMouseEvent *e);
  void updateMarker(double x, double y);

protected:
  void redrawGraph() override;
  void setupGraph() override;

private:
  void calcKDE(const doubleVec &x, const doubleVec &y);
  QCPColorMap *colormap_;
  QCPGraph *graph_;
  QScopedPointer<KDE> kde_;
};

} // namespace widgets

#endif // GUI_WIDGETS_KDE2DPLOT_H_

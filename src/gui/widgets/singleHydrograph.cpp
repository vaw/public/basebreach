// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/widgets/singleHydrograph.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <qcustomplot.h>

#include "gui/widgets/ui_singleHydrograph.h"
#include "gui/widgets/widgetBase.h"
#include "types.h"

namespace widgets {

SingleHydrographPlot::SingleHydrographPlot(QWidget *parent)
    : TimeModelWidgetBase(parent) {
  setupGraph();
}

void SingleHydrographPlot::clear() {
  model_.reset();
  delete curve_;
  delete cursor_;
  varIndexX_ = 0;
  varIndexY_ = 1;
  replot();
}

void SingleHydrographPlot::setModel(model::FullModel_t model) {
  TimeModelWidgetBase::setModel(model);
  if (varIndexX_ == -1 || varIndexY_ == -1) {
    setVariable(0, 1);
    setVariable(1, 2);
  }
  redrawGraph();
}

void SingleHydrographPlot::setTime(double time) {
  TimeModelWidgetBase::setTime(time);
  if (model_.get() != nullptr) {
    redrawGraph();
  }
}

void SingleHydrographPlot::setVariable(int indexV, int indexXY) {
  // Ignore invalid indices
  if (indexV == -1) {
    return;
  }
  // Update axis labels and indices
  if (indexXY == 1) {
    varIndexX_ = indexV;
    xAxis->setLabel(
        QString::fromStdString(model_->getOutputVariableNames().at(indexV)));
  } else if (indexXY == 2) {
    varIndexY_ = indexV;
    yAxis->setLabel(
        QString::fromStdString(model_->getOutputVariableNames().at(indexV)));
  }
  // Only proceed if a model is loaded
  if (model_ == nullptr) {
    return;
  }
  // Load  data
  QVector<double> dataModel, dataPlot;
  double max, min;
  getData(indexV, dataModel, min, max);
  // Update graph data
  QCPCurveDataContainer *data = curve_->data().data();
  if (data->size() != dataModel.size()) {
    dataPlot.resize(dataModel.size());
  }
  for (int i = 0; i < data->size(); i++) {
    if (indexXY == 1) {
      dataPlot.append(data->at(i)->value);
    } else if (indexXY == 2) {
      dataPlot.append(data->at(i)->key);
    }
  }
  // Update axes
  if (indexXY == 1) {
    curve_->setData(dataModel, dataPlot);
    xAxis->setRange(min, fabs(max) * 1.05);
  } else if (indexXY == 2) {
    curve_->setData(dataPlot, dataModel);
    yAxis->setRange(min, fabs(max) * 1.05);
  }
  redrawGraph();
}

void SingleHydrographPlot::redrawGraph() {
  if (model_ == nullptr || varIndexX_ == -1 || varIndexY_ == -1) {
    return;
  }
  QVector<double> x, y;
  x << 0.0 << model_->getOutputVariableValue(varIndexX_)
    << model_->getOutputVariableValue(varIndexX_);
  y << model_->getOutputVariableValue(varIndexY_)
    << model_->getOutputVariableValue(varIndexY_) << 0.0;
  cursor_->setData(x, y);
  replot();
}

void SingleHydrographPlot::setupGraph() {
  // Add graphs
  curve_ = new QCPCurve(xAxis, yAxis);
  curve_->setPen(QPen(QColor(0, 30, 210, 180), Qt::DotLine));
  cursor_ = new QCPCurve(xAxis, yAxis);
  cursor_->setPen(QPen(Qt::darkGray));
  // Add axis labels
  xAxis->grid()->setVisible(false);
  QSharedPointer<QCPAxisTicker> xTicker(new QCPAxisTicker);
  xAxis->setTicker(xTicker);
  yAxis->grid()->setVisible(false);
  QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
  yAxis->setTicker(yTicker);
  replot();
}

void SingleHydrographPlot::getData(int outputIndex, QVector<double> &data,
                                   double &min, double &max) {
  // Reset models
  model_->setLevels(model_->reservoirlevelTS().at(0),
                    model_->breachlevelTS().at(0));
  // Set default values for min/max
  max = model_->getOutputVariableValue(outputIndex);
  min = model_->getOutputVariableValue(outputIndex);
  // Load all data points, updating min/max as needed
  for (uint i = 0; i < model_->timeTS().size(); i++) {
    model_->setLevelsWithIndex(i);
    double value = model_->getOutputVariableValue(outputIndex);
    data.append(value);
    max = value > max ? value : max;
    min = value < min ? value : min;
  }
}

} // namespace widgets

namespace gui {

SingleHydrograph::SingleHydrograph(QWidget *parent) : QWidget(parent) {
  ui_.reset(new Ui::SingleHydrograph());
  ui_->setupUi(this);
  // Create position line/cursor
  ui_->timeSlider->setRange(0, 1);
  ui_->timeSlider->setValue(0);
  // curve_ = new QCPCurve(ui_->hydrograph->xAxis, ui_->hydrograph->yAxis);
  // curve_->setPen(QPen(Qt::darkGray));
  // Set axis tickers
  ui_->hydrograph->xAxis->grid()->setVisible(false);
  ui_->hydrograph->yAxis->grid()->setVisible(false);
  QSharedPointer<QCPAxisTicker> xTicker(new QCPAxisTicker);
  QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
  ui_->hydrograph->xAxis->setTicker(xTicker);
  ui_->hydrograph->yAxis->setTicker(yTicker);

  // Hook up signals

  QObject::connect(ui_->xAxisVariable, &QComboBox::currentIndexChanged, this,
                   &SingleHydrograph::hydrographVariablesChanged);
  QObject::connect(ui_->yAxisVariable, &QComboBox::currentIndexChanged, this,
                   &SingleHydrograph::hydrographVariablesChanged);
  QObject::connect(ui_->timeSlider, &QSlider::valueChanged, this,
                   &SingleHydrograph::timeUpdated);
}

void SingleHydrograph::clear() { model_.reset(); }

void SingleHydrograph::setModel(model::FullModel_t model) {
  model_ = model;
  ui_->hydrograph->setModel(model);
  const int oldIndexX = ui_->xAxisVariable->currentIndex();
  const int oldIndexY = ui_->yAxisVariable->currentIndex();
  if (oldIndexX == -1 || oldIndexY == -1) {
    // Populate axis dropdown lists
    ui_->xAxisVariable->clear();
    ui_->yAxisVariable->clear();
    stringVec fields = model->getOutputVariableNames();
    for (size_t i = 0; i < fields.size(); i++) {
      ui_->xAxisVariable->addItem(QString::fromStdString(fields[i]));
      ui_->yAxisVariable->addItem(QString::fromStdString(fields[i]));
    }
    ui_->xAxisVariable->setCurrentIndex(0);
    ui_->yAxisVariable->setCurrentIndex(1);
  }
  // Update slider extents
  ui_->timeSlider->setRange(model->timeTS().front(), model->timeTS().back());
  ui_->timeSlider->setValue(model->TimeQp());
  emit timeUpdated(model->TimeQp());
  emit hydrographVariablesChanged();
}

void SingleHydrograph::hydrographVariablesChanged() {
  if (model_ == nullptr)
    return;
  int x, y;
  x = ui_->xAxisVariable->currentIndex();
  y = ui_->yAxisVariable->currentIndex();
  if (x < 0 || y < 0) {
    return;
  }
  ui_->hydrograph->setVariable(y, 2);
  ui_->hydrograph->setVariable(x, 1);
}

void SingleHydrograph::timeUpdated(double time) {
  if (model_ == nullptr)
    return;
  ui_->hydrograph->setTime(time);
  ui_->timeValue->setText(QString::number(time) + QString(" s"));
  uint indexX = ui_->xAxisVariable->currentIndex();
  if (indexX != -1) {
    ui_->xAxisValue->setText(
        QString::number(model_->getOutputVariableValue(indexX)));
  }
  uint indexY = ui_->yAxisVariable->currentIndex();
  if (indexY != -1) {
    ui_->yAxisValue->setText(
        QString::number(model_->getOutputVariableValue(indexY)));
  }
  emit timeChanged(time);
}

} // namespace gui

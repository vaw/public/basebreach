// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_MULTIHYDROGRAPH_H_
#define GUI_WIDGETS_MULTIHYDROGRAPH_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include "model/modelDefinition.h"
#include "model/standardBreach.h"
#include "types.h"

namespace Ui {

// Forward declaration of GUI implementation included via AUTOUIC
class MultiHydrograph;

} // namespace Ui

namespace widgets {

namespace modelDef = model::definition;

/**
 * QCustomPlot subclass used for single model hydrograph plots.
 *
 * This includes hooks for changing the X and Y axis variable, but only allows
 * display of one model at a time.
 */
class MultiHydrographPlot : public QCustomPlot {
  Q_OBJECT

public:
  MultiHydrographPlot(QWidget *parent = nullptr);

  /** Get a mapping of model type IDs to loaded model runs. */
  const QMap<String_T, model::FullModel_t> getModels() const;
  /** Get the internal standard breach reference model. */
  const model::StandardBreach *getStandardBreach() const;
  /** Reset the view position and camera zoom. */
  void resetView();

public slots:
  /** Remove all data curves from the plot and resets the marker and axis. */
  void clear();
  /**
   * Load the given model run into the plot.
   *
   * This adds the plot data to the plot according to the variables specified.
   * If a plot was already registered for the given model ID, it is discarded
   * and replaced with the new model.
   */
  void loadModelRun(const modelDef::ModelType *modelType,
                    model::FullModel_t model);
  /**
   * Set the time used for the plot.
   *
   * This both affects current plots and any new plots added. This also redraws
   * the plot cursor. */
  void setTime(double time = 0.0);
  /**
   * Set the model variable to display in the plot.
   *
   * This allows changing the data set displayed along the vertical axis in the
   * plot. The horizontal axis is always time and cannot be changed for this
   * plot type.
   *
   * Use model::FullModel_t::getVariableNames() to query the current model for
   * available names and their indices.
   */
  void setVariable(int variableIndex);
  /** Update the standard breach reference level. */
  void updateDefBreach(double damHeight);

signals:
  /** Update hook for the time axis range. */
  void timeRangeChanged(double min, double max);

private:
  /** Replace the curve data with ones loaded from the given model. */
  void populateDataCurve(QCPCurve *curve, model::FullModel_t model);
  /** Recalculate the X and Y ranges of all models and update the plot axes. */
  void recalculateAxesRanges();
  /** Return the colour to use for a given model type. */
  QColor getPlotColour(const modelDef::ModelType *modelType) const;

  /** Map of model IDs to plot curves. */
  QMap<String_T, QCPCurve *> dataCurves_;
  /** Map of model IDs to model runs. */
  QMap<String_T, model::FullModel_t> modelRuns_;
  /** Cursor plot curve; redrawn when. */
  QCPCurve *cursor_ = nullptr;
  /** Internal reference model. */
  model::StandardBreach *stdBreach_ = nullptr;
  /** A point marker for the standard breach estimate for a given value. */
  QCPCurve *stdBreachMarker_ = nullptr;
  /** Current time across all models. */
  double time_ = 0.0;
  /** Index of the model variable displayed. */
  uint varIndex_ = 1;
};

} // namespace widgets

namespace gui {

namespace modelDef = model::definition;

/**
 * A large QCP widget showing a single model run.
 *
 * This also allows specifying the X and Y axis variables to plot against one
 * another.
 */
class MultiHydrograph : public QWidget {
  Q_OBJECT

public:
  MultiHydrograph(QWidget *parent = nullptr);

  /** Set the available models for the plot. */
  void setModelDefinitions(QList<modelDef::ModelType> modelTypes);
  /** Update the default breach when the reservoir values are changed. */
  void parametersChanged(double damHeight);
  /** Update the list of enabled models. */
  void setEnabledModels(QMap<String_T, bool> enabledModels);

signals:
  /** Update the hydrogrpah after the display time was changed. */
  void timeChanged(double time);
  /** Sent when models are enabled/disabled via the model grid. */
  void modelEnabledChanged(String_T const &modelId, bool enabled);

public slots:
  /** Reset the graph, also resets the time slider and dropdowns. */
  void clear();
  /** Add a new model run to the plot. */
  void addModel(const modelDef::ModelType *modelType, model::FullModel_t model);

protected:
  /** Update the main hydrograph to use the currently selected variables. */
  void hydrographVariablesChanged();
  /** Update the time indicator for value display and the shared hydrograph. */
  void timeUpdated(double time);
  /** Callback for a model enabled checkbox being clicked. */
  void modelEnabledClicked();

  /** Internal GUI container. */
  QScopedPointer<Ui::MultiHydrograph> ui_;
  /** Current time value. */
  double time_ = 0.0;

  /** References to dynamically generated UI elements. */
  QMap<String_T, QLabel *> modelGridCurrent_;
  QMap<String_T, QLabel *> modelGridPeak_;
  QMap<String_T, QLabel *> modelGridTotalDischarge_;
  QMap<String_T, QCheckBox *> modelGridEnabled_;
};

} // namespace gui

#endif // GUI_WIDGETS_MULTIHYDROGRAPH_H_

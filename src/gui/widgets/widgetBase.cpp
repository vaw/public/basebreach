// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "gui/widgets/widgetBase.h"

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include "ExceptionHandling.h"
#include "model/fullModel.h"
#include "types.h"

namespace widgets {

CustomWidgetBase::CustomWidgetBase(QWidget *parent) : QCustomPlot(parent) {}

void CustomWidgetBase::clear() {}

void CustomWidgetBase::setupGraph() {}

void CustomWidgetBase::redrawGraph() {}

ModelWidgetBase::ModelWidgetBase(QWidget *parent) : CustomWidgetBase(parent) {}

ModelWidgetBase::ModelWidgetBase(model::FullModel_t model, QWidget *parent)
    : CustomWidgetBase(parent) {
  model_ = model;
  emit modelChanged(model_);
}

void ModelWidgetBase::clear() { model_.reset(); }

void ModelWidgetBase::setModel(model::FullModel_t model) {
  BB_ASSERT(model.get(), "null pointer passed, expected model");
  model_ = model;
  emit modelChanged(model_);
}

void ModelWidgetBase::setupGraph() {}

void ModelWidgetBase::redrawGraph() {}

TimeModelWidgetBase::TimeModelWidgetBase(QWidget *parent)
    : ModelWidgetBase(parent) {}
TimeModelWidgetBase::TimeModelWidgetBase(model::FullModel_t model,
                                         QWidget *parent)
    : ModelWidgetBase(model, parent) {}
TimeModelWidgetBase::TimeModelWidgetBase(model::FullModel_t model, double time,
                                         QWidget *parent)
    : ModelWidgetBase(model, parent) {
  time_ = time;
  emit timeChanged(time);
}

void TimeModelWidgetBase::clear() {
  ModelWidgetBase::clear();
  time_ = -1.0;
}

void TimeModelWidgetBase::setTime(double time) {
  time_ = time;
  if (model_.get()) {
    model_->setTime(time);
  }
  emit timeChanged(time);
}

ParameterWidgetBase::ParameterWidgetBase(QWidget *parent)
    : CustomWidgetBase(parent) {}

void ParameterWidgetBase::setupGraph() {}

void ParameterWidgetBase::redrawGraph() {}

} // namespace widgets

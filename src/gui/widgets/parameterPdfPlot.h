// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_PARAMETERPDFPLOT_H_
#define GUI_WIDGETS_PARAMETERPDFPLOT_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <kde.hpp>

#include "gui/widgets/widgetBase.h"
#include "types.h"

namespace widgets {

/**
 * A plot widget displaying a 1D PDF.
 */
class ParameterPdfPlot : public ParameterWidgetBase {

  Q_OBJECT

public:
  ParameterPdfPlot(QWidget *parent = nullptr);

public slots:
  void setData(const doubleVec &data, uint Np);
  void setValue(double value);

protected:
  void redrawGraph() override;
  void setupGraph() override;

private:
  void calcPdf(const doubleVec &data);
  QScopedPointer<KDE> kde_;
  double xPos_ = 0.0;
  QCPGraph *graph_;
  QCPCurve *positionLine_;
};

} // namespace widgets

#endif // GUI_WIDGETS_PARAMETERPDFPLOT_H_

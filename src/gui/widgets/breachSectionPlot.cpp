// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Edited: August 2023
 *      Editor: Matthew Halso
 */
 
#include "gui/widgets/breachSectionPlot.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <qcustomplot.h>

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>

#include "model/fullModel.h"
#include "types.h"

namespace {

template <typename T> inline QVector<T> stdVecToQVec(std::vector<T> vec) {
  return QVector<T>(vec.begin(), vec.end());
}

} // namespace

namespace widgets {

void BreachSectionPlot::setModel(model::FullModel_t model) {
  TimeModelWidgetBase::setModel(model);
  if (baseLine_ == nullptr) {
    setupGraph();
  }
  redrawGraph();
}

void BreachSectionPlot::setTime(double time) {
  TimeModelWidgetBase::setTime(time);
  if (model_.get() != nullptr) {
    redrawGraph();
  }
}

void BreachSectionPlot::redrawGraph() {
  // Dam baseline
  QVector<double> x, y;
  x << -xmax_ << xmax_;
  y << 0.0 << 0.0;
  baseLine_->setData(x, y);
  // Dam breach shape data
  QVector<double> xs, ys;
  doubleVecVec shape = model_->BreachShape(100);
  xs = stdVecToQVec<double>(shape.at(0));
  xs.append(xmax_);
  ys = stdVecToQVec<double>(shape.at(1));
  ys.append(ys.last());
  shapeCurve_->setData(xs, ys); // Only covers half the x-axis
  for (qsizetype i = 0; i < xs.size(); i++)
    xs.replace(i, -1.0 * xs.at(i));
  shapeCurve_->addData(xs, ys); // Copy with flipped x-axis
  // Erosion zone outline
  doubleVecVec erosion = model_->ErosionZone(100);
  /**
   * NOTE: As of Sep 2021, only the Peter and PeterCal models report a valid
   * erosion zone. For the other models, this is either not implemented or not
   * sensible.
   */
  if (erosion.size() == 2) {
    xs = stdVecToQVec<double>(erosion.at(0));
    ys = stdVecToQVec<double>(erosion.at(1));
    erosionCurve_->setData(xs, ys); // Only covers half the x-axis
    erosionCurve_->addData(0.0, NAN);
    for (qsizetype i = 0; i < xs.size(); i++)
      xs.replace(i, -1.0 * xs.at(i));
    erosionCurve_->addData(xs, ys); // Copy with flipped x-axis
  }
  // Reservoir water level
  doubleVec xi(2), yi(2);
  xi.at(0) = -1.0 * model_->ReservoirLevelLocation();
  xi.at(1) = model_->ReservoirLevelLocation();
  yi.at(0) = yi.at(1) = model_->ReservoirLevel();
  reservoirLine_->setData(stdVecToQVec<double>(xi), stdVecToQVec<double>(yi));
  // Breach water level
  double waterLevel = model_->BreachLevel() + model_->WaterDepth();
  xi.at(0) = -1.0 * model_->ReservoirLevelLocation();
  xi.at(1) = model_->ReservoirLevelLocation();
  yi.at(0) = yi.at(1) = waterLevel;
  waterdepthLine_->setData(stdVecToQVec<double>(xi), stdVecToQVec<double>(yi));
  replot();
}

void BreachSectionPlot::setupGraph() {
  // Switch to the final time step to determine the maximum breach size
  model_->setTime(model_->timeTS().back());
  doubleVecVec shape = model_->BreachShape(2);
  xmax_ = shape.at(0).back() * 1.2;       // 20% horizontal padding
  double ymax = shape.at(1).back() * 1.1; // 10% vertical padding
  xAxis->setRange(-xmax_, xmax_);
  
  // Prefer keeping x and y units in sync
  yAxis->setScaleRatio(xAxis, 1.0);
  QCPRange yrange = yAxis->range();
  if (yrange.lower > 0.0 || yrange.upper < 1.0) {
    yAxis->setRange(0.0, ymax);
    xAxis->setScaleRatio(yAxis, 1.0);
    xmax_ = xAxis->range().upper;
  }

  // Define axis
  QSharedPointer<QCPAxisTicker> xTicker(new QCPAxisTicker);
  xAxis->setTicker(xTicker);
  QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
  yAxis->setTicker(yTicker);
  xAxis->setRange(-xmax_, xmax_);
  yAxis->setScaleRatio(xAxis, 1.0);

  // Create graphs
  reservoirLine_ = addGraph();
  reservoirLine_->setPen(QPen(Qt::NoPen));
  waterdepthLine_ = addGraph();
  waterdepthLine_->setPen(QPen(Qt::NoPen));
  QPen damBodyColor = QPen(QColor(210, 180, 60, 255));
  shapeCurve_ = new QCPGraph(xAxis, yAxis);
  shapeCurve_->setPen(damBodyColor);
  baseLine_ = addGraph();
  baseLine_->setPen(damBodyColor);
  QPen erosionColor = QPen(QColor(210, 90, 0, 255));
  erosionColor.setWidth(2);
  erosionCurve_ = new QCPGraph(xAxis, yAxis);
  erosionCurve_->setPen(erosionColor);

  // Add shape fills
  shapeCurve_->setBrush(QBrush(QColor(239, 229, 186, 255)));
  reservoirLine_->setBrush(QBrush(QColor(0, 30, 210, 90)));
  waterdepthLine_->setBrush(QBrush(QColor(0, 30, 210, 180)));

  // Axis labels
  xAxis->setLabel("breach width [m]");
  xAxis->grid()->setVisible(false);
  xAxis->setTicks(true);
  yAxis->setLabel("level [m]");
  yAxis->grid()->setVisible(false);
  yAxis->setTicks(true);

  // Set model timestep to time of peak discharge
  setTime(model_->TimeQp());

  replot();
}

} // namespace widgets

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef GUI_WIDGETS_BREACHSECTIONPLOT_H_
#define GUI_WIDGETS_BREACHSECTIONPLOT_H_

#include <QtCore>
#include <QtWidgets>
#include <qcustomplot.h>

#include <vector>

#include "gui/widgets/widgetBase.h"
#include "types.h"

namespace widgets {

/**
 * A plot widget displaying a breach cross section.
 *
 * After instantiation, use the setModel slot to specify the model run
 * to display. By default, this plot will show the time of maximum
 * discharge. Use the setTime slot to specify the time step to display.
 */
class BreachSectionPlot : public TimeModelWidgetBase {
  Q_OBJECT

  using TimeModelWidgetBase::TimeModelWidgetBase;

public slots:
  /** Select the model run to display in the hydrograph plot. */
  void setModel(model::FullModel_t model);
  /** Update the displayed time of the hydrograph. */
  void setTime(double time);

protected:
  void redrawGraph();
  void setupGraph();

private:
  QCPGraph *baseLine_ = nullptr;
  QCPGraph *erosionCurve_ = nullptr;
  QCPGraph *reservoirLine_ = nullptr;
  QCPGraph *shapeCurve_ = nullptr;
  QCPGraph *waterdepthLine_ = nullptr;
  double xmax_ = 10.0;
};

} // namespace widgets

#endif // GUI_WIDGETS_BREACHSECTIONPLOT_H_

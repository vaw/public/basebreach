// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IOutput.h
 *
 *  Created on: May 5, 2014
 *      Author: samuelpeter
 */

#ifndef API_IOUTPUT_H_
#define API_IOUTPUT_H_

#include <string>
#include <vector>

#include "types.h"

namespace api {

class IOutput {
public:
  IOutput() { ; }
  virtual ~IOutput() { ; }

  virtual void setOutputTimeStep(double) = 0;
  virtual void setValues(std::vector<std::string>) = 0;
  virtual void init() = 0;
  virtual void write() = 0;
  virtual void writeQpLog(double, double) = 0;
  virtual void finalize() = 0;
};

class IOutputpyBB : public virtual IOutput {
public:
  IOutputpyBB() { ; }
  virtual ~IOutputpyBB() { ; }

  virtual const doubleVecVec &getData() = 0;
};

} // namespace api

#endif // API_IOUTPUT_H_

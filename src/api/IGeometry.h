// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IGeometry.h
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 */

#ifndef API_IGEOMETRY_H_
#define API_IGEOMETRY_H_

#include "types.h"

namespace api {

/** Abstract interface for dam geometry implementations. */
class IGeometry {
public:
  IGeometry() { ; }
  virtual ~IGeometry() { ; }

  /** Reset the geometry model to its initial state. */
  virtual void init() = 0;
  /** Return the initial level of the breach. [m] */
  virtual double getInitialLevel() = 0;
  /** Return the current level of the breach. [m] Always >= 0.0. */
  virtual double getBreachlevel() = 0;
  /**
   * Return the theoretical breach level variable. [m]
   *
   * Vertical erosion is occurring if this value is still greater
   * than 0.0, lateral widening of the breach otherwise.
   */
  virtual double getLevel() = 0;
  /** Return the width of the breach as its highest, widest point. [m] */
  virtual double getTopWidth() = 0;
  /** Return the angle of the breach wall at its top. [deg] */
  virtual double getTopAngle() = 0;
  /**
   * Return true if the breach has progressed to the fixed level. [m]
   *
   * No more vertical erosion can occur after this, though the breach
   * may continue to grow laterally.
   */
  virtual bool hasReachedBottom() = 0;
  /** Distance from the bottom of the breach to the fixed level. [m] */
  virtual double relativeDistanceToBottom() = 0;
  /** Current change in breach volume per change in breach level. [m3/m] */
  virtual double getVolumeErosionRate() = 0;
  virtual double calcFlowArea(double h) = 0;
  virtual double calcWettedPerimeter(double h) = 0;
  virtual doubleVecVec getErosionZone(double h, uint N) = 0;
  virtual doubleVecVec getShape(uint N) = 0;
  virtual double getLocationAtLevel(double level) = 0;
  /** Return the average width of the breach. [m] */
  virtual double getAverageWidth() = 0;
  /** Print debug information about the state of the geometry model. */
  virtual void debug() = 0;
  /** Update the breach level and recalculate dependent variables. */
  virtual void update(double) = 0;
};

// the following interfaces are used by the other modules, e.g. hydraulics
class IGeometryPolynom : public IGeometry {
public:
  IGeometryPolynom() { ; }
  virtual ~IGeometryPolynom() { ; }

  /**
   * Coefficient used to calculate the hydraulic variables. [-]
   *
   * (omega+1) / omega
   */
  virtual double getLambda() = 0;
  /**
   * Coefficient used to calculate the hydraulic variables. [-]
   *
   * 2*lambda / (2*lambda + 1)
   */
  virtual double getSigma() = 0;
  virtual double calcErodiblePerimeter(double h) = 0;
  virtual void setIntegrationPrecision(double) = 0;
};

// the following interfaces are used by the other modules, e.g. hydraulics
class IGeometryTriTra : public IGeometry {
public:
  IGeometryTriTra() { ; }
  virtual ~IGeometryTriTra() { ; }

  /** Return the bottom slope of the breach sides. [deg] */
  virtual double getSlope() = 0;
  /** Return the bottom width of the breach. [m] */
  virtual double getBottomWidth() = 0;
};

} // namespace api

#endif // API_IGEOMETRY_H_

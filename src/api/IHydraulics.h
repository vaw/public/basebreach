// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IHydraulics.h
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#ifndef API_IHYDRAULICS_H_
#define API_IHYDRAULICS_H_

#include "types.h"

namespace api {

/** Interface used by the solver class and the actual dam break model class. */
class IHydraulics {
public:
  IHydraulics() { ; }
  virtual ~IHydraulics() { ; }

  /** Reset the hydraulics model to its initial state. */
  virtual void init() = 0;
  /** Return the current breach discharge. [m3/s] */
  virtual double getDischarge() = 0;
  /** Return the critical water depth in the breach cross section. [m] */
  virtual double getCriticalWaterDepth() = 0;
  /** Return the flow area of the critical cross section. [m2] */
  virtual double getFlowArea() = 0;
  /** Return the flow velocity in the critical cross section. [m/s] */
  virtual double getFlowVelocity() = 0;
  /** Return the hydraulic radius of the critical cross section. [m] */
  virtual double getHydraulicRadius() = 0;
  /** Return the wetted perimetre of the critical cross section. [m] */
  virtual double getWettedPerimeter() = 0;
  /**
   * Return the erodible perimeter of the critical cross section. [m]
   *
   * Once the fixed bottom level of the breach has been reached, this
   * only covers the breach walls.
   */
  virtual double getErodiblePerimeter() = 0;
  /**
   * Return true for a steady-state solution (Poleni), or false for
   * a non-steady solution (Ritter).
   */
  virtual bool isSteady() = 0;
  virtual doubleVecVec getErosionZone(uint N) = 0;
  /**
   * Update all hydraulic variables using the water level in the associated
   * reservoir, the breach water level, and the associated geometry object.
   */
  virtual void update() = 0;
};

} // namespace api

#endif // API_IHYDRAULICS_H_

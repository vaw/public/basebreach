// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IDam.h
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 */

#ifndef API_IDAM_H_
#define API_IDAM_H_

namespace api {

class IDam {
public:
  IDam() { ; }
  virtual ~IDam() { ; }

  /** Height of the dam. [m] */
  virtual double getHeight() = 0;
  /** Width of the dam crest. [m] */
  virtual double getCrestWidth() = 0;
  /** Embankment slope (upstream and downstream symmetrical). [-] */
  virtual double getEmbankmentSlope() = 0;
  /** Level of the fixed bottom where no more erosion is possible. [m] */
  virtual double getBottomLevel() = 0;
};

} // namespace api

#endif // API_IDAM_H_

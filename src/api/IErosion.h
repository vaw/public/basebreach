// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IErosion.h
 *
 *  Created on: Apr 16, 2014
 *      Author: samuelpeter
 */

#ifndef API_IEROSION_H_
#define API_IEROSION_H_

namespace api {

class IErosion {
public:
  IErosion() { ; }
  virtual ~IErosion() { ; }

  /** Reset the Erosion model to its initial values. */
  virtual void init() = 0;
  /** Calibration coefficient used to scale the transport rate. [?] */
  virtual double getCalibrationCoefficient() = 0;
  /** Exponent of the flow velocity. [-] */
  virtual double getExponentA() = 0;
  /** Exponent of the hyrdraulic radius. [-] */
  virtual double getExponentB() = 0;
  /** Return the calculated transport rate. [m3/s/m] */
  virtual double getTransportRate() = 0;
  /**
   * Update the sediment transport and transport rate values.
   *
   * The sediment transport goes like: qs ~ calCoeff * v^(A) * Rhyd^(B)
   */
  virtual void update() = 0;
  /** Get the maximum erosion width */
  virtual double getErosionLimit() = 0;
  virtual void freeze(bool frozen = true) = 0;
};

} // namespace api

#endif // API_IEROSION_H_

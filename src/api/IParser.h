// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IParser.h
 *
 *  Created on: May 1, 2014
 *      Author: samuelpeter
 */

#ifndef API_IPARSER_H_
#define API_IPARSER_H_

#include <QtCore>

#include <string>
#include <vector>

#include "types.h"

namespace api {

class IParserTag {
public:
  IParserTag() { ; }
  virtual ~IParserTag() { ; }
  virtual std::string getName() = 0;
  virtual void setValue(std::string) = 0;
  virtual void setValue(double) = 0;
  virtual bool hasValue() = 0;
  virtual std::string getStringValue() = 0;
  virtual bool hasDouble() = 0;
  virtual double getDoubleValue() = 0;
  virtual void addValue(std::string) = 0;
  virtual bool hasList() = 0;
  virtual std::vector<std::string> getValueList() = 0;
  virtual std::string getValueAsString() = 0;
};

class IParserBlock {
public:
  IParserBlock() { ; }
  virtual ~IParserBlock() { ; }
  virtual std::string getName() = 0;
  virtual void addTag(IParserTag_t) = 0;
  virtual tagVec getTags() = 0;
  virtual api::IParserTag_t getTagWithName(std::string name) = 0;
  virtual void removeEmptyTags() = 0;
  virtual uint getNumTags() = 0;
};

class IParser {
public:
  IParser() { ; }
  virtual ~IParser() { ; }
  /** Load the model parameters from the given input file. */
  virtual void readInputFile(std::string) = 0;
  virtual void readInputPars(QMap<QString, QMap<QString, QVariant>>,
                             QString) = 0;
  virtual void addBlock(IParserBlock_t) = 0;
  virtual blockVec getBlocks() = 0;
  virtual IParserBlock_t getBlockWithName(std::string) = 0;
  virtual IParserBlock_t getProjectBlock() = 0;
  virtual IParserBlock_t getGlobalBlock() = 0;
  virtual void setGlobalTag(std::string, std::string) = 0;
  virtual void setGlobalTag(std::string, double) = 0;
  virtual void printLogContent() = 0;
  virtual void printContent() = 0;
  virtual bool init() = 0;
};

} // namespace api

#endif // API_IPARSER_H_

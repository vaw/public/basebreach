// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IDistribution.h
 *
 *  Created on: January 20, 2015
 *      Author: samuelpeter
 */

#ifndef API_IDISTRIBUTION_H_
#define API_IDISTRIBUTION_H_

#include <QObject>

#include <string>

#include "types.h"

namespace api {

class IDistribution : public QObject {
public:
  IDistribution() {}

  virtual void resetBy(IDistribution_t) = 0;
  virtual IDistribution_t getCopy() = 0;

  // some auxiliary stuff
  virtual void setName(std::string) = 0;
  virtual std::string getName() = 0;
  virtual std::string getType() = 0;
  virtual stringVec getValidTypes() = 0;
  virtual IDistribution_t createDistribution(int) = 0;
  virtual void printProperties() = 0;

  // parameters
  virtual void setParameters(ValuePair) = 0;
  virtual ValuePair getParameters() = 0;
  virtual void setMoments(ValuePair) = 0;
  virtual ValuePair getMoments() = 0;
  /** Return true if parameters are defined. */
  virtual bool isDefined() = 0;
  virtual bool isSecondParameterNeeded() = 0;

  // bounds
  virtual void setBounds(ValuePair) = 0;
  virtual ValuePair getBounds() = 0;
  virtual bool isWithinBounds(double) = 0;
  /** Return minimum distance from parameter distribution bounds. */
  virtual double getDistanceFromBounds(double) = 0;
  /** Return the reflected value for out-of-bounds values. */
  virtual double bounceOffBounds(double) = 0;

  // Distribution functions
  virtual double pdf(double) = 0;
  virtual double cdf(double) = 0;
  virtual double quantile(double) = 0;

  // Descriptive stats
  virtual double mean() = 0;
  virtual double var() = 0;
  virtual double stdev() = 0;

  // draw random numbers
  virtual doubleVec drawNumbers(uint) = 0;
};

} // namespace api

#endif // API_IDISTRIBUTION_H_

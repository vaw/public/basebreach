// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IPDESystemSolver.h
 *
 *  Created on: Apr 29, 2014
 *      Author: samuelpeter
 */

#ifndef API_IPDESYSTEMSOLVER_H_
#define API_IPDESYSTEMSOLVER_H_

#include "types.h"

namespace api {

class IPDESystemSolver {
public:
  IPDESystemSolver() { ; }
  virtual ~IPDESystemSolver() { ; }

  /** Reset all system variables to their initial values. */
  virtual void initSystem() = 0;
  /**
   * Perform the integration in time.
   *
   * The implementation of this function only cares about the numerics!
   */
  virtual void evaluate() = 0;
  /** Return the solver argument. */
  virtual double getSolverArgument() = 0;
  /**
   * Set the solver argument.
   *
   * The solver argument depends on the solver implementation.
   */
  virtual void setSolverArgument(double) = 0;
  virtual double getTime() = 0;
  /** Return a vector of implemented solver names. */
  virtual stringVec getImplementedSolvers() = 0;
  /** Return the descriptions of the solver arguments. */
  virtual stringVec getArgumentDescriptions() = 0;
  virtual void debug() = 0;

protected:
  /**
   * Calculate the derivatives of the differential equation.
   *
   * The implementation of this function only cares about the physics!
   */
  virtual void calcDerivatives(const doubleVec &) = 0;
  /** Update the system state using the given values. */
  virtual void setNewSystemState(const doubleVec &) = 0;
};

} // namespace api

#endif // API_IPDESYSTEMSOLVER_H_

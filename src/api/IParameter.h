// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IParameter.h
 *
 *  Created on: Feb 3, 2015
 *      Author: samuelpeter
 */

#ifndef API_IPARAMETER_H_
#define API_IPARAMETER_H_

#include <QObject>

#include "types.h"

namespace api {

class IParameter : public QObject {

public:
  IParameter() { ; }
  virtual ~IParameter() { ; }

  /**
   * Set a fixed value for the parameter.
   *
   * The value specified will be available through the getValue()
   * getter. When getRealizations() is called, this value will be
   * repeated.
   *
   * Calling this method will unset any distribution.
   */
  virtual void setValue(double) = 0;
  /**
   * Set a distribution for this parameter.
   *
   * The distribution specified will be available through the
   * getDistribution() getter. When getRealizations() is called, the
   * values will be sampled according to this distribution.
   *
   * Calling this method will unset any fixed value.
   */
  virtual void setDistribution(IDistribution_t) = 0;
  /** Return whether a distribution is defined for this parameter. */
  virtual bool isDistributionDefined() = 0;
  /**
   * Get the fixed value set via setValue(). Returns NAN if a
   * distribution was specified instead.
   */
  virtual double getValue() = 0;
  /**
   * Get the distribution set via setDistribution(). Returns a NULL
   * pointer if a fixed value was specified instead.
   */
  virtual IDistribution_t getDistribution() = 0;
  /**
   * Return a vector of parameter realizations.
   *
   * If a fixed value was specified, it will be used for all indices.
   * If a distribution was specified, the values in the returned
   * vector will be sampled according to the distribution.
   */
  virtual doubleVec getRealizations(uint) = 0;
};

} // namespace api

#endif /* API_IPARAMETER_H_ */

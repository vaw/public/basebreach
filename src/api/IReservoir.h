// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IReservoir.h
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#ifndef API_IRESERVOIR_H_
#define API_IRESERVOIR_H_

namespace api {

class IReservoir {

public:
  IReservoir() { ; }
  virtual ~IReservoir() { ; }

  /** Reinitialize the reservoir to its initial water level. */
  virtual void init() = 0;
  /**
   * Initially stored volume above the breach bottom. [m3]
   *
   * This is the maximum amount of water that will be released for the
   * given breach bottom level and initial reservoir level.
   */
  virtual double getVolumeAboveBreachBottom() = 0;
  /** Return the maximum storage volume of the dam-reservoir system. [m3] */
  virtual double getMaximumVolume() = 0;
  /** Initial water level in the reservoir. [m] */
  virtual double getInitialLevel() = 0;
  /**
   * Return the shape coefficient of the reservoir. [-]
   *
   * The stored volume is proportional to the water level raised to the
   * shapeCoefficient: volume ~ level^shapeCoefficient
   */
  virtual double getShapeCoefficient() = 0;
  /** Return the current inflow into the reservoir. [m3/s] */
  virtual double getInflow(double time) = 0;
  /** Return the volume of water stored in the reservoir. [m3] */
  virtual double getVolume() = 0;
  /** Return the water surface area. [m2] */
  virtual double getWaterSurface() = 0; // Is this cross section or X/Y area?
  /** Return the current water level in the reservoir. [m] */
  virtual double getLevel() = 0;
  /** Update the water level and recalculate dependent variables. */
  virtual void update(double) = 0;
};

} // namespace api

#endif // API_IRESERVOIR_H_

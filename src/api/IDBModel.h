// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * IDBModel.h
 *
 *  Created on: May 12, 2014
 *      Author: samuelpeter
 */

#ifndef API_IDBMODEL_H_
#define API_IDBMODEL_H_

#include <types.h>

namespace api {

class IDBModel {
public:
  IDBModel() { ; }
  virtual ~IDBModel() { ; }

  virtual double getCutoffTime() const = 0;
  virtual void setCutoffTime(double cutoff_time) = 0;

  virtual void setAbortCriterion(double) = 0;
  virtual void setSimulationType(int) = 0;
  virtual void run() = 0;
  virtual double getPeakDischarge() = 0;
  virtual double getAverageBreachWidth() = 0;
  virtual IDam_t getDam() = 0;
  virtual IReservoir_t getReservoir() = 0;
  virtual IHydraulics_t getHydraulics() = 0;
  virtual IGeometry_t getGeometry() = 0;
  virtual IErosion_t getErosion() = 0;
  virtual void setDataOutput(IOutputpyBB_t) = 0;
  virtual const doubleVecVec &getData() = 0;
  virtual uint getNumTimeSteps() = 0;
};

} // namespace api

#endif // API_IDBMODEL_H_

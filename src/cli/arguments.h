// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef CLI_ARGUMENTS_H_
#define CLI_ARGUMENTS_H_

#include <QtCore>

#include "types.h"

namespace cli {

/**
 * Process the command line arguments for the given parser.
 *
 * This handles the merging of input files, model type selection, and finally
 * generates a model config that can be passed to the BaseBreach core model
 * runner.
 */
QList<ModelConfig_T> processCliArguments(QCommandLineParser &parser);

/**
 * Command line parser definition.
 *
 * This function sets up the available arguments and the help text for each.
 */
void setUpClArguments(QCommandLineParser &parser);

/**
 * Generate the BASEbreach name and version banner.
 */
String_T getWelcomeMessage();

} // namespace cli

#endif // CLI_ARGUMENTS_H_

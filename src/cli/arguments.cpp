// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "cli/arguments.h"

#include <QtCore>

#include "ExceptionHandling.h"
#include "model/modelDefinition.h"
#include "types.h"

#ifndef BBVERSION
#define BBVERSION "X.X (DEV)"
#endif

namespace cli {

namespace modelDef = model::definition;

String_T getWelcomeMessage() {
  size_t N = QString(BBVERSION).size();
  QStringList msg;
  msg << "***************************************************\n";
  msg << "*                BASEbreach v" << BBVERSION;
  for (size_t ii = 0; ii < 21 - N; ii++)
    msg << " ";
  msg << "*\n";
  msg << "*           by samuel.j.peter (ETHZ)              *\n";
  msg << "***************************************************\n";
  return msg.join("");
}

QList<ModelConfig_T> processCliArguments(QCommandLineParser &parser) {
  // Read positional arguments as input files
  QStringList input_files = parser.positionalArguments();
  for (auto &file : input_files) {
    // Ensure the given files exist
    if (!QFile::exists(file)) {
      BB_EXCEPTION(("File not found: " + file).toStdString());
    }
  }
  // Generate model config
  if (input_files.size() < 1) {
    BB_MESSAGE("At least one input configuration is required.");
    parser.showHelp(1);
  }
  ModelConfig_T config_base;
  for (auto &file : input_files) {
    ModelConfig_T config_it = modelDef::loadModelConfig(file);
    modelDef::mergeModelConfigs(config_base, config_it);
  }
  // Insert custom controller tags into the model config based on CLI flags
  if (!config_base.contains("GLOBAL")) {
    config_base.insert("GLOBAL", ParameterMap_T());
  }
  // Switch for peak-only simulation type ("full" is default and does not have
  // to be explicitly set)
  if (parser.isSet("peak")) {
    config_base["GLOBAL"]["simulation"] = "peak";
  }
  // Add CLI logging with user-defined timestep
  if (parser.isSet("logging")) {
    bool ok;
    double timestep = parser.value("logging").toDouble(&ok);
    if (!ok) {
      BB_EXCEPTION("Invalid value for --logging: Must be a number.");
    }
    config_base["GLOBAL"]["console"] = timestep;
  }
  // Select models
  QStringList selected_models = parser.values("models");
  // If no model is specified, run all
  if (selected_models.size() < 1) {
    BB_MESSAGE("No model selected. Using all models.");
    selected_models = {"bbAwel", "bbMacchione", "bbPeter", "bbPeterCal"};
  }
  // Generate model configs
  QList<ModelConfig_T> configs;
  for (auto &model_id : selected_models) {
    ModelConfig_T config_it = ModelConfig_T(config_base);
    config_it["PROJECT"]["model"] = model_id;
    configs.append(config_it);
  }
  return configs;
}

void setUpClArguments(QCommandLineParser &parser) {
  parser.setApplicationDescription(
      "A high-performance progressive dam breach model.");
  // Generic options
  parser.addHelpOption();
  parser.addVersionOption();
  // All positional arguments are interpreted as simulation input files to
  // merge
  parser.addPositionalArgument(
      "input-files", "Input files to merge into a model configuration.",
      "[input-files...]");
  // Model selection
  QCommandLineOption modelOption(
      QStringList() << "m"
                    << "model",
      "ID of a model to run. If not specified, all models are run.");
  parser.addOption(modelOption);
  // Thread count
  QCommandLineOption threadsOption(
      QStringList() << "n"
                    << "num-threads",
      "Number of threads to use. If not specified, a single thread is used.",
      "threads", "1");
  parser.addOption(threadsOption);
  // Generate a dummy input file instead of running a simulation
  QCommandLineOption generateParameterDummy(
      QStringList() << "d"
                    << "dummy",
      "Generate a dummy input file containing the parameters for all models. "
      "Instead of parameter values, the fields will contain descriptions of "
      "their respective parameters.");
  parser.addOption(generateParameterDummy);
  // Optional peak-only mode
  QCommandLineOption peakOnly(
      QStringList() << "p"
                    << "peak",
      "Only calculate the peak discharge for each model. Generally higher "
      "simulation throughput than running a full simulation.");
  parser.addOption(peakOnly);
  // Optional command-line logging
  QCommandLineOption consoleLogging(
      QStringList() << "l"
                    << "logging",
      "Log simulation output with the given timestep.", "timestep");
  parser.addOption(consoleLogging);
}

} // namespace cli

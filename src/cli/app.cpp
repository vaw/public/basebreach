// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */
 
#include "cli/app.h"

#include <QtCore>

#include "ExceptionHandling.h"
#include "ModelTypes.h"
#include "cli/arguments.h"
#include "core.h"
#include "model/PDESystemSolver.h"
#include "model/modelDefinition.h"
#include "types.h"

namespace cli {

BaseBreachCli::BaseBreachCli(int &argc, char **argv)
    : QCoreApplication(argc, argv) {
  core_.reset(new BaseBreachCore(this));
  // Load CLI
  cli::setUpClArguments(parser_);
  // Parse arguments
  parser_.process(*this);

  // We have to parse the arguments here, but we can't run any events as the Qt
  // event loop is not yet active. This zero-delay timer is used to process the
  // parsed arguments right after the event loop is started.

  QTimer::singleShot(0, this, SLOT(processArguments()));
}

void BaseBreachCli::processArguments() {
  // Check for dummy mode
  if (parser_.isSet("d")) {
    String_T filename = QDir::currentPath() + "/BASEbreach_parameters.json";
    BB_MESSAGE(("Generated parameter dummy at " + filename).toStdString());
    modelDef::saveModelConfig(generateParameterDummy(), filename);
    emit exit();
    return;
  }
  uint num_threads = parser_.value("n").toUInt();
  QList<ModelConfig_T> configs = cli::processCliArguments(parser_);
  // Connect model completion signal
  QObject::connect(core_.get(), &BaseBreachCore::modelRunFinished, this,
                   &BaseBreachCli::outputResults);
  // Schedule model run
  BB_MESSAGE(getWelcomeMessage().toStdString())
  num_results_ = configs.size();
  core_->runModelBatch(configs, num_threads);
}

ModelConfig_T BaseBreachCli::generateParameterDummy() const {
  ModelConfig_T config;
  // Breach
  config["BREACH"] = ParameterMap_T();
  config["BREACH"]["initial_level"] = "Level of the initial breach [m].";
  // Dam
  config["DAM"] = ParameterMap_T();
  config["DAM"]["height"] = "Total dam height [m].";
  config["DAM"]["bottom_level"] = "Bottom level of the dam [m].";
  config["DAM"]["crest_width"] = "Dam crest width [m].";
  config["DAM"]["embankment_slope"] = "Rise of run slope of the dam [m].";
  // Reservoir
  config["RESERVOIR"] = ParameterMap_T();
  config["RESERVOIR"]["volume"] = "Total reservoir volume [m^3].";
  config["RESERVOIR"]["shape_exponent"] = "Reservoir shape exponent [-].";
  config["RESERVOIR"]["initial_level"] = "Initial reservoir level [m].";
  config["RESERVOIR"]["inflow"] = "Inflow into the reservoir [m^3/s].";
  // Simulation
  config["SIMULATION"] = ParameterMap_T();
  config["SIMULATION"]["cutoff_time"] = "Simulation cutoff time [s].";
  config["SIMULATION"]["precision_at_qmax"] =
      "Required precision at peak discharge. Default: 0.01";
  config["SIMULATION"]["erosion_limit"] =
      "Maximum eroded width of the breach top [m].";
  // Numerics
  config["NUMERICS"] = ParameterMap_T();
  auto solver = model::PDESolverEmpty();
  QStringList solver_names;
  for (auto name : solver.getImplementedSolvers()) {
    solver_names.append(QString::fromStdString(name));
  }
  config["NUMERICS"]["pde_solver"] = "Name of the PDE solver to use. Available "
                                     "solvers: " +
                                     solver_names.join(", ") + ".";
  config["NUMERICS"]["pde_solver_argument"] =
      "Argument to pass to the PDE solver. Default: 1";
  config["NUMERICS"]["integration_precision"] =
      "Target precision for integration step. Default: 0.01";
  // Add model parameters
  config["BREACH"]["angle"] = "Critical slope of breach [deg]. Used by models: "
                              "bbMacchione, bbPeter, bbPeterCal. Default: 70.0";
  config["EROSION"] = ParameterMap_T();
  config["EROSION"]["scaling_coefficient"] =
      "Scaling coefficient for the "
      "erosion model 8-]. Used by models: bbMacchione, bbPeter, bbPeterCal. "
      "Default: 1.5e-4";
  config["EROSION"]["exponent_v"] = "Exponent of flow velocity [-]. Used by "
                                    "models: bbPeter, bbPeterCal. Default: 4.4";
  config["EROSION"]["exponent_rhy"] =
      "Exponent of flow velocity [-]. Used by models: bbPeter, bbPeterCal. "
      "Default: -0.74";
  return config;
}

void BaseBreachCli::outputResults(const modelDef::ModelType *modelType,
                                  const model::FullModel_t result) {
  String_T now = QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm-ss");
  String_T filename =
      QDir::currentPath() + "/" + modelType->getName() + "_" + now + ".csv";
  BB_MESSAGE("\nExported results to " + filename.toStdString());
  core_->exportModelRun(result, filename);
  // Quit once all results have been exported
  if (--num_results_ <= 0) {
    emit exit();
  }
}

} // namespace cli

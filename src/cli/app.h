// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef CLI_APP_H_
#define CLI_APP_H_

#include <QtCore>

#include "core.h"
#include "model/modelDefinition.h"
#include "types.h"

namespace cli {

/**
 * CLI version of the BASEbreach application.
 */
class BaseBreachCli : public QCoreApplication {
  Q_OBJECT

public:
  BaseBreachCli(int &argc, char **argv);

public slots:
  /**
   * Parse the command line arguments for the CL parser and run the appropriate
   * actions.
   */
  void processArguments();

private:
  /**
   * Generates a dummy model config containing description strings rather than
   * parameter values. Used as part of the CLI to generate a model config
   * template.
   */
  ModelConfig_T generateParameterDummy() const;
  /** Result slot for exporting model runs after completion. */
  void outputResults(const modelDef::ModelType *modelType,
                     const model::FullModel_t result);

  QScopedPointer<BaseBreachCore> core_;
  QCommandLineParser parser_;
  uint num_results_ = 0;
};

} // namespace cli

#endif // CLI_APP_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "modelTypes.h"

#include <QtCore>

#include "model/modelDefinition.h"
#include "types.h"

namespace model::definition {

QList<ModelType> defineModelTypes() {
  QList<ModelType> models;
  // Limit type shorthands
  ParameterLimitType crit = ParameterLimitType::CRITICAL;
  // ParameterLimitType warn = ParameterLimitType::WARNING;
  ParameterLimitType info = ParameterLimitType::INFORMATION;

  /* ------------------------- Parameter Definitions ------------------------ */

  // Breach side angle
  Parameter breachAngle(
      "BREACH.angle", "Breach side angle",
      "Side angle of the progressive dam breach cross section", "[deg]",
      std::nan(""));
  breachAngle.addLimit(crit, "Side angle must lie between 0 and 90 degrees",
                       0.0, 90, true, false);

  // Erosion scaling coefficient
  Parameter scalingCoeff("EROSION.scaling_coefficient", "Scaling coefficient",
                         "", "-", 0.0025);
  scalingCoeff.addLimit(crit, "Scaling coefficient limits exceeded", 0.0,
                        std::nan(""), true);

  // Exponent of flow velocity
  Parameter exponentV("EROSION.exponent_v", "Exponent of flow velocity", "",
                      "[-]", std::nan(""));

  // Exponent of hydraulic radius
  Parameter exponentRhy("EROSION.exponent_rhy", "Exponent of hydraulic radius",
                        "", "[-]", std::nan(""));

  /* --------------------------------- Awel --------------------------------- */

  ModelType awel("bbAwel", "Awel");
  models.append(awel);

  /* ------------------------------- Macchione ------------------------------ */

  ModelType macchione("bbMacchione", "Macchione");
  macchione.addParameter(breachAngle);
  macchione.setDefault("BREACH.angle", 78.69);
  Parameter charVelocity("MACCHIONE.char_velocity", "Characteristic velocity",
                         "Characteristic velocity of the dam breach", "[m/s]",
                         0.07);
  charVelocity.addLimit(info,
                        "Recommended values for the characteristic"
                        "velocity are between 0.05 and 0.10 m/s",
                        0.05, 0.10);
  macchione.addParameter(charVelocity);
  models.append(macchione);

  /* --------------------------------- Peter -------------------------------- */

  ModelType peter("bbPeter", "Peter");
  peter.addParameter(breachAngle);
  peter.setDefault("BREACH.angle", 67.5);
  peter.addAuxiliaryLimit(
      info, "BREACH.angle", "Breach side angle",
      "Breach side angle exceeds calibration limits (60 to 80°)", 60, 80);
  peter.addParameter(scalingCoeff);
  peter.setDefault("EROSION.scaling_coefficient", 0.00025);
  peter.addParameter(exponentV);
  peter.setDefault("EROSION.exponent_v", 4.15);
  peter.addParameter(exponentRhy);
  peter.setDefault("EROSION.exponent_rhy", -0.65);
  models.append(peter);

  /* ------------------------------- PeterCal ------------------------------- */

  ModelType peterCal("bbPeterCal", "PeterCal");
  peterCal.addParameter(breachAngle);
  peterCal.setDefault("BREACH.angle", 67.5);
  peter.addAuxiliaryLimit(
      info, "BREACH.angle", "Breach side angle",
      "Breach side angle exceeds calibration limits (60 to 80°)", 60, 80);
  peterCal.addParameter(scalingCoeff);
  peterCal.setDefault("EROSION.scaling_coefficient", 0.00025);
  peterCal.addParameter(exponentV);
  peterCal.setDefault("EROSION.exponent_v", 4.15);
  peterCal.addParameter(exponentRhy);
  peterCal.setDefault("EROSION.exponent_rhy", -0.65);
  models.append(peterCal);

  return models;
}

QList<Parameter> defineScenarioParameters() {
  QList<Parameter> params;
  double nan = std::nan("");

  /* -------------------------------- Project ------------------------------- */

  params.append(Parameter("PROJECT.title", "Project title",
                          "Display name of the project.", "", nan));
  params.append(Parameter("PROJECT.author", "Author",
                          "Project author information.", "", nan));
  params.append(Parameter("PROJECT.date", "Date",
                          "Publication date of the project.", "", nan));

  /* ---------------------------------- Dam --------------------------------- */

  params.append(Parameter("DAM.height", "Height",
                          "Elevation of the top edge of the dam.", "m", nan));
  params.append(Parameter(
      "DAM.bottom_level", "Bottom level",
      "Elevation of the bottom edge of the dam. The effective dam height is "
      "the difference between the \"Dam height\" parameter and this value.",
      "m", 0.0));
  Parameter crestWidth("DAM.crest_width", "Crest width",
                       "Width of the dam crest.", "m", nan);
  crestWidth.addLimit(ParameterLimitType::CRITICAL,
                      "Crest width must be greater than zero", 0.0,
                      std::nan(""), true);
  params.append(crestWidth);
  Parameter slope("DAM.embankment_slope", "Embankment slope",
                  "Rise-over-run slope of the dam.", "-", nan);
  slope.addLimit(ParameterLimitType::CRITICAL,
                 "Embankment slope must be greater than zero", 0.0,
                 std::nan(""), true);
  params.append(slope);

  /* ------------------------------- Reservoir ------------------------------ */

  Parameter volume("RESERVOIR.volume", "Reservoir capacity",
                   "Total volume of the attached reservoir.", "m³", nan);
  volume.addLimit(ParameterLimitType::CRITICAL,
                  "Reservoir capacity must be greater than zero", 0.0,
                  std::nan(""), true);
  params.append(volume);
  Parameter shape("RESERVOIR.shape_exponent", "Basin shape",
                  "Exponent linking the remaining reservoir volume to "
                  "the current reservoir level.",
                  "-", 1.5);
  shape.addLimit(ParameterLimitType::CRITICAL,
                 "Basin shape exponent must be greater than zero", 0.0,
                 std::nan(""), true);
  params.append(shape);
  Parameter inflow("RESERVOIR.inflow", "Inflow rate",
                   "Constant-rate inflow rate into the reservoir.", "m³/s",
                   0.0);
  inflow.addLimit(ParameterLimitType::WARNING, "Inflow rate is negative", 0.0);
  params.append(inflow);

  /* --------------------------- Initial conditions ------------------------- */

  params.append(Parameter("BREACH.initial_level", "Breach level",
                          "Level of the initial breach.", "m", nan));
  params.append(Parameter("RESERVOIR.initial_level", "Reservoir level",
                          "Initial level of the reservoir.", "m", nan));

  /* ---------------------------- Simulation -------------------------------- */

  Parameter cutoffTime("SIMULATION.cutoff_time", "Simulation time",
                       "Simulated time span for the simulation.", "s", 10000);
  cutoffTime.addLimit(ParameterLimitType::CRITICAL,
                      "Erosion limit must be greater than zero", 0.0);
  params.append(cutoffTime);

  Parameter relativePrecision("SIMULATION.precision_at_qmax",
                              "Relative precision",
                              "Relative precision of the simulation at the "
                              "time of peak discharge",
                              "", 0.01);
  relativePrecision.addLimit(ParameterLimitType::CRITICAL,
                             "Erosion limit must be greater than zero", 0.0);
  params.append(relativePrecision);

  Parameter erosionLimit("SIMULATION.erosion_limit", "Maximum breach width",
                         "Maximum top width of the breach. Erosion is disabled "
                         "once this value is reached.",
                         "m", nan);
  erosionLimit.addLimit(ParameterLimitType::CRITICAL,
                        "Erosion limit must be greater than zero", 0.0);
  params.append(erosionLimit);

  return params;
}

QList<ParameterLimitMessage>
validateScenario(const ModelConfig_T &config,
                 const QList<Parameter> scenarioParams) {
  QList<ParameterLimitMessage> messages;
  // Limit type shorthands
  ParameterLimitType crit = ParameterLimitType::CRITICAL;
  ParameterLimitType warn = ParameterLimitType::WARNING;
  // ParameterLimitType info = ParameterLimitType::INFORMATION;

  // Validate generic parameter tests
  for (auto param : scenarioParams) {
    String_T dottedName = param.getId();
    String_T block = dottedName.split(".")[0];
    String_T key = dottedName.split(".")[1];
    messages.append(param.validateInput(config[block][key].toDouble()));
  }

  // Calculate dam-relative elevations
  double bottomLevel = config["DAM"]["bottom_level"].toDouble();
  double relDamHeight = config["DAM"]["height"].toDouble() - bottomLevel;
  double relResLevel =
      config["RESERVOIR"]["initial_level"].toDouble() - bottomLevel;
  double relBreachLevel =
      config["BREACH"]["initial_level"].toDouble() - bottomLevel;

  // Validate relative elevations
  if (relResLevel < 0.0)
    messages.append(ParameterLimitMessage(
        crit, "Reservoir level may not exceed dam height"));
  if (relBreachLevel < 0.0)
    messages.append(ParameterLimitMessage(
        crit, "Initial breach depth must be greater than zero"));
  if ((1 - relBreachLevel / relDamHeight) > 0.5)
    messages.append(ParameterLimitMessage(
        warn, "Initial breach exceeds 50% of dam height"));

  // Reservoir volume
  double volume = config["RESERVOIR"]["volume"].toDouble();
  if (volume > 50000)
    messages.append(ParameterLimitMessage(
        warn, "BASEbreach has not been validated for use with "
              "reservoir volumes greater than 50'000 m³"));

  return messages;
}

} // namespace model::definition

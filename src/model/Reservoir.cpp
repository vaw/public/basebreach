// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Reservoir.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#include "model/Reservoir.h"

#include <math.h>

#include <algorithm>

#include "ExceptionHandling.h"
#include "model/LookupTable.h"

namespace model {

// init with max volume (not dependent of the initial water level)
Reservoir::Reservoir() : inflow_hydrograph_(0.0) {
  initialLevel_ = 0.0;
  // will be set through defintiion of the retention curve
  shapeCoeff_ = 0.0;
  beta_ = 0.0;
  maxVolume_ = 0.0;
  volumeReleased_ = 0.0;
  // will be set through first update call
  level_ = 0.0;
  volume_ = 0.0;
  waterSurface_ = 0.0;
}

Reservoir::Reservoir(double initialLevel) : Reservoir() {
  initialLevel_ = initialLevel;
}

Reservoir::Reservoir(double initialLevel, double inflow_rate)
    : Reservoir(initialLevel) {
  inflow_hydrograph_ = LookupTable(inflow_rate);
}

Reservoir::Reservoir(double initialLevel, const String_T &inflow_hydrograph)
    : Reservoir(initialLevel) {
  inflow_hydrograph_ = LookupTable(inflow_hydrograph);
}

Reservoir::~Reservoir() {
  // Auto-generated destructor stub
}

void Reservoir::setRetentionPowerLaw(double alpha, double beta) {
  shapeCoeff_ = alpha;
  beta_ = beta;
  maxVolume_ = beta * pow(dam_->getHeight(), alpha);
  volumeReleased_ =
      beta * (pow(initialLevel_, alpha) - pow(dam_->getBottomLevel(), alpha));
}

void Reservoir::update(double newLevel) {
  if (level_ > 0.0 && newLevel > dam_->getHeight()) {
    BB_EXCEPTION("Reservoir level exceeds dam height!");
  }
  if (newLevel >= 0.0) {
    level_ = newLevel;
    waterSurface_ = beta_ * shapeCoeff_ * pow(newLevel, shapeCoeff_ - 1.0);
    volume_ = beta_ * pow(newLevel, shapeCoeff_);
  } else {
    level_ = 0.0;
    waterSurface_ = 0.0;
    volume_ = 0;
  }
}

// init with maximum volume
ReservoirMaximumVolume::ReservoirMaximumVolume(double Vmax, double initialLevel,
                                               double shapeCoefficient,
                                               double inflow_rate,
                                               api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_rate) {
  dam_ = theDam;
  // calculate surface coefficient
  double beta = Vmax / pow(theDam->getHeight(), shapeCoefficient);
  this->setRetentionPowerLaw(shapeCoefficient, beta);
}

ReservoirMaximumVolume::ReservoirMaximumVolume(
    double Vmax, double initialLevel, double shapeCoefficient,
    const String_T &inflow_hydrograph, api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_hydrograph) {
  dam_ = theDam;
  // calculate surface coefficient
  double beta = Vmax / pow(theDam->getHeight(), shapeCoefficient);
  this->setRetentionPowerLaw(shapeCoefficient, beta);
}

ReservoirMaximumVolume::~ReservoirMaximumVolume() {
  // Auto-generated destructor stub
}

// init with drop in water level and corresponding water release
ReservoirReleasedVolume::ReservoirReleasedVolume(double Vw, double initialLevel,
                                                 double shapeCoefficient,
                                                 double inflow_rate,
                                                 api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_rate) {
  dam_ = theDam;
  // calculate surface coefficient
  double beta = Vw / (pow(initialLevel, shapeCoefficient) -
                      pow(theDam->getBottomLevel(), shapeCoefficient));
  this->setRetentionPowerLaw(shapeCoefficient, beta);
}

ReservoirReleasedVolume::ReservoirReleasedVolume(
    double Vw, double initialLevel, double shapeCoefficient,
    const String_T &inflow_hydrograph, api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_hydrograph) {
  dam_ = theDam;
  // calculate surface coefficient
  double beta = Vw / (pow(initialLevel, shapeCoefficient) -
                      pow(theDam->getBottomLevel(), shapeCoefficient));
  this->setRetentionPowerLaw(shapeCoefficient, beta);
}

ReservoirReleasedVolume::~ReservoirReleasedVolume() {
  // Auto-generated destructor stub
}

ReservoirAwel::ReservoirAwel(double maxVolume, double initialLevel,
                             double inflow_rate, api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_rate) {
  dam_ = theDam;
  // calculate the calibrated shape coefficient
  double alpha = this->calcReservoirShape(
      maxVolume, theDam->getHeight() - theDam->getBottomLevel());
  double beta = maxVolume / pow(theDam->getHeight(), alpha);
  this->setRetentionPowerLaw(alpha, beta);
}

ReservoirAwel::ReservoirAwel(double maxVolume, double initialLevel,
                             const String_T &inflow_hydrograph,
                             api::IDam_t theDam)
    : Reservoir(initialLevel, inflow_hydrograph) {
  dam_ = theDam;
  // calculate the calibrated shape coefficient
  double alpha = this->calcReservoirShape(
      maxVolume, theDam->getHeight() - theDam->getBottomLevel());
  double beta = maxVolume / pow(theDam->getHeight(), alpha);
  this->setRetentionPowerLaw(alpha, beta);
}

ReservoirAwel::~ReservoirAwel() {
  // Auto-generated destructor stub
}

double ReservoirAwel::calcReservoirShape(double Vr, double Hd) {
  // alpha is dependent on the reservoir volume and the dam height
  double alpha0 = 0.0;
  // coefficients for power law function: alpha0 = a*W^b + 1
  doubleVec h = {0.5, 1.0, 2.0, 4.0, 6.0, 8.0, 10.0};
  doubleVec a = {0.688198315116697,  2.315111625915300,  9.559049555243899,
                 97.369259688835996, 74.133739049747390, 139.4765978505993,
                 215.6440910639549};
  doubleVec b = {-0.501340327511106, -0.521745775870359, -0.557076561338237,
                 -0.680348540821250, -0.561160058371108, -0.572409303757560,
                 -0.577224271811670};
  // interpolate between specific values for dam heights
  int i2 = 0;
  for (uint ii = 0; ii < h.size(); ii++) {
    if (Hd > h.at(ii))
      i2 = ii + 1;
  }
  int i1 = std::max(0, i2 - 1);
  i2 = std::min(static_cast<int>(h.size()), i2);
  double alpha1 = a.at(i1) * pow(Vr, b.at(i1)) + 1.0;
  double alpha2 = a.at(i2) * pow(Vr, b.at(i2)) + 1.0;
  if (i1 == i2) {
    alpha0 = alpha1;
  } else {
    alpha0 =
        alpha1 + (alpha2 - alpha1) * (Hd - h.at(i1)) / (h.at(i2) - h.at(i1));
  }
  // upper value of 2.0
  return std::min(alpha0, 2.0);
}

} // namespace model

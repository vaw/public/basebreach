// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef MODEL_MODELDEFINITION_H_
#define MODEL_MODELDEFINITION_H_

#include <QtCore>

#include <vector>

#include "types.h"

namespace model::definition {

/**
 * Parameter limit types specify the consequences of a limit being exceeded.
 *
 * CRITICAL limits will prevent the model from running, WARNING limits will be
 * logged and notify the user that warnings occurred, and INFORMATION limits
 * will be logged with no further user-facing prompts.
 *
 * NONE is a special value that denotes that a parameter limit did not provoke a
 * message and is treated as a NULL value.
 */
enum class ParameterLimitType { CRITICAL, WARNING, INFORMATION, NONE };

/**
 * Helper object for storing parameter limit messages.
 */
struct ParameterLimitMessage {
  ParameterLimitMessage() : paramId_("none"), paramName_("Unnamed Parameter") {}
  ParameterLimitMessage(ParameterLimitType limitType, String_T message,
                        String_T id = "", String_T name = "")
      : limitType_(limitType), message_(message), paramId_(id),
        paramName_(name) {}

  /** Severity of the breached limit. */
  ParameterLimitType limitType_ = ParameterLimitType::NONE;
  /** Internal ID of the breached parameter. */
  String_T paramId_;
  /** Human-readable name of the breached parameter. */
  String_T paramName_;
  /**
   * Human-readable name of the model defining this limit.
   * For scenario parameters, this is an empty string.
   */
  String_T modelName_ = "";
  /** Limit message. */
  String_T message_ = "No information given";
};

/**
 * Single limit definition for a scenario or model parameter.
 *
 * If the value is not within the specified range, a message will be reported
 * when invoking the "validate()" method.
 */
struct ParameterLimit {
  /** Default constructor. */
  ParameterLimit() {}
  /** Constructor for scenario parameters. */
  ParameterLimit(ParameterLimitType limitType, String_T const &paramId,
                 String_T const &paramName, String_T const &message,
                 double min = std::nan(""), double max = std::nan(""),
                 bool minExclusive = false, bool maxExclusive = true)
      : limitType_(limitType), paramId_(paramId), paramName_(paramName),
        message_(message), min_(min), max_(max), minExclusive_(minExclusive),
        maxExclusive_(maxExclusive) {}

  /** Constructor for model-specific parameters. */
  ParameterLimit(ParameterLimitType limitType, String_T const &paramId,
                 String_T const &paramName, String_T const &modelName,
                 String_T const &message, double min = std::nan(""),
                 double max = std::nan(""), bool minExclusive = false,
                 bool maxExclusive = true)
      : ParameterLimit(limitType, paramId, paramName, message, min, max,
                       minExclusive, maxExclusive) {
    modelName_ = modelName;
  }

  /**
   * Validates the given value against the limit.
   * @param value The value to validate.
   * @return A ParameterLimitMessage object describing the result of the
   * validation. If the value is within the limit, the message will be of
   * ParameterLimitType::None.
   */
  ParameterLimitMessage validate(double value) const;

  ParameterLimitType limitType_ = ParameterLimitType::NONE;
  String_T paramId_ = "none";
  String_T paramName_ = "Unknown parameter";
  String_T modelName_ = ""; // Empty string means scenario parameter
  String_T message_ = "No information given";
  double min_ = std::nan("");
  double max_ = std::nan("");
  bool minExclusive_ = false;
  bool maxExclusive_ = false;
};

/**
 * A scenario or model parameter.
 *
 * Parameters can be registered to a ModelType instance to specify the types
 * of inputs available for that model.
 */
class Parameter {
public:
  /** Default constructor. */
  Parameter()
      : id_("none"), name_("Unnamed Parameter"), default_(std::nan("")) {}
  /** Constructor for scenario parameters. */
  Parameter(String_T const &id, String_T const &name, String_T const &desc = "",
            String_T const &unit = "-", double initialVal = std::nan(""));
  /** Constructor for model-specific parameters. */
  Parameter(String_T const &id, String_T const &name, String_T const &modelName,
            String_T const &desc = "", String_T const &unit = "-",
            double initialVal = std::nan(""));

  /**
   * Define a new parameter limit for this parameter.
   * @param limitType Thetype of limit. See the ParameterLimitType enum for
   * details.
   * @param message Error message to display when the limits are exceeded.
   * This should be a generic phrase meeting all types of invalid inputs, e.g.
   * "<value> must be greater than zero".
   * @param min Lower boundary of the limit.
   * @param max Upper boundary of the limit.
   * @param exclusiveMin Whether the lower limit includes the boundary value
   * @param exclusiveMax Whether the upper limit includes the bnoundary value
   */
  void addLimit(ParameterLimitType limitType, String_T const &message = "",
                double min = std::nan(""), double max = std::nan(""),
                bool exclusiveMin = false, bool exclusiveMax = false);
  /** Add a copy of an existing parameter limit to the parameter. */
  void addLimit(ParameterLimit limit);
  /** Get the ID of the parameter. */
  String_T getId() const { return id_; }
  /** Get the display name of the parameter. */
  String_T getName() const { return name_; }
  /**
   * Return the model name for this parameter.
   * This will be an empty string for scenario parameters.
   */
  String_T getModelName() const { return modelName_; }
  /** Get the description string of the parameter. */
  String_T getDesc() const { return desc_; }
  /** Return the unit string of the parameter. */
  String_T getUnit() const { return unit_; }
  /** Return the default value of the parameter. */
  double getDefault() const { return default_; }
  /**
   * Validate the given input according to all set limits for this
   * parameter.
   *
   * This filters for NONE type parameter limit messages and only
   * returns "real" error messages.
   * @param value The value to check against the parameter limits
   * @return A list of failed ParameterLimitMessage structs, may be
   * empty
   */
  QList<ParameterLimitMessage> validateInput(double Value) const;

protected:
  String_T id_;
  String_T name_;
  String_T modelName_ = "";
  String_T desc_;
  String_T unit_;
  double default_;
  std::vector<ParameterLimit> limits_;
};

/**
 * Main class storing model type definitions.
 *
 * This class stores all model-specific information and is responsible for
 * dispatching generic app calls to the respective model implementation.
 */
class ModelType {
public:
  ModelType() : id_("none"), name_("Unnamed Parameter") {}
  ModelType(String_T const &id, String_T const &name) : id_(id), name_(name) {}

  /** Add an existing parameter to the model type. */
  void addParameter(Parameter const &param);
  /**
   * Create and add a new model parameter.
   * @param id Unique identifier of the parameter.
   * @param name Display name of the model parameter.
   * @param desc A brief description of the parameter.
   * @param unit The unit string of the parameter (e.g. "deg", "m3")
   * @param initialVal Default value of the parameter, if any.
   * @return The created parameter instance.
   */
  Parameter addParameter(String_T const &id, String_T const &name,
                         String_T const &desc = "", String_T const &unit = "",
                         double initialVal = std::nan(""));
  /**
   * Return the parameter matching the given id.
   *
   * This does not check if the given ID is valid, use the
   * getModelParams() method to get the list of valid IDs.
   */
  Parameter getParameter(String_T const &id) const;
  /**
   * Add a generic limit for arbitrary parameters.
   * @param limitType The type of limit. See the ParameterLimitType enum for
   * details.
   * @param paramId Unique identifier of the parameter to validate.
   * @param paramName Short name of the parameter.
   * @param message Error message to display when the limits are exceeded.
   * This should be a generic phrase meeting all types of invalid inputs, e.g.
   * "<value> must be greater than zero".
   * @param min Lower boundary of the limit.
   * @param max Upper boundary of the limit.
   * @param exclusiveMin Whether the lower limit includes the boundary value
   * @param exclusiveMax Whether the upper limit includes the bnoundary value
   */
  ParameterLimit
  addAuxiliaryLimit(ParameterLimitType limitType, String_T const &paramId,
                    String_T const &paramName, String_T const &message = "",
                    double min = std::nan(""), double max = std::nan(""),
                    bool exclusiveMin = false, bool exclusiveMax = false);
  void setDefault(const String_T &paramId, double value);
  double getDefault(const String_T &paramId) const;
  double getDefault(const Parameter &param) const;
  /** Return a list of all registered parameter IDs. */
  QList<String_T> getModelParams() const;
  /** Return the unique ID of the model type. */
  String_T getId() const { return id_; }
  /** Return the display name of the model type. */
  String_T getName() const { return name_; }
  /** Validate the given configuration for this model. */
  QList<ParameterLimitMessage>
  validateConfig(QMap<QString, QVariantMap> const &config) const;

private:
  String_T id_;
  String_T name_;
  QList<Parameter> params_;
  QMap<String_T, double> customDefaults_;
  QMap<String_T, QList<ParameterLimit>> extraLimits_;
};

/**
 * Return the given nested value from the map.
 *
 * A "dotted" key is one where the key names are separated by dots, e.g.
 * "BLOCK.key" will access map["BLOCK"]["key"].
 * @param map The map to access.
 * @param key The nested key to access, using dot notation.
 * @return The value at the given key, or an empty QVariant if the key does
 * not exist.
 */
QVariant getDottedKey(QMap<QString, QVariantMap> const &map,
                      String_T const &key);

/**
 * Convert the given ModelConfig into a JSON document and write it to disk.
 * @param config The configuration to save.
 * @param filename The file to write to.
 */
void saveModelConfig(const ModelConfig_T &modelConfig, const QString &fileName);

/**
 * Create a model configuration from the given file.
 *
 * This performs no checks to to see if the given model configuration is valid
 * or complete.
 * @param filename The file to read from.
 */
ModelConfig_T loadModelConfig(const QString &fileName);

/**
 * Helper function for merging two model config maps.
 *
 * This works similar to "QMap::insert()", but also merges the keys of nested
 * maps. The `source` map's keys will overwrite the `target`'s. The target map
 * is mutated by this function.
 * @param target The target map that will be modified
 * @param source Auxiliary map to merge; will not be modified
 */
void mergeModelConfigs(ModelConfig_T &target, const ModelConfig_T &source);

} // namespace model::definition

#endif // MODEL_MODELDEFINITION_H_

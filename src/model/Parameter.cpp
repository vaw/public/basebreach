// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include <model/Parameter.h>
#include <cmath>

namespace model {

void Parameter::setValue(double x) {
    value_ = x;
    distribution_.reset();
    emit Changed();
}

void Parameter::setDistribution(api::IDistribution_t dist) {
    distribution_ = dist;
    value_ = NAN;
    emit Changed();
}

bool Parameter::isDistributionDefined() {
    return (distribution_ != NULL);
}

double Parameter::getValue() {
    return value_;
}

api::IDistribution_t Parameter::getDistribution() {
    return distribution_;
}

doubleVec Parameter::getRealizations(uint N) {
    doubleVec values;
    if (distribution_ == NULL) {
        for (uint ii=0; ii<N; ii++) {
            values.push_back(value_);
        }
    } else {
        values = distribution_->drawNumbers(N);
    }
    return values;
}

}

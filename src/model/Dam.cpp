// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Dam.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 */

#include "model/Dam.h"

namespace model {

Dam::Dam(double height, double crestWidth, double embankmentSlope) {
  Hd_ = height;
  wc_ = crestWidth;
  s_ = embankmentSlope;
  Yb_ = 0.0;
}

Dam::Dam(double height, double crestWidth, double embankmentSlope,
         double fixedBottom) {
  Hd_ = height;
  wc_ = crestWidth;
  s_ = embankmentSlope;
  Yb_ = fixedBottom;
}

Dam::~Dam() {
  // Auto-generated destructor stub
}

} // namespace model

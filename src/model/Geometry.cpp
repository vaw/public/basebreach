// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Geometry.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */

#define _USE_MATH_DEFINES

#include "model/Geometry.h"

#include <cfloat>
#include <cmath>

#include <boost/math/special_functions/fpclassify.hpp>

#include "ExceptionHandling.h"

namespace model {

GeometryPolynom::GeometryPolynom(double initLevel, double beta,
                                 api::IDam_t theDam)
    : relPrecision_(1.0e-6), dam_(theDam), beta_(beta) {
  initialLevel_ = initLevel;
  // init width and shape
  tanBeta_ = tan(beta / 180.0 * M_PI);
  intFac1_ = tanBeta_ * tanBeta_;
  if (beta >= 90.0) {
    dWdh_ = 24.0 / 25.0 * std::sqrt(0.6);
  } else if (beta <= 45.0) {
    dWdh_ = 2.0;
  } else {
    double lambda = findLambdaViaBeta(beta);
    dWdh_ = 0.64 * std::sqrt(0.4) * std::pow(lambda, 1.0 - lambda) *
            std::pow(0.5 * (2.0 * lambda + 1.0), 0.5 * (2.0 * lambda + 1.0));
  }
  // initialize the other variables
  Y_ = 0.0;
  level_ = 0.0;
  width_ = 0.0;
  sigma_ = 0.0;
  dVbdY_ = 0.0;
  depth_ = 0.0;
  pWetted_ = 0.0;
  isPwCalculated_ = false;
  rectangle_ = false;
  triangle_ = false;
}

GeometryPolynom::~GeometryPolynom() {
  // Auto-generated destructor stub
}

void GeometryPolynom::init() {
  rectangle_ = false;
  triangle_ = false;
  if (beta_ >= 90.0) {
    setRectangle();
  } else if (beta_ <= 45.0) {
    setTriangle();
  } else {
    this->checkShape(dWdh_);
  }
  this->update(initialLevel_);
}

void GeometryPolynom::update(double newLevel) {
  BB_ASSERT(newLevel <= dam_->getHeight(),
            "new breach level cannot be above the dam height!")
  BB_ASSERT(Y_ <= initialLevel_,
            "ERROR: new breach level is above initial level!");
  Y_ = newLevel;
  isPwCalculated_ = false;
  // resetting breach depth and width
  this->updateLevelAndWidth();
  // the breach shape changes therefore
  this->updateShape();
  // debug output
  // this->debug();
}

void GeometryPolynom::updateLevelAndWidth() {
  level_ = fmax(Y_, dam_->getBottomLevel());
  depth_ = dam_->getHeight() - level_;
  width_ = dWdh_ * depth_ + (level_ - Y_);
}

void GeometryPolynom::updateShape() {
  // try to calculate the shape coefficients according to s(x) =
  // hb*x^(1/(lambda-1))
  this->checkShape(width_ / depth_);
  // recalculate the volume change rate
  // in case we integrate width dY from Y>0 to Y<0,
  // then dVbdY is calculated only from Y>0,
  // what introduces a (small?) error ...
  if (Y_ > dam_->getBottomLevel()) {
    // dVb/dY = -dV/dh and dW/dY = -2/(tan(beta)*(lambda-1))
    dVbdY_ = (-2.0) * width_ *
             (dam_->getCrestWidth() +
              3.0 * dam_->getEmbankmentSlope() * depth_ / (lambda_ + 1));
  } else {
    // dVb/dY = -dV/dW and dW/dY = -1
    double lambda2 = lambda_ * lambda_;
    dVbdY_ = (-1.0) * depth_ / lambda2 *
             (dam_->getCrestWidth() * (2.0 * lambda_ - 1.0) +
              2.0 * dam_->getEmbankmentSlope() * depth_ *
                  (3.0 * lambda2 - 1.0) / std::pow(lambda_ + 1.0, 2));
  }
  // updating for hydraulic calculations
  sigma_ = (2.0 * lambda_) / (2.0 * lambda_ + 1.0);
}

void GeometryPolynom::checkShape(double dWdh) {
  // check for change in shape only for non-rectangular shape
  if (!rectangle_) {
    lambda_ = 2.0 / (dWdh * tanBeta_) + 1.0;
    if (fabs(lambda_ - 1.0) < 1e-3) {
      setRectangle();
    } else if (fabs(lambda_ - 2.0) < 1e-3) {
      setTriangle();
    } else {
      rectangle_ = false;
      triangle_ = false;
      intFac2_ = dWdh * tanBeta_ - 2.0;
    }
  }
}

void GeometryPolynom::setRectangle() {
  rectangle_ = true;
  triangle_ = false;
  lambda_ = 1.0;
}

void GeometryPolynom::setTriangle() {
  rectangle_ = false;
  triangle_ = true;
  lambda_ = 2.0;
}

double GeometryPolynom::calcFlowArea(double h) {
  if (h <= 0.0) {
    return 0.0;
  } else if (rectangle_) {
    return width_ * h;
  } else if (triangle_) {
    return 0.5 * width_ / depth_ * std::pow(h, 2);
  } else {
    return width_ * std::pow(depth_, 1.0 - lambda_) / lambda_ *
           std::pow(h, lambda_);
  }
}

double GeometryPolynom::calcWettedPerimeter(double h) {
  if (h <= 0.0) {
    pWetted_ = 0.0;
  } else if (rectangle_) {
    pWetted_ = width_ + 2.0 * h;
  } else if (triangle_) {
    pWetted_ = 2.0 * h * std::sqrt(2.0);
  } else {
    double xh = this->getLocationAtLevel(h);
    pWetted_ = 2.0 * this->doIntegration(0.0, xh);
    BB_ASSERT(!boost::math::isnan(pWetted_),
              "integration along breach side walls failed!");
  }
  isPwCalculated_ = true;
  return pWetted_;
}

double GeometryPolynom::calcErodiblePerimeter(double h) {
  double pE;
  if (h <= 0.0) {
    pE = 0.0;
  } else if (!this->hasReachedBottom()) {
    if (isPwCalculated_) {
      pE = pWetted_;
    } else {
      pE = this->calcWettedPerimeter(h);
    }
  } else if (rectangle_) {
    pE = 2.0 * h;
  } else if (triangle_) {
    pE = 2.0 * h * std::sqrt(2.0);
  } else {
    double x2 = this->getLocationAtLevel(h);
    double x1 = (2.0 - lambda_) / lambda_ * x2;
    pE = 2.0 * this->doIntegration(x1, x2);
    if (boost::math::isnan(pE)) {
      BB_EXCEPTION("integration along breach side walls failed!");
    }
  }
  return pE;
}

double GeometryPolynom::functionToBeIntegrated(double x) {
  BB_ASSERT(!rectangle_, "integration of rectangular shape handled differently")
  return sqrt(1.0 + intFac1_ * std::pow(2.0 * x / width_, intFac2_));
}

doubleVecVec GeometryPolynom::getErosionZone(double h, uint N) {
  doubleVecVec shape;
  double x1;
  double x2 = this->getLocationAtLevel(h);
  if (rectangle_) {
    doubleVec xs, ys;
    if (Y_ > dam_->getBottomLevel()) {
      xs.push_back(0.0);
      ys.push_back(level_);
    }
    xs.push_back(0.5 * width_ - FLT_EPSILON);
    xs.push_back(0.5 * width_ + FLT_EPSILON);
    ys.push_back(level_);
    ys.push_back(level_ + h);
    shape.push_back(xs);
    shape.push_back(ys);
  } else if (triangle_) {
    doubleVec xs = {0.0, 0.5 * width_ / depth_ * h};
    doubleVec ys = {level_, level_ + h};
    shape.push_back(xs);
    shape.push_back(ys);
  } else {
    x1 = (Y_ > dam_->getBottomLevel()) ? 0.0 : (2.0 - lambda_) / lambda_ * x2;
    shape = getBreachShape(x1, x2, N);
  }
  return shape;
}

doubleVecVec GeometryPolynom::getShape(uint N) {
  doubleVecVec shape;
  if (rectangle_) {
    doubleVec xs = {0.0, 0.5 * width_ - FLT_EPSILON,
                    0.5 * width_ + FLT_EPSILON};
    doubleVec ys = {level_, level_, level_ + depth_};
    shape.push_back(xs);
    shape.push_back(ys);
  } else if (triangle_) {
    doubleVec xs = {0.0, 0.5 * width_};
    doubleVec ys = {level_, level_ + depth_};
    shape.push_back(xs);
    shape.push_back(ys);
  } else {
    shape = getBreachShape(0.0, 0.5 * width_, N);
  }
  return shape;
}

doubleVecVec GeometryPolynom::getBreachShape(double x1, double x2, uint N) {
  BB_ASSERT(!rectangle_, "this call is not valid for rectangular shape")
  BB_ASSERT(!triangle_, "this call is not valid for triangular shape")
  // only for x1 >= 0.0 because of symmetrie
  x1 = fmax(0.0, x1);
  doubleVecVec shape;
  doubleVec xi, yi;
  double dx = (x2 - x1) / static_cast<double>(N);
  for (uint ii = 0; ii < N + 1; ii++) {
    xi.push_back(x1 + ii * dx); // sampling points
    yi.push_back(this->getLevelAtLocation(xi.at(ii)) +
                 level_); // breach shape points
  }
  shape.push_back(xi);
  shape.push_back(yi);
  return shape;
}

double GeometryPolynom::getLocationAtLevel(double level) {
  if (rectangle_) {
    return 0.5 * width_;
  } else {
    return 0.5 * width_ * std::pow(level / depth_, lambda_ - 1.0);
  }
}

double GeometryPolynom::getLevelAtLocation(double x) {
  if (fabs(x) >= 0.5 * width_) {
    return depth_; // this is a problem ...
  } else if (rectangle_) {
    return 0.0;
  } else {
    return depth_ * std::pow(2.0 * fabs(x) / width_, 1.0 / (lambda_ - 1.0));
  }
}

double GeometryPolynom::getAverageWidth() {
  // total breach area divided by breach depth
  return width_ / lambda_;
}

double GeometryPolynom::relativeDistanceToBottom() {
  return (level_ - dam_->getBottomLevel()) /
         (initialLevel_ - dam_->getBottomLevel());
}

double GeometryPolynom::findLambdaViaBeta(double beta) {
  if (beta == 90.0) {
    return 1.0;
  } else if (beta == 45.0) {
    return 2.0;
  } else {
    double lambdaTemp, betaTemp;
    double beta1(45.0), beta2(90.0);
    double lambda1(2.0), lambda2(1.0);
    while (true) {
      lambdaTemp =
          lambda1 + (lambda2 - lambda1) / (beta2 - beta1) * (beta - beta1);
      betaTemp = calcBetaFromLambda(lambdaTemp);
      if (fabs(betaTemp - beta) / beta < relPrecision_) {
        return lambdaTemp;
      } else if (betaTemp < beta) {
        beta1 = betaTemp;
        lambda1 = lambdaTemp;
      } else if (betaTemp > beta) {
        beta2 = betaTemp;
        lambda2 = lambdaTemp;
      }
    }
  }
}

double GeometryPolynom::calcBetaFromLambda(double lambda) {
  double lambdaDalpha = 0.5 * (2.0 * lambda + 1.0);
  return std::atan(3.125 * std::sqrt(2.5) * std::pow(lambda, lambda - 1.0) /
                   (lambda - 1.0) * std::pow(lambdaDalpha, -lambdaDalpha)) /
         M_PI * 180.0;
}

void GeometryPolynom::debug() {
  BB_MESSAGE("********** GEOMETRY **********");
  BB_MESSAGE("new level: " << Y_);
  BB_MESSAGE("depth: " << depth_);
  BB_MESSAGE("width: " << width_);
  BB_MESSAGE("level: " << level_);
  BB_MESSAGE("lambda: " << lambda_);
  BB_MESSAGE("sigma: " << sigma_);
  BB_MESSAGE("erosion rate: " << dVbdY_);
}

GeometryTriTra::GeometryTriTra(double initLevel, double beta,
                               api::IDam_t theDam) {
  initialLevel_ = initLevel;
  dam_ = theDam;
  // constant variables
  beta_ = (90.0 - beta) / 180.0 *
          M_PI; // according to Macchione (vertical -> beta=0, 1:1 -> beta=45)
  tanBeta_ = tan(beta_);
  cosBeta_ = cos(beta_);
  // initialize the other parameters
  Y_ = 0.0;
  level_ = 0.0;
  width_ = 0.0;
  bWidth_ = 0.0;
  dVbdY_ = 0.0;
  depth_ = 0.0;
}

GeometryTriTra::~GeometryTriTra() {
  // Auto-generated destructor stub
}

void GeometryTriTra::update(double newLevel) {
  if (depth_ != 0.0 && newLevel > dam_->getHeight()) {
    BB_EXCEPTION("new breach level cannot be above the dam height!");
  }
  Y_ = newLevel;
  // resetting breach depth and width
  this->updateLevelAndWidth();
  // the breach shape changes therefore
  this->updateShape();
  // debug output
  // this->debug();
}

double GeometryTriTra::calcFlowArea(double h) {
  if (h > 0.0) {
    return h * (bWidth_ + h * tanBeta_);
  } else {
    return 0.0;
  }
}

double GeometryTriTra::calcWettedPerimeter(double h) {
  if (h > 0.0) {
    return bWidth_ + 2.0 * h / cosBeta_;
  } else {
    return 0.0;
  }
}

void GeometryTriTra::updateLevelAndWidth() {
  if (Y_ > initialLevel_) {
    BB_EXCEPTION("ERROR: new breach level is above initial level!");
  } else if (Y_ >= 0.0) {
    // still vertical erosion (triangular shape)
    depth_ = dam_->getHeight() - Y_;
    bWidth_ = 0.0;
    // update breach level
    level_ = Y_;
  } else {
    // lateral widening
    depth_ = dam_->getHeight();
    bWidth_ = 2.0 * fabs(Y_ * tanBeta_);
    // update breach level
    level_ = 0.0;
  }
  width_ = bWidth_ + 2.0 * depth_ * tanBeta_;
  //BB_EXCEPTION("GeometryTriTra::updateLevelAndWidth. width_ = " << width_);  // MCH. This is correctly the full top width.
}

void GeometryTriTra::updateShape() {
  // recalculate the volume change rate
  dVbdY_ = (-2.0) * depth_ * tanBeta_ *
           (dam_->getCrestWidth() + dam_->getEmbankmentSlope() * depth_);
}

doubleVecVec GeometryTriTra::getShape(uint N) {
  doubleVecVec shape;
  doubleVec xi, yi;
  //BB_EXCEPTION("GeometryTriTra::getShape. width_ = " << width_);  // MCH. this width is correctly the full top width.
  // sampling points
  double dx = width_ / static_cast<double>(2 * N);
  for (uint ii = 0; ii < 2 * N + 1; ii++) {
    xi.push_back(-0.5 * width_ + ii * dx);
  }
  // BB_EXCEPTION("GeometryTriTra::getShape. xi[0] = " << xi[0] << ". xi[1] = " << xi[1] << ". xi[2] = " << xi[2] << ". xi[3] = " << xi[3] << ". xi[4] = " << xi[4]); // MCH: 
  shape.push_back(xi);
  // breach shape points
  for (uint ii = 0; ii < xi.size(); ii++) {
    double y;
    if (fabs(xi.at(ii)) > 0.5 * bWidth_) {
      //y = fabs(xi.at(ii)) / tanBeta_;
      y = ( fabs(xi.at(ii)) - bWidth_/2 )/ tanBeta_; //MCH
    } else {
      y = 0.0;
    }
    yi.push_back(y);
  }
  // BB_EXCEPTION("GeometryTriTra::getShape. yi[0] = " << yi[0] << ". yi[1] = " << yi[1] << ". yi[2] = " << yi[2] << ". yi[3] = " << yi[3] << ". yi[4] = " << yi[4]); // MCH: 
  if (Y_ >= 0.0) {
    // lift up the shape
    for (uint ii = 0; ii < yi.size(); ii++)
      yi.at(ii) += Y_;
  }
  shape.push_back(yi);
  return shape;
}

double GeometryTriTra::getLocationAtLevel(double level) {
  return 0.5 * bWidth_ + level * tanBeta_;
}

double GeometryTriTra::getAverageWidth() {
  // total breach area divided by breach depth
  return bWidth_ + depth_ * tanBeta_;
}

double GeometryTriTra::relativeDistanceToBottom() {
  return (level_ - dam_->getBottomLevel()) /
         (initialLevel_ - dam_->getBottomLevel());
}

void GeometryTriTra::debug() {
  BB_MESSAGE("********** GEOMETRY **********");
  BB_MESSAGE("new level: " << Y_);
  BB_MESSAGE("depth: " << depth_);
  BB_MESSAGE("bottom width: " << bWidth_);
  BB_MESSAGE("level: " << level_);
  BB_MESSAGE("erosion rate: " << dVbdY_);
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Hydraulics.h
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#ifndef HYDRAULICS_H_
#define HYDRAULICS_H_

#include <api/IGeometry.h>
#include <api/IHydraulics.h>
#include <api/IReservoir.h>
#include <types.h>

namespace model {

class Hydraulics : public api::IHydraulics {

public:
  Hydraulics(api::IReservoir_t theReservoir, api::IGeometry_t theGeometry);
  virtual ~Hydraulics() { ; }

  // interface methods: for explanations see IHydraulics.h
  void init();
  double getDischarge() { return discharge_; }
  double getCriticalWaterDepth() { return hc_; }
  double getFlowArea() { return area_; }
  double getFlowVelocity() { return vc_; }
  double getHydraulicRadius() { return rHydr_; }
  double getWettedPerimeter() { return rhoWet_; }
  double getErodiblePerimeter() { return rhoErod_; }
  bool isSteady() { return steady_; }
  void debug(); // print the main variables to console

protected:
  api::IReservoir_t reservoir_; // pointer to the reservoir object
  api::IGeometry_t geometry_;

protected:
  void setZero();
  double discharge_;
  double hc_;
  double area_;
  double vc_;
  double rHydr_;
  double rhoWet_;
  double rhoErod_;
  bool steady_; // describes
};

class HydraulicsPolynom : public Hydraulics {
public:
  HydraulicsPolynom(api::IReservoir_t theReservoir,
                    api::IGeometryPolynom_t theGeometry);
  virtual ~HydraulicsPolynom() { ; }
  /** this is the only function that we have to adjust */
  void update();

  // delegating getters to the basis class of all hydrualic classes
  double getDischarge() { return Hydraulics::getDischarge(); }
  double getCriticalWaterDepth() { return Hydraulics::getCriticalWaterDepth(); }
  double getFlowArea() { return Hydraulics::getFlowArea(); }
  double getFlowVelocity() { return Hydraulics::getFlowVelocity(); }
  double getHydraulicRadius() { return Hydraulics::getHydraulicRadius(); }
  double getWettedPerimeter() { return Hydraulics::getWettedPerimeter(); }
  double getErodiblePerimeter() { return Hydraulics::getErodiblePerimeter(); }
  bool isSteady() { return Hydraulics::isSteady(); }
  doubleVecVec getErosionZone(uint N);

private:
  api::IGeometryPolynom_t geometry_; // pointer to the geometry object
};

/** after Macchione 2008 */
class HydraulicsTriTra : public Hydraulics {
public:
  HydraulicsTriTra(api::IReservoir_t theReservoir,
                   api::IGeometryTriTra_t theGeometry);
  virtual ~HydraulicsTriTra() { ; }
  /** this is the only function that we have to adjust */
  void update();

  // delegating getters to the basis class of all hydrualic classes
  double getDischarge() { return Hydraulics::getDischarge(); }
  double getCriticalWaterDepth() { return Hydraulics::getCriticalWaterDepth(); }
  double getFlowArea() { return Hydraulics::getFlowArea(); }
  double getFlowVelocity() { return Hydraulics::getFlowVelocity(); }
  double getHydraulicRadius() { return Hydraulics::getHydraulicRadius(); }
  double getWettedPerimeter() { return Hydraulics::getWettedPerimeter(); }
  double getErodiblePerimeter() { return Hydraulics::getErodiblePerimeter(); }
  bool isSteady() { return Hydraulics::isSteady(); }
  doubleVecVec getErosionZone(uint N) {
    doubleVecVec shape;
    return shape;
  } // TODO

private:
  api::IGeometryTriTra_t geometry_;
};

} // namespace model

#endif // HYDRAULICS_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Hydraulics.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#include "model/Hydraulics.h"

#include <math.h>

#include <boost/math/special_functions/fpclassify.hpp>

#include "ExceptionHandling.h"

namespace model {

Hydraulics::Hydraulics(api::IReservoir_t theReservoir,
                       api::IGeometry_t theGeometry) {
  geometry_ = theGeometry;
  reservoir_ = theReservoir;
  this->setZero();
}

void Hydraulics::setZero() {
  // initialize all variables
  discharge_ = 0.0;
  hc_ = 0.0;
  area_ = 0.0;
  vc_ = 0.0;
  rHydr_ = 0.0;
  rhoWet_ = 0.0;
  rhoErod_ = 0.0;
  steady_ = false;
}

void Hydraulics::init() {
  // first initialize the guys we are dependent on ...
  geometry_->init();
  reservoir_->init();
  // then we can calculate some initial values
  this->update();
  // this->debug();
}

void Hydraulics::debug() {
  BB_MESSAGE("********** HYDRAULICS **********");
  BB_MESSAGE("water depth: " << hc_);
  BB_MESSAGE("velocity: " << vc_);
  BB_MESSAGE("area: " << area_);
  BB_MESSAGE("discharge: " << discharge_);
  BB_MESSAGE("wetted perimeter: " << rhoWet_);
  BB_MESSAGE("hydraulic radius: " << rHydr_);
  BB_MESSAGE("erodible perimeter: " << rhoErod_);
}

HydraulicsPolynom::HydraulicsPolynom(api::IReservoir_t theReservoir,
                                     api::IGeometryPolynom_t theGeometry)
    : Hydraulics(theReservoir, theGeometry) {
  geometry_ = theGeometry;
}

void HydraulicsPolynom::update() {
  double sigma = geometry_->getSigma();
  double lambda = geometry_->getLambda();
  double Z = reservoir_->getLevel();
  double level = geometry_->getBreachlevel();
  // first check if the water level is higher than the breach level
  if (Z <= level) {
    this->setZero();
  } else {
    // then check if the configuration allows a non-steady state solution
    // once it is steady, it will never be again non-steady, because we are
    // dealing with slowly eroding dams!
    if (!steady_) {
      // to calculate the steady state discharge, we use Z-Y as H0
      // to calculate the non-steady state discharge, we use Z as H0
      // check if non-steady solution is still applicable
      if (Z * (1.0 - sigma) <= level) {
        steady_ = true;
      }
    }
    // then calculate the main hydraulic variables
    if (steady_) {
      double H = Z - level;
      hc_ = sigma * H;
      vc_ = sqrt(sigma / lambda * G * H);
    } else {
      double H = Z;
      hc_ = pow(sigma, 2.0) * H;
      vc_ = sigma * sqrt(G * H / lambda);
    }
    area_ = geometry_->calcFlowArea(hc_);
    discharge_ = area_ * vc_;
    // and the others as well
    rhoWet_ = geometry_->calcWettedPerimeter(hc_);
    if (area_ > 0.0 && rhoWet_ > 0.0) {
      rHydr_ = area_ / rhoWet_;
    }
    rhoErod_ = geometry_->calcErodiblePerimeter(hc_);
  }
  // this->debug();
}

doubleVecVec HydraulicsPolynom::getErosionZone(uint N) {
  return geometry_->getErosionZone(hc_, N);
}

HydraulicsTriTra::HydraulicsTriTra(api::IReservoir_t theReservoir,
                                   api::IGeometryTriTra_t theGeometry)
    : Hydraulics(theReservoir, theGeometry) {
  geometry_ = theGeometry;
}

void HydraulicsTriTra::update() {
  double Z = reservoir_->getLevel();
  double Y = geometry_->getLevel();
  // first check if the water level is higher than the breach level
  if (Z <= geometry_->getBreachlevel()) {
    this->setZero();
  } else {
    // then check if the configuration allows a non-steady state solution
    // once it is steady, it will never be again non-steady, because we are
    // dealing with slowly eroding dams!
    if (!steady_) {
      // in this set up only steady state solution is used, so far ...
      steady_ = true;
    }
    // then calculate the main hydraulic variables (hc, vc, area, discharge)
    if (steady_) {
      if (Y >= 0.0) {
        // triangular shape
        hc_ = 0.8 * (Z - Y);
        vc_ = sqrt(0.5 * G * hc_);
        discharge_ = vc_ * geometry_->getSlope() * pow(hc_, 2);

      } else {
        // trapezoidal shape
        hc_ = 0.2 * (2.0 * Z + 3.0 * Y +
                     sqrt(9.0 * pow(Y, 2) + 4.0 * pow(Z, 2) - 8.0 * Y * Z));
        vc_ = sqrt(0.5 * G * hc_ * (hc_ - 2.0 * Y) / (hc_ - Y));
        discharge_ = vc_ * geometry_->getSlope() * hc_ * (hc_ - 2.0 * Y);
      }
      area_ = discharge_ / vc_;
    }
    // and the others as well (wetted perimeter, hydraulic radius, erodible
    // perimeter)
    rhoWet_ = geometry_->calcWettedPerimeter(hc_);
    if (area_ > 0.0 && rhoWet_ > 0.0) {
      rHydr_ = area_ / rhoWet_;
    }
    if (Y >= 0.0) {
      rhoErod_ = rhoWet_;
    } else {
      rhoErod_ = rhoWet_ - geometry_->getBottomWidth();
    }
  }
  // this->debug();
}

} // namespace model

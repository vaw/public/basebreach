// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Dam.h
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_DAM_H_
#define MODEL_DAM_H_

#include "api/IDam.h"
#include "types.h"

namespace model {

class Dam : public api::IDam {
public:
  Dam(double height, double crestWidth, double embankmentSlope);
  Dam(double height, double crestWidth, double embankmentSlope,
      double fixedBottom);
  virtual ~Dam();

  // interface methods: for explanations see IDam.h
  double getHeight() { return Hd_; }
  double getCrestWidth() { return wc_; }
  double getEmbankmentSlope() { return s_; }
  double getBottomLevel() { return Yb_; }

private:
  double Hd_; // height of the dam
  double wc_; // width of the dam crest
  double s_;  // slope of the embankment, downstream=upstream
  double Yb_; // level of fixed bottom, from here on: lateral erosion
};

} // namespace model

#endif // MODEL_DAM_H_

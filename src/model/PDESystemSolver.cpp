// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * PDESystemSolver.cpp
 *
 *  Created on: Apr 29, 2014
 *      Author: samuelpeter
  *  Edited: June 2023
 *      Editor: Matthew Halso
 */

#include "model/PDESystemSolver.h"

#include <math.h>

namespace model {

PDESystem::PDESystem(api::IReservoir_t theReservoir,
                     api::IGeometry_t theGeometry,
                     api::IHydraulics_t theHydraulics,
                     api::IErosion_t theErosion) {
  solverArgument_ = 0.0;
  time_ = 0.0;
  dt_ = 0.0;
  reservoir_ = theReservoir;
  geometry_ = theGeometry;
  hydraulics_ = theHydraulics;
  erosion_ = theErosion;
  Y_.resize(dim_);
  dYdt_.resize(dim_);
  Y0_.resize(dim_);
  Y0_.at(0) = reservoir_->getInitialLevel();
  Y0_.at(1) = geometry_->getInitialLevel();
}

PDESystem::~PDESystem() {
  // Auto-generated destructor stub
}

void PDESystem::calcDerivatives(const doubleVec &Y) {
  // set the levels
  // BB_MESSAGE("********** derivatives for Z = " << Y[0] << " and Y = " << Y[1]
  // << " **********");
  reservoir_->update(Y.at(0));
  geometry_->update(Y.at(1));
  hydraulics_->update();
  erosion_->freeze(geometry_->getTopWidth() * 2 > erosion_->getErosionLimit());
  erosion_->update();
  // calculate depletion of reservoir level
  if (reservoir_->getWaterSurface() == 0.0) {
    dYdt_.at(0) = 0.0;
  } else {
    //double inflow = reservoir_->getInflow(time_) * dt_; //MCH. not comparable with discharge when multiplied by dt!!
    double inflow = reservoir_->getInflow(time_); //MCH. getInflow is a rate, not volume over 1 second
    double discharge = hydraulics_->getDischarge(); //MCH. getDischarge is a rate, not volume
    dYdt_.at(0) = (-discharge + inflow) / reservoir_->getWaterSurface();
  }
  // calculate depletion of breach level
  if (geometry_->getVolumeErosionRate() == 0.0)
    dYdt_.at(1) = 0.0;
  else
    dYdt_.at(1) = erosion_->getTransportRate() *
                  hydraulics_->getErodiblePerimeter() /
                  geometry_->getVolumeErosionRate();
  // BB_MESSAGE("********** dZdt = " << dYdt_[0] << " and dYdt = " << dYdt_[1]
  // << " **********");
}

void PDESystem::setNewSystemState(const doubleVec &Y) {
  reservoir_->update(Y.at(0));
  geometry_->update(Y.at(1));
  hydraulics_->update();
  erosion_->freeze(geometry_->getTopWidth() * 2 > erosion_->getErosionLimit());
  erosion_->update();
  this->updateSystemState();
}

void PDESystem::initSystem() {
  time_ = 0.0;
  // we only have to initialize the erosion block, which is dependent on all
  // others and therefore takes care of initializing them
  erosion_->init();
  this->updateSystemState();
}

void PDESystem::updateSystemState() {
  // Y1 = reservoir level
  Y_.at(0) = reservoir_->getLevel();
  // Y2 = breach level
  Y_.at(1) = geometry_->getLevel();
  // BB_MESSAGE("********** system states for Z = " << Y_[0] << " and Y = " <<
  // Y_[1] << " **********"); this->debug();
}

void PDESystem::debug() {
  BB_MESSAGE("breach discharge = " << hydraulics_->getDischarge());
  BB_MESSAGE("reservoir water surface = " << reservoir_->getWaterSurface());
  BB_MESSAGE("reservoir level = " << reservoir_->getLevel());
  BB_MESSAGE("breach level = " << geometry_->getLevel());
  BB_MESSAGE("flow velocity = " << hydraulics_->getFlowVelocity());
  BB_MESSAGE("water depth = " << hydraulics_->getCriticalWaterDepth());
  BB_MESSAGE("flow area = " << hydraulics_->getFlowArea());
  BB_MESSAGE("hydraulic radius = " << hydraulics_->getHydraulicRadius());
  BB_MESSAGE("wetted perimeter = " << hydraulics_->getWettedPerimeter());
  BB_MESSAGE("transport rate = " << erosion_->getTransportRate());
  BB_MESSAGE("erodible perimeter = " << hydraulics_->getErodiblePerimeter());
  BB_MESSAGE("volume change rate = " << geometry_->getVolumeErosionRate());
}

PDESolverExplicitEulerFixedTimeStep::PDESolverExplicitEulerFixedTimeStep(
    api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
    api::IHydraulics_t theHydraulics, api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {}

PDESolverExplicitEulerFixedTimeStep::~PDESolverExplicitEulerFixedTimeStep() {
  // Auto-generated destructor stub
}

void PDESolverExplicitEulerFixedTimeStep::evaluate() {
  doubleVec Ytemp(dim_);
  // here the solver argument is the global time step 'dt'
  dt_ = solverArgument_;
  // one single integration step
  this->calcDerivatives(Y_);
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + dt_ * dYdt_.at(ii);
  this->setNewSystemState(Ytemp);
  // update time
  time_ += dt_;
}

PDESolverRK2StageMidpoint::PDESolverRK2StageMidpoint(
    api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
    api::IHydraulics_t theHydraulics, api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {}

PDESolverRK2StageMidpoint::~PDESolverRK2StageMidpoint() {
  // Auto-generated destructor stub
}

void PDESolverRK2StageMidpoint::evaluate() {
  doubleVec Ytemp(dim_), kY1(dim_), kY2(dim_);
  // here the solver argument is the global time step 'dt'
  dt_ = solverArgument_;
  // first step
  this->calcDerivatives(Y_);
  for (uint ii = 0; ii < dim_; ii++)
    kY1.at(ii) = dt_ * dYdt_.at(ii);
  // second step
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + 0.5 * kY1.at(ii);
  this->calcDerivatives(Ytemp);
  for (uint ii = 0; ii < dim_; ii++)
    kY2.at(ii) = dt_ * dYdt_.at(ii);
  // final intergation
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + kY2.at(ii);
  this->setNewSystemState(Ytemp);
  // update time
  time_ += dt_;
}

PDESolverRK2StageHeun::PDESolverRK2StageHeun(api::IReservoir_t theReservoir,
                                             api::IGeometry_t theGeometry,
                                             api::IHydraulics_t theHydraulics,
                                             api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {}

PDESolverRK2StageHeun::~PDESolverRK2StageHeun() {
  // Auto-generated destructor stub
}

void PDESolverRK2StageHeun::evaluate() {
  doubleVec Ytemp(dim_), kY1(dim_), kY2(dim_);
  // here the solver argument is the global time step 'dt'
  dt_ = solverArgument_;
  // first step
  this->calcDerivatives(Y_);
  for (uint ii = 0; ii < dim_; ii++)
    kY1.at(ii) = dt_ * dYdt_.at(ii);
  // second step
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + kY1.at(ii);
  this->calcDerivatives(Ytemp);
  for (uint ii = 0; ii < dim_; ii++)
    kY2.at(ii) = dt_ * dYdt_.at(ii);
  // final intergation
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + 0.5 * (kY1.at(ii) + kY2.at(ii));
  this->setNewSystemState(Ytemp);
  // update time
  time_ += dt_;
}

PDESolverRK4StageClassic::PDESolverRK4StageClassic(
    api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
    api::IHydraulics_t theHydraulics, api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {}

PDESolverRK4StageClassic::~PDESolverRK4StageClassic() {
  // Auto-generated destructor stub
}

void PDESolverRK4StageClassic::evaluate() {
  doubleVec Ytemp(dim_), kY1(dim_), kY2(dim_), kY3(dim_), kY4(dim_);
  // here the solver argument is the global time step 'dt'
  dt_ = solverArgument_;
  // first step
  this->calcDerivatives(Y_);
  for (uint ii = 0; ii < dim_; ii++)
    kY1.at(ii) = dt_ * dYdt_.at(ii);
  // second step
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + 0.5 * kY1.at(ii);
  this->calcDerivatives(Ytemp);
  for (uint ii = 0; ii < dim_; ii++)
    kY2.at(ii) = dt_ * dYdt_.at(ii);
  // third step
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + 0.5 * kY2.at(ii);
  this->calcDerivatives(Ytemp);
  for (uint ii = 0; ii < dim_; ii++)
    kY3.at(ii) = dt_ * dYdt_.at(ii);
  // fourth step
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + kY3.at(ii);
  this->calcDerivatives(Ytemp);
  for (uint ii = 0; ii < dim_; ii++)
    kY4.at(ii) = dt_ * dYdt_.at(ii);
  // final intergation
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) =
        Y_.at(ii) +
        (kY1.at(ii) + 2.0 * kY2.at(ii) + 2.0 * kY3.at(ii) + kY4.at(ii)) / 6.0;
  this->setNewSystemState(Ytemp);
  // update time
  time_ += dt_;
}

PDESolverExplicitEulerVariableTimeStep::PDESolverExplicitEulerVariableTimeStep(
    api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
    api::IHydraulics_t theHydraulics, api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {}

PDESolverExplicitEulerVariableTimeStep::
    ~PDESolverExplicitEulerVariableTimeStep() {
  // Auto-generated destructor stub
}

void PDESolverExplicitEulerVariableTimeStep::evaluate() {
  doubleVec Ytemp(dim_);
  // do only one step
  this->calcDerivatives(Y_);
  // look for the largest absolute derivative, and setting time step (with the
  // solver argument)
  uint index = 0;
  for (uint ii = 0; ii < dim_; ii++) {
    if (fabs(dYdt_.at(ii)) > fabs(dYdt_[index]))
      index = ii;
  }
  // here the solver argument is the global limit for relative change Y
  dt_ = fabs(solverArgument_ * Y0_[index] / dYdt_[index]);
  // final intergation
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + dt_ * dYdt_.at(ii);
  this->setNewSystemState(Ytemp);
  // update time
  time_ += dt_;
}

PDESolverExplicitEuler2StepVariable::PDESolverExplicitEuler2StepVariable(
    api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
    api::IHydraulics_t theHydraulics, api::IErosion_t theErosion)
    : PDESystem(theReservoir, theGeometry, theHydraulics, theErosion) {
  dt0_ = 10.0;
  r_ = 1.0;
  safetyMargin_ = 0.1;
}

PDESolverExplicitEuler2StepVariable::~PDESolverExplicitEuler2StepVariable() {
  // Auto-generated destructor stub
}

void PDESolverExplicitEuler2StepVariable::evaluate() {
  doubleVec Ytemp(dim_), kY1(dim_), kY2(dim_);
  if (time_ == 0.0)
    dt_ = dt0_;
  // here the solver argument is the truncation error
  double epsilon = solverArgument_;
  // find appropriate time step
  r_ = 1.0;
  while (r_ > epsilon) {
    // first step
    this->calcDerivatives(Y_);
    for (uint ii = 0; ii < dim_; ii++)
      kY1.at(ii) = dYdt_.at(ii);
    // second step
    for (uint ii = 0; ii < dim_; ii++)
      Ytemp.at(ii) = Y_.at(ii) + 0.5 * dt_ * kY1.at(ii);
    this->calcDerivatives(Ytemp);
    for (uint ii = 0; ii < dim_; ii++)
      kY2.at(ii) = dYdt_.at(ii);
    // update rate
    r_ = 0.0;
    for (uint ii = 0; ii < dim_; ii++) {
      double rnew = 0.5 * fabs(kY1.at(ii) - kY2.at(ii));
      if (rnew > r_)
        r_ = rnew;
    }
    // update time step
    if (r_ > epsilon)
      dt_ *= safetyMargin_ * epsilon / r_;
  }
  // final integration
  for (uint ii = 0; ii < dim_; ii++)
    Ytemp.at(ii) = Y_.at(ii) + dt_ * kY2.at(ii);
  this->setNewSystemState(Ytemp);
  // update time and trial time step
  time_ += dt_;
  dt_ *= safetyMargin_ * epsilon / r_;
}

} // namespace model

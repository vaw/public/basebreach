// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "model/Distribution.h"

#include <algorithm>
#define _USE_MATH_DEFINES
#include <cmath>
#include <memory>
#include <random>

#include "ExceptionHandling.h"

namespace model {

DistributionQObjectBaseClass::DistributionQObjectBaseClass()
    : name_(""), type_(""), parameters_(1.0, 1.0), moments_(0.0, 0.0),
      bounds_(NAN, NAN) {
  uint seed = std::random_device()();
  urng_.seed(seed);
}

void DistributionQObjectBaseClass::resetBy(
    api::IDistribution_t newDistribution) {
  emit newDistCreated(newDistribution);
}

api::IDistribution_t DistributionQObjectBaseClass::getCopy() {
  int index = 0;
  for (uint ii = 0; ii < getValidTypes().size(); ii++) {
    if (getValidTypes().at(ii) == this->getType()) {
      index = ii;
      break;
    }
  }
  api::IDistribution_t copy = this->createDistribution(index);
  copy->setName(this->getName());
  copy->setParameters(this->getParameters());
  copy->setBounds(this->getBounds());
  return copy;
}

void DistributionQObjectBaseClass::setName(std::string name) {
  name_ = name;
  emit Changed();
}

std::string DistributionQObjectBaseClass::getName() { return name_; }

std::string DistributionQObjectBaseClass::getType() { return type_; }

stringVec DistributionQObjectBaseClass::getValidTypes() {
  return {"UniformDistribution", "NormalDistribution", "LognormalDistribution",
          "ExponentialDistribution", "GammaDistribution"};
}

api::IDistribution_t
DistributionQObjectBaseClass::createDistribution(int index) {
  std::shared_ptr<api::IDistribution> dist;
  if (index == 0) {
    dist = std::make_shared<UniformDistribution>();
  } else if (index == 1) {
    dist = std::make_shared<NormalDistribution>();
  } else if (index == 2) {
    dist = std::make_shared<LognormalDistribution>();
  } else if (index == 3) {
    dist = std::make_shared<ExponentialDistribution>();
  } else if (index == 4) {
    dist = std::make_shared<GammaDistribution>();
  } else {
    BB_EXCEPTION("Undefined distribution type index");
  }
  return dist;
}

void DistributionQObjectBaseClass::printProperties() {
  BB_MESSAGE(name_ << " (" << type_ << ")");
  BB_MESSAGE("------------------------------");
  BB_MESSAGE("parameters: " << parameters_.first() << ", "
                            << parameters_.second());
  BB_MESSAGE("moments: " << moments_.first() << ", " << moments_.second());
  if (std::isnan(bounds_.first()) && std::isnan(bounds_.second())) {
    BB_MESSAGE("no bounds defined");
  }
  if (!std::isnan(bounds_.first())) {
    BB_MESSAGE("lower bound: " << bounds_.first());
  }
  if (!std::isnan(bounds_.second())) {
    BB_MESSAGE("upper bound: " << bounds_.second());
  }
}

void DistributionQObjectBaseClass::setParameters(ValuePair params) {
  setDistributionParameters(params);
  emit Changed();
}

ValuePair DistributionQObjectBaseClass::getParameters() { return parameters_; }

void DistributionQObjectBaseClass::setMoments(ValuePair moms) {
  setDistributionMoments(moms);
  emit Changed();
}

ValuePair DistributionQObjectBaseClass::getMoments() { return moments_; }

bool DistributionQObjectBaseClass::isDefined() {
  return (parameters_.isFirstDefined() && parameters_.isSecondDefined())
             ? true
             : false;
}

bool DistributionQObjectBaseClass::isSecondParameterNeeded() { return true; }

void DistributionQObjectBaseClass::setBounds(ValuePair bounds) {
  bounds_ = bounds;
  emit Changed();
}

ValuePair DistributionQObjectBaseClass::getBounds() { return bounds_; }

bool DistributionQObjectBaseClass::isWithinBounds(double x) {
  if ((!bounds_.isFirstDefined() && !bounds_.isSecondDefined()) ||
      (bounds_.isFirstDefined() && bounds_.first() <= x &&
       !bounds_.isSecondDefined()) ||
      (!bounds_.isFirstDefined() && bounds_.isSecondDefined() &&
       x <= bounds_.second()) ||
      (bounds_.isFirstDefined() && bounds_.first() <= x &&
       bounds_.isSecondDefined() && x <= bounds_.second())) {
    return true;
  } else {
    return false;
  }
}

double DistributionQObjectBaseClass::getDistanceFromBounds(double x) {
  if (bounds_.isFirstDefined() && bounds_.isSecondDefined()) {
    return std::min(fabs(x - bounds_.first()), fabs(x - bounds_.second()));
  } else if (bounds_.isFirstDefined()) {
    return fabs(x - bounds_.first());
  } else if (bounds_.isSecondDefined()) {
    return fabs(x - bounds_.second());
  } else {
    return NAN;
  }
}

double DistributionQObjectBaseClass::bounceOffBounds(double x) {
  if (bounds_.isFirstDefined() && x <= bounds_.first()) {
    return 2.0 * bounds_.first() - x;
  } else if (bounds_.isSecondDefined() && x >= bounds_.second()) {
    return 2.0 * bounds_.second() - x;
  } else {
    return x;
  }
}

double DistributionQObjectBaseClass::mean() { return getMoments().first(); }

double DistributionQObjectBaseClass::var() {
  return pow(getMoments().second(), 2.0);
}

double DistributionQObjectBaseClass::stdev() { return getMoments().second(); }

UniformDistribution::UniformDistribution() : DistributionBaseClass() {
  type_ = "UniformDistribution";
  setDistributionParameters(ValuePair(0.0, 1.0));
}

UniformDistribution::~UniformDistribution() {
  // auto generate destructor
}

void UniformDistribution::setDistributionParameters(ValuePair params) {
  if (params.isFirstDefined() && params.isSecondDefined() &&
      (params.first() < params.second())) {
    parameters_(params.first(), params.second());
    parameters2moments();
    init();
  } else {
    std::cerr << "both parameters must be defined!" << std::endl;
  }
}

void UniformDistribution::setDistributionMoments(ValuePair moms) {
  if (moms.isFirstDefined() && moms.isSecondDefined()) {
    moments_(moms.first(), moms.second());
    moments2parameters();
    init();
  } else {
    std::cerr << "both moments must be defined!" << std::endl;
  }
}

void UniformDistribution::parameters2moments() {
  double m = 0.5 * (parameters_.first() + parameters_.second());
  double s = sqrt(pow(parameters_.second() - parameters_.first(), 2) / 12.0);
  moments_(m, s);
}

void UniformDistribution::moments2parameters() {
  double t = 0.5 * moments_.second() * sqrt(12.0);
  parameters_(moments_.first() - t, moments_.first() + t);
}

void UniformDistribution::init() {
  distributionMath_ = boost::math::uniform_distribution<double>(
      parameters_.first(), parameters_.second());
  distributionRandom_ = boost::random::uniform_real_distribution<double>(
      parameters_.first(), parameters_.second());
  generator_ = boost::variate_generator<
      boost::mt19937, boost::random::uniform_real_distribution<double>>(
      urng_, distributionRandom_);
}

NormalDistribution::NormalDistribution() : DistributionBaseClass() {
  type_ = "NormalDistribution";
  setDistributionParameters(ValuePair(0.0, 1.0));
}

NormalDistribution::~NormalDistribution() {
  // auto generate destructor
}

void NormalDistribution::setDistributionParameters(ValuePair params) {
  if (params.isFirstDefined() && params.isSecondDefined()) {
    parameters_(params.first(), params.second());
    parameters2moments();
    init();
  } else {
    std::cerr << "both parameters must be defined!" << std::endl;
  }
}

void NormalDistribution::setDistributionMoments(ValuePair moms) {
  if (moms.isFirstDefined() && moms.isSecondDefined()) {
    moments_(moms.first(), moms.second());
    moments2parameters();
    init();
  } else {
    std::cerr << "both moments must be defined!" << std::endl;
  }
}

void NormalDistribution::parameters2moments() {
  moments_(parameters_.first(), parameters_.second());
}

void NormalDistribution::moments2parameters() {
  parameters_(moments_.first(), moments_.second());
}

void NormalDistribution::init() {
  distributionMath_ = boost::math::normal_distribution<double>(
      parameters_.first(), parameters_.second());
  distributionRandom_ = boost::random::normal_distribution<double>(
      parameters_.first(), parameters_.second());
  generator_ =
      boost::variate_generator<boost::mt19937,
                               boost::random::normal_distribution<double>>(
          urng_, distributionRandom_);
}

LognormalDistribution::LognormalDistribution() : DistributionBaseClass() {
  type_ = "LognormalDistribution";
  setDistributionParameters(ValuePair(0.0, 1.0));
}

LognormalDistribution::~LognormalDistribution() {
  // auto generate destructor
}

void LognormalDistribution::setDistributionParameters(ValuePair params) {
  if (params.isFirstDefined() && params.isSecondDefined() &&
      params.second() > 0.0) {
    parameters_(params.first(), params.second());
    parameters2moments();
    init();
  } else {
    std::cerr << "both parameters must be defined (second parameter > 0)!"
              << std::endl;
  }
}

void LognormalDistribution::setDistributionMoments(ValuePair moms) {
  if (moms.isFirstDefined() && moms.isSecondDefined()) {
    moments_(fabs(moms.first()), fabs(moms.second()));
    moments2parameters();
    init();
  } else {
    std::cerr << "both moments must be defined!" << std::endl;
  }
}

void LognormalDistribution::parameters2moments() {
  double par2sqr = parameters_.second() * parameters_.second();
  double m = exp(parameters_.first() + 0.5 * par2sqr);
  double s = sqrt(exp(2.0 * parameters_.first() + par2sqr) * expm1(par2sqr));
  moments_(m, s);
}

void LognormalDistribution::moments2parameters() {
  double msqr = moments_.first() * moments_.first();
  double var = moments_.second() * moments_.second();
  parameters_(log(msqr / sqrt(var + msqr)), sqrt(log1p(var / msqr)));
}

void LognormalDistribution::init() {
  distributionMath_ = boost::math::lognormal_distribution<double>(
      parameters_.first(), parameters_.second());
  distributionRandom_ = boost::random::lognormal_distribution<double>(
      parameters_.first(), parameters_.second());
  generator_ =
      boost::variate_generator<boost::mt19937,
                               boost::random::lognormal_distribution<double>>(
          urng_, distributionRandom_);
}

ExponentialDistribution::ExponentialDistribution() : DistributionBaseClass() {
  type_ = "ExponentialDistribution";
  setDistributionParameters(ValuePair(1.0, NAN));
}

ExponentialDistribution::~ExponentialDistribution() {
  // auto generate destructor
}

void ExponentialDistribution::setDistributionParameters(ValuePair params) {
  if (params.isFirstDefined() && params.first() > 0.0) {
    parameters_(params.first(), NAN);
    parameters2moments();
    init();
  } else {
    std::cerr << "first parameter must be defined (>0)!" << std::endl;
  }
}

void ExponentialDistribution::setDistributionMoments(ValuePair moms) {
  if (moms.isFirstDefined() && moms.first() > 0.0) {
    moments_(moms.first(), moms.first());
    moments2parameters();
    init();
  } else {
    std::cerr << "both moments must be defined (first>0)!" << std::endl;
  }
}

void ExponentialDistribution::parameters2moments() {
  moments_(1.0 / parameters_.first(), 1.0 / parameters_.first());
}

void ExponentialDistribution::moments2parameters() {
  parameters_(1.0 / moments_.first(), NAN);
}

void ExponentialDistribution::init() {
  distributionMath_ =
      boost::math::exponential_distribution<double>(parameters_.first());
  distributionRandom_ =
      boost::random::exponential_distribution<double>(parameters_.first());
  generator_ =
      boost::variate_generator<boost::mt19937,
                               boost::random::exponential_distribution<double>>(
          urng_, distributionRandom_);
}

bool ExponentialDistribution::isDefined() {
  return parameters_.isFirstDefined() ? true : false;
}

bool ExponentialDistribution::isSecondParameterNeeded() { return false; }

GammaDistribution::GammaDistribution() : DistributionBaseClass(1.0, 1.0) {
  type_ = "GammaDistribution";
  setDistributionParameters(ValuePair(1.0, 1.0));
}

GammaDistribution::~GammaDistribution() {
  // auto generate destructor
}

void GammaDistribution::setDistributionParameters(ValuePair params) {
  if (params.isFirstDefined() && params.first() > 0.0 &&
      params.isSecondDefined() && params.second() > 0.0) {
    parameters_(params.first(), params.second());
    parameters2moments();
    init();
  } else {
    std::cerr << "both parameters must be defined (>0)!" << std::endl;
  }
}

void GammaDistribution::setDistributionMoments(ValuePair moms) {
  if (moms.isFirstDefined() && moms.first() > 0.0 && moms.isSecondDefined()) {
    moments_(moms.first(), moms.second());
    moments2parameters();
    init();
  } else {
    std::cerr << "both moments must be defined!" << std::endl;
  }
}

void GammaDistribution::parameters2moments() {
  moments_(parameters_.first() * parameters_.second(),
           sqrt(parameters_.first()) * parameters_.second());
}

void GammaDistribution::moments2parameters() {
  parameters_(pow(moments_.first() / moments_.second(), 2),
              moments_.second() * moments_.second() / moments_.first());
}

void GammaDistribution::init() {
  distributionMath_ = boost::math::gamma_distribution<>(parameters_.first(),
                                                        parameters_.second());
  distributionRandom_ = boost::random::gamma_distribution<double>(
      parameters_.first(), parameters_.second());
  generator_ =
      boost::variate_generator<boost::mt19937,
                               boost::random::gamma_distribution<double>>(
          urng_, distributionRandom_);
}

} /* namespace model */

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * DBModel.h
 *
 *  Created on: Apr 30, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_DBMODEL_H_
#define MODEL_DBMODEL_H_

#include <map>

#include "api/IDBModel.h"
#include "api/IDam.h"
#include "api/IErosion.h"
#include "api/IGeometry.h"
#include "api/IHydraulics.h"
#include "api/IOutput.h"
#include "api/IPDESystemSolver.h"
#include "api/IReservoir.h"
#include "model/Output.h"
#include "types.h"

namespace model {

enum SimulationType {
  FullWithOutput = 0,
  UntilPeakDischarge = 1,
  FullWithoutOutput = 2
};

class DBModel : public api::IDBModel {
public:
  DBModel(api::IDam_t theDam, api::IReservoir_t theReservoir,
          api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
          api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver,
          api::outputVec theOutputs);
  virtual ~DBModel() = default;

  double getCutoffTime() const { return t_cutoff_; }
  void setCutoffTime(double cutoff_time) { t_cutoff_ = cutoff_time; }

private:
  void setAbortCriterion(double value) { abortCriterion_ = value; }
  void setSimulationType(int type) {
    simulationType_ = static_cast<SimulationType>(type);
  }
  void run();
  /**
   * Determine the integration timestep required to reach the desired precision
   * at peak discharge.
   *
   * The return value is mostly for monitoring purposes, the solver has already
   * been set to the appropriate solver argument when this function returns.
   */
  double refineSolverArgumentToTargetPrecision();
  double getPeakDischarge() { return Qmax_; }
  double getAverageBreachWidth() { return geometry_->getAverageWidth(); }
  api::IDam_t getDam() { return dam_; }
  api::IReservoir_t getReservoir() { return reservoir_; }
  api::IGeometry_t getGeometry() { return geometry_; }
  api::IHydraulics_t getHydraulics() { return hydraulics_; }
  api::IErosion_t getErosion() { return erosion_; }

  void setDataOutput(api::IOutputpyBB_t);
  const doubleVecVec &getData();
  uint getNumTimeSteps();

  api::IDam_t dam_;
  api::IReservoir_t reservoir_;
  api::IGeometry_t geometry_;
  api::IHydraulics_t hydraulics_;
  api::IErosion_t erosion_;
  api::IPDESystemSolver_t solver_;
  api::outputVec outputs_;
  api::IOutputpyBB_t dataOutput_;
  void calcOneSolutionUntilQmax(uint &iCount, uint &dCount);
  void calcOneSolutionUntilQmaxWithOutput();
  void calcOneFullSolutionUntilTcutoff(double t_cutoff);
  void calcOneFullSolutionUntilTcutoffWithOutput(double t_cutoff);

  /** Current discharge minus current inflow. [m3/s] */
  double getEffectiveDischarge();

  double Qmax_;
  double tmax_;
  double t_cutoff_;
  double abortCriterion_;
  SimulationType simulationType_;
};

} // namespace model

#endif // MODEL_DBMODEL_H_

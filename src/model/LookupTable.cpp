// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "LookupTable.h"

#include <QtCore>

#include "types.h"

namespace model {

LookupTable::LookupTable(double fixedValue) : data_{{0.0, fixedValue}} {}

LookupTable::LookupTable(const String_T &filename) : data_{{0.0, 0.0}} {
  load(filename);
}

double LookupTable::getValue(double position) const {
  // If the map only has a single value, return that value
  if (data_.size() == 1) {
    return data_.first();
  }
  // If the position is outside the lookup table, return the value at the
  // nearest boundary
  if (position <= data_.firstKey()) {
    return data_.first();
  }
  if (position >= data_.lastKey()) {
    return data_.last();
  }
  // For intermediate values, find the closest control points and interpolate
  // between them
  auto it = data_.lowerBound(position);
  // If position is a key, return the value
  if (it.key() == position) {
    return it.value();
  }
  // "it" is now pointing to the first control point key greater than "position"
  // If it is the last element, return the value at the last key
  if (it == data_.end()) {
    return data_.last();
  }
  double x2 = it.key();
  double y2 = it.value();
  // Get previous control point
  it--;
  double x1 = it.key();
  double y1 = it.value();
  // Interpolate
  return y1 + (y2 - y1) * (position - x1) / (x2 - x1);
}

void LookupTable::load(const String_T &filename) {
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    throw std::runtime_error("Could not open lookup table file");
  }

  data_.clear();

  QTextStream in(&file);
  while (!in.atEnd()) {
    QString line = in.readLine();

    // Skip comments and blank lines
    if (line.startsWith('#') || line.isEmpty()) {
      continue;
    }

    // Split the line into a key and a value
    QStringList parts = line.split(' ', Qt::SplitBehaviorFlags::SkipEmptyParts);
    if (parts.size() < 2) {
      throw std::runtime_error("Invalid line in lookup table");
    }
    bool ok1, ok2;
    double position = parts[0].toDouble(&ok1);
    double value = parts[1].toDouble(&ok2);
    if (!ok1 || !ok2) {
      throw std::runtime_error("Invalid line in lookup table");
    }

    data_.insert(position, value);
  }
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * GeometryWithNumerics.cpp
 *
 *  Created on: May 28, 2014
 *      Author: samuelpeter
 */

#include "model/GeometryWithNumerics.h"

#include <math.h>

#include <algorithm>
#include <stdexcept>
#include <vector>

#include "ExceptionHandling.h"

#define PI 3.1415926535897932

namespace model {

GeometryAdaptiveTrapezSimpson::GeometryAdaptiveTrapezSimpson(double initLevel,
                                                             double beta,
                                                             api::IDam_t theDam)
    : GeometryPolynom(initLevel, beta, theDam) {
  this->RECMAX = 1000;
  this->EPS = 1e-15;
  this->Idelta = 0.0;
  this->I = 0.0;
}

GeometryAdaptiveTrapezSimpson::~GeometryAdaptiveTrapezSimpson() {
  // Auto-generated destructor stub
}

double GeometryAdaptiveTrapezSimpson::doIntegration(double a, double b) {
  // initial values
  double fa = this->functionToBeIntegrated(a);
  double fb = this->functionToBeIntegrated(b);
  double fm = this->functionToBeIntegrated(0.5 * (a + b));
  // coarse estimation of result (simpson formula)
  double Is = (b - a) / 6.0 * (fa + 4.0 * fm + fb);
  Idelta = 0.5 * (abs(Is) + b - a) * relPrecision_ / EPS;
  // enter recursion
  uint count = 0;
  return this->recursionFunction(a, b, fa, fm, fb, count);
}

double GeometryAdaptiveTrapezSimpson::recursionFunction(double a, double b,
                                                        double fa, double fm,
                                                        double fb, uint &ii) {
  if (ii > RECMAX) {
    BB_EXCEPTION("ERROR: integration did not converge!");
  } else {
    ii++; // increase counter
    double h = 0.25 * (b - a);
    double m = 0.5 * (a + b);
    double fml = this->functionToBeIntegrated(a + h);
    double fmr = this->functionToBeIntegrated(b - h);
    double i1 = h / 1.5 * (fa + 4.0 * fm + fb); // trapez formula
    double i2 =
        h / 3.0 * (fa + 4.0 * (fml + fmr) + 2.0 * fm + fb); // simpson formula
    i1 = (16.0 * i2 - i1) / 15.0; // better estimator according to romberg
    if (Idelta + i1 == Idelta + i2) {
      return i1;
    } else {
      return this->recursionFunction(a, m, fa, fml, fm, ii) +
             this->recursionFunction(m, b, fm, fmr, fb, ii);
    }
  }
}

/*
GeometryGauKron::GeometryGauKron(double initLevel, double initWidth, double
sigma, api::IDam_t theDam) : GeometryPolynom(initLevel, initWidth, sigma,
theDam){
        // nothing to do so far
}

GeometryGauKron::~GeometryGauKron() {
        // Auto-generated destructor stub
}

double GeometryGauKron::doIntegration(double a, double b) {
        double v;
        // first, autogkstate is initialized
        alglib::autogkstate s;
        alglib::autogkreport rep;
        // then we call integration function
        alglib::autogksmooth(a, b, s);
        boost::function<void(double, double, double, double &, void *)> f;

        typedef void(GeometryGauKron::* f_t)(double, double, double, double &,
void *);
        //f = boost::bind(&GeometryGauKron::int_function_1_func, this, _1, _2,
_3, _4, _5); f_t asdf;

        alglib::autogkintegrate(s, asdf, NULL);
        // and finally we obtain results with autogkresults() call
        autogkresults(s, v, rep);
        return double(v);
}

void GeometryGauKron::int_function_1_func(double x, double xminusa, double
bminusx, double &y, void *ptr) {
        // this callback calculates f(x)
        y = this->functionToBeIntegrated(x);
}
*/

GeometryGauLeg::GeometryGauLeg(double initLevel, double beta,
                               api::IDam_t theDam)
    : GeometryPolynom(initLevel, beta, theDam) {
  this->EPS = 1e-14;
  this->n = m = 0;
  this->z1 = zi = xm = xl = 0.0;
  this->pp = p3 = p2 = p1 = 0.0;
}

GeometryGauLeg::~GeometryGauLeg() {
  // Auto-generated destructor stub
}

double GeometryGauLeg::doIntegration(double a, double b) {
  /*
   * Given the lower and upper limits of integration "x1" and "x2",
   * this routine returns the integrated value of the function defined in
   * "fObj", using the Gauss-Legendre N-point quadrature formula.
   */
  double nd = 0.5 * lambda_ / (lambda_ - 1.0) +
              1.0; // calculataing the number of Gauss points (k = 2N-1, ceiled)
  BB_ASSERT(nd <= INT_MAX, "out of range");
  n = static_cast<uint>(nd);
  m = (n + 1) / 2; // the roots are symmetric in the interval, so we only have
                   // to find half of them
  doubleVec z(m);
  doubleVec w(m); // allocate the vectors containing the abscissas and weights
  // check if the same order of quadrature has been done before
  std::vector<uint>::iterator it = find(N.begin(), N.end(), n);
  if (it != N.end()) {
    // already calculated
    size_t index = std::distance(N.begin(), it);
    // get the abcissas and the weights
    z = Z.at(index);
    w = W.at(index);
  } else {
    // we have to calculate the abcissas and the weights
    // loop over the desired roots
    for (uint ii = 0; ii < m; ii++) {
      zi = cos(
          PI * (ii + 0.75) /
          (n + 0.5)); // starting with this approximation to the "i"th root, we
                      // enter the main loop of refinement by Newton's method
      do {
        p1 = 1.0;
        p2 = 0.0;
        for (uint jj = 0; jj < n; jj++) {
          p3 = p2;
          p2 = p1;
          p1 = ((2.0 * jj + 1.0) * zi * p2 - jj * p3) / (jj + 1);
        }
        // "p1" is now the desired Legendre polynomial.
        // we next compute "pp", its derivative, by a standard relation
        // involving also "p2", the polynomial of one lower order.
        pp = n * (zi * p1 - p2) / (zi * zi - 1.0);
        z1 = zi;
        zi = z1 - p1 / pp; // Newton's method
      } while (fabs(zi - z1) > EPS);
      // scale the root to the desired interval
      z.at(ii) = zi;
      // compute the weight
      w.at(ii) = 2.0 / ((1.0 - zi * zi) * pp * pp);
    }
    // and store the found calculated values in the container
    N.push_back(n);
    Z.push_back(z);
    W.push_back(w);
  }
  // prepare for transformation
  xm = 0.5 * (b + a);
  xl = 0.5 * (b - a);
  // finally do the summation
  double sum = 0.0;
  for (uint ii = 0; ii < m; ii++) {
    double x1 = xm - xl * z.at(ii); // left side abcisse
    double x2 = xm + xl * z.at(ii); // right side abcisse
    sum += w.at(ii) * (this->functionToBeIntegrated(x1) +
                       this->functionToBeIntegrated(x2));
  }
  return xl * sum;
}

GeometrySimpson::GeometrySimpson(double initLevel, double beta,
                                 api::IDam_t theDam)
    : GeometryPolynom(initLevel, beta, theDam) {
  this->JMAX = 20;
  this->iteration = 1;
  this->s = sTemp = os = osTemp = 0.0;
  this->tnm = del = xi = sum = 0.0;
}

GeometrySimpson::~GeometrySimpson() {
  // Auto-generated destructor stub
}

double GeometrySimpson::doIntegration(double lowerBound, double upperBound) {
  /*
   * Returns the integral of the function "f" from "a" to "b".
   * The constant "eps" can be set to the desired fractional accuracy.
   * JMAX is set so that 2 to the power of JMAX-1 is the maximum allowed number
   * of steps. The Integration is performed by Simpson's rule.
   */
  os = 0.0;
  osTemp = 0.0;
  iteration = 1;

  for (uint n = 1; n < JMAX + 1; n++) {
    /*
     * This routine computes the "n"-th refinement of an extended trapezoidal
     * rule. "f" is input as the function to be integrated between limits "a"
     * and "b", also input. When called with "n=1", the routine returns the
     * crudest estimate of integrate(f(x),a,b,x). Subsequent calls with
     * n=2,3,... (in that sequential order!) will improve the accuracy by adding
     * 2^(n-2) additional interior points.
     */
    if (n == 1) {
      sTemp = 0.5 * (upperBound - lowerBound) *
              (this->functionToBeIntegrated(lowerBound) +
               this->functionToBeIntegrated(upperBound));
    } else {
      iteration <<= 1; // 2,4,8,16,... (binary 10,100,... by shifting left)
      tnm = static_cast<double>(iteration);
      del = (upperBound - lowerBound) / tnm;
      xi = lowerBound + 0.5 * del;
      sum = 0.0;
      for (uint ii = 0; ii < iteration; ii++, xi += del)
        sum += this->functionToBeIntegrated(xi);
      sTemp = 0.5 * (sTemp + (upperBound - lowerBound) * sum / tnm);
    }
    s = (4.0 * sTemp - osTemp) / 3.0;
    if (n > 3)
      if (fabs(s - os) < relPrecision_ * fabs(os) || (s == 0.0 && os == 0.0))
        return s;
    os = s;
    osTemp = sTemp;
  }
  BB_EXCEPTION("Error in integration routine: did not converge!")
  return 0.0;
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Distribution.h
 *
 *  Created on: January 20, 2015
 *      Author: samuelpeter
 */

#ifndef MODEL_DISTRIBUTION_H_
#define MODEL_DISTRIBUTION_H_

#include <QtCore>

#ifndef Q_MOC_RUN
#include <boost/math/distributions.hpp>
#include <boost/random.hpp>
#endif

#include <string>

#include "api/IDistribution.h"
#include "types.h"

namespace model {

class DistributionQObjectBaseClass : public api::IDistribution {
  Q_OBJECT

public:
  DistributionQObjectBaseClass();

  void resetBy(api::IDistribution_t);
  api::IDistribution_t getCopy();

  // some auxiliary stuff
  void setName(std::string name);
  std::string getName();
  std::string getType();
  stringVec getValidTypes();
  api::IDistribution_t createDistribution(int);
  void printProperties();

  // parameters, moments
  void setParameters(ValuePair params);
  ValuePair getParameters();
  void setMoments(ValuePair);
  ValuePair getMoments();
  bool isDefined();
  bool isSecondParameterNeeded();

  // bounds
  void setBounds(ValuePair bounds);
  ValuePair getBounds();
  bool isWithinBounds(double x);
  double getDistanceFromBounds(double x);
  double bounceOffBounds(double x);

  // distribution specific stuff
  double mean();
  double var();
  double stdev();

  // to be implemented in actual distribution class
  virtual doubleVec drawNumbers(uint) = 0;
  virtual void setDistributionParameters(ValuePair) = 0;
  virtual void setDistributionMoments(ValuePair) = 0;
  virtual double pdf(double x) = 0;
  virtual double cdf(double x) = 0;

protected:
  std::string name_;
  std::string type_;
  ValuePair parameters_;
  ValuePair moments_;
  ValuePair bounds_;
  boost::mt19937 urng_;

signals:
  void Changed();
  void newDistCreated(api::IDistribution_t);
};

template <typename DistributionMath_t, typename DistributionRandom_t>
class DistributionBaseClass : public DistributionQObjectBaseClass {
public:
  DistributionBaseClass() : generator_(urng_, distributionRandom_) {}
  DistributionBaseClass(double a, double b)
      : distributionMath_(a, b), distributionRandom_(a, b),
        generator_(urng_, distributionRandom_) {}

  doubleVec drawNumbers(uint Ntot) {
    doubleVec values, tempValues;
    double x;
    size_t Ngen = Ntot;
    while (Ngen > 0) {
      for (size_t ii = 0; ii < Ngen; ii++) {
        // generate random number
        x = generator_();
        // check if within bounds
        if (isWithinBounds(x))
          tempValues.push_back(x);
      }
      // update generated numbers
      values.insert(values.end(), tempValues.begin(), tempValues.end());
      tempValues.resize(0);
      Ngen = Ntot - values.size();
    }
    return values;
  }
  double pdf(double x) { return boost::math::pdf(distributionMath_, x); }
  double cdf(double x) { return boost::math::cdf(distributionMath_, x); }
  double quantile(double x) {
    return boost::math::quantile(distributionMath_, x);
  }

protected:
  // to be implemented in actual distribution class
  virtual void setDistributionParameters(ValuePair) = 0;
  virtual void setDistributionMoments(ValuePair) = 0;

  DistributionMath_t distributionMath_;
  DistributionRandom_t distributionRandom_;
  boost::variate_generator<boost::mt19937, DistributionRandom_t> generator_;
};

class UniformDistribution
    : public DistributionBaseClass<
          boost::math::uniform_distribution<double>,
          boost::random::uniform_real_distribution<double>> {
public:
  UniformDistribution();
  virtual ~UniformDistribution();

private:
  void setDistributionParameters(ValuePair params);
  void setDistributionMoments(ValuePair moms);
  void parameters2moments();
  void moments2parameters();
  void init();
};

class NormalDistribution
    : public DistributionBaseClass<boost::math::normal_distribution<double>,
                                   boost::random::normal_distribution<double>> {
public:
  NormalDistribution();
  virtual ~NormalDistribution();

private:
  void setDistributionParameters(ValuePair params);
  void setDistributionMoments(ValuePair moms);
  void parameters2moments();
  void moments2parameters();
  void init();
};

class LognormalDistribution
    : public DistributionBaseClass<
          boost::math::lognormal_distribution<double>,
          boost::random::lognormal_distribution<double>> {
public:
  LognormalDistribution();
  virtual ~LognormalDistribution();

private:
  void setDistributionParameters(ValuePair params);
  void setDistributionMoments(ValuePair moms);
  void parameters2moments();
  void moments2parameters();
  void init();
};

class ExponentialDistribution
    : public DistributionBaseClass<
          boost::math::exponential_distribution<double>,
          boost::random::exponential_distribution<double>> {
public:
  ExponentialDistribution();
  virtual ~ExponentialDistribution();

  // reimplementation
  bool isDefined();
  bool isSecondParameterNeeded();

private:
  void setDistributionParameters(ValuePair params);
  void setDistributionMoments(ValuePair moms);
  void parameters2moments();
  void moments2parameters();
  void init();
};

class GammaDistribution
    : public DistributionBaseClass<boost::math::gamma_distribution<>,
                                   boost::random::gamma_distribution<double>> {
public:
  GammaDistribution();
  virtual ~GammaDistribution();

private:
  void setDistributionParameters(ValuePair params);
  void setDistributionMoments(ValuePair moms);
  void parameters2moments();
  void moments2parameters();
  void init();
};

} // namespace model

#endif // MODEL_DISTRIBUTION_H_

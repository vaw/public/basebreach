// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * GeometryWithNumerics.h
 *
 *  Created on: May 28, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_GEOMETRYWITHNUMERICS_H_
#define MODEL_GEOMETRYWITHNUMERICS_H_

#include <vector>

#include "model/Geometry.h"
#include "types.h"

namespace model {

// Gauss-Kronrod quadrature (using alglib)
class GeometryGauKron : public model::GeometryPolynom {
public:
  GeometryGauKron(double initLevel, double beta, api::IDam_t theDam);
  virtual ~GeometryGauKron();
  double doIntegration(double a, double b);

private:
  void int_function_1_func(double x, double xminusa, double bminusx, double &y,
                           void *ptr);
};

// adaptive algorithm using simpson and trapez
class GeometryAdaptiveTrapezSimpson : public model::GeometryPolynom {
public:
  GeometryAdaptiveTrapezSimpson(double initLevel, double beta,
                                api::IDam_t theDam);
  virtual ~GeometryAdaptiveTrapezSimpson();
  double doIntegration(double a, double b);

private:
  uint RECMAX;
  double recursionFunction(double a, double b, double fa, double fm, double fb,
                           uint &ii);
  double I;
  double EPS, Idelta;
};

// Gaussian-Legendre rule as implemented integration routine
class GeometryGauLeg : public model::GeometryPolynom {
public:
  GeometryGauLeg(double initLevel, double beta, api::IDam_t theDam);
  virtual ~GeometryGauLeg();
  double doIntegration(double a, double b);

private:
  uint n, m;
  double EPS;
  double z1, zi;
  double xm, xl;
  double pp, p3, p2, p1;
  std::vector<uint> N;
  doubleVecVec Z, W;
};

// Simpson rule as implemented integration routine
class GeometrySimpson : public model::GeometryPolynom {
public:
  GeometrySimpson(double initLevel, double beta, api::IDam_t theDam);
  virtual ~GeometrySimpson();
  double doIntegration(double a, double b);

private:
  uint JMAX, iteration;
  double s, sTemp, os, osTemp;
  double xi, tnm, sum, del;
};

} // namespace model

#endif // MODEL_GEOMETRYWITHNUMERICS_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * DBFactory.h
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_DBFACTORY_H_
#define MODEL_DBFACTORY_H_

#include "api/IDBModel.h"
#include "api/IDam.h"
#include "api/IErosion.h"
#include "api/IGeometry.h"
#include "api/IHydraulics.h"
#include "api/IOutput.h"
#include "api/IPDESystemSolver.h"
#include "api/IParser.h"
#include "api/IReservoir.h"
#include "types.h"

namespace model {

class DBFactory {
public:
  DBFactory(api::IParser_t);
  virtual ~DBFactory();
  api::IDBModel_t createDBModel();

private:
  api::IDBModel_t createPeterModel();
  api::IDBModel_t createPeterCalibrationModel();
  api::IDBModel_t createMacchioneModel();
  api::IDBModel_t createAwelModel();
  api::IDam_t createDamObject();
  api::IPDESystemSolver_t createSolverObject(api::IReservoir_t,
                                             api::IGeometry_t,
                                             api::IHydraulics_t,
                                             api::IErosion_t);
  api::outputVec createOutputObjects(api::IReservoir_t, api::IGeometry_t,
                                     api::IHydraulics_t, api::IErosion_t,
                                     api::IPDESystemSolver_t);
  api::IDBModel_t createModel(api::IDam_t, api::IReservoir_t, api::IGeometry_t,
                              api::IHydraulics_t, api::IErosion_t,
                              api::IPDESystemSolver_t, api::outputVec outputs);
  api::IParser_t parser_;
};

} // namespace model

#endif // MODEL_DBFACTORY_H_

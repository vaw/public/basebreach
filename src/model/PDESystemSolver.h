// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * PDESystemSolver.h
 *
 *  Created on: Apr 29, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_PDESYSTEMSOLVER_H_
#define MODEL_PDESYSTEMSOLVER_H_

#include <string>

#include "ExceptionHandling.h"
#include "api/IErosion.h"
#include "api/IGeometry.h"
#include "api/IHydraulics.h"
#include "api/IPDESystemSolver.h"
#include "api/IReservoir.h"
#include "types.h"

namespace model {

class PDESystem : public api::IPDESystemSolver {
public:
  PDESystem() { ; }
  PDESystem(api::IReservoir_t theReservoir, api::IGeometry_t theGeometry,
            api::IHydraulics_t theHydraulics, api::IErosion_t theErosion);
  virtual ~PDESystem();
  void setSolverArgument(double arg) { solverArgument_ = arg; }
  double getSolverArgument() { return solverArgument_; }
  double getTime() { return time_; }
  void debug();
  stringVec getImplementedSolvers() {
    return {"euler_explicit", "rk2_midpoint",   "rk2_heun",
            "rk4_classic",    "euler_variable", "euler_2step"};
  }
  stringVec getArgumentDescriptions() {
    return {"initial time step",
            "initial time step",
            "initial time step",
            "initial time step",
            "global limit for relative change",
            "truncation error"};
  }

protected:
  void setNewSystemState(const doubleVec &);
  void calcDerivatives(const doubleVec &);
  double solverArgument_;
  double time_;
  double dt_;
  static const int dim_ = 2;
  doubleVec Y_;
  doubleVec dYdt_;
  doubleVec Y0_;

private:
  void initSystem();
  void updateSystemState();
  api::IReservoir_t reservoir_;
  api::IGeometry_t geometry_;
  api::IHydraulics_t hydraulics_;
  api::IErosion_t erosion_;
};

class PDESolverEmpty : public PDESystem {
public:
  PDESolverEmpty() { ; }
  virtual ~PDESolverEmpty() { ; }
  void evaluate() { ; }
  std::string getArgumentDescription() { return ""; }
};

class PDESolverExplicitEulerFixedTimeStep : public PDESystem {
public:
  PDESolverExplicitEulerFixedTimeStep(api::IReservoir_t theReservoir,
                                      api::IGeometry_t theGeometry,
                                      api::IHydraulics_t theHydraulics,
                                      api::IErosion_t theErosion);
  virtual ~PDESolverExplicitEulerFixedTimeStep();
  void evaluate();
};

class PDESolverRK2StageMidpoint : public PDESystem {
public:
  PDESolverRK2StageMidpoint(api::IReservoir_t theReservoir,
                            api::IGeometry_t theGeometry,
                            api::IHydraulics_t theHydraulics,
                            api::IErosion_t theErosion);
  virtual ~PDESolverRK2StageMidpoint();
  void evaluate();
};

class PDESolverRK2StageHeun : public PDESystem {
public:
  PDESolverRK2StageHeun(api::IReservoir_t theReservoir,
                        api::IGeometry_t theGeometry,
                        api::IHydraulics_t theHydraulics,
                        api::IErosion_t theErosion);
  virtual ~PDESolverRK2StageHeun();
  void evaluate();
};

class PDESolverRK4StageClassic : public PDESystem {
public:
  PDESolverRK4StageClassic(api::IReservoir_t theReservoir,
                           api::IGeometry_t theGeometry,
                           api::IHydraulics_t theHydraulics,
                           api::IErosion_t theErosion);
  virtual ~PDESolverRK4StageClassic();
  void evaluate();
};

class PDESolverExplicitEulerVariableTimeStep : public PDESystem {
public:
  PDESolverExplicitEulerVariableTimeStep(api::IReservoir_t theReservoir,
                                         api::IGeometry_t theGeometry,
                                         api::IHydraulics_t theHydraulics,
                                         api::IErosion_t theErosion);
  virtual ~PDESolverExplicitEulerVariableTimeStep();
  void evaluate();
};

class PDESolverExplicitEuler2StepVariable : public PDESystem {
public:
  PDESolverExplicitEuler2StepVariable(api::IReservoir_t theReservoir,
                                      api::IGeometry_t theGeometry,
                                      api::IHydraulics_t theHydraulics,
                                      api::IErosion_t theErosion);
  virtual ~PDESolverExplicitEuler2StepVariable();
  void evaluate();

private:
  double dt0_;
  double r_;
  double safetyMargin_;
};

} // namespace model

#endif // MODEL_PDESYSTEMSOLVER_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Output.cpp
 *
 *  Created on: May 5, 2014
 *      Author: samuelpeter
 */

#include "model/Output.h"

#include <time.h>

#include <string>
#include <vector>

#include "ExceptionHandling.h"

namespace model {

Output::Output(api::IParser_t theParser, api::IReservoir_t theReservoir,
               api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
               api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver) {
  parser_ = theParser;
  reservoir_ = theReservoir;
  geometry_ = theGeometry;
  hydraulics_ = theHydraulics;
  erosion_ = theErosion;
  solver_ = theSolver;
  timeStep_ = 0.0;
  lastOutputWritten_ = 0.0;
  headerIsWritten_ = false;
}

Output::~Output() {
  // Auto-generated destructor stub
}

void Output::setOutputTimeStep(double dt) {
  timeStep_ = dt;
  lastOutputWritten_ = -dt;
}

void Output::write() {
  if (solver_->getTime() >= lastOutputWritten_ + timeStep_) {
    if (!headerIsWritten_) {
      this->writeHeader();
      headerIsWritten_ = true;
    }
    this->writeOutput();
    lastOutputWritten_ += timeStep_;
  }
}

void Output::setValues(std::vector<std::string> values) {
  if (values.size() > 0) {
    for (uint ii = 0; ii < values.size(); ii++) {
      if (values[ii] == "Y")
        valueIds_.push_back(Y);
      else if (values[ii] == "breach_level")
        valueIds_.push_back(breach_level);
      else if (values[ii] == "breach_width")
        valueIds_.push_back(breach_width);
      else if (values[ii] == "breach_discharge")
        valueIds_.push_back(breach_discharge);
      else if (values[ii] == "reservoir_level")
        valueIds_.push_back(reservoir_level);
      else if (values[ii] == "reservoir_surface")
        valueIds_.push_back(reservoir_surface);
      else if (values[ii] == "breach_volume_change_rate")
        valueIds_.push_back(breach_volume_change_rate);
      else if (values[ii] == "water_depth")
        valueIds_.push_back(water_depth);
      else if (values[ii] == "flow_velocity")
        valueIds_.push_back(flow_velocity);
      else if (values[ii] == "flow_area")
        valueIds_.push_back(flow_area);
      else if (values[ii] == "wetted_perimeter")
        valueIds_.push_back(wetted_perimeter);
      else if (values[ii] == "hydraulic_radius")
        valueIds_.push_back(hydraulic_radius);
      else if (values[ii] == "erodible_perimeter")
        valueIds_.push_back(erodible_perimeter);
      else if (values[ii] == "transport_rate")
        valueIds_.push_back(transport_rate);
      else if (values[ii] == "sediment_discharge")
        valueIds_.push_back(sediment_discharge);
      else if (values[ii] == "ritter")
        valueIds_.push_back(ritter);
      else if (values[ii] == "all") {
        for (int jj = Y; jj != N_enums; jj++) {
          valueIds_.push_back(static_cast<valueId>(jj));
        }
      } else {
        std::ostringstream sout;
        sout << "Error in output values: entry '" << values[ii]
             << "' not valid!\n";
        sout << "valid "
                "are:\tY\n\t\tbreach_level\n\t\tbreach_width\n\t\tbreach_"
                "discharge\n\t\treservoir_level\n\t\treservoir_surface";
        sout << "\n\t\tbreach_volume_change_rate\n\t\twater_depth\n\t\tflow_"
                "velocity\n\t\tflow_area\n\t\twetted_perimeter";
        sout << "\n\t\thydraulic_radius\n\t\terodible_perimeter\n\t\ttransport_"
                "rate\n\t\tsediment_discharge\n\t\tall";
        BB_EXCEPTION(sout.str());
      }
    }
  }
}

double Output::getValue(valueId id) {
  switch (id) {
  case Y:
    return geometry_->getLevel();
    break;
  case breach_level:
    return geometry_->getBreachlevel();
    break;
  case reservoir_level:
    return reservoir_->getLevel();
    break;
  case breach_width:
    return geometry_->getAverageWidth();
    break;
  case breach_top_width:
    return geometry_->getTopWidth();
    break;
  case water_depth:
    return hydraulics_->getCriticalWaterDepth();
    break;
  case flow_area:
    return hydraulics_->getFlowArea();
    break;
  case flow_velocity:
    return hydraulics_->getFlowVelocity();
    break;
  case breach_discharge:
    return hydraulics_->getDischarge();
    break;
  case wetted_perimeter:
    return hydraulics_->getWettedPerimeter();
    break;
  case hydraulic_radius:
    return hydraulics_->getHydraulicRadius();
    break;
  case reservoir_surface:
    return reservoir_->getWaterSurface();
    break;
  case erodible_perimeter:
    return hydraulics_->getErodiblePerimeter();
    break;
  case transport_rate:
    return erosion_->getTransportRate();
    break;
  case sediment_discharge:
    return erosion_->getCalibrationCoefficient() *
           erosion_->getTransportRate() * hydraulics_->getErodiblePerimeter();
    break;
  case breach_volume_change_rate:
    return geometry_->getVolumeErosionRate();
    break;
  case ritter:
    return 1.0 - hydraulics_->isSteady();
    break;
  case N_enums:
    return ritter;
    break;
  default:
    BB_EXCEPTION("this should never happen!");
  }
}

std::string Output::getHeader(valueId id) {
  switch (id) {
  case Y:
    return "Y[m]";
    break;
  case breach_level:
    return "breach level [m]";
    break;
  case reservoir_level:
    return "reservoir level [m]";
    break;
  case breach_width:
    return "breach width [m]";
    break;
  case breach_top_width:
    return "breach top width [m]";
    break;
  case water_depth:
    return "water depth [m]";
    break;
  case flow_area:
    return "flow area [m2]";
    break;
  case flow_velocity:
    return "flow velocity [m/s]";
    break;
  case breach_discharge:
    return "breach discharge [m3/s]";
    break;
  case wetted_perimeter:
    return "wetted perimeter [m]";
    break;
  case hydraulic_radius:
    return "hydraulic radius [m]";
    break;
  case reservoir_surface:
    return "reservoir surface [m2]";
    break;
  case erodible_perimeter:
    return "erodible perimeter [m]";
    break;
  case transport_rate:
    return "transport rate [m2/s]";
    break;
  case sediment_discharge:
    return "sediment discharge [m3/s]";
    break;
  case breach_volume_change_rate:
    return "breach volume change rate [m3/m]";
    break;
  case ritter:
    return "non-steady solution [-]";
    break;
  case N_enums:
    return "N_enums";
    break;
  default:
    BB_EXCEPTION("this should never happen!");
  }
}

ConsoleOutput::ConsoleOutput(api::IParser_t theParser,
                             api::IReservoir_t theReservoir,
                             api::IGeometry_t theGeometry,
                             api::IHydraulics_t theHydraulics,
                             api::IErosion_t theErosion,
                             api::IPDESystemSolver_t theSolver)
    : Output(theParser, theReservoir, theGeometry, theHydraulics, theErosion,
             theSolver) {
  time_ = 0;
}

ConsoleOutput::~ConsoleOutput() {
  // Auto-generated destructor stub
}

void ConsoleOutput::init() {
  parser_->printLogContent();
  double prec = parser_->getBlockWithName("SIMULATION")
                    ->getTagWithName("precision_at_qmax")
                    ->getDoubleValue();
  BB_MESSAGE("-----------------------------------------------------------------"
             "----------------\n");
  // starting calculation ...
  BB_MESSAGE(" - simulation starts ... wait until convergence is reached "
             "(needed precision: "
             << prec << ")!");
  // setting timer
  time_ = clock();
}

void ConsoleOutput::writeHeader() {
  BB_MESSAGE("-----------------------------------------------------------------"
             "----------------\n");
  std::ostringstream sout;
  sout << "time [s]";
  for (uint ii = 0; ii < valueIds_.size(); ii++)
    sout << ", " << this->getHeader(valueIds_[ii]);
  BB_MESSAGE(sout.str());
  BB_MESSAGE("-----------------------------------------------------------------"
             "----------------");
}

void ConsoleOutput::writeOutput() {
  std::ostringstream sout;
  if (valueIds_.size() > 0) {
    sout << solver_->getTime();
    for (uint ii = 0; ii < valueIds_.size(); ii++)
      sout << ", " << this->getValue(valueIds_[ii]);
  }
  BB_MESSAGE(sout.str());
}

void ConsoleOutput::writeQpLog(double qp, double tp) {
  BB_MESSAGE(" - calculated Qp = " << qp << " [m3/s] at t = " << tp
                                   << "[s] with solver argument "
                                   << solver_->getSolverArgument());
}

void ConsoleOutput::finalize() {
  this->write(); // make sure that the last calculated time is written as well
  BB_MESSAGE("-----------------------------------------------------------------"
             "----------------");
  BB_MESSAGE(" - runtime: " << (double)(clock() - time_) / CLOCKS_PER_SEC
                            << " sec");
  BB_MESSAGE("***************************************************");
}

AsciiOutput::AsciiOutput(api::IParser_t theParser,
                         api::IReservoir_t theReservoir,
                         api::IGeometry_t theGeometry,
                         api::IHydraulics_t theHydraulics,
                         api::IErosion_t theErosion,
                         api::IPDESystemSolver_t theSolver)
    : Output(theParser, theReservoir, theGeometry, theHydraulics, theErosion,
             theSolver) {}

AsciiOutput::~AsciiOutput() {
  // Auto-generated destructor stub
}

void AsciiOutput::init() {
  std::string filename;
  api::tagVec tags = parser_->getProjectBlock()->getTags();
  for (uint ii = 0; ii < tags.size(); ii++) {
    if (tags[ii]->getName() == "title")
      filename = tags[ii]->getStringValue();
  }
  filename.append(".out");
  file_.open(filename.c_str(), std::ios::out);
  if (!file_.is_open()) {
    BB_EXCEPTION("Could not open the output file.");
  }
}

void AsciiOutput::writeHeader() {
  file_ << "# time [s]";
  for (uint ii = 0; ii < valueIds_.size(); ii++)
    file_ << ", " << this->getHeader(valueIds_[ii]);
  file_ << std::endl;
}

void AsciiOutput::writeOutput() {
  if (valueIds_.size() > 0) {
    file_ << solver_->getTime();
    for (uint ii = 0; ii < valueIds_.size(); ii++)
      file_ << ", " << this->getValue(valueIds_[ii]);
    file_ << std::endl;
  }
}

void AsciiOutput::finalize() {
  this->writeOutput(); // make sure that the last calculated time is written as
                       // well
  if (file_.is_open()) {
    file_.close();
  }
}

StdpyBBOutput::StdpyBBOutput(api::IParser_t theParser,
                             api::IReservoir_t theReservoir,
                             api::IGeometry_t theGeometry,
                             api::IHydraulics_t theHydraulics,
                             api::IErosion_t theErosion,
                             api::IPDESystemSolver_t theSolver)
    : Output(theParser, theReservoir, theGeometry, theHydraulics, theErosion,
             theSolver) {}

StdpyBBOutput::~StdpyBBOutput() {
  // Auto-generated destructor stub
}

void StdpyBBOutput::init() { data_.resize(valueIds_.size() + 1); }

const doubleVecVec &StdpyBBOutput::getData() { return data_; }

void StdpyBBOutput::writeOutput() {
  // adding a row to the array
  if (valueIds_.size() > 0) {
    data_.at(0).push_back(solver_->getTime());
    for (uint ii = 0; ii < valueIds_.size(); ii++)
      data_.at(ii + 1).push_back(this->getValue(valueIds_[ii]));
  }
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Parser.cpp
 *
 *  Created on: May 1, 2014
 *      Author: samuelpeter
 */

#include "model/Parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <QtCore>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "ExceptionHandling.h"
#include "types.h"

namespace model {

ParserTag::ParserTag(std::string name, std::string value) {
  name_ = name;
  value_ = value;
  hasDouble_ = false;
  valueDouble_ = 0.0;
  this->checkDouble();
  hasList_ = false;
}

ParserTag::ParserTag(std::string name, double value) {
  name_ = name;
  value_ = "";
  hasDouble_ = true;
  valueDouble_ = value;
  hasList_ = false;
}

ParserTag::~ParserTag() {
  // Auto-generated destructor stub
}

void ParserTag::setValue(std::string value) {
  value_ = value;
  hasList_ = false;
  valueList_.clear();
  this->checkDouble();
}

void ParserTag::setValue(double value) {
  valueDouble_ = value;
  std::stringstream s;
  s << value;
  value_ = s.str();
  value_ = "";
  hasDouble_ = true;
  hasList_ = false;
  valueList_.clear();
}

bool ParserTag::hasValue() {
  if (!this->hasList()) {
    if (this->hasDouble()) {
      return true;
    } else {
      if (this->getStringValue() != "")
        return true;
    }
  } else {
    if (valueList_.size() > 0)
      return true;
  }
  return false;
}

std::string ParserTag::getValueAsString() {
  std::ostringstream s;
  if (!hasList()) {
    if (hasDouble()) {
      s << getDoubleValue();
    } else {
      s << getStringValue();
    }
  } else {
    s << "(";
    uint kk;
    for (kk = 0; kk < valueList_.size() - 1; kk++) {
      s << valueList_[kk] << ", ";
    }
    s << valueList_[kk] << ")";
  }
  return s.str();
}

void ParserTag::addValue(std::string value) {
  // in case this is the second value, we have first to create the list, and
  // delete the single value stored
  if (valueList_.empty()) {
    hasList_ = true;
    if (value_ != "") {
      valueList_.push_back(value_);
      value_ = "";
    }
  }
  valueList_.push_back(value);
}

void ParserTag::checkDouble() {
  // check if string is not empty
  if (value_.size() != 0) {
    // so, our value can look like this:
    // -1.0e+23
    // note [+:1] means expect a plus and go to test1.
    uint ii = 0;
    int stage = 1;
    while (ii < value_.size()) {
      if (stage == -1)
        break;
      switch (stage) {
      case 0:
        break;
      case 1:
        stage = doubleTestStage1(value_[ii]);
        break;
      case 2:
        stage = doubleTestStage2(value_[ii]);
        break;
      case 3:
        stage = doubleTestStage3(value_[ii]);
        break;
      case 4:
        stage = doubleTestStage4(value_[ii]);
        break;
      case 5:
        stage = doubleTestStage5(value_[ii]);
        break;
      case 6:
        stage = doubleTestStage6(value_[ii]);
        break;
      case 7:
        stage = doubleTestStage7(value_[ii]);
        break;
      }
      ii++;
    }
    // false if not whole string was parsed
    if (ii == value_.size()) {
      // valid exists for this expressions were stages including a w:0
      // or if the last char was a .
      if (stage == 3 || stage == 4 || stage == 5 || stage == 7) {
        hasDouble_ = true;
        valueDouble_ = atof(value_.c_str());
      }
    } else {
      hasDouble_ = false;
      valueDouble_ = 0.0;
    }
  }
}

ParserBlock::ParserBlock(std::string name) { name_ = name; }

ParserBlock::~ParserBlock() {
  // Auto-generated destructor stub
}

api::IParserTag_t ParserBlock::getTagWithName(std::string name) {
  for (uint ii = 0; ii < tags_.size(); ii++) {
    if (tags_[ii]->getName() == name) {
      return tags_[ii];
    }
  }
  BB_WARNING("no parser tag found in block '" << name_ << "' with name '"
                                              << name << "'!");
}

void ParserBlock::removeEmptyTags() {
  BB_ASSERT(tags_.size() < INT_MAX, "tag count overflow");
  int size = static_cast<int>(tags_.size());
  for (int ii = size - 1; ii >= 0; ii--) {
    if (!tags_[ii]->hasValue()) {
      tags_.erase(tags_.begin() + ii);
    }
  }
}

Parser::Parser() {
  state_ = parseBlockName;
  blockIndex_ = -1;
  tagIndex_ = -1;
  this->createEmptyParser();
  this->defineDefault();
}

Parser::~Parser() {
  // Auto-generated destructor stub
}

api::IParserBlock_t Parser::getProjectBlock() {
  int ii = this->getBlockIndexViaName("PROJECT");
  if (ii >= 0) {
    return blocks_[ii];
  } else {
    BB_EXCEPTION("no project block defined!")
  }
}

void Parser::readInputFile(std::string filename) {
  std::ifstream inFile(filename.c_str());
  std::string line;
  std::string text = "";
  // try to open the file
  if (inFile.is_open()) {
    // and append each line to one string
    while (std::getline(inFile, line)) {
      this->removeComments(line);
      text += " " + line; // add white space because 'getline' discards the
                          // newline character
    }
  } else {
    BB_EXCEPTION("Could not open the command file '" << filename << "'.");
  }
  // now parse each character
  // BB_MESSAGE("-> parsing input file '" << filename << "' ...")
  name_ = "";
  for (uint ii = 0; ii < text.size(); ii++) {
    char ch = text[ii];
    switch (state_) {
    case parseBlockName:
      this->doBlockParsing(ch);
      break;
    case parseTagName:
      this->doTagNameParsing(ch);
      break;
    case parseTagValue:
      this->doTagValueParsing(ch);
      break;
    case parseTagValueList:
      this->doTagValueListParsing(ch);
      break;
    }
  }
  // BB_MESSAGE("-> successfully read!")
}

void Parser::readInputPars(QMap<QString, QMap<QString, QVariant>> pars,
                           QString modelId) {
  // Not all models support absolute bottom_level elevation, so we instead
  // normalise all elevations to a bottom_level of zero.
  double offset = pars["DAM"]["bottom_level"].toDouble();

  if (modelId == "bbPeter" || modelId == "bbPeterCal") {
    if (modelId == "bbPeter")
      this->getTagOfBlockViaNames("PROJECT", "model")->setValue("bbPeter");
    if (modelId == "bbPeterCal")
      this->getTagOfBlockViaNames("PROJECT", "model")->setValue("bbPeterCal");
    // dam object
    this->getTagOfBlockViaNames("DAM", "height")
        ->setValue(pars["DAM"]["height"].toDouble() - offset);
    this->getTagOfBlockViaNames("DAM", "bottom_level")->setValue(0.0);
    this->getTagOfBlockViaNames("DAM", "crest_width")
        ->setValue(pars["DAM"]["crest_width"].toDouble());
    this->getTagOfBlockViaNames("DAM", "embankment_slope")
        ->setValue(pars["DAM"]["embankment_slope"].toDouble());
    // breach geometry object
    this->getTagOfBlockViaNames("BREACH", "angle")
        ->setValue(pars["BREACH"]["angle"].toDouble());
    this->getTagOfBlockViaNames("BREACH", "initial_level")
        ->setValue(pars["BREACH"]["initial_level"].toDouble() - offset);
    // reservoir object
    this->getTagOfBlockViaNames("RESERVOIR", "volume")
        ->setValue(pars["RESERVOIR"]["volume"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "shape_exponent")
        ->setValue(pars["RESERVOIR"]["shape_exponent"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "initial_level")
        ->setValue(pars["RESERVOIR"]["initial_level"].toDouble() - offset);
    auto value = pars["RESERVOIR"]["inflow"];
    auto inflow_tag = this->getTagOfBlockViaNames("RESERVOIR", "inflow");
    bool is_double = true;
    double val = value.toDouble(&is_double);
    if (is_double) {
      inflow_tag->setValue(val);
    } else {
      inflow_tag->setValue(value.toString().toStdString());
    }
    // erosion object
    this->getTagOfBlockViaNames("EROSION", "scaling_coefficient")
        ->setValue(pars["EROSION"]["scaling_coefficient"].toDouble());
    this->getTagOfBlockViaNames("EROSION", "exponent_v")
        ->setValue(pars["EROSION"]["exponent_v"].toDouble());
    this->getTagOfBlockViaNames("EROSION", "exponent_rhy")
        ->setValue(pars["EROSION"]["exponent_rhy"].toDouble());
    // project block
    this->getTagOfBlockViaNames("PROJECT", "title")->setValue("pyBB");
    this->getTagOfBlockViaNames("PROJECT", "author")->setValue("pyBB");
  } else if (modelId == "bbMacchione") {
    this->getTagOfBlockViaNames("PROJECT", "model")->setValue("bbMacchione");
    // dam object
    this->getTagOfBlockViaNames("DAM", "height")
        ->setValue(pars["DAM"]["height"].toDouble() - offset);
    this->getTagOfBlockViaNames("DAM", "bottom_level")->setValue(0.0);
    this->getTagOfBlockViaNames("DAM", "crest_width")
        ->setValue(pars["DAM"]["crest_width"].toDouble());
    this->getTagOfBlockViaNames("DAM", "embankment_slope")
        ->setValue(pars["DAM"]["embankment_slope"].toDouble());
    // breach geometry object
    this->getTagOfBlockViaNames("BREACH", "angle")
        ->setValue(pars["BREACH"]["angle"].toDouble());
    this->getTagOfBlockViaNames("BREACH", "initial_level")
        ->setValue(pars["BREACH"]["initial_level"].toDouble() - offset);
    // reservoir object
    this->getTagOfBlockViaNames("RESERVOIR", "volume")
        ->setValue(pars["RESERVOIR"]["volume"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "shape_exponent")
        ->setValue(pars["RESERVOIR"]["shape_exponent"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "initial_level")
        ->setValue(pars["RESERVOIR"]["initial_level"].toDouble() - offset);
    auto value = pars["RESERVOIR"]["inflow"];
    auto inflow_tag = this->getTagOfBlockViaNames("RESERVOIR", "inflow");
    bool is_double = true;
    double val = value.toDouble(&is_double);
    if (is_double) {
      inflow_tag->setValue(val);
    } else {
      inflow_tag->setValue(value.toString().toStdString());
    }
    // erosion object
    this->getTagOfBlockViaNames("MACCHIONE", "char_velocity")
        ->setValue(pars["MACCHIONE"]["char_velocity"].toDouble());
    // project block
    this->getTagOfBlockViaNames("PROJECT", "title")->setValue("pyBB");
    this->getTagOfBlockViaNames("PROJECT", "author")->setValue("pyBB");
  } else if (modelId == "bbAwel") {
    this->getTagOfBlockViaNames("PROJECT", "model")->setValue("bbAwel");
    // dam object
    this->getTagOfBlockViaNames("DAM", "height")
        ->setValue(pars["DAM"]["height"].toDouble() - offset);
    this->getTagOfBlockViaNames("DAM", "bottom_level")->setValue(0.0);
    this->getTagOfBlockViaNames("DAM", "crest_width")
        ->setValue(pars["DAM"]["crest_width"].toDouble());
    this->getTagOfBlockViaNames("DAM", "embankment_slope")
        ->setValue(pars["DAM"]["embankment_slope"].toDouble());
    // reservoir object
    this->getTagOfBlockViaNames("RESERVOIR", "volume")
        ->setValue(pars["RESERVOIR"]["volume"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "shape_exponent")
        ->setValue(pars["RESERVOIR"]["shape_exponent"].toDouble());
    this->getTagOfBlockViaNames("RESERVOIR", "initial_level")
        ->setValue(pars["RESERVOIR"]["initial_level"].toDouble() - offset);
    this->getTagOfBlockViaNames("RESERVOIR", "inflow")
        ->setValue(pars["RESERVOIR"]["inflow"].toString().toStdString());
    // project block
    this->getTagOfBlockViaNames("PROJECT", "title")->setValue("pyBB");
    this->getTagOfBlockViaNames("PROJECT", "author")->setValue("pyBB");
  } else {
    BB_EXCEPTION("undefined model type!")
  }
  QString t = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
  this->getTagOfBlockViaNames("PROJECT", "date")->setValue(t.toStdString());
  // Simulation info
  this->getTagOfBlockViaNames("SIMULATION", "cutoff_time")
      ->setValue(pars["SIMULATION"]["cutoff_time"].toDouble());
  this->getTagOfBlockViaNames("SIMULATION", "precision_at_qmax")
      ->setValue(pars["SIMULATION"]["precision_at_qmax"].toDouble());
  this->getTagOfBlockViaNames("SIMULATION", "erosion_limit")
      ->setValue(pars["SIMULATION"]["erosion_limit"].toDouble());
  // numerics
  this->getTagOfBlockViaNames("NUMERICS", "pde_solver_argument")
      ->setValue(pars["NUMERICS"]["pde_solver_argument"].toDouble());
  this->getTagOfBlockViaNames("NUMERICS", "integration_precision")
      ->setValue(pars["NUMERICS"]["integration_precision"].toDouble());
  this->getTagOfBlockViaNames("NUMERICS", "pde_solver")
      ->setValue(pars["NUMERICS"]["pde_solver"].toString().toStdString());
}

void Parser::setGlobalTag(std::string name, std::string value) {
  api::tagVec tags = globalBlock_->getTags();
  for (uint ii = 0; ii < tags.size(); ii++) {
    if (tags[ii]->getName() == name) {
      tags[ii]->setValue(value);
      return;
    }
  }
  BB_EXCEPTION("Error while setting global tags: '" << name << "' not valid.");
}

void Parser::setGlobalTag(std::string name, double value) {
  api::tagVec tags = globalBlock_->getTags();
  for (uint ii = 0; ii < tags.size(); ii++) {
    if (tags[ii]->getName() == name) {
      tags[ii]->setValue(value);
      return;
    }
  }
  BB_EXCEPTION("Error while setting global tags: '" << name << "' not valid.");
}

bool Parser::init() {
  // do some automatic reconfiguration of the output block
  double console_dt = -1.0;
  std::string pyBB = "false";
  // std::string simulation = "full";
  // look into the global block
  api::tagVec globalTags = globalBlock_->getTags();
  for (uint ii = 0; ii < globalTags.size(); ii++) {
    if (globalTags[ii]->getName() == "console") {
      console_dt = globalTags[ii]->getDoubleValue();
    } else if (globalTags[ii]->getName() == "pyBB") {
      pyBB = globalTags[ii]->getStringValue();
      // } else if (globalTags[ii]->getName() == "simulation") {
      //   simulation = globalTags[ii]->getStringValue();
    }
  }
  // do the logic stuff
  if (pyBB == "true") {
    // delete all output blocks, a special one will be created later in the
    // factory to pass data to python, eventually
    std::vector<uint> index;
    for (size_t ii = 0; ii < blocks_.size(); ii++) {
      if (blocks_[ii]->getName() == "OUTPUT") {
        BB_ASSERT(ii <= UINT_MAX, "index overflow");
        index.push_back(static_cast<uint>(ii));
      }
    }
    if (index.size() > 0) {
      for (size_t ii = index.size(); ii > 0; ii--) {
        blocks_[index[ii - 1]] = blocks_.back();
        blocks_.pop_back();
      }
    }
  }
  // eventually add a console output block
  if (console_dt > 0.0) {
    this->addConsoleOutput(console_dt);
  }
  // delete empty tags and blocks
  for (size_t ii = blocks_.size() - 1; ii > 0; ii--) {
    blocks_[ii]->removeEmptyTags();
    if (blocks_[ii]->getNumTags() == 0)
      blocks_.erase(blocks_.begin() + ii);
  }
  return true;
}

void Parser::printContent() {
  for (uint ii = 0; ii < blocks_.size(); ii++) {
    api::IParserBlock_t block = blocks_.at(ii);
    BB_MESSAGE(block->getName());
    api::tagVec tags = block->getTags();
    for (uint jj = 0; jj < tags.size(); jj++) {
      api::IParserTag_t tag = tags.at(jj);
      BB_MESSAGE("\t" << tag->getName());
      if (tag->hasList()) {
        stringVec values = tag->getValueList();
        for (uint kk = 0; kk < values.size(); kk++)
          BB_MESSAGE("\t\t" << values.at(kk))
      } else {
        BB_MESSAGE("\t\t" << tag->getValueAsString())
      }
    }
  }
}

void Parser::printLogContent() {
  BB_MESSAGE(" - BASEbreach model successfully created ...")
  api::tagVec tags = getProjectBlock()->getTags();
  BB_MESSAGE("\tPROJECT:")
  for (uint ii = 0; ii < tags.size(); ii++) {
    if (tags[ii]->hasValue()) {
      BB_MESSAGE("\t\t" << tags[ii]->getName() << " = "
                        << tags[ii]->getValueAsString())
    }
  }
  BB_MESSAGE(" - with input parameters:")
  for (uint ii = 0; ii < blocks_.size(); ii++) {
    std::string blockName = blocks_[ii]->getName();
    if (blockName != "PROJECT" && blockName != "OUTPUT") {
      BB_MESSAGE("\t" << blockName << ":")
      tags = blocks_[ii]->getTags();
      for (uint jj = 0; jj < tags.size(); jj++) {
        if (tags[jj]->hasValue()) {
          BB_MESSAGE("\t\t" << tags[jj]->getName() << " = "
                            << tags[jj]->getValueAsString())
        }
      }
    }
  }
}

void Parser::doBlockParsing(char ch) {
  if (this->isValidChar(ch)) {
    // were are in the middle of the block name
    name_ += ch;
  } else {
    // here were are either before or after the block name
    // first try to define the block name
    if (name_.size() > 0) {
      // there u go! a possible candidate for the block name
      if (name_ == "OUTPUT") {
        // in case of a OUTPUT block, add default output block
        this->addOutput();
        BB_ASSERT(blocks_.size() < INT_MAX, "block size overflow");
        blockIndex_ = static_cast<int>(blocks_.size()) - 1;
      } else {
        blockIndex_ = this->getBlockIndexViaName(name_);
      }
      if (blockIndex_ < 0) {
        BB_EXCEPTION("block name '" << name_ << "' not valid!");
      }
      name_ = ""; // delete the name buffer
    }
    // go on with block parsing until you find an opening bracket
    if (ch == '{')
      state_ = parseTagName;
  }
}

void Parser::doTagNameParsing(char ch) {
  if (this->isValidChar(ch)) {
    // were are in the middle of the tag name
    name_ += ch;
  } else {
    // here were are either before or after the tag name
    // first try to define the tag name
    if (name_.size() > 0) {
      // there u go! a possible candidate for the tag name
      tagIndex_ = this->getTagIndexOfBlockViaName(blockIndex_, name_);
      if (tagIndex_ < 0) {
        BB_EXCEPTION("tag name '" << name_ << "' not valid!");
      }
      name_ = ""; // delete the name buffer
    }
    // go on with tag name parsing until you find an equal sign or closing
    // brackets
    if (ch == '=')
      state_ = parseTagValue;
    else if (ch == '}')
      state_ = parseBlockName;
  }
}

void Parser::doTagValueParsing(char ch) {
  if (this->isValidChar(ch)) {
    // were are in the middle of the tag value
    name_ += ch;
  } else {
    if (name_.size() > 0) {
      // here we are right after the value
      // set the single value
      api::tagVec tags = blocks_[blockIndex_]->getTags();
      tags[tagIndex_]->setValue(name_);
      name_ = ""; // delete the name buffer
      tagIndex_ = -1;
      // change to tag name parsing or block name parsing
      state_ = (ch == '}') ? parseBlockName : parseTagName;
    } else if (ch == '(') {
      // ok, we are dealing with a list of values
      state_ = parseTagValueList;
    }
  }
}

void Parser::doTagValueListParsing(char ch) {
  if (this->isValidChar(ch)) {
    // were are in the middle of a single tag value
    name_ += ch;
  } else {
    if (name_.size() > 0) {
      // we are at the end of a value: add the value to the list
      api::tagVec tags = blocks_[blockIndex_]->getTags();
      tags[tagIndex_]->addValue(name_);
      // BB_MESSAGE(tags.at(tagIndex_)->getName());
      // stringVec values =
      // getTagOfBlockViaNames("OUTPUT","values")->getValueList();//tags[tagIndex_]->getValueList();
      // for (uint ii=0; ii<values.size(); ii++) BB_MESSAGE(values.at(ii))
      name_ = ""; // delete the name buffer
      // check if we are at the end of the list
      if (ch == ')') {
        // immediately change to tag name parsing
        state_ = parseTagName;
        tagIndex_ = -1;
      }
    }
  }
}

bool Parser::isValidChar(char ch) {
  std::string nonValid = " :;\t\n\r=,{}()";
  for (uint ii = 0; ii < nonValid.size(); ii++) {
    std::string::size_type found;
    found = nonValid.find(ch, 0);
    if (found != std::string::npos) {
      return false;
    }
  }
  return true;
}

int Parser::getBlockIndexViaName(std::string blockname) {
  for (uint ii = 0; ii < blocks_.size(); ii++) {
    if (blocks_[ii]->getName() == blockname)
      return ii;
  }
  BB_EXCEPTION("no parser block found with name '" << blockname << "'!")
}

api::IParserBlock_t Parser::getBlockWithName(std::string blockname) {
  for (uint ii = 0; ii < blocks_.size(); ii++) {
    if (blocks_[ii]->getName() == blockname)
      return blocks_[ii];
  }
  BB_EXCEPTION("no parser block found with name '" << blockname << "'!")
}

int Parser::getTagIndexOfBlockViaName(int blockindex, std::string tagname) {
  api::tagVec tags = blocks_[blockindex]->getTags();
  for (uint ii = 0; ii < tags.size(); ii++) {
    if (tags[ii]->getName() == tagname)
      return ii;
  }
  BB_EXCEPTION("no parser tag found in block '"
               << blocks_[blockindex]->getName() << "' with name '" << tagname
               << "'!")
}

api::IParserTag_t Parser::getTagOfBlockViaNames(std::string blockname,
                                                std::string tagname) {
  int blockindex = this->getBlockIndexViaName(blockname);
  if (blockindex >= 0) {
    int tagindex = this->getTagIndexOfBlockViaName(blockindex, tagname);
    if (tagindex >= 0) {
      return blocks_[blockindex]->getTags()[tagindex];
    }
  }
  BB_EXCEPTION("no parser tag found in block '" << blockname << "' with name '"
                                                << tagname << "'!")
}

void Parser::removeComments(std::string &str) {
  std::string::size_type pos = str.find("//", 0);
  str = str.substr(0, pos);
}

void Parser::createEmptyParser() {
  // create all possible blocks and tags, even if not used in a specific model.
  // all logic stuff is done in the factory ...
  // project block
  api::IParserBlock_t project(new ParserBlock("PROJECT"));
  api::IParserTag_t project1(new ParserTag("title", ""));
  project->addTag(project1);
  api::IParserTag_t project2(new ParserTag("author", ""));
  project->addTag(project2);
  api::IParserTag_t project3(new ParserTag("date", ""));
  project->addTag(project3);
  api::IParserTag_t project4(new ParserTag("model", ""));
  project->addTag(project4);
  this->addBlock(project);
  // dam block
  api::IParserBlock_t dam(new ParserBlock("DAM"));
  api::IParserTag_t dam1(new ParserTag("height", ""));
  dam->addTag(dam1);
  api::IParserTag_t dam2(new ParserTag("bottom_level", ""));
  dam->addTag(dam2);
  api::IParserTag_t dam3(new ParserTag("crest_width", ""));
  dam->addTag(dam3);
  api::IParserTag_t dam4(new ParserTag("embankment_slope", ""));
  dam->addTag(dam4);
  api::IParserTag_t dam5(new ParserTag("erosion_limit", ""));
  dam->addTag(dam5);
  this->addBlock(dam);
  // reservoir block
  api::IParserBlock_t reservoir(new ParserBlock("RESERVOIR"));
  api::IParserTag_t reservoir1(new ParserTag("volume", ""));
  reservoir->addTag(reservoir1);
  api::IParserTag_t reservoir2(new ParserTag("initial_level", ""));
  reservoir->addTag(reservoir2);
  api::IParserTag_t reservoir3(new ParserTag("shape_exponent", ""));
  reservoir->addTag(reservoir3);
  api::IParserTag_t reservoir4(new ParserTag("inflow", ""));
  reservoir->addTag(reservoir4);
  this->addBlock(reservoir);
  // breach block
  api::IParserBlock_t breach(new ParserBlock("BREACH"));
  api::IParserTag_t breach1(new ParserTag("initial_level", ""));
  breach->addTag(breach1);
  api::IParserTag_t breach4(new ParserTag("angle", ""));
  breach->addTag(breach4);
  this->addBlock(breach);
  // erosion block
  api::IParserBlock_t erosion(new ParserBlock("EROSION"));
  api::IParserTag_t erosion1(new ParserTag("scaling_coefficient", ""));
  erosion->addTag(erosion1);
  api::IParserTag_t erosion2(new ParserTag("exponent_v", ""));
  erosion->addTag(erosion2);
  api::IParserTag_t erosion3(new ParserTag("exponent_rhy", ""));
  erosion->addTag(erosion3);
  this->addBlock(erosion);
  // macchione-specific erosion coeff
  api::IParserBlock_t macchione(new ParserBlock("MACCHIONE"));
  api::IParserTag_t macchione1(new ParserTag("char_velocity", ""));
  macchione->addTag(macchione1);
  this->addBlock(macchione);
  // solver block
  api::IParserBlock_t numerics(new ParserBlock("NUMERICS"));
  api::IParserTag_t numerics1(new ParserTag("pde_solver", ""));
  numerics->addTag(numerics1);
  api::IParserTag_t numerics2(new ParserTag("pde_solver_argument", ""));
  numerics->addTag(numerics2);
  api::IParserTag_t numerics3(new ParserTag("integration_precision", ""));
  numerics->addTag(numerics3);
  this->addBlock(numerics);
  // simulation block
  api::IParserBlock_t simulation(new ParserBlock("SIMULATION"));
  api::IParserTag_t simulation1(new ParserTag("cutoff_time", ""));
  simulation->addTag(simulation1);
  api::IParserTag_t simulation2(new ParserTag("precision_at_qmax", ""));
  simulation->addTag(simulation2);
  api::IParserTag_t simulation3(new ParserTag("erosion_limit", ""));
  simulation->addTag(simulation3);
  this->addBlock(simulation);
  // global tags
  api::IParserBlock_t globalBlock(new ParserBlock("GLOBALTAGS"));
  api::IParserTag_t globalTag1(new ParserTag("console", "1"));
  globalBlock->addTag(globalTag1);
  api::IParserTag_t globalTag2(new ParserTag("simulation", "full"));
  globalBlock->addTag(globalTag2);
  api::IParserTag_t globalTag3(new ParserTag("pyBB", "false"));
  globalBlock->addTag(globalTag3);
  this->addBlock(globalBlock);
  globalBlock_ = globalBlock;
}

void Parser::defineDefault() {
  this->getTagOfBlockViaNames("DAM", "bottom_level")->setValue("0.0");
}

void Parser::addOutput() {
  // output block
  api::IParserBlock_t output(new ParserBlock("OUTPUT"));
  api::IParserTag_t output1(new ParserTag("format", ""));
  output->addTag(output1);
  api::IParserTag_t output2(new ParserTag("timestep", ""));
  output->addTag(output2);
  api::IParserTag_t output3(new ParserTag("values", ""));
  output->addTag(output3);
  this->addBlock(output);
}

void Parser::addConsoleOutput(double dt) {
  // output block
  api::IParserBlock_t output(new ParserBlock("OUTPUT"));
  api::IParserTag_t output1(new ParserTag("format", "console"));
  output->addTag(output1);
  api::IParserTag_t output2(new ParserTag("timestep", dt));
  output->addTag(output2);
  api::IParserTag_t output3(new ParserTag("values", "breach_discharge"));
  output3->addValue("breach_level");
  output3->addValue("breach_width");
  output3->addValue("reservoir_level");
  output->addTag(output3);
  this->addBlock(output);
}

bool isNumberChar(char ch) {
  switch (ch) {
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    return true;
  default:
    return false;
  }
}
/** stage 1, expect [+:2,-:2,d:3] */
int doubleTestStage1(char ch) {
  if (ch == '+' || ch == '-')
    return 2;
  if (isNumberChar(ch))
    return 3;
  return -1;
}
/** stage 2, expect [d:3,e:6,E:6] */
int doubleTestStage2(char ch) {
  if (isNumberChar(ch))
    return 3;
  if (ch == 'e' || ch == 'E')
    return 6;
  return -1;
}
/** stage 3, expect [d:3,.:4,w:0,e:6,E:6] */
int doubleTestStage3(char ch) {
  if (ch == ' ' || ch == '\t')
    return 0;
  if (isNumberChar(ch))
    return 3;
  if (ch == '.')
    return 4;
  if (ch == 'e' || ch == 'E')
    return 6;
  return -1;
}
/** stage 4, expect [d:5,e:6,E:6] */
int doubleTestStage4(char ch) {
  if (isNumberChar(ch))
    return 5;
  if (ch == 'e' || ch == 'E')
    return 6;
  return -1;
}
/** stage 5, expect [d:5,e:6,E:6,w:0] */
int doubleTestStage5(char ch) {
  if (ch == ' ' || ch == '\t')
    return 0;
  if (isNumberChar(ch))
    return 5;
  if (ch == 'e' || ch == 'E')
    return 6;
  return -1;
}
/** stage 6, expect [+:7,-:7,d:7] */
int doubleTestStage6(char ch) {
  if (ch == '+' || ch == '-' || isNumberChar(ch))
    return 7;
  return -1;
}
/** stage 7, expect [d:7,w:0] */
int doubleTestStage7(char ch) {
  if (isNumberChar(ch))
    return 7;
  if (ch == ' ')
    return 0;
  return -1;
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */
 
#include "model/fullModel.h"

#include <math.h>

#include <algorithm>

#include "ExceptionHandling.h"
#include "model/DBFactory.h"
#include "model/DBModel.h"
#include "model/Dam.h"
#include "model/Erosion.h"
#include "model/GeometryWithNumerics.h"
#include "model/Hydraulics.h"
#include "model/Output.h"
#include "model/PDESystemSolver.h"
#include "model/Parser.h"
#include "model/Reservoir.h"

namespace model {

FullModel::FullModel() {
  // do nothing here
}

FullModel::FullModel(api::IParser_t parser) {
  // create the model using the factory
  model::DBFactory factory(parser);
  dbm_ = factory.createDBModel();
  // get the pointers to the needed model components
  reservoir_ = dbm_->getReservoir();
  hydraulics_ = dbm_->getHydraulics();
  geometry_ = dbm_->getGeometry();
  erosion_ = dbm_->getErosion();
}

void FullModel::runModel() {
  if (dbm_) {
    dbm_->run();
    this->getData();
    emit hasFinished();
  } else {
    BB_EXCEPTION("create model using a valid parser object!")
  }
}

stringDoubleMap FullModel::getParameters() {
  stringDoubleMap pars;
  pars["Hd"] = dbm_->getDam()->getHeight();
  pars["Yb"] = dbm_->getDam()->getBottomLevel();
  pars["wc"] = dbm_->getDam()->getCrestWidth();
  pars["se"] = dbm_->getDam()->getEmbankmentSlope();
  pars["Vw"] = dbm_->getReservoir()->getVolumeAboveBreachBottom();
  pars["alpha"] = dbm_->getReservoir()->getShapeCoefficient();
  pars["Z0"] = dbm_->getReservoir()->getInitialLevel();
  pars["Y0"] = dbm_->getGeometry()->getInitialLevel();
  pars["beta"] = dbm_->getGeometry()->getTopAngle();
  pars["e1"] = dbm_->getErosion()->getExponentA();
  pars["e2"] = dbm_->getErosion()->getExponentB();
  pars["fe"] = dbm_->getErosion()->getCalibrationCoefficient();
  return pars;
}

double FullModel::Qp() {
  if (!dbm_)
    throw;
  else
    return dbm_->getPeakDischarge();
}

double FullModel::Wb() {
  if (!dbm_) {
    throw;
  } else {
    this->setTime(T_.back());
    return this->AverageBreachWidth();
  }
}

doubleVec FullModel::getCharacteristicTimes() {
  uint N = 8;
  doubleVec times(N);
  uint ti = 0;
  uint ii = 0;
  doubleVec qc = {0.0, 0.3, 0.5, 0.7, 1.0, 0.7, 0.3, 0.0};
  // add the first time
  times.at(ti) = T_.front();
  ti++;
  ii++;
  // handle distinct cases differently
  if (this->IndexQp() == 0) {
    BB_EXCEPTION("IndexQp = 0: this case is not implemented yet");
  } else if (this->IndexQp() < N / 2) {
    BB_EXCEPTION("IndexQp < " << this->IndexQp()
                              << ": this case is not implemented yet");
  } else if (this->IndexQp() == N / 2) {
    BB_EXCEPTION("IndexQp = " << this->IndexQp()
                              << ": this case is not implemented yet");
  } else {
    double qi;
    while (ti < N - 1 && ii < T_.size() - 1) {
      if (T_.at(ii) <= TimeQp()) {
        // peak discharge not passed yet
        // the discharge we are looking for is relative to Qp-Q0
        qi = qc.at(ti) * (Qp() - Q_.front());
        if (Q_.at(ii) - Q_.front() >= qi) {
          // ok, we have advance far enough, we can now interpolate
          // do not forget to add Q0 to the value to be interpolated
          times.at(ti) =
              this->interpolateValues(Q_.at(ii - 1), T_.at(ii - 1), Q_.at(ii),
                                      T_.at(ii), qi + Q_.front());
          ti++;
        }
      } else {
        // we passed peak discharge now
        // the discharge we are looking for is relative to Qp itself
        qi = qc.at(ti) * Qp();
        if (Q_.at(ii) <= qi) {
          // ok, we have advance far enough, we can now interpolate
          times.at(ti) = this->interpolateValues(Q_.at(ii - 1), T_.at(ii - 1),
                                                 Q_.at(ii), T_.at(ii), qi);
          ti++;
        }
      }
      ii++;
    }
  }
  // add the final time
  times.at(ti) = T_.back();
  return times;
}

void FullModel::setTime(double t) {
  time_ = t;
  for (uint ii = 0; ii < T_.size(); ii++) {
    if (T_.at(ii) == t) {
      this->setLevels(Z_.at(ii), Y_.at(ii));
      break;
    } else if (T_.at(ii) > t) {
      double z = this->interpolateValues(T_.at(ii - 1), Z_.at(ii - 1),
                                         T_.at(ii), Z_.at(ii), t);
      double y = this->interpolateValues(T_.at(ii - 1), Y_.at(ii - 1),
                                         T_.at(ii), Y_.at(ii), t);
      this->setLevels(z, y);
      break;
    }
  }
}

void FullModel::setLevels(double Z, double Y) {
  reservoir_->update(Z);
  geometry_->update(Y);
  hydraulics_->update();
  erosion_->freeze(geometry_->getTopWidth() > erosion_->getErosionLimit()); // getTopWidth() * 2; MCH: getTopWidth returns width_, which is already the whole top width in geometry.cpp
  erosion_->update();
}

void FullModel::setLevelsWithIndex(int index) {
  this->setLevels(Z_.at(index), Y_.at(index));
  this->time_ = T_.at(index);
}

double FullModel::Time() { return time_; }

double FullModel::BreachLevel() { return geometry_->getBreachlevel(); }

double FullModel::ReservoirLevel() { return reservoir_->getLevel(); }

double FullModel::WaterDepth() { return hydraulics_->getCriticalWaterDepth(); }

double FullModel::FlowVelocity() { return hydraulics_->getFlowVelocity(); }

double FullModel::Discharge() { return hydraulics_->getDischarge(); }

double FullModel::Inflow() { return reservoir_->getInflow(time_); }

double FullModel::HydraulicRadius() {
  return hydraulics_->getHydraulicRadius();
}

double FullModel::WaterDepthLocation() {
  double h = hydraulics_->getCriticalWaterDepth();
  return geometry_->getLocationAtLevel(h);
}

double FullModel::ReservoirLevelLocation() {
  double z = reservoir_->getLevel();
  double breachLevel = geometry_->getBreachlevel();
  return geometry_->getLocationAtLevel(z - breachLevel);
}

doubleVecVec FullModel::BreachShape(uint N) { return geometry_->getShape(N); }

doubleVecVec FullModel::ErosionZone(uint N) {
  return hydraulics_->getErosionZone(N);
}

double FullModel::AverageBreachWidth() { return geometry_->getAverageWidth(); }

double FullModel::TransportRate() { return erosion_->getTransportRate(); }

double FullModel::SedimentDischarge() {
  return erosion_->getTransportRate() * hydraulics_->getErodiblePerimeter();
}

const doubleVec &FullModel::timeTS() { return T_; }

const doubleVec &FullModel::breachlevelTS() { return Y_; }

const doubleVec &FullModel::reservoirlevelTS() { return Z_; }

double FullModel::getOutputVariableValue(uint index) {
  //MCH. these cases are selected by the GUI -> Overview/model tab -> Plot Variable dropdown menu
  switch (index) {
  case 0:
    return this->Time();
  case 1:
    return this->Discharge();
  case 2:
    return this->FlowVelocity();
  case 3:
    return this->HydraulicRadius();
  case 4:
    return this->TransportRate();
  case 5:
    return this->SedimentDischarge();
  case 6:
    // Model uses half width internally, but we want the full width in the
    // output
    return geometry_->getTopWidth(); // * 2.0; MCH: this returns width_, which is already the whole top width in geometry.cpp
  case 7:
    return this->ReservoirLevel();
  case 8:
    return this->Inflow();
  default:
    BB_EXCEPTION("this should never happen!");
  }
}

const stringDoubleMap FullModel::getDebugData() {
  stringDoubleMap map;
  map["water level"] = this->ReservoirLevel();
  map["breach bottom"] = this->BreachLevel();
  map["breach width"] = this->AverageBreachWidth();
  map["water depth"] = this->WaterDepth();
  map["flow velocity"] = this->FlowVelocity();
  map["breach discharge"] = this->Discharge();
  map["hydraulic radius"] = this->HydraulicRadius();
  map["transport rate"] = this->TransportRate();
  map["sediment discharge"] = this->SedimentDischarge();
  return map;
}

double FullModel::interpolateValues(double x1, double v1, double x2, double v2,
                                    double xm) {
  return v1 + (v2 - v1) / (x2 - x1) * (xm - x1);
}

void FullModel::getData() {
  T_.clear();
  Q_.clear();
  Y_.clear();
  Z_.clear();
  double t0 = dbm_->getData().at(0).at(0);
  tp_ = 0.0;
  td_ = 0.0;
  volout_ = 0.0;
  volqp_ = 0.0;
  indexQp_ = 0;
  bool qpPassed = false;
  uint count = 0;
  for (uint ii = 0; ii < dbm_->getNumTimeSteps(); ii++) {
    double q = dbm_->getData().at(3).at(ii);
    T_.push_back(dbm_->getData().at(0).at(ii) - t0);
    Y_.push_back(dbm_->getData().at(1).at(ii));
    Z_.push_back(dbm_->getData().at(2).at(ii));
    Q_.push_back(q);
    if (count > 0) {
      double dV = (T_.at(count) - T_.at(count - 1)) * 0.5 *
                  (Q_.at(count) + Q_.at(count - 1));
      volout_ += dV;
      if (!qpPassed) {
        volqp_ += dV;
      }
    }
    if (q == dbm_->getPeakDischarge()) {
      indexQp_ = count;
      tp_ = T_.at(count);
      qpPassed = true;
    }
    if (td_ == 0.0 && Y_.at(count) <= dbm_->getDam()->getBottomLevel()) {
      td_ = T_.at(count);
    }
    count++;
  }
}

} // namespace model

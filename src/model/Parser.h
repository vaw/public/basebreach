// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Parser.h
 *
 *  Created on: May 1, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_PARSER_H_
#define MODEL_PARSER_H_

#include <string>

#include "ExceptionHandling.h"
#include "api/IParser.h"
#include "types.h"

namespace model {

class ParserTag : public api::IParserTag {
public:
  ParserTag(std::string name, std::string value);
  ParserTag(std::string name, double value);
  virtual ~ParserTag();
  std::string getName() { return name_; }
  void setValue(std::string value);
  void setValue(double value);
  bool hasValue();
  std::string getStringValue() { return value_; }
  std::string getValueAsString();
  bool hasDouble() { return hasDouble_; }
  double getDoubleValue() { return valueDouble_; }
  void addValue(std::string value);
  bool hasList() { return hasList_; }
  stringVec getValueList() { return valueList_; }

private:
  void checkDouble();
  std::string name_;
  std::string value_;
  bool hasDouble_;
  double valueDouble_;
  bool hasList_;
  stringVec valueList_;
};

class ParserBlock : public api::IParserBlock {
public:
  explicit ParserBlock(std::string name);
  virtual ~ParserBlock();
  std::string getName() { return name_; }
  void addTag(api::IParserTag_t tag) { tags_.push_back(tag); }
  api::tagVec getTags() { return tags_; }
  api::IParserTag_t getTagWithName(std::string name);
  void removeEmptyTags();
  uint getNumTags() {
    size_t ret = tags_.size();
    BB_ASSERT(ret <= UINT_MAX, "return value overflow");
    return static_cast<uint>(ret);
  }

private:
  api::tagVec tags_;
  std::string name_;
};

class Parser : public api::IParser {
public:
  Parser();
  virtual ~Parser();
  void readInputFile(std::string filename);
  void readInputPars(QMap<QString, QMap<QString, QVariant>> pars,
                     QString modelId);
  void addBlock(api::IParserBlock_t block) { blocks_.push_back(block); }
  api::blockVec getBlocks() { return blocks_; }
  api::IParserBlock_t getBlockWithName(std::string blockName);
  api::IParserBlock_t getProjectBlock();
  api::IParserBlock_t getGlobalBlock() { return globalBlock_; }
  void setGlobalTag(std::string name, std::string value);
  void setGlobalTag(std::string name, double value);
  void printContent();
  void printLogContent();
  bool init();

private:
  enum parseState {
    parseBlockName,
    parseTagName,
    parseTagValue,
    parseTagValueList
  };
  void doBlockParsing(char ch);
  void doTagNameParsing(char ch);
  void doTagValueParsing(char ch);
  void doTagValueListParsing(char ch);
  bool isValidChar(char ch);
  int getBlockIndexViaName(std::string str);
  int getTagIndexOfBlockViaName(int blockindex, std::string tagname);
  api::IParserTag_t getTagOfBlockViaNames(std::string blockname,
                                          std::string tagname);
  void removeComments(std::string &str);
  void createEmptyParser();
  void defineDefault();
  void addConsoleOutput(double timeStep);
  void addOutput();
  api::blockVec blocks_;
  api::IParserBlock_t globalBlock_;
  parseState state_;
  int blockIndex_;
  int tagIndex_;
  std::string name_;
};

int doubleTestStage1(char ch);
int doubleTestStage2(char ch);
int doubleTestStage3(char ch);
int doubleTestStage4(char ch);
int doubleTestStage5(char ch);
int doubleTestStage6(char ch);
int doubleTestStage7(char ch);

} // namespace model

#endif // MODEL_PARSER_H_

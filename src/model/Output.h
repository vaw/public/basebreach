// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Output.h
 *
 *  Created on: May 5, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_OUTPUT_H_
#define MODEL_OUTPUT_H_

#include <fstream>
#include <memory>
#include <string>
#include <vector>

#include "api/IErosion.h"
#include "api/IGeometry.h"
#include "api/IHydraulics.h"
#include "api/IOutput.h"
#include "api/IPDESystemSolver.h"
#include "api/IParser.h"
#include "api/IReservoir.h"
#include "types.h"

namespace model {

class Output : public api::IOutput {
public:
  Output(api::IParser_t theParser, api::IReservoir_t theReservoir,
         api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
         api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver);
  virtual ~Output();

  void setOutputTimeStep(double dt);
  void setValues(std::vector<std::string> values);
  void write();

protected:
  enum valueId {
    Y,
    breach_level,
    reservoir_level,
    breach_width,
    breach_top_width,
    water_depth,
    flow_area,
    flow_velocity,
    breach_discharge,
    wetted_perimeter,
    hydraulic_radius,
    reservoir_surface,
    erodible_perimeter,
    sediment_discharge,
    transport_rate,
    breach_volume_change_rate,
    ritter,
    N_enums
  };
  std::vector<valueId> valueIds_;
  double getValue(valueId id);
  std::string getHeader(valueId id);

  // abstract methods to be implemented in children
  virtual void init() = 0;
  virtual void writeHeader() = 0;
  virtual void writeOutput() = 0;
  virtual void writeQpLog(double, double) = 0;
  virtual void finalize() = 0;

  double timeStep_;
  double lastOutputWritten_;
  bool headerIsWritten_;
  api::IParser_t parser_;
  api::IReservoir_t reservoir_;
  api::IGeometry_t geometry_;
  api::IHydraulics_t hydraulics_;
  api::IErosion_t erosion_;
  api::IPDESystemSolver_t solver_;
};

class ConsoleOutput : public Output {
public:
  ConsoleOutput(api::IParser_t theParser, api::IReservoir_t theReservoir,
                api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
                api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver);
  virtual ~ConsoleOutput();

private:
  void init();
  void writeHeader();
  void writeOutput();
  void writeQpLog(double qp, double tp);
  void finalize();
  uint time_;
};

class AsciiOutput : public Output {
public:
  AsciiOutput(api::IParser_t theParser, api::IReservoir_t theReservoir,
              api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
              api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver);
  virtual ~AsciiOutput();

private:
  void init();
  void writeHeader();
  void writeOutput();
  void writeQpLog(double, double) { ; }
  void finalize();

  std::ofstream file_;
};

class StdpyBBOutput : public Output, public virtual api::IOutputpyBB {
public:
  StdpyBBOutput(api::IParser_t theParser, api::IReservoir_t theReservoir,
                api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
                api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver);
  virtual ~StdpyBBOutput();

private:
  void setOutputTimeStep(double dt) { Output::setOutputTimeStep(dt); }
  void setValues(std::vector<std::string> values) { Output::setValues(values); }
  void write() { Output::write(); }

  void init();
  void writeHeader() { ; }
  void writeQpLog(double, double) { ; }
  void finalize() { ; }

  void writeOutput();
  const doubleVecVec &getData();
  doubleVecVec data_;
};

typedef std::shared_ptr<StdpyBBOutput> OutputpyBB_t;

} // namespace model

#endif // MODEL_OUTPUT_H_

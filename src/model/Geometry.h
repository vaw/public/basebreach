// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * BreachGeometry.h
 *
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */

#ifndef MODEL_GEOMETRY_H_
#define MODEL_GEOMETRY_H_

#include <map>

#include "ExceptionHandling.h"
#include "api/IDam.h"
#include "api/IGeometry.h"
#include "types.h"

namespace model {

class Geometry : public api::IGeometry {
public:
  Geometry() { ; }
  virtual ~Geometry() { ; }

  double getInitialLevel() { return initialLevel_; }
  double getBreachlevel() { return level_; }
  double getLevel() { return Y_; }
  double getTopWidth() { return width_; }
  double getVolumeErosionRate() { return dVbdY_; }
  bool hasReachedBottom() { return (Y_ < level_) ? true : false; }

protected:
  double initialLevel_; // initial breach level
  double level_;        // actual breach level (always > 0.0)
  double Y_; // actual level of the breach level variable (vertical erosion >
             // 0.0, lateral widening < 0.0)
  double width_; // actual breach width at top of the breach. MCH: This already include the whole width (not half)
  double dVbdY_; // actual breach volume that will be eroded per change in
                 // breach level
};

class GeometryPolynom : public Geometry, public api::IGeometryPolynom {
public:
  GeometryPolynom(double initialLevel, double beta, api::IDam_t dam);
  virtual ~GeometryPolynom();

  // delegating getters to the basis class of all geometry classes
  double getInitialLevel() { return Geometry::getInitialLevel(); }
  double getBreachlevel() { return Geometry::getBreachlevel(); }
  double getLevel() { return Geometry::getLevel(); }
  double getTopWidth() { return Geometry::getTopWidth(); }
  bool hasReachedBottom() { return Geometry::hasReachedBottom(); }
  double getVolumeErosionRate() { return Geometry::getVolumeErosionRate(); }

  // interface methods: for explanations see IGeometry.h
  void init();
  void update(double newLevel);
  double getTopAngle() { return beta_; }
  double calcFlowArea(double h);
  double calcWettedPerimeter(double h);
  double calcErodiblePerimeter(double h);
  doubleVecVec getErosionZone(double h, uint N);
  doubleVecVec getShape(uint N);
  double getLocationAtLevel(double level);
  double getAverageWidth();
  double relativeDistanceToBottom();

  // additional functions from interface IGeometryPolynom
  double getLambda() { return lambda_; }
  double getSigma() { return sigma_; }
  void setIntegrationPrecision(double x) { relPrecision_ = x; }

protected:
  // integration stuff, needed for wetted perimeter
  double functionToBeIntegrated(double x);
  double relPrecision_;
  virtual double doIntegration(double lowerBound, double upperBound) = 0;
  double getLevelAtLocation(double x);

  void checkShape(double dWdh);
  void debug(); // print all calculated geometric variables in cout

  api::IDam_t dam_;
  double beta_; // breach side angle at the top of the breach

  // some auxiliary variables
  double tanBeta_; // tangens of breach side angle
  double lambda_;  // order of the polynom that describes the breach area
                   // (function of width, level)
  double sigma_; // parameter that is needed for hydraulic calculations, but is
                 // a function of only geometric variables (lambda)
  double dWdh_;  // coefficient that describes the relation between the widening
                 // rate dW and the depletion rate dY (for Y>0)
  double depth_; // actual breach depth (dam height - breach level)
  double pWetted_; // wetted perimeter (stored because it can be used to
                   // calculate the erodible perimeter as well)

private:
  void updateLevelAndWidth(); // resetting the main geometric variables
  void updateShape(); // recalculating all variables that describe the shape,
                      // dependent on the main geometric variables
  double findLambdaViaBeta(double);
  double calcBetaFromLambda(double);
  void setRectangle();
  void setTriangle();
  doubleVecVec getBreachShape(double x1, double x2, uint N);
  bool rectangle_; // is true if omega gets too large
  bool triangle_;
  double intFac1_;
  double intFac2_;
  bool isPwCalculated_;
};

class GeometryTriTra : public Geometry, public api::IGeometryTriTra {
public:
  GeometryTriTra(double initialLevel, double beta, api::IDam_t dam);
  virtual ~GeometryTriTra();

  // delegating getters to the basis class of all geometry classes
  double getInitialLevel() { return Geometry::getInitialLevel(); }
  double getBreachlevel() { return Geometry::getBreachlevel(); }
  double getLevel() { return Geometry::getLevel(); }
  double getTopWidth() { return Geometry::getTopWidth(); }
  bool hasReachedBottom() { return Geometry::hasReachedBottom(); }
  double getVolumeErosionRate() { return Geometry::getVolumeErosionRate(); }

  // interface methods: for explanations see IGeometry.h
  void init() { this->update(initialLevel_); }
  void update(double newLevel);
  double getTopAngle() { return beta_; }
  double calcFlowArea(double h);
  double calcWettedPerimeter(double h);
  doubleVecVec getErosionZone(double h, uint N) {
    doubleVecVec shape;
    return shape;
  } // TODO
  doubleVecVec getShape(uint N);
  double getLocationAtLevel(double level);
  double getAverageWidth();
  double relativeDistanceToBottom();

  // additional functions from interface IGeometryTriTra
  double getSlope() { return tanBeta_; }
  double getBottomWidth() { return bWidth_; }

  void debug(); // print all calculated geometric variables in cout

  double beta_;   // breach side angle at the top of the breach
  double bWidth_; // the width at the bottom of the breach (0.0 in case of
                  // triangular shape)
  double depth_;  // actual breach depth (dam height - breach level)
  api::IDam_t dam_;

  // some auxiliary variables
  double tanBeta_; // precalculated value of tan(beta)
  double cosBeta_; // precalculated value of cos(beta)

private:
  void updateLevelAndWidth(); // resetting the main geometric variables
  void updateShape(); // recalculating all variables that describe the shape,
                      // dependent on the main geometric variables
};

} // namespace model

#endif // MODEL_GEOMETRY_H_

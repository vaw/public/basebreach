// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Erosion.h
 *
 *  Created on: Apr 16, 2014
 *      Author: samuelpeter
 */

#ifndef MODEL_EROSION_H_
#define MODEL_EROSION_H_

#include <limits>

#include "api/IErosion.h"
#include "api/IHydraulics.h"
#include "types.h"

namespace model {

class Erosion : public api::IErosion {
public:
  Erosion(api::IHydraulics_t theHydraulics);
  Erosion(api::IHydraulics_t theHydraulics, double erosionLimit);
  // interface methods: for explanations see IErosion.h
  void init();
  double getCalibrationCoefficient() { return calCoeff_; }
  double getExponentA() { return expA_; }
  double getExponentB() { return expB_; }
  double getTransportRate() {
    if (!isFrozen_) {
      return qs_;
    }
    return 0.0;
  }
  void update();
  double getErosionLimit() {
    if (erosionLimit_ > 0.0) {
      return erosionLimit_;
    } else {
      return std::numeric_limits<double>::max();
    }
  }
  void freeze(bool frozen = true) { isFrozen_ = frozen; }

protected:
  void debug();     // prints debug information
  double expA_;     // exponent of the flow velocity
  double expB_;     // exponent of the hydraulic radius
  double calCoeff_; // calibration coefficient which scales the transport rate
  double qs_;       // sediment transport rate
  api::IHydraulics_t hydr_;
  double erosionLimit_ = std::numeric_limits<double>::max();
  bool isFrozen_ = false;
};

/** transport formula as power law: exponents for flow velocity and hydraulic
 * radius can be defined freely */
class ErosionPeter : public Erosion {
public:
  ErosionPeter(double expVelocity, double expRhy, double scalingCoeff,
               double erosionLimit, api::IHydraulics_t theHydraulics);
  virtual ~ErosionPeter();
};

/** transport formula as power law: Meyer-Peter Mueller (v^3 and rhyd^-0.5),
 * scaling coefficient can be defined freely */
class ErosionMPM : public Erosion {
public:
  ErosionMPM(double scalingCoeff, double erosionLimit,
             api::IHydraulics_t theHydraulics);
  virtual ~ErosionMPM();
};

/** transport formula as power law: Meyer-Peter Mueller (v^3 and rhyd^-0.5),
 * scaling coefficient is calibrated for AWEL */
class ErosionAwel : public Erosion {
public:
  ErosionAwel(double Vr, double Hd, double Wc, double erosionLimit,
              api::IHydraulics_t theHydraulics);
  virtual ~ErosionAwel();

private:
  double calcErosionVelocity(double Vr, double Hd, double Wc);
};

} // namespace model

#endif // MODEL_EROSION_H_

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "model/modelDefinition.h"

#include <QtCore>

#include "ExceptionHandling.h"
#include "types.h"

namespace model::definition {

ParameterLimitMessage ParameterLimit::validate(double value) const {
  bool failed = false;
  // Lower limit
  if (value < min_ || (value == min_ && minExclusive_))
    failed = true;
  // Upper limit
  if (value > max_ || (value == max_ && maxExclusive_))
    failed = true;
  // Generate message
  ParameterLimitMessage msg;
  if (failed)
    msg = ParameterLimitMessage(
        limitType_, message_ + " (current: " + QString::number(value) + ")");
  return msg;
}

Parameter::Parameter(String_T const &id, String_T const &name,
                     String_T const &desc, String_T const &unit,
                     double initialVal) {
  id_ = id;
  name_ = name;
  desc_ = desc;
  unit_ = unit;
  default_ = initialVal;
}

void Parameter::addLimit(ParameterLimitType limitType, String_T const &message,
                         double min, double max, bool exclusiveMin,
                         bool exclusiveMax) {
  ParameterLimit limit(limitType, id_, name_, message, min, max, exclusiveMin,
                       exclusiveMax);
  addLimit(limit);
}

void Parameter::addLimit(ParameterLimit limit) {
  ParameterLimit newLimit(limit.limitType_, id_, name_, modelName_,
                          limit.message_, limit.min_, limit.max_,
                          limit.minExclusive_, limit.maxExclusive_);
  limits_.push_back(newLimit);
}

QList<ParameterLimitMessage> Parameter::validateInput(double value) const {
  QList<ParameterLimitMessage> messages;
  // Validate all parameters
  for (auto it = limits_.begin(); it != limits_.end(); it++) {
    ParameterLimitMessage msg = it->validate(value);
    msg.paramId_ = getId();
    msg.paramName_ = getName();
    messages.append(msg);
  }
  // Filter for non-empty messages
  QList<ParameterLimitMessage> filteredMessages;
  for (auto it = messages.begin(); it != messages.end(); it++)
    if (it->limitType_ != ParameterLimitType::NONE)
      filteredMessages.append(*it);
  return filteredMessages;
}

void ModelType::addParameter(Parameter const &param) {
  for (auto it = params_.begin(); it != params_.end(); it++) {
    if (it->getId() == param.getId()) {
      BB_EXCEPTION("Parameter with id " << param.getId().toStdString().c_str()
                                        << " already exists!");
    }
  }
  params_.append(param);
}

Parameter ModelType::addParameter(String_T const &id, String_T const &name,
                                  String_T const &desc, String_T const &unit,
                                  double initialVal) {
  Parameter param = Parameter(id, name, desc, unit, initialVal);
  addParameter(param);
  return param;
}

Parameter ModelType::getParameter(String_T const &id) const {
  for (auto it = params_.begin(); it != params_.end(); it++) {
    if (it->getId() == id) {
      return *it;
    }
  }
  BB_EXCEPTION("Parameter " << id.toStdString().c_str() << " does not exist");
}

ParameterLimit ModelType::addAuxiliaryLimit(ParameterLimitType limitType,
                                            String_T const &paramId,
                                            String_T const &paramName,
                                            String_T const &message, double min,
                                            double max, bool exclusiveMin,
                                            bool exclusiveMax) {
  ParameterLimit limit(limitType, paramId, paramName, name_, message, min, max,
                       exclusiveMin, exclusiveMax);
  // Store in map
  if (!extraLimits_.contains(paramId))
    extraLimits_[paramId] = QList<ParameterLimit>();
  extraLimits_[paramId].append(limit);
  return limit;
}

void ModelType::setDefault(const String_T &paramId, double value) {
  customDefaults_[paramId] = value;
}

double ModelType::getDefault(const String_T &paramId) const {
  if (customDefaults_.contains(paramId))
    return customDefaults_[paramId];
  else
    return getParameter(paramId).getDefault();
}

double ModelType::getDefault(const Parameter &param) const {
  return getDefault(param.getId());
}

QList<String_T> ModelType::getModelParams() const {
  QList<String_T> param_names;
  for (auto it = params_.begin(); it != params_.end(); it++) {
    param_names.append(it->getId());
  }
  return param_names;
}

QList<ParameterLimitMessage>
ModelType::validateConfig(QMap<QString, QVariantMap> const &config) const {
  QList<ParameterLimitMessage> messages;
  // Validate model-specific scenario limits
  for (auto it = extraLimits_.begin(); it != extraLimits_.end(); it++) {
    String_T paramId = it.key();
    double value = getDottedKey(config, paramId).toDouble();
    QList<ParameterLimit> limits = it.value();
    for (auto it2 = limits.begin(); it2 != limits.end(); it2++) {
      // Prefix parameter message with current model type
      ParameterLimitMessage msg = it2->validate(value);
      msg.message_ = name_ + ": " + msg.message_;
      messages.append(msg);
    }
  }
  // Validate model-specific model parameters
  for (auto it = params_.begin(); it != params_.end(); it++) {
    QString paramId = it->getId();
    QStringList chunks = paramId.split(".");
    QString block = chunks.first();
    QString key = chunks.last();
    // Handle missing keys
    if (!config.contains(block) || !config[block].contains(key)) {
      BB_MESSAGE("Unable to locate key: " + paramId.toStdString());
      continue;
    }
    // Validate
    double value = config[block][key].toDouble();
    messages.append(it->validateInput(value));
  }
  return messages;
}

QVariant getDottedKey(QMap<QString, QVariantMap> const &map,
                      String_T const &key) {
  QStringList chunks = key.split(".");
  // Get top-level key
  QString first = chunks.first();
  if (!map.contains(first))
    return QVariant(); // Key not found
  // Re-stitch remaining key
  QString last = chunks.last();
  return map[first][last];
}

void saveModelConfig(const ModelConfig_T &modelConfig,
                     const QString &fileName) {
  // Convert model config to JSON
  QJsonObject json;
  for (auto bi = modelConfig.begin(); bi != modelConfig.end(); ++bi) {
    // Create key if not already present
    if (!json.contains(bi.key()))
      json[bi.key()] = QJsonObject();
    json[bi.key()] = QJsonObject::fromVariantMap(bi.value());
  }
  // Create output file
  QFile outputFile(fileName);
  if (!outputFile.open(QIODevice::WriteOnly)) {
    BB_EXCEPTION("Couldn't open output file.");
  }
  // Write JSON to file
  outputFile.write(QJsonDocument(json).toJson());
}

ModelConfig_T loadModelConfig(const QString &fileName) {
  // Read input file
  QFile inputFile(fileName);
  if (!inputFile.open(QIODevice::ReadOnly)) {
    BB_EXCEPTION("Couldn't open input file.");
  }
  QByteArray saveData = inputFile.readAll();
  QJsonDocument jsonDoc(QJsonDocument::fromJson(saveData));
  // Convert JSON to model config
  ModelConfig_T modelConfig;
  QJsonObject json = jsonDoc.object();
  for (auto bi = json.begin(); bi != json.end(); ++bi) {
    // Create key if not already present
    if (!modelConfig.contains(bi.key()))
      modelConfig[bi.key()] = ParameterMap_T();
    modelConfig[bi.key()] = bi.value().toObject().toVariantMap();
  }
  return modelConfig;
}

void mergeModelConfigs(ModelConfig_T &target, const ModelConfig_T &source) {
  // Iterate source blocks (i.e. outer keys)
  for (auto ib = source.begin(); ib != source.end(); ib++) {
    // If the source key is not contained in the target, insert as normal
    if (!target.contains(ib.key())) {
      target[ib.key()] = ib.value();
      continue;
    }
    ParameterMap_T block = ib.value();
    // Iterate source tags (i.e. inner keys of current block) and merge keys
    for (auto it = block.begin(); it != block.end(); it++)
      target[ib.key()][it.key()] = it.value();
  }
}

} // namespace model::definition

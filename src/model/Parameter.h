// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef PARAMETER_H
#define PARAMETER_H

#include <QObject>
#include <types.h>
#include <api/IParameter.h>
#include <api/IDistribution.h>

namespace model {

class Parameter : public api::IParameter {

    Q_OBJECT

public:
    Parameter() : value_(NAN) {;}
    virtual ~Parameter() {;}

    void setValue(double);
    void setDistribution(api::IDistribution_t);
    bool isDistributionDefined();
    double getValue();
    api::IDistribution_t getDistribution();
    doubleVec getRealizations(uint);

signals:
    void Changed();

private:
    api::IDistribution_t distribution_;
    double value_;
};

} // end namespace

#endif // PARAMETER_H

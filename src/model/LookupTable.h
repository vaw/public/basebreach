// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef MODEL_LOOKUPTABLE_H_
#define MODEL_LOOKUPTABLE_H_

#include <QtCore>

#include "types.h"

namespace model {

class LookupTable {
public:
  /**
   * Initialise the lookup table with the given value.
   *
   * The lookup table is created so that every position returns the given value.
   */
  explicit LookupTable(double fixedValue);
  /**
   * Load the lookup table from the given file.
   */
  explicit LookupTable(const String_T &filename);

  /** Return the interpolated value for the given position. */
  double getValue(double position) const;

private:
  /** Load the lookup table from the given file. */
  void load(const String_T &filename);

  QMap<double, double> data_;
};

} // namespace model

#endif // MODEL_LOOKUPTABLE_H_

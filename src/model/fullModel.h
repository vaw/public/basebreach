// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Created on: Apr 17, 2014
 *      Author: samuelpeter
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */
 
#ifndef MODEL_FULLMODEL_H_
#define MODEL_FULLMODEL_H_

#include <QMetaType>
#include <QObject>

#include "ExceptionHandling.h"
#include "api/IDBModel.h"
#include "api/IErosion.h"
#include "api/IGeometry.h"
#include "api/IHydraulics.h"
#include "api/IOutput.h"
#include "api/IReservoir.h"
#include "types.h"

namespace model {

class FullModel : public QObject {
  Q_OBJECT

public:
  FullModel();
  explicit FullModel(api::IParser_t parser);
  virtual ~FullModel() { ; }

  void runModel();
  void getData();
  stringDoubleMap getParameters();
  double Qp();
  double Wb();
  double VolOut() { return volout_; }
  double VolQp() { return volqp_; }

  double TimeQp() { return tp_; }
  double TimeBottom() { return td_; }
  uint IndexQp() { return indexQp_; }
  doubleVec getCharacteristicTimes();

  void setLevels(double Z, double Y);
  void setLevelsWithIndex(int index);
  void setTime(double t);

  double Time();
  double BreachLevel();
  double ReservoirLevel();
  double WaterDepth();
  double FlowVelocity();
  double Discharge();
  double HydraulicRadius();
  double Inflow();

  doubleVecVec BreachShape(uint);
  doubleVecVec ErosionZone(uint);
  double WaterDepthLocation();
  double ReservoirLevelLocation();
  double AverageBreachWidth();
  double TransportRate();
  double SedimentDischarge();

  uint IntegrationSteps() {
    size_t ret = timeTS().size();
    BB_ASSERT(ret <= UINT_MAX, "return value overflow");
    return static_cast<uint>(ret);
  }
  double TimeDelta() { return timeTS().at(1) - timeTS().at(0); }
  const doubleVec &timeTS();
  const doubleVec &breachlevelTS();
  const doubleVec &reservoirlevelTS();

  stringVec getOutputVariableNames() {
    return {"time [s]",
            "breach discharge [m3/s]",
            "flow velocity [m/s]",
            "hydraulic radius [m]",
            "transport rate [m2/s]",
            "sediment discharge [m3/s]",
            "breach width [m]",
            "reservoir level [m]",
            "Inflow [m3/s]"};
  }
  double getOutputVariableValue(uint index);
  const stringDoubleMap getDebugData();

signals:
  void hasFinished();

private:
  double interpolateValues(double x1, double v1, double x2, double v2,
                           double xm);

  api::IGeometry_t geometry_;
  api::IReservoir_t reservoir_;
  api::IHydraulics_t hydraulics_;
  api::IErosion_t erosion_;
  api::IOutputpyBB_t output_;
  api::IDBModel_t dbm_;

  doubleVec T_;
  doubleVec Y_;
  doubleVec Z_;
  doubleVec Q_;

  double time_;
  uint indexQp_;
  double tp_;
  double td_;
  double volout_;
  double volqp_;

  // enum
  // outputs{breachDischarge,flowVelocity,hydraulicRadius,transportRate,sedimentDischarge};
};

} // namespace model

#endif // MODEL_FULLMODEL_H_

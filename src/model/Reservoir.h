// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Reservoir.h
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#ifndef RESERVOIR_H_
#define RESERVOIR_H_

#include <api/IDam.h>
#include <api/IReservoir.h>
#include <types.h>

#include "model/LookupTable.h"

namespace model {

/** reservoir base class */
class Reservoir : public api::IReservoir {

public:
  Reservoir();
  explicit Reservoir(double initialLevel);
  Reservoir(double initialLevel, double inflow_rate);
  Reservoir(double initialLevel, const String_T& inflow_hydrograph);
  virtual ~Reservoir();

  // interface methods: for explanations see IReservoir.h
  void init() { this->update(initialLevel_); }
  double getVolumeAboveBreachBottom() { return volumeReleased_; }
  double getMaximumVolume() { return maxVolume_; }
  double getInflow(double time) { return inflow_hydrograph_.getValue(time); }
  double getInitialLevel() { return initialLevel_; }
  double getShapeCoefficient() { return shapeCoeff_; }
  double getVolume() { return volume_; }
  double getWaterSurface() { return waterSurface_; }
  double getLevel() { return level_; }
  void update(double newLevel);

protected:
  void setRetentionPowerLaw(double alpha,
                            double beta); // defines the retention curve

  double initialLevel_;   // initial water level
  double volumeReleased_; // volume initially stored above the breach bottom
  double maxVolume_;  // maximum storage volume (could be just a hypothetical
                      // number)
  double shapeCoeff_; // scaling exponent to describe the volume-level relation
  double level_;      // actual water level
  double volume_;     // actual stored volume
  double waterSurface_; // actual water surface
  api::IDam_t dam_;     // pointer to dam object

  // some auxiliary variables
  double beta_; // factor for volume-level formula, is being calculated once
                // when initiating

  LookupTable inflow_hydrograph_;
};

/** reservoir that is initialized via the maximum volume, not dependent on the
 * initial level */
class ReservoirMaximumVolume : public Reservoir {
public:
  ReservoirMaximumVolume(double maxVolume, double initialLevel,
                         double shapeCoefficient, double inflow_rate,
                         api::IDam_t dam);
  ReservoirMaximumVolume(double maxVolume, double initialLevel,
                         double shapeCoefficient,
                         const String_T& inflow_hydrograph, api::IDam_t dam);
  virtual ~ReservoirMaximumVolume();
};

/** reservoir that is initialized via the drop in water level and the
 * corresponding released volume */
class ReservoirReleasedVolume : public Reservoir {
public:
  ReservoirReleasedVolume(double releasedVolume, double initialLevel,
                          double shapeCoefficient, double inflow_rate,
                          api::IDam_t dam);
  ReservoirReleasedVolume(double releasedVolume, double initialLevel,
                          double shapeCoefficient,
                          const String_T& inflow_hydrograph, api::IDam_t dam);
  virtual ~ReservoirReleasedVolume();
};

/** reservoir with calibrated alpha value */
class ReservoirAwel : public Reservoir {
public:
  ReservoirAwel(double maxVolume, double initialLevel, double inflow_rate,
                api::IDam_t dam);
  ReservoirAwel(double maxVolume, double initialLevel,
                const String_T& inflow_hydrograph, api::IDam_t dam);
  virtual ~ReservoirAwel();

private:
  double calcReservoirShape(double Vr, double Hd); // calibrated reservoir shape
};

} // namespace model

#endif // RESERVOIR_H_

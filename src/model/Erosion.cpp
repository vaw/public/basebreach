// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * Erosion.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: samuelpeter
 */

#include "model/Erosion.h"

#include <math.h>

#include "ExceptionHandling.h"

namespace model {

Erosion::Erosion(api::IHydraulics_t theHydraulics) {
  qs_ = 0.0;
  hydr_ = theHydraulics;
  expA_ = 0.0;
  expB_ = 0.0;
  isFrozen_ = false;
}

Erosion::Erosion(api::IHydraulics_t theHydraulics, double erosionLimit)
    : Erosion(theHydraulics) {
  erosionLimit_ = erosionLimit;
}

void Erosion::init() {
  isFrozen_ = false;
  hydr_->init();  // first initialize the hydraulics!
  this->update(); // then calculate initial values
}

void Erosion::update() {
  if (hydr_->getFlowVelocity() > 0.0 && hydr_->getHydraulicRadius() > 0.0) {
    qs_ = getCalibrationCoefficient() * pow(hydr_->getFlowVelocity(), expA_) *
          pow(hydr_->getHydraulicRadius(), expB_);
  } else {
    qs_ = 0.0;
  }
  // this->debug();
}

void Erosion::debug() {
  BB_MESSAGE("********** EROSION **********");
  BB_MESSAGE("transport rate: " << qs_);
}

ErosionPeter::ErosionPeter(double expVelocity, double expRhy,
                           double scalingCoeff, double erosionLimit,
                           api::IHydraulics_t theHydraulics)
    : Erosion(theHydraulics, erosionLimit) {
  expA_ = expVelocity;
  expB_ = expRhy;
  calCoeff_ = scalingCoeff;
}

ErosionPeter::~ErosionPeter() {
  // Auto-generated destructor stub
}

ErosionMPM::ErosionMPM(double scalingCoeff, double erosionLimit, api::IHydraulics_t theHydraulics)
    : Erosion(theHydraulics, erosionLimit) {
  // Meyer-Peter Müller configuration
  expA_ = 3.0;
  expB_ = -0.5;
  calCoeff_ = scalingCoeff;
}

ErosionMPM::~ErosionMPM() {
  // Auto-generated destructor stub
}

ErosionAwel::ErosionAwel(double Vr, double Hd, double Wc, double erosionLimit,
                         api::IHydraulics_t theHydraulics)
    : Erosion(theHydraulics, erosionLimit) {
  // Meyer-Peter Müller configuration
  expA_ = 3.0;
  expB_ = -0.5;
  // scaling coefficient is dependent on reservoir volume, dam height, and crest
  // width!!!
  calCoeff_ = this->calcErosionVelocity(Vr, Hd, Wc) * pow(G, -1.5);
}

double ErosionAwel::calcErosionVelocity(double Vr, double Hd, double Wc) {
  // ve for fixed dam heights (2,4,8) and fixed reservoir volumes:
  double h1 = 2.0;
  double h2 = 4.0;
  double h3 = 8.0;
  double v1 = 2000.0;
  double ve_1_1 = 0.05;
  double ve_1_2 = 0.055;
  double ve_1_3 = 0.042;
  double v2 = 20000.0;
  double ve_2_1 = 0.115;
  double ve_2_3 = 0.042;
  double v3 = 150000.0;
  double ve_3_1 = 0.06;
  double ve_3_3 = 0.19;
  double ve_a_2 = 0.040117006264973;
  double ve_b_2 = -0.075873726224716;
  // let's do the interpolation between the reservoir volumes
  double ve_2, ve_4, ve_8;
  if (Vr <= v1) {
    ve_2 = ve_1_1;
    ve_4 = ve_1_2;
    ve_8 = ve_1_3;
  } else if ((Vr > v1) && (Vr <= v2)) {
    ve_2 = ve_1_1 + (ve_2_1 - ve_1_1) / (v2 - v1) * (Vr - v1);
    ve_4 = ve_a_2 * log10(Vr) + ve_b_2;
    ve_8 = ve_2_3;
  } else if ((Vr > v2) && (Vr <= v3)) {
    ve_2 = ve_2_1 + (ve_3_1 - ve_2_1) / (v3 - v2) * (Vr - v2);
    ve_4 = ve_a_2 * log10(Vr) + ve_b_2;
    ve_8 = ve_2_3 + (ve_3_3 - ve_2_3) / (v3 - v2) * (Vr - v2);
  } else if (Vr > v3) {
    ve_2 = ve_3_1;
    ve_4 = ve_a_2 * log10(Vr) + ve_b_2;
    ve_8 = ve_3_3;
  }
  // let's do the interpolation between the dam heights
  double ve = 0.0;
  if (Hd <= h1) {
    ve = ve_2;
  } else if ((Hd > h1) && (Hd <= h2)) {
    ve = ve_2 + (ve_4 - ve_2) * (Hd - h1) / h1;
  } else if ((Hd > h2) && (Hd <= h3)) {
    ve = ve_4 + (ve_8 - ve_4) * (Hd - h2) / h2;
  } else if (Hd > h3) {
    ve = ve_8;
  }
  // correction factor for large crest widths:
  double wc1 = 2.0;
  double fve1 = 1.0;
  double wc2 = 10.0;
  double fve2 = 0.75;
  double fve = 0.0;
  if (Wc <= wc1) {
    fve = fve1;
  } else if ((Wc > wc1) && (Wc <= wc2)) {
    fve = fve1 + (fve2 - fve1) / (wc2 - wc1) * (Wc - wc1);
  } else if (Wc > wc2) {
    fve = fve2;
  }
  // return final erosion velocity
  return fve * ve;
}

ErosionAwel::~ErosionAwel() {
  // Auto-generated destructor stub
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include "model/standardBreach.h"

#include <cmath>

#include "ExceptionHandling.h"
#include "types.h"

namespace model {

void StandardBreach::updateParameters(double damHeight) {
  damHeight_ = damHeight;
}

bool StandardBreach::fieldIsValid(String_T fieldName) const {
  return (fieldName == "breach discharge [m3/s]" ||
          fieldName == "breach width [m]");
}

double StandardBreach::getField(String_T fieldName) const {
  BB_ASSERT(fieldIsValid(fieldName),
            "unsupported fieldName for default breach");
  if (fieldName == "breach discharge [m3/s]")
    return qMax(damHeight_);
  return breachTopWidth(damHeight_);
}

double StandardBreach::qMax(double damHeight) const {
  return 2.58 * pow(damHeight, 5.0 / 2.0);
}

double StandardBreach::breachTopWidth(double damHeight) const {
  // Factor of 2 to calculate the breach bottom width, another factor of 2 to
  // account for the 1:1 ratio breach slopes
  return damHeight * 4;
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * DBFactory.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: samuelpeter
 */

#include "model/DBFactory.h"

#include <memory>
#include <string>

#include "ExceptionHandling.h"
#include "model/DBModel.h"
#include "model/Dam.h"
#include "model/Erosion.h"
#include "model/Geometry.h"
#include "model/GeometryWithNumerics.h"
#include "model/Hydraulics.h"
#include "model/Output.h"
#include "model/PDESystemSolver.h"
#include "model/Reservoir.h"

namespace model {

DBFactory::DBFactory(api::IParser_t theParser) {
  parser_ = theParser;
  // does some checks dependent on the global tags of the parser
  parser_->init();
}

DBFactory::~DBFactory() {
  // Auto-generated destructor stub
}

api::IDBModel_t DBFactory::createDBModel() {
  api::IParserBlock_t projectBlock = parser_->getBlockWithName("PROJECT");
  std::string modelType =
      projectBlock->getTagWithName("model")->getStringValue();
  std::ostringstream modules;
  modules << "Unknown model type \"" << modelType << "\"; known models are:";
#ifdef BBPETER
  if (modelType == "bbPeter")
    return createPeterModel();
  modules << "\nbbPeter";
#endif
#ifdef BBPETERCAL
  if (modelType == "bbPeterCal")
    return createPeterCalibrationModel();
  modules << "\nbbPeterCal";
#endif
#ifdef BBMACCHIONE
  if (modelType == "bbMacchione")
    return createMacchioneModel();
  modules << "\nbbMacchione";
#endif
#ifdef BBAWEL
  if (modelType == "bbAwel")
    return createAwelModel();
  modules << "\nbbAwel";
#endif
  BB_EXCEPTION(modules.str());
}

api::IDBModel_t DBFactory::createPeterModel() {
  // dam object, the same for all models so far
  api::IDam_t dam = createDamObject();

  auto simBlock = parser_->getBlockWithName("SIMULATION");
  double erosionLimit =
      simBlock->getTagWithName("erosion_limit")->getDoubleValue();
  BB_ASSERT(erosionLimit >= 0.0, "Error in tag 'erosion_limit' in block "
                                 "'SIMULATION': expected value > 0.0");

  // reservoir object
  api::IParserBlock_t reservoirBlock = parser_->getBlockWithName("RESERVOIR");
  double vr = reservoirBlock->getTagWithName("volume")->getDoubleValue();
  BB_ASSERT(vr > 0.0,
            "Error in tag 'volume' in block 'RESERVOIR': expected value > 0.0");
  double lir =
      reservoirBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(lir > 0.0, "Error in tag 'initial_level' in block 'RESERVOIR': "
                       "expected value > 0.0");
  if (lir > dam->getHeight())
    BB_WARNING("Warning: 'initial_level' in block 'RESERVOIR' is larger than "
               "'height' in 'DAM'!");
  double alpha =
      reservoirBlock->getTagWithName("shape_exponent")->getDoubleValue();
  BB_ASSERT(alpha > 0.0, "Error in tag 'shape_exponent' in block 'RESERVOIR': "
                         "expected value > 0.0");
  auto inflow_tag = reservoirBlock->getTagWithName("inflow");
  auto inflow = QString::fromStdString(inflow_tag->getValueAsString());
  bool ok;
  double val = inflow.toDouble(&ok);
  api::IReservoir_t reservoir;
  if (ok) {
    BB_ASSERT(val >= 0.0, "Error in tag 'inflow' in block 'RESERVOIR': "
                          "expected value >= 0.0");
    reservoir =
        std::make_shared<ReservoirMaximumVolume>(vr, lir, alpha, val, dam);
  } else {
    reservoir =
        std::make_shared<ReservoirMaximumVolume>(vr, lir, alpha, inflow, dam);
  }

  // geometry object
  api::IParserBlock_t geometryBlock = parser_->getBlockWithName("BREACH");
  double lig = geometryBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(
      lig > 0.0,
      "Error in tag 'initial_level' in block 'BREACH': expected value > 0.0");
  BB_ASSERT(lig < dam->getHeight(), "Error in tag 'initial_level' in block "
                                    "'BREACH': expected value < dam height");
  BB_ASSERT(lig < lir, "Error in tag 'initial_level' in block 'BREACH: "
                       "expected value < reservoir level");
  double beta = geometryBlock->getTagWithName("angle")->getDoubleValue();
  BB_ASSERT(
      beta > 45.0 && beta < 90.0,
      "Error in tag 'angle' in block 'BREACH': 90.0 < expected value > 45.0");
  double relPrec = parser_->getBlockWithName("NUMERICS")
                       ->getTagWithName("integration_precision")
                       ->getDoubleValue();
  BB_ASSERT(relPrec > 0.0, "Error in tag 'integration_precision' in block "
                           "'NUMERICS': expected value > 0.0");
  // catch possible input errors
  // api::IGeometry_t geometry(new GeometrySimpson(l,beta,dam));
  // api::IGeometry_t geometry(new GeometryGauLeg(l,beta,dam));
  api::IGeometryPolynom_t geometry =
      std::make_shared<GeometryAdaptiveTrapezSimpson>(lig, beta, dam);
  geometry->setIntegrationPrecision(relPrec);

  // hydraulics object
  api::IHydraulics_t hydraulics =
      std::make_shared<HydraulicsPolynom>(reservoir, geometry);

  // erosion object
  api::IParserBlock_t erosionBlock = parser_->getBlockWithName("EROSION");
  double ev = erosionBlock->getTagWithName("exponent_v")->getDoubleValue();
  BB_ASSERT(
      ev > 0.0,
      "Error in tag 'exponent_v' in block 'EROSION': expected value > 0.0");
  double er = erosionBlock->getTagWithName("exponent_rhy")->getDoubleValue();
  double fe =
      erosionBlock->getTagWithName("scaling_coefficient")->getDoubleValue();
  BB_ASSERT(fe >= 0.0, "Error in tag 'scaling_coefficient' in block 'EROSION': "
                       "expected value > 0.0");
  api::IErosion_t erosion =
      std::make_shared<ErosionPeter>(ev, er, fe, erosionLimit, hydraulics);

  // solver
  api::IPDESystemSolver_t solver =
      createSolverObject(reservoir, geometry, hydraulics, erosion);

  // outputs
  api::outputVec outputs =
      createOutputObjects(reservoir, geometry, hydraulics, erosion, solver);

  // finally the model
  return createModel(dam, reservoir, geometry, hydraulics, erosion, solver,
                     outputs);
}

api::IDBModel_t DBFactory::createPeterCalibrationModel() {
  // dam object, the same for all models so far
  api::IDam_t dam = createDamObject();

  auto simBlock = parser_->getBlockWithName("SIMULATION");
  double erosionLimit =
      simBlock->getTagWithName("erosion_limit")->getDoubleValue();
  BB_ASSERT(erosionLimit >= 0.0, "Error in tag 'erosion_limit' in block "
                                 "'SIMULATION': expected value > 0.0");

  // reservoir object
  api::IParserBlock_t reservoirBlock = parser_->getBlockWithName("RESERVOIR");
  double vr = reservoirBlock->getTagWithName("volume")->getDoubleValue();
  BB_ASSERT(vr > 0.0,
            "Error in tag 'volume' in block 'RESERVOIR': expected value > 0.0");
  double lir =
      reservoirBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(lir > dam->getBottomLevel(),
            "Error in tag 'initial_level' in block 'RESERVOIR': expected value "
            "> 'bottom_level' in 'DAM' (default 0.0)");
  if (lir > dam->getHeight())
    BB_WARNING("Warning: 'initial_level' in block 'RESERVOIR' is larger than "
               "'height' in 'DAM'!");
  double alpha =
      reservoirBlock->getTagWithName("shape_exponent")->getDoubleValue();
  BB_ASSERT(alpha > 0.0, "Error in tag 'shape_exponent' in block 'RESERVOIR': "
                         "expected value > 0.0");
  auto inflow_tag = reservoirBlock->getTagWithName("inflow");
  auto inflow = QString::fromStdString(inflow_tag->getValueAsString());
  bool ok;
  double val = inflow.toDouble(&ok);
  api::IReservoir_t reservoir;
  if (ok) {
    BB_ASSERT(val >= 0.0, "Error in tag 'inflow' in block 'RESERVOIR': "
                          "expected value >= 0.0");
    reservoir =
        std::make_shared<ReservoirReleasedVolume>(vr, lir, alpha, val, dam);
  } else {
    reservoir =
        std::make_shared<ReservoirReleasedVolume>(vr, lir, alpha, inflow, dam);
  }

  // geometry object
  api::IParserBlock_t geometryBlock = parser_->getBlockWithName("BREACH");
  double lig = geometryBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(lig >= dam->getBottomLevel(),
            "Error in tag 'initial_level' in block 'BREACH': expected value >= "
            "dam bottom level");
  BB_ASSERT(lig < dam->getHeight(), "Error in tag 'initial_level' in block "
                                    "'BREACH': expected value < dam height");
  BB_ASSERT(lig < lir, "Error in tag 'initial_level' in block 'BREACH: "
                       "expected value < reservoir level");
  double beta = geometryBlock->getTagWithName("angle")->getDoubleValue();
  BB_ASSERT(
      beta >= 45.0 && beta <= 90.0,
      "Error in tag 'angle' in block 'BREACH': 90.0 <= expected value => 45.0");
  double relPrec = parser_->getBlockWithName("NUMERICS")
                       ->getTagWithName("integration_precision")
                       ->getDoubleValue();
  BB_ASSERT(relPrec > 0.0, "Error in tag 'integration_precision' in block "
                           "'NUMERICS': expected value > 0.0");
  // here we have to init a new Geometry class with numerics ...
  api::IGeometryPolynom_t geometry =
      std::make_shared<GeometryAdaptiveTrapezSimpson>(lig, beta, dam);
  geometry->setIntegrationPrecision(relPrec);

  // hydraulics object
  api::IHydraulics_t hydraulics =
      std::make_shared<HydraulicsPolynom>(reservoir, geometry);

  // erosion object
  api::IParserBlock_t erosionBlock = parser_->getBlockWithName("EROSION");
  double ev = erosionBlock->getTagWithName("exponent_v")->getDoubleValue();
  double er = erosionBlock->getTagWithName("exponent_rhy")->getDoubleValue();
  double fe =
      erosionBlock->getTagWithName("scaling_coefficient")->getDoubleValue();
  BB_ASSERT(fe >= 0.0, "Error in tag 'scaling_coefficient' in block 'EROSION': "
                       "expected value > 0.0");
  api::IErosion_t erosion =
      std::make_shared<ErosionPeter>(ev, er, fe, erosionLimit, hydraulics);

  // solver
  api::IPDESystemSolver_t solver =
      createSolverObject(reservoir, geometry, hydraulics, erosion);

  // outputs
  api::outputVec outputs =
      createOutputObjects(reservoir, geometry, hydraulics, erosion, solver);

  // finally the model
  return createModel(dam, reservoir, geometry, hydraulics, erosion, solver,
                     outputs);
}

api::IDBModel_t DBFactory::createMacchioneModel() {
  // dam object, the same for all models so far
  api::IDam_t dam = createDamObject();

  auto simBlock = parser_->getBlockWithName("SIMULATION");
  double erosionLimit =
      simBlock->getTagWithName("erosion_limit")->getDoubleValue();
  BB_ASSERT(erosionLimit >= 0.0, "Error in tag 'erosion_limit' in block "
                                 "'SIMULATION': expected value > 0.0");

  // reservoir object
  api::IParserBlock_t reservoirBlock = parser_->getBlockWithName("RESERVOIR");
  double vr = reservoirBlock->getTagWithName("volume")->getDoubleValue();
  BB_ASSERT(vr > 0.0,
            "Error in tag 'volume' in block 'RESERVOIR': expected value > 0.0");
  double lir =
      reservoirBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(lir > 0.0, "Error in tag 'initial_level' in block 'RESERVOIR': "
                       "expected value > 0.0");
  if (lir > dam->getHeight())
    BB_WARNING("Warning: 'initial_level' in block 'RESERVOIR' is larger than "
               "'height' in 'DAM'!");
  double alpha =
      reservoirBlock->getTagWithName("shape_exponent")->getDoubleValue();
  BB_ASSERT(alpha > 0.0, "Error in tag 'shape_exponent' in block 'RESERVOIR': "
                         "expected value > 0.0");
  auto inflow_tag = reservoirBlock->getTagWithName("inflow");
  auto inflow = QString::fromStdString(inflow_tag->getValueAsString());
  bool ok;
  double val = inflow.toDouble(&ok);
  api::IReservoir_t reservoir;
  if (ok) {
    BB_ASSERT(val >= 0.0, "Error in tag 'inflow' in block 'RESERVOIR': "
                          "expected value >= 0.0");
    reservoir =
        std::make_shared<ReservoirMaximumVolume>(vr, lir, alpha, val, dam);
  } else {
    reservoir =
        std::make_shared<ReservoirMaximumVolume>(vr, lir, alpha, inflow, dam);
  }

  // geometry object
  api::IParserBlock_t geometryBlock = parser_->getBlockWithName("BREACH");
  double lig = geometryBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(
      lig > 0.0,
      "Error in tag 'initial_level' in block 'BREACH': expected value > 0.0");
  BB_ASSERT(lig < dam->getHeight(), "Error in tag 'initial_level' in block "
                                    "'BREACH': expected value < dam height");
  BB_ASSERT(lig < lir, "Error in tag 'initial_level' in block 'BREACH: "
                       "expected value < reservoir level");
  double beta = geometryBlock->getTagWithName("angle")->getDoubleValue();
  BB_ASSERT(
      beta > 45.0 && beta < 90.0,
      "Error in tag 'angle' in block 'BREACH': 90.0 < expected value > 45.0");
  api::IGeometryTriTra_t geometry =
      std::make_shared<GeometryTriTra>(lig, beta, dam);

  // hydraulic object
  api::IHydraulics_t hydraulics =
      std::make_shared<HydraulicsTriTra>(reservoir, geometry);

  // erosion object
  api::IParserBlock_t macchioneBlock = parser_->getBlockWithName("MACCHIONE");
  double char_velocity =
      macchioneBlock->getTagWithName("char_velocity")->getDoubleValue();
  BB_ASSERT(char_velocity >= 0.0,
            "Error in tag 'char_velocity' in block 'MACCHIONE': "
            "expected value > 0.0");
  // Convert characteristic velocity to scaling coefficient
  qDebug() << "char_velocity: " << char_velocity;
  double scaling_coeff = std::pow(G, -3.0 / 2.0) * char_velocity;
  qDebug() << "scaling_coeff: " << scaling_coeff;
  api::IErosion_t erosion =
      std::make_shared<ErosionMPM>(scaling_coeff, erosionLimit, hydraulics);

  // solver
  api::IPDESystemSolver_t solver =
      createSolverObject(reservoir, geometry, hydraulics, erosion);

  // outputs
  api::outputVec outputs =
      createOutputObjects(reservoir, geometry, hydraulics, erosion, solver);

  // finally the model
  return createModel(dam, reservoir, geometry, hydraulics, erosion, solver,
                     outputs);
}

api::IDBModel_t DBFactory::createAwelModel() {
  // dam object, the same for all models so far
  auto damBlock = parser_->getBlockWithName("DAM");
  double relHeight = damBlock->getTagWithName("height")->getDoubleValue() -
                     damBlock->getTagWithName("bottom_level")->getDoubleValue();
  BB_ASSERT(0.0 < relHeight && relHeight <= 10.0,
            "Error in tag 'height' in block 'DAM': "
            "0.0 < expected value <= 10.0");
  api::IDam_t dam = createDamObject();

  auto simBlock = parser_->getBlockWithName("SIMULATION");
  double erosionLimit =
      simBlock->getTagWithName("erosion_limit")->getDoubleValue();
  BB_ASSERT(erosionLimit >= 0.0, "Error in tag 'erosion_limit' in block "
                                 "'SIMULATION': expected value > 0.0");

  // reservoir object
  api::IParserBlock_t reservoirBlock = parser_->getBlockWithName("RESERVOIR");
  double vr = reservoirBlock->getTagWithName("volume")->getDoubleValue();
  BB_ASSERT(vr > 0.0,
            "Error in tag 'volume' in block 'RESERVOIR': expected value > 0.0");
  double lir =
      reservoirBlock->getTagWithName("initial_level")->getDoubleValue();
  BB_ASSERT(lir > 0.0, "Error in tag 'initial_level' in block 'RESERVOIR': "
                       "expected value > 0.0");
  if (lir > dam->getHeight())
    BB_WARNING("Warning: 'initial_level' in block 'RESERVOIR' is larger than "
               "'height' in 'DAM'!");
  auto inflow_tag = reservoirBlock->getTagWithName("inflow");
  auto inflow = QString::fromStdString(inflow_tag->getValueAsString());
  bool ok;
  double val = inflow.toDouble(&ok);
  api::IReservoir_t reservoir;
  if (ok) {
    BB_ASSERT(val >= 0.0, "Error in tag 'inflow' in block 'RESERVOIR': "
                          "expected value >= 0.0");
    reservoir = std::make_shared<ReservoirAwel>(vr, lir, val, dam);
  } else {
    reservoir = std::make_shared<ReservoirAwel>(vr, lir, inflow, dam);
  }

  // geometry object
  double lig = 0.99 * reservoir->getInitialLevel();
  double beta = 70.0; // fixed!!!
  api::IGeometryTriTra_t geometry =
      std::make_shared<GeometryTriTra>(lig, beta, dam);

  // hydraulic object
  api::IHydraulics_t hydraulics =
      std::make_shared<HydraulicsTriTra>(reservoir, geometry);

  // erosion object
  api::IErosion_t erosion = std::make_shared<ErosionAwel>(
      reservoir->getMaximumVolume(), dam->getHeight(), dam->getCrestWidth(),
      erosionLimit, hydraulics);

  // solver
  api::IPDESystemSolver_t solver =
      createSolverObject(reservoir, geometry, hydraulics, erosion);

  // outputs
  api::outputVec outputs =
      createOutputObjects(reservoir, geometry, hydraulics, erosion, solver);

  // finally the model
  return createModel(dam, reservoir, geometry, hydraulics, erosion, solver,
                     outputs);
}

api::IDam_t DBFactory::createDamObject() {
  api::IParserBlock_t damBlock = parser_->getBlockWithName("DAM");
  double hd = damBlock->getTagWithName("height")->getDoubleValue();
  double yb = damBlock->getTagWithName("bottom_level")->getDoubleValue();
  double wc = damBlock->getTagWithName("crest_width")->getDoubleValue();
  double se = damBlock->getTagWithName("embankment_slope")->getDoubleValue();
  // catch possible input errors
  BB_ASSERT(hd > 0.0,
            "Error in tag 'height' in block 'DAM': expected value > 0.0");
  BB_ASSERT(wc >= 0.0,
            "Error in tag 'crest_width' in block 'DAM': expected value >= 0.0");
  BB_ASSERT(
      se >= 0.0,
      "Error in tag 'embankment_slope' in block 'DAM': expected value >= 0.0");
  BB_ASSERT(
      yb >= 0.0,
      "Error in tag 'bottom_level' in block 'DAM': expected value >= 0.0");
  // create instance
  return std::make_shared<Dam>(hd, wc, se, yb);
}

api::IPDESystemSolver_t DBFactory::createSolverObject(
    api::IReservoir_t reservoir, api::IGeometry_t geometry,
    api::IHydraulics_t hydraulics, api::IErosion_t erosion) {

  api::IParserBlock_t numericBlock = parser_->getBlockWithName("NUMERICS");
  std::string n = numericBlock->getTagWithName("pde_solver")->getStringValue();
  api::IPDESystemSolver_t solver;
  if (n == "euler_explicit") {
    solver = std::make_shared<PDESolverExplicitEulerFixedTimeStep>(
        reservoir, geometry, hydraulics, erosion);
  } else if (n == "rk2_midpoint") {
    solver = std::make_shared<PDESolverRK2StageMidpoint>(reservoir, geometry,
                                                         hydraulics, erosion);
  } else if (n == "rk2_heun") {
    solver = std::make_shared<PDESolverRK2StageHeun>(reservoir, geometry,
                                                     hydraulics, erosion);
  } else if (n == "rk4_classic") {
    solver = std::make_shared<PDESolverRK4StageClassic>(reservoir, geometry,
                                                        hydraulics, erosion);
  } else if (n == "euler_variable") {
    solver = std::make_shared<PDESolverExplicitEulerVariableTimeStep>(
        reservoir, geometry, hydraulics, erosion);
  } else if (n == "euler_2step") {
    solver = std::make_shared<PDESolverExplicitEuler2StepVariable>(
        reservoir, geometry, hydraulics, erosion);
  } else {
    std::ostringstream sout;
    sout << "Error in tag 'pde_solver' in block 'NUMERICS': could not find pde "
            "solver with name '"
         << n << "'!\n";
    sout << "Valid are: euler_explicit, rk2_midpoint, rk2_heun, rk4_classic, "
            "euler_variable, euler_2step";
    BB_EXCEPTION(sout.str());
  }
  double solverArg =
      numericBlock->getTagWithName("pde_solver_argument")->getDoubleValue();
  BB_ASSERT(solverArg > 0.0, "Error in tag 'pde_solver_argument' in block "
                             "'NUMERICS': expected value > 0.0");
  solver->setSolverArgument(solverArg);
  return solver;
}

api::outputVec DBFactory::createOutputObjects(api::IReservoir_t reservoir,
                                              api::IGeometry_t geometry,
                                              api::IHydraulics_t hydraulics,
                                              api::IErosion_t erosion,
                                              api::IPDESystemSolver_t solver) {
  api::outputVec outputs;
  api::blockVec blocks = parser_->getBlocks();
  // get all output blocks (could be multiple)
  for (uint ii = 0; ii < blocks.size(); ii++) {
    if (blocks[ii]->getName() == "OUTPUT") {
      api::IOutput_t output;
      std::string format =
          blocks[ii]->getTagWithName("format")->getStringValue();
      if (format == "console") {
        output = std::make_shared<ConsoleOutput>(parser_, reservoir, geometry,
                                                 hydraulics, erosion, solver);
      } else if (format == "ascii") {
        output = std::make_shared<AsciiOutput>(parser_, reservoir, geometry,
                                               hydraulics, erosion, solver);
      } else {
        std::ostringstream sout;
        sout << "Error in tag 'format' in block 'OUTPUT': non valid format '"
             << format << "'!\n";
        sout << "Valid are: console and ascii";
        BB_EXCEPTION(sout.str());
      }
      double dt = blocks[ii]->getTagWithName("timestep")->getDoubleValue();
      BB_ASSERT(
          dt > 0.0,
          "Error in tag 'timestep' in block 'OUTPUT': expected value > 0.0");
      output->setOutputTimeStep(dt);
      api::IParserTag_t tagWithValues = blocks[ii]->getTagWithName("values");
      stringVec values;
      if (tagWithValues->hasList()) {
        values = tagWithValues->getValueList();
      } else {
        values.push_back(tagWithValues->getStringValue());
      }
      output->setValues(values);
      outputs.push_back(output);
    }
  }
  return outputs;
}

api::IDBModel_t
DBFactory::createModel(api::IDam_t dam, api::IReservoir_t reservoir,
                       api::IGeometry_t geometry, api::IHydraulics_t hydraulics,
                       api::IErosion_t erosion, api::IPDESystemSolver_t solver,
                       api::outputVec outputs) {
  // create the model object
  api::IDBModel_t model = std::make_shared<DBModel>(
      dam, reservoir, geometry, hydraulics, erosion, solver, outputs);
  // Set cutoff time
  double cutoff_time = parser_->getBlockWithName("SIMULATION")
                           ->getTagWithName("cutoff_time")
                           ->getDoubleValue();
  model->setCutoffTime(cutoff_time);
  // set abort criterium
  double acrit = parser_->getBlockWithName("SIMULATION")
                     ->getTagWithName("precision_at_qmax")
                     ->getDoubleValue();
  BB_ASSERT(acrit > 0.0, "Error in tag 'precision_at_qmax' in block "
                         "'SIMULATION': expected value > 0.0");
  model->setAbortCriterion(acrit);
  // set simulation type
  api::IParserTag_t tag =
      parser_->getGlobalBlock()->getTagWithName("simulation");
  if (tag->getStringValue() == "full") {
    model->setSimulationType(SimulationType::FullWithOutput);
    // check if pyBB is active
    if (parser_->getGlobalBlock()->getTagWithName("pyBB")->getStringValue() ==
        "true") {
      // special output
      api::IOutputpyBB_t pyBBoutput = std::make_shared<StdpyBBOutput>(
          parser_, reservoir, geometry, hydraulics, erosion, solver);
      pyBBoutput->setOutputTimeStep(0.0);
      stringVec values;
      values.push_back("Y");
      values.push_back("reservoir_level");
      values.push_back("breach_discharge");
      pyBBoutput->setValues(values);
      model->setDataOutput(pyBBoutput);
    }
  } else if (tag->getStringValue() == "peak") {
    model->setSimulationType(SimulationType::UntilPeakDischarge);
  } else if (tag->getStringValue() == "width") {
    model->setSimulationType(SimulationType::FullWithoutOutput);
  }
  return model;
}

} // namespace model

// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef MODEL_STANDARDBREACH_H_
#define MODEL_STANDARDBREACH_H_

#include "types.h"

namespace model {

/** Default breach reference model. */
class StandardBreach {
public:
  StandardBreach() {}
  explicit StandardBreach(double damHeight) : damHeight_(damHeight) {}

  /** Update the standard breach model. */
  void updateParameters(double damHeight);

  /** Return whether the given field is defined for the default breach. */
  bool fieldIsValid(String_T field) const;
  /**
   * Calculate the value of the given field via the default breach.
   *
   * Use fieldIsValid() to determine whether the field is valid for the default
   * breach pseudomodel.
   */
  double getField(String_T field) const;

private:
  /** Calculate maximum breach discharge. */
  double qMax(double damHeight) const;
  /** Calculate the width of the top of the breach trapezoid. */
  double breachTopWidth(double damHeight) const;

  double damHeight_ = 0.0;
};

} // namespace model

#endif // MODEL_STANDARDBREACH_H_

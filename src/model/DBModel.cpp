// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 * DBModel.cpp
 *
 *  Created on: Apr 30, 2014
 *      Author: samuelpeter
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */

#include "model/DBModel.h"

#include <float.h>
#include <math.h>
#include <time.h>

#include "ExceptionHandling.h"

namespace model {

DBModel::DBModel(api::IDam_t theDam, api::IReservoir_t theReservoir,
                 api::IGeometry_t theGeometry, api::IHydraulics_t theHydraulics,
                 api::IErosion_t theErosion, api::IPDESystemSolver_t theSolver,
                 api::outputVec theOutputs)
    : dam_{theDam}, reservoir_{theReservoir}, geometry_{theGeometry},
      hydraulics_{theHydraulics}, erosion_{theErosion}, solver_{theSolver},
      outputs_{theOutputs}, t_cutoff_{3600.0} {
  // Reset Qmax_ and tmax_, the value and time of peak discharge
  Qmax_ = 0.0;
  tmax_ = 0.0;
}

double DBModel::refineSolverArgumentToTargetPrecision() {
  /* init the variables needed for convergence checks:
  break simulation, if
  - target precision is reached AND peak discharge is not at the beginning
  (usual case)
  - target precision is reached AND breach level is still larger than zero
  (usual case)
  - maximum number of iterations is reached (ITMAX)
  - number of non-converging iterations has reached a maximum number
  (NONCONVMAX) otherwise iterate by dividing the solver argument by 2
  */

  // Maximum number of iterations (i.e. halvings of the solver argument)
  uint ITMAX = 16; // never ever do more iterations than this
  // Maximum number of subsequent iterations without improved precision
  uint NONCONVMAX = 5;
  // Minimum number of integration steps prior to point of peak discharge
  uint INTCOUNTMIN = 5;
  // Minimum number of integration steps before reaching fixed bottom (vertical
  // erosion)
  uint DOWNCOUNTMIN = 10;

  double MAXBOTTOMDISTANCE = 1.0 - 1.0 / static_cast<double>(DOWNCOUNTMIN);

  uint nonConvergenceCount = 0;
  double precOld = FLT_MAX;
  double precNew;
  double QmaxOld = FLT_MIN;
  uint itCount = 0;
  uint downCount;
  uint integrationCount;

  while (true) {
    // Run the simulation until Qmax is reached
    calcOneSolutionUntilQmax(integrationCount, downCount);
    // Calculate new relative precision
    precNew = fabs((Qmax_ - QmaxOld) / QmaxOld);

    // Check whether simulation reached target precision
    if (precNew <= abortCriterion_) {
      // do not stop the convergence in case of abrupt stop
      if (getEffectiveDischarge() > 0.0) {
        // do not stop if the vertical erosion was too fast
        if (downCount == 0 || downCount >= DOWNCOUNTMIN) {
          // do not stop the convergence in case of very fast ascending to peak
          if (integrationCount > INTCOUNTMIN) {
            // Target precision reached and integration count sufficiently high
            break;
          } else if (integrationCount == 1 &&
                     geometry_->relativeDistanceToBottom() >
                         MAXBOTTOMDISTANCE) {
            // Target precision reached and breach still above fixed bottom
            break;
          }
        }
      }
      // check if we have improved the precision
    } else if (precNew > precOld) {
      // Simulation has not reached required precision and actually got worse
      nonConvergenceCount++;
      if (nonConvergenceCount == NONCONVMAX) {
        // Maximum number of non-converging iterations reached
        BB_MESSAGE("Model did not converge to required precision");
        break;
      }
    } else {
      // Simulation has not reached requried precision, but precision improved
      if (--nonConvergenceCount < 0) {
        nonConvergenceCount = 0;
      }
    }
    itCount++;
    precOld = precNew;
    QmaxOld = Qmax_;
    // trying to be more accurate: half the integration time step
    solver_->setSolverArgument(0.5 * solver_->getSolverArgument());
    if (itCount > ITMAX) {
      // Targert precision not reached but maximum number of iterations exceeded
      BB_MESSAGE("Model did not reach target precision after "
                 << itCount << " iterations");
      break;
    }
  }
  return solver_->getSolverArgument();
}

void DBModel::run() {
  // Initialise any registered output objects
  for (auto &output : outputs_) {
    output->init();
  }

  auto baseSolverArgument = solver_->getSolverArgument();
  // Refine the solver argument required to reach desired precision at Qmax_:
  // This also sets Qmax_ and tmax_, which now may not be modified again
  refineSolverArgumentToTargetPrecision(); // already sets solver argument
  qWarning() << "Solver argument was refined from" << baseSolverArgument << "to"
             << solver_->getSolverArgument();

  // Run the actual simulation according to the selected simulation type
  switch (simulationType_) {
  case SimulationType::FullWithOutput:
    this->calcOneFullSolutionUntilTcutoffWithOutput(t_cutoff_);
    break;
  case SimulationType::UntilPeakDischarge:
    if (outputs_.size() > 0)
      this->calcOneFullSolutionUntilTcutoff(0.0);
    break;
  case SimulationType::FullWithoutOutput:
    this->calcOneFullSolutionUntilTcutoff(t_cutoff_);
    break;
  default:
    qCritical() << "Simulation type not implemented:" << simulationType_;
  }
}

void DBModel::calcOneSolutionUntilQmax(uint &iCount, uint &dCount) {
  // this function is called only to assess the integration time step
  // to reach a specific accuracy in peak discharge
  iCount = 0;
  dCount = 0;
  solver_->initSystem();
  // this is the only function where we actually change Qmax_
  Qmax_ = -1000000.0; //MCH: this dummy causes an issue if inflow(*-1) is greater than it. changed from -1.0!!
  tmax_ = 0.0;
  while (Qmax_ < getEffectiveDischarge()) {
    Qmax_ = getEffectiveDischarge();
    tmax_ = solver_->getTime();
    solver_->evaluate();
    iCount++;
    if (geometry_->hasReachedBottom() && dCount == 0) {
      dCount = iCount;
    }
  }
  // output
  for (auto &output : outputs_) {
    output->writeQpLog(Qmax_, tmax_);
  }
}

void DBModel::calcOneSolutionUntilQmaxWithOutput() {
  solver_->initSystem();
  while (getEffectiveDischarge() < Qmax_) {
    for (auto &output : outputs_) {
      output->write();
    }
    solver_->evaluate();
  }
  for (auto &output : outputs_) {
    output->finalize();
  }
}

void DBModel::calcOneFullSolutionUntilTcutoff(double t_cutoff) {
  solver_->initSystem();
  while (reservoir_->getLevel() > 0.0 &&
         reservoir_->getLevel() > geometry_->getBreachlevel()) {
    // after reaching peak discharge, we check if we can break the simulation
    if (solver_->getTime() > tmax_) {
      // already reached a very small part of Qp?
      if (solver_->getTime() > t_cutoff) {
        break;
      }
    }
    // then do one integration step
    solver_->evaluate();
  }
  for (auto &output : outputs_) {
    output->writeQpLog(Qmax_, tmax_);
  }
}

void DBModel::calcOneFullSolutionUntilTcutoffWithOutput(double t_cutoff) {
  solver_->initSystem();
  while (reservoir_->getLevel() > 0.0 &&
         reservoir_->getLevel() > geometry_->getBreachlevel()) {
    // write output
    for (uint ii = 0; ii < outputs_.size(); ii++) {
      outputs_[ii]->write();
    }
    // after reaching peak discharge, we check if we can break the simulation
    if (solver_->getTime() > tmax_) {
      // already reached a very small part of Qp?
      if (solver_->getTime() > t_cutoff) {
        break;
      }
    }
    // then do one integration step
    solver_->evaluate();
  }
  for (auto &output : outputs_) {
    output->finalize();
  }
}

void DBModel::setDataOutput(api::IOutputpyBB_t dataOutput) {
  dataOutput_ = dataOutput;
  outputs_.push_back(dataOutput);
}

const doubleVecVec &DBModel::getData() {
  if (dataOutput_) {
    return dataOutput_->getData();
  } else {
    BB_EXCEPTION("no data output defined!")
  }
}

uint DBModel::getNumTimeSteps() {
  if (dataOutput_) {
    size_t ret = dataOutput_->getData().at(0).size();
    BB_ASSERT(ret <= UINT_MAX, "return value overflow");
    return static_cast<uint>(ret);
  } else {
    BB_EXCEPTION("no data output defined!")
  }
}

double DBModel::getEffectiveDischarge() {
  return hydraulics_->getDischarge() -
         reservoir_->getInflow(solver_->getTime());
}

} // namespace model

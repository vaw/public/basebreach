// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef TYPES_H_
#define TYPES_H_

#include <QtCore>

#include <cmath>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

// constants
#define G 9.81

// some global typdefs
typedef unsigned int uint;
typedef std::vector<double> doubleVec;
typedef std::vector<std::string> stringVec;
typedef std::vector<std::vector<double>> doubleVecVec;
typedef std::map<std::string, double> stringDoubleMap;

class ValuePair {
public:
  ValuePair() {}
  ValuePair(double v1, double v2) { v1_ = v1, v2_ = v2; }
  virtual ~ValuePair() { ; }
  void operator()(double v1, double v2) { v1_ = v1, v2_ = v2; }
  bool isFirstDefined() { return !std::isnan(v1_); }
  double first() { return v1_; }
  bool isSecondDefined() { return !std::isnan(v2_); }
  double second() { return v2_; }

private:
  double v1_, v2_;
};

class Point2 {
public:
  Point2() {}
  Point2(double x, double y) {
    x_ = x;
    y_ = y;
  }
  double distance(Point2 p) {
    return sqrt(pow(p.getX() - x_, 2.0) + pow(p.getY() - y_, 2.0));
  }
  double getX() { return x_; }
  double getY() { return y_; }
  Point2 operator-(Point2 p) { return Point2(x_ - p.getX(), y_ - p.getY()); }
  Point2 operator+(Point2 p) { return Point2(x_ + p.getX(), y_ + p.getY()); }
  Point2 operator+=(Point2 p) { return Point2(x_ + p.getX(), y_ + p.getY()); }
  Point2 operator*(double d) { return Point2(x_ * d, y_ * d); }
  Point2 operator/(double d) { return Point2(x_ / d, y_ / d); }

private:
  double x_;
  double y_;
};

namespace api {
class IDBModel;
class IDam;
class IDistribution;
class IReservoir;
class IErosion;
class IFullModel;
class IGeometry;
class IGeometryPolynom;
class IGeometryTriTra;
class IHydraulics;
class IOutput;
class IOutputpyBB;
class IPDESystemSolver;
class IParameter;
class IParserTag;
class IParserBlock;
class IParser;

typedef std::shared_ptr<IDBModel> IDBModel_t;
typedef std::shared_ptr<IDam> IDam_t;
typedef std::shared_ptr<IDistribution> IDistribution_t;
typedef std::shared_ptr<IReservoir> IReservoir_t;
typedef std::shared_ptr<IErosion> IErosion_t;
typedef std::shared_ptr<IGeometry> IGeometry_t;
typedef std::shared_ptr<IGeometryPolynom> IGeometryPolynom_t;
typedef std::shared_ptr<IGeometryTriTra> IGeometryTriTra_t;
typedef std::shared_ptr<IHydraulics> IHydraulics_t;
typedef std::shared_ptr<IOutput> IOutput_t;
typedef std::vector<IOutput_t> outputVec;
typedef std::shared_ptr<IOutputpyBB> IOutputpyBB_t;
typedef std::shared_ptr<IParameter> IParameter_t;
typedef std::shared_ptr<IPDESystemSolver> IPDESystemSolver_t;
typedef std::shared_ptr<IParserTag> IParserTag_t;
typedef std::vector<IParserTag_t> tagVec;
typedef std::shared_ptr<IParserBlock> IParserBlock_t;
typedef std::vector<IParserBlock_t> blockVec;
typedef std::shared_ptr<IParser> IParser_t;
typedef std::shared_ptr<IDistribution> IDistribution_t;
typedef std::shared_ptr<IParameter> IParameter_t;

} // namespace api

namespace model {
class FullModel;

typedef std::shared_ptr<FullModel> FullModel_t;

} // namespace model

typedef QString String_T;
typedef QMap<String_T, QVariant> ParameterMap_T;
typedef QMap<String_T, ParameterMap_T> ModelConfig_T;

#endif // TYPES_H_

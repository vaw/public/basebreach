// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef CORE_H_
#define CORE_H_

#include <QtCore>

#include "model/modelDefinition.h"
#include "types.h"

namespace modelDef = model::definition;

/**
 * Core BASEbreach state handler.
 *
 * This is separated from the QApplication entity as it is available to both the
 * GUI and CLI versions of the application.
 */
class BaseBreachCore : public QObject {
  Q_OBJECT

public:
  BaseBreachCore(QObject *parent = nullptr);

  /**
   * Getter for accessing the registered model types.
   */
  const QList<modelDef::ModelType> getModelTypes() const;
  /**
   * Getter for accessing the scenario parameter list.
   */
  const QList<modelDef::Parameter> getScenarioParameters() const;

  /**
   * Export the given model run in CSV format.
   * @param modelRun The model run to export.
   * @param filename The filename to export to.
   */
  static void exportModelRun(model::FullModel_t modelRun,
                             const String_T &filename);

public slots:
  /**
   * Run a single model. When the model is done, the signal `modelRunDone()`
   * will be emitted.
   * @param config The model configuration to run.
   */
  void runModel(const ModelConfig_T &config);
  /**
   * Schedule multiple models to run in sequence.
   *
   * This is a convenience method using multiple threads; the effect is the same
   * as calling `runModel()` multiple times.
   * @param configs The list of model configurations to run.
   * @param numThreads The number of threads to use.
   */
  void runModelBatch(const QList<ModelConfig_T> configs, uint numThreads);
  /**
   * Schedule many model runs of the same type to run in parallel.
   * Unlike `runBatch()`, this will run all provided models first and emit a
   * list of finished model runs when all are complete.
   * @param configs List of model configurations to run.
   * @param numThreads Number of threads to use.
   */
  void runModelParallel(const QList<ModelConfig_T> configs, uint numThreads);

signals:
  /** Emitted when a model run has completed. */
  void modelRunFinished(const modelDef::ModelType *modelType,
                        model::FullModel_t model);
  /** Emitted when a bulk model run has completed. */
  void modelRunFinishedBulk(const modelDef::ModelType *modelType,
                            QList<model::FullModel_t> results);

protected:
  /** Helper function for creating model instances from configurations. */
  model::FullModel_t createModel(const ModelConfig_T &config) const;
  /** Return a ModelType definition from the given model type ID. */
  const modelDef::ModelType *getModelType(const String_T &id) const;

private:
  QList<modelDef::ModelType> modelTypes_;
  QList<modelDef::Parameter> scenarioParams_;
};

#endif // CORE_H_

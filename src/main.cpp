// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#include <QtCore>

#include "cli/app.h"
#include "gui/app.h"

/** Factory method for instantiating the application object. */
QCoreApplication *createApplication(int &argc, char **argv) {
  // Select the application version
  if (argc <= 1) {
    return new gui::BaseBreachGui(argc, argv);
  }
  return new cli::BaseBreachCli(argc, argv);
}

int main(int argc, char **argv) {
  // Create the appropriate Qt application
  QScopedPointer<QCoreApplication> app(createApplication(argc, argv));
  return app->exec();
}

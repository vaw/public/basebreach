// Copyright 2023 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

/*
 *  Edited: June 2023
 *      Editor: Matthew Halso
 */
 
#include "core.h"

#include <QtCore>
#include <omp.h>

#include <fstream>
#include <string>

#include "ExceptionHandling.h"
#include "api/IParser.h"
#include "model/Parser.h"
#include "model/fullModel.h"
#include "model/modelDefinition.h"
#include "modelTypes.h"
#include "types.h"

namespace modelDef = model::definition;

BaseBreachCore::BaseBreachCore(QObject *parent)
    : QObject(parent), modelTypes_(modelDef::defineModelTypes()),
      scenarioParams_(modelDef::defineScenarioParameters()) {}

const QList<modelDef::ModelType> BaseBreachCore::getModelTypes() const {
  return modelTypes_;
}

//MCH. export model run to csv. This runs when Export Model Run button in Gui is clicked and output file is selected.
void BaseBreachCore::exportModelRun(model::FullModel_t modelRun,
                                    const String_T &filename) {
  std::ofstream csvfile;
  csvfile.open(filename.toStdString());
  // Write header
  stringVec fieldNames = modelRun->getOutputVariableNames();
  csvfile << fieldNames[0];
  for (size_t i = 1; i < fieldNames.size(); i++) {
    csvfile << ", " << fieldNames[i];
  }
  csvfile << "\n";
  // Write columns
  double stopTime = modelRun->timeTS().back();
  for (uint i = 0; i < stopTime; i++) {
    modelRun->setTime(i);
    // Write column data
    csvfile << modelRun->getOutputVariableValue(0);
    for (uint f = 1; f < fieldNames.size(); f++) {
      csvfile << ", " << modelRun->getOutputVariableValue(f);
    }
    csvfile << "\n";
  }
  csvfile.close();
}

void BaseBreachCore::runModel(const ModelConfig_T &config) {
  model::FullModel_t model = createModel(config);
  model->runModel();
  // Get model type
  String_T modelTypeId = config["PROJECT"]["model"].toString();
  const modelDef::ModelType *modelType = getModelType(modelTypeId);
  emit modelRunFinished(modelType, model);
}

void BaseBreachCore::runModelBatch(const QList<ModelConfig_T> configs,
                                   uint numThreads) {
  const uint numRuns = configs.size();
  omp_set_num_threads(numThreads);
#pragma omp parallel for schedule(dynamic, 1)
  for (uint i = 0; i < numRuns; i++) {
    model::FullModel_t model = createModel(configs[i]);
    model->runModel();
    // Get model type
    String_T modelTypeId = configs[i]["PROJECT"]["model"].toString();
    const modelDef::ModelType *modelType = getModelType(modelTypeId);
    // Emit results immediately
    emit modelRunFinished(modelType, model);
  }
}

void BaseBreachCore::runModelParallel(const QList<ModelConfig_T> configs,
                                      uint numThreads) {
  if (configs.size() == 0) {
    return;
  }
  QList<model::FullModel_t> results;
  const uint numRuns = configs.size();
  // Run models
  omp_set_num_threads(numThreads);
#pragma omp parallel for schedule(dynamic, 1)
  for (uint i = 0; i < numRuns; i++) {
    model::FullModel_t model = createModel(configs[i]);
    model->runModel();
    results.append(model);
  }
  // Emit signals for all models at once
  String_T typeStr = configs[0]["PROJECT"]["model"].toString();
  const modelDef::ModelType *modelType = getModelType(typeStr);
  emit modelRunFinishedBulk(modelType, results);
}

model::FullModel_t
BaseBreachCore::createModel(const ModelConfig_T &config) const {
  // Get model type
  String_T modelId = config["PROJECT"]["model"].toString();
  api::IParser_t parser = std::make_shared<model::Parser>();
  // Esoteric tags copied from original implementation; they control internal
  // behaviour of the model and are controlled by CLI arguments.
  std::string simulationType = "full";
  if (config.contains("GLOBAL") && config["GLOBAL"].contains("simulation")) {
    simulationType = config["GLOBAL"]["simulation"].toString().toStdString();
  }
  parser->setGlobalTag("simulation", simulationType);
  double console = 0.0;
  if (config.contains("GLOBAL") && config["GLOBAL"].contains("console")) {
    console = config["GLOBAL"]["console"].toDouble();
  }
  parser->setGlobalTag("console", console);
  parser->setGlobalTag("pyBB", "true");
  // Populate parser
  parser->readInputPars(config, modelId);
  // Create model
  return std::make_shared<model::FullModel>(parser);
}

const modelDef::ModelType *
BaseBreachCore::getModelType(const String_T &id) const {
  bool found = false;
  const modelDef::ModelType *modelType = nullptr;
  // Process known model types
  for (const auto &type : modelTypes_) {
    if (type.getId() == id) {
      modelType = &type;
      found = true;
      break;
    }
  }
  BB_ASSERT(found,
            "Unknown model type ID encountered: \"" + id.toStdString() + "\"");
  return modelType;
}

const QList<modelDef::Parameter> BaseBreachCore::getScenarioParameters() const {
  return scenarioParams_;
}

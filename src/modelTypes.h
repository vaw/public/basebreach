// Copyright 2022 ETH Zurich, Laboratory of Hydraulics, Hydrology and Glaciology

#ifndef MODELDEFINITIONS_H_
#define MODELDEFINITIONS_H_

#include <QtCore>

#include "model/modelDefinition.h"
#include "types.h"

namespace model::definition {

/** Define the available model types, their parameters and parameter limits. */
QList<ModelType> defineModelTypes();

/** Define the available scenario parameters and their limits. */
QList<Parameter> defineScenarioParameters();

/** Global validation rules for scenario parameters. */
QList<ParameterLimitMessage>
validateScenario(const ModelConfig_T &config,
                 const QList<Parameter> scenarioParams);

} // namespace model::definition

#endif // MODELDEFINITIONS_H_

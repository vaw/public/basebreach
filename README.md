# BASEbreach

BASEbreach is a dam breach modelling software allowing comparison of different parameter models.

## Usage

Please refer to the [BASEbreach Wiki](https://gitlab.ethz.ch/vaw/public/basebreach/wikis/home) for usage instructions.

### Short User Manual

The definitions `-DBB...` represent different dam breach models. They differ in their submodules as listed below. On the command line any of these model can be run, specifying the model type in the command file (see *examples* directory).

| Module     | *Macchione*         | *Awel*                 | *Peter*        | *PeterCal*      |
|------------|---------------------|------------------------|----------------|-----------------|
| Reservoir  | Maximum Volume      | Maximum Volume, inflow | Maximum Volume | Released Volume |
| Geometry   | Triangle, Trapezoid | Triangle, Trapezoid    | Polynom        | Polynom         |
| Hydraulics | Critical            | Critical               | Critical       | Critical        |
| Erosion    | MPM                 | MPM, calibrated        | Empirical      | Empirical       |

Detailed description of the implemented dam breach models, such as model approximations and model limitations, can be found in according literature. No default values implemented, i.e. all model parameters must be defined explicitely within their valid range. The references are:

* *Macchione*: [Macchione, Francesco (2008). Model for Predicting Floods due to Earthen Dam Breaching. I: Formulation and Evaluation. Journal of Hydraulic Engineering, 134(12): 1688–1696.](https://doi.org/10.1061/(ASCE)0733-9429(2008)134:12(1688))
* *AWEL*: [Vonwiller, Lukas; Vetsch, David F.; Peter, Samuel. J.; Boes, Robert M. (2015). Methode zur Beurteilung des maximalen Breschenabflusses bei progressivem Bruch homogener Erdschüttdämme an kleinen Stauhaltungen. Wasser Energie Luft, 107(1): 37–43.](https://www.research-collection.ethz.ch/handle/20.500.11850/107722)
* *Peter*: [Peter, Samuel J.; Nagel, Joseph B.; Marelli, Stefano; Boes, Robert M.; Sudret, Bruno; Vetsch, David; Siviglia, Annunziato (2018). Development of probabilistic dam breach model using Bayesian inference. Water Resources Research.](https://doi.org/10.1029/2017WR021176)

The difference between *Peter* and *PeterCal* is the interpretation of the reservoir volume only. While the reservoir volume is assumed to be the storage volume at a specific reservoir level in case of *Peter*, the reservoir volume is interpreted as the released water volume during the breach process in case of *PeterCal*, i.e. the volume above the fixed bottom level at a specific reservoir level.

## Installation

The recommended means of installation for BASEbreach is via the installer packages provided as part of the [BASEbreach releases](https://gitlab.ethz.ch/vaw/public/basebreach/-/releases).

Alternatively, you can build BASEbreach from source using the instructions found in the following section.

### Building from Source

The following points apply to building BASEbreach locally using the files provided in this repository.

#### Requirements

* A compiler supporting C++ 11
* [CMake](https://cmake.org/) v3.3 or greater
* [Boost](https://www.boost.org/) (v1.57 or greater)
* [Qt5/Qt6](https://www.qt.io/) (Qt6 any version, at least v5.15 for Qt5)
* A local clone of the project source from this repository

#### CMake Configuration

Depending on the installation directories for the Boost and Qt libraries, CMake may require additional configuration to locate these dependencies.

One simple way to achieve this is to set the `CMAKE_PREFIX_PATH` environment variable to a list of paths including the Qt and Boost libraries (e.g. `C:/src/Qt6/6.1.3/msvc2019_64;C:/src/boost_1_76_0/stage`).

#### Collecting Shared Qt Libraries

After building the project using CMake and the configured build system, the BASEbreach executable will have been compiled and generated in the project's build directory.

On Windows, the resulting executable is generally not standalone and will require Qt libraries to function. These can be collected via the `windeployqt` utility provided by Qt. Run it from the generated location of the BASEbreach executable as in the example below:

```
C:\path\to\qt\6.1.3\msvc2019_64\bin\windeployqt.exe ./BASEbreach.exe
```

This will scan the BASEbreach executable, identify any required DLLs and place them in the directory.

This process only has to be performed once, subsequent builds of the BASEbreach executable will work with the generated folder structure.

> **Note:** If you are a developer and want to create an installer package or local installation of BASEbreach, please refer to the corresponding section in the [Developer Notes](https://gitlab.ethz.ch/vaw/public/basebreach/-/blob/master/DEVELOPERS.md).

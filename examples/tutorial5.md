# Tutorial 5: Monte Carlo Simulation

## Introduction

The BASEbreach simulation workflow supports uncertainty analysis using the Monte-Carlo method. This tutorial will show how to enable MC features in BASEbreach and define parameter distributions.

The selection of distributions and parameter uncertainties is outside the scope of this tutorial.

## Workflow

### Supported Model Types

Monte Carlo simulation is currently only supported for the *'Peter'* and *'PeterCal'* models.

### Enabling MC Features

For models supporting Monte-Carlo simulation, the GUI exposes an additional checkbox at the bottom of the model parameters panel. This checkbox allows switching into MC Mode, which will replace the fixed parameters with controls allowing specification of parameter distributions.

<img src="images/tutorial5-01-mc_parameters.png" width="600"/>

*Figure 1: Transformation of parameter input mask when MC mode is enabled.*

In addition to the input parameters, the GUI will also replace the plot windows in the right panel. These will be covered in detail in a later section.

### Specifying Parameter Distributions

To specify a parameter distribution for a parameter, press the *'Define...'* button next to the parameter name. This will open the parameter definition window.

<img src="images/tutorial5-02-distribution_editor.png" width="400"/>

*Figure 2: Parameter Distribution editor window with example distribution entered.*

The first field is an optional identifier allowing the user to name the description (2). The second field is a dropdown menu used to select the type of distribution used (3).

Every distribution supports two input modes: parameter-based input, or moment-based input. Which of the two is used can be selected via the parameter type dropdown (4), which controls how the first and second parameter field are interpreted (5).

In the example above, the parameters for a normal distribution are the mean and standard deviation.

Finally, the sampler also supports a lower and upper bound for the parameter, specified via the *'Bounds'* fields (6). In the example above, the slide angle is not allowed to go below 60 degrees or above 80 degrees, and any sample values outside these bounds will be clamped to the nearest bound.

To store the distribution, press the *'OK'* button (7) at the bottom of the window.

Note that while parameter distributions are stored throughout the current application session, they are not saved along with the project file.

### Distribution Types

The following table lists an overview of all available distribution types and their respective parameters:

| Distribution Type | First parameter | Second parameter   |
|-------------------|-----------------|--------------------|
| Uniform           | Lower limit     | Upper limit        |
| Normal            | Mean            | Standard deviation |
| Log-Normal        | Location        | Scale              |
| Exponential       | Lambda          | *(unused)*         |
| Gamma             | Shape           | Scale              |

### Running Simulations

After specifying distributions for all model parameters, press the *'Run MC Analysis'* button at the bottom of the model tab. This will prompt the user to select the number of parameter samples to generate and start the simulation.

Note that depending on the model setup number of samples, the simulation may take a while to complete.

### Interpreting the Results

After completion of the simulation, the MC-specific plots in the right panel of the model tab will be populated with the MC analysis results.

<img src="images/tutorial5-03-results.png" width="600"/>

*Figure 3: Example results after a Monte Carlo simulation run.*

The top plot in the results view (8) shows a 2D kernel density estimate (KDE) of the parameter samples. The colour indicates the probability density of a given combination of final breach width and peak breach discharge.

The bottom plot shows a 1D probability density function (PDF) of the breach width (9).

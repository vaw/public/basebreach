# Tutorial 4: Settlement and Overtopping

## Introduction

This tutorial will cover the simulation of dam breaches through settlement and subsequent overtopping of the remaining effective dam height.

As BASEbreach does not model the events leading to the forming of the initial breach, the exact mechanism is not relevant to the simulation definition.

In this case, we will only concern ourselves with the post-overtopping state, which is reflected in our model setup by a significantly larger initial breach than in the previous tutorials.

## Model Setup

For this tutorial.

| Parameter        | Value   |
|------------------|---------|
| Height           | 10.0    |
| Bottom level     | 4.0     |
| Crest width      | 4.5     |
| Embankment slope | 3.0     |
| Storage volume   | 50000.0 |
| Basin shape      | 2.5     |
| Breach level     | 6.5     |
| Reservoir level  | 9.75    |
| Simulation time  | 2500    |

For illustration, we will only enable the *'Peter'* model for this tutorial.

## Simulation Run

Note that depending on the specified depth of the initial breach, BASEbreach may produce a warning indicating that the given initial breach depth exceeds 50% of the dam height.

This is purely informational as initial breaches of this size are not expected for most model scenarios; BASEbreach will still run correctly and produce valid output.

<img src="images/tutorial4-01-large_initial_breach.png" width="600"/>

*Figure 1: Model setup and overview hydrograph for a simulation with large initial breach (over 50% of the dam height).*

Note that this model run does not display the initial period of low discharge that was present in previous tutorials. This is due to the initial breach being large enough to immediately produce erosion sufficient to cause further erosion of the breach sides.

<img src="images/tutorial4-02-initial_breach_size.png" width="600"/>

*Figure 2: Visualization of the initial breach at time t=0.*

This immediate transition into a high discharge state is the key difference between this tutorial and the previous model setups.

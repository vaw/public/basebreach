# Tutorial 3: Simulation With Reservoir Inflow

## Introduction

This tutorial will demonstrate the BASEbreach workflow for simulations using constant or variable reservoir inflow.

## Constant Reservoir Inflow

First, we will look at a basic scenario with a constant reservoir inflow rate.

The overall simulation scenario will be identical to the one used for tutorials 1 and 2. If you just have an available project file from either of those tutorials, you can use load it and use it here.

Alternatively, here are the parameters we will use for this tutorial:

| Parameter        | Value   |
|------------------|---------|
| Height           | 6.0     |
| Bottom level     | 0.0     |
| Crest width      | 4.5     |
| Embankment slope | 3.0     |
| Storage volume   | 50000.0 |
| Basin shape      | 1.5     |
| Breach level     | 5.0     |
| Reservoir level  | 5.5     |

### Model Parameters

For this tutorial, we will only use the *'Awel'* model, which has no user-configurable parameters beyond those defined in the scenario.

This is done to keep the tutorial simple and focused on the effects of the inflow parameter. All models are compatible with reservoir inflow using the same approach.

At this stage, you may want to run the model once to get a baseline of the results produced for a simulation without any inflow:

<img src="images/tutorial3-01-no_inflow.png" width="600"/>

*Figure 1: Example model setup with no reservoir inflow.*

Next, we will apply a gentle inflow into the reservoir using a fixed rate of 1.0 m3/s.

### Inflow Parameter

In the scenario parameter list, locate the checkbox labelled *'Enable reservoir inflow'* and enable it.

This will in turn enable the *'Inflow rate'* parameter input and *'Browse...'* button. The latter will be covered later in this tutorial. For now, enter the desired inflow rate of 1.0 m3/s into the input field and re-run the simulation.

| Parameter   | Value |
|-------------|-------|
| Inflow rate |  1.0  |

<img src="images/tutorial3-02-const_inflow.png" width="600"/>

*Figure 2: Example model setup with constant reservoir inflow of 1.0 m3/s.*

Despite the low inflow rate, the results show that the peak discharge is slightly higher than in the previous run, and the peak occurs later in the simulation.

### Limitations of Fixed-rate Inflow

If we try to increase the inflow rate (e.g. to 5.0 m3/s), we will encounter an exception during the simulation:

    Error: new reservoir level cannot be above the dam height

This exception indicates that at some point during the simulation, the reservoir level exceeds the dam height, which causes the simulation to abort.

As can be seen in the previous screenshot (Figure 2), during the first 400 seconds of the simulation, the breach has not yet progressed sufficiently to discharge the inflow, causing the reservoir level to rise.

To avoid this, we can do any of the following:

- Reduce the inflow rate to a value that will not overtop the dam before the breach fully develops

- Lower the initial reservoir level to give the reservoir room to fill up without overtopping the dam

- Increase the size of the initial breach (i.e. lower the initial breach elevation) to increase discharge in the early stages of the simulation

Additionally, it is possible that for large solver arguments, the reservoir overfill limit will be exceeded while refining the solver argument. For this reason, a smaller solver argument may help in avoiding false positives.

## Variable Reservoir Inflow

In addition to the fixed-rate inflow covered above, BASEbreach also supports variable-rate inflow specified via an external text file.

The inflow hydrograph is defined via any number of control points, each with a time and inflow rate. The inflow rate at any given time is then linearly interpolated between the control points.

The inflow hydrograph is defined in a whitespace-separated text file, with each line containing a time and inflow rate. The time is specified in seconds, and the inflow rate is specified in m3/s.

    # Time    Inflow rate
    500       0.0
    1000      10.0
    1500      5.0
    2000      1.0
    2500      4.0
    3000      0.0

The location of the input file can be specified via the same parameter as the fixed-rate inflow by entering the input file's absolute path. Alternatively, the file can also be selected via the *'Browse...'* button below the inflow rate input field.

As this simulation will take significantly longer than the previous ones, we must also adjust the *'Simulation time'* parameter to ensure the full process is simulated.  
Additionally, we will use a lower solver argument of 32 to avoid false positives during solver argument refinement, as covered in the previous section.

| Parameter       | Value |
|-----------------|-------|
| Simulation time |  4000 |
| Solver argument |   32  |

Running the above simulation using this inflow hydrograph file produces the following results:

<img src="images/tutorial3-03-variable_inflow.png" width="600"/>

*Figure 3: Example model setup with variable inflow rate.*

Note that the model ran successfully despite the much higher peak inflow, without causing the overtopping exception described above.

This is due to the inflow file slowly ramping up to the peak inflow rate, which gives the model time to develop a larger breach capable of discharging the increased inflows without overtopping.

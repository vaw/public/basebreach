# Tutorial 2: Model Setup With Limited Breach Width

## Introduction

This tutorial will demonstrate the BASEbreach workflow for model scenarios with limited dam erosion width.

Erosion width limiting is achieved by evaluating the maximum breach width at each simulated time step. If the breach width exceeds the specified maximum, the erosion rate is immediately reduced to zero, resulting in no further erosion.

It should be noted that this approach will generally lead to overshoot in the final breach width of the dam as the model will not aim towards hitting any given erosion limit; it will simply disable erosion when the limit is exceeded.

This effect and ways to mitigate it will be explored further in a later section.

## Model Setup

This tutorial will follow the same overall model setup as used for tutorial 1. If you have a copy of the scenario parameters from tutorial 1, you may import and reuse it here.

Alternatively, here are the parameters we will use for this tutorial:

| Parameter        | Value   |
|------------------|---------|
| Height           | 6.0     |
| Bottom level     | 0.0     |
| Crest width      | 4.5     |
| Embankment slope | 3.0     |
| Storage volume   | 50000.0 |
| Basin shape      | 1.5     |
| Breach level     | 5.0     |
| Reservoir level  | 5.5     |

For simplicity, we will only enable on the *'Awel'* model for this tutorial.

## Erosion Width Limiting

At the bottom of the parameter widget, locate the checkbox labelled *'Limit breach erosion'* and check it.

Next, specify the desired maximum breach width as 10 metres via the *'Maximum breach width'* input field.

| Parameter            | Value |
|----------------------|-------|
| Maximum breach width |  10.0 |

If we now run the simulation, we will find that the peak discharge occurs earlier, with the trailing end of the discharge hydrograph stretching well beyond the end of the graph.

<img src="images/tutorial2-01-overview_qmax.png" width="600"/>

*Figure 1: Simulation results with breach width limited to 10 metres.*

The reason for this lower peak discharge becomes apparent if we switch the plot variable at the top of the overview panel to *'breach width [m]'*.

<img src="images/tutorial2-02-breach_width.png" width="600"/>

*Figure 2: Breach width over time with breach width limited to 10 metres.*

Note that the actual peak breach width is slightly higher than the 10-metre limit we specified. This is the overshoot mentioned in the introduction to this tutorial, and it is unavoidable in most scenarios.

### Reducing Breach Width Overshoot

There are several ways to mitigate this overshoot:

1. The primary source of the overshoot is the discretization of the model. We can lower the current solver's *'Solver Argument'* parameter to improve the resolution of the model. This parameter is located in the *'Differential Equation Solver'* group in the Advanced Parameters section at the bottom of the parameter panel.

2. Likewise, the *'Relative precision at Qmax'* parameter in the *'Simulation'* parameter group is used to refine the solver argument to reach a given relative precision.

    Lowering this value will therefore also increase the resolution of the model and thus reduce final breach width overshoot.

3. Due to the erosion limit only being applied once the eroded width is greater than the maximum permitted breach width, it may be beneficial to use a value lower than the intended maximum width to avoid exceeding this limit.

Note that options 1 and 2 are linked and may require tweaking for optimal results. Additionally, they directly affect the overall computational time of the model and may lead to significantly longer simulations.

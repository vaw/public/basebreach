# Tutorial 1: Basic Simulation Workflow

## Introduction

This tutorial will illustrate the basic simulation workflow used to simulate a dam breach scenario using BASEbreach.

## Workflow

### Overview Panel

<img src="images/tutorial1-01-overview_blank.png" width="600"/>

*Figure 1: Empty scenario after first opening BASEbreach.*

The overview panel is the initial tab BASEbreach will open when the application is launched. It contains the project settings (1), the scenario-specific parameters shared between all models (2), as well as buttons for importing or exporting the current scenario (3).

The right half of the overview panel contains a multimodel hydrograph used to display simulation output for all enabled models (4), and a table listing the enabled models (5).

### Defining the Scenario

When first opening BASEbreach, an empty scenario is displayed.

For this tutorial, we will manually specify the scenario parameters. If you have an existing scenario saved, you can also import it via the *'Import...'* button at the bottom of the left-hand panel.

#### Dam Geometry

We must first define the overall dam geometry parameters. These are listed in the *'Dam Geometry'* section of the left-hand panel in the *'Overview'* tab.

Note that for any elevation parameters, you may use either the absolute elevation or the elevation relative to the dam foot. When using absolute elevations, set the *'Bottom level'* field to 0.0. For details on how elevations are interpreted, please refer to the corresponding section in the [scenario parameter reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/general-parameters#bottom-level).

For this tutorial, we will use the following, fictional geometry:

| Parameter        | Value |
|------------------|-------|
| Height           |  6.0  |
| Bottom level     |  0.0  |
| Crest width      |  4.5  |
| Embankment slope |  3.0  |

#### Reservoir

Next, the reservoir properties must be specified. Aside from the reservoir volume, the *'Basin shape'* parameter is required. This is an exponent used to define the relation between the current reservoir water level ($`H_r`$) and the remaining reservoir volume: $`V_r = \beta \times (H_r)^{\alpha}`$, $`\alpha`$ being the reservoir shape exponent.

We will use a reservoir storage volume of 50'000 m³ and a shape exponent of 1.5:

| Parameter      | Value |
|----------------|-------|
| Storage volume | 50000 |
| Basin shape    |  1.5  |

#### Initial Conditions

Further, we must specify the initial conditions for both the reservoir fill level and the size of the initial breach. The initial reservoir level controls the water level in the reservoir at the start of the simulation. The initial breach level controls the elevation of the bottom of the breach at the start of the simulation and must be lower than the initial reservoir level for a valid scenario.

We will use the following initial conditions:

| Parameter        | Value |
|------------------|-------|
| Breach level     |  5.0  |
| Reservoir breach |  5.5  |

After specifying all parameters for the scenario, you can save it to a JSON file via the *'Export...'* button at the bottom of the left-hand panel. This exported scenario can be imported for new scenarios as described earlier in this tutorial.

For more information on scenario parameters, please refer to the [scenario parameter reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/general-parameters) or click the help button beside the parameter edit field to open the corresponding section of the parameter reference in your browser.

#### Simulation Parameters

Finally, we must specify the duration and resolution of the simulation. The *'Simulation time'* parameter controls the number of seconds to simulate, whereas *'Relative precision at Qmax'* controls the time step of the simulation.

This is done by iteratively refining the *'Solver argument'* until the specified target precision is reached. The solver argument is generally tied to the computational effort required, refer to the [parameter reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/general-parameters#differential-equation-solver) for solver-specific details.

This tutorial will use the following:

| Parameter                  | Value |
|----------------------------|-------|
| Simulation time            | 2000  |
| Relative precision at Qmax | 0.001 |

### Selecting Model Types

>**Note:** For details on how model types differ and what their respective application limits are, please refer to the [Model Type Reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/dam-breach-models) as this is outside the scope of this tutorial.

When first creating a new project, all model types are selected by default. For this tutorial, we will only be using the *'Awel'*, *'Macchione'*, and *'Peter'* model types.

Uncheck the *'PeterCal'* model type at the model overview table to disable the model (6). Alternatively, you can also select the enabled models via the *'Edit→Choose Models'* menu option.

<img src="images/tutorial1-02-unchecking_models.png" width="600"/>

*Figure 2: Unchecking a model in the model overview table will disable it globally and not include it in any simulations.*

### Model Tabs

Every model type available in BASEbreach has its own designated tab for both model parameter specification and result display. Disabling a model type will also grey out the tab for this model type.

<img src="images/tutorial1-03-modeltab_blank.png" width="600"/>

*Figure 3: Overview over the model tab for the Peter model type.*

Model panels are created for each implemented model type. Its left panel contains model-specific parameters (7), which can be imported or exported similar to scenario parameters (8).

>**Note:** The same parameter may be interpreted differently by different model types. For details on how model parameters are implemented, please refer to the [model parameter reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/dam-breach-models).

The right panel contains a breach cross-section overview (9), a hydrograph plot (10), and a separate run button that will only re-run the simulation for the current model (11).

### Customizing Model Parameters

By default, all model parameters are set to reasonable but arbitrary default values. This means that they require manual calibration or adjustment to accurately represent a given real-world scenario.

The calibration of these parameters must be done on a per-model basis as the same parameter values are interpreted differently by different models (the parameter values can generally be shared between the *'Peter'* and *'PeterCal'* models as their only implementation difference lies in the interpretation of the reservoir volume parameter).

Note that the *'Awel'* model has been created to suit the dam conditions in the canton of Zurich and therefore does not provide any user-provided configuration. Its applicability must be determined by the user. For more information, please refer to the [Model Type Reference](https://gitlab.ethz.ch/vaw/public/basebreach/-/wikis/dam-breach-models#awel).

For the scenario in this tutorial, we will use the following model parameters:

| Model     | Parameter                    | Value    |
|-----------|------------------------------|----------|
| Macchione | Breach slide angle           | 78.69    |
| Macchione | Scaling coefficient          |  0.07    |
| Peter     | Breach slide angle           | 67.5     |
| Peter     | Exponent of hydraulic radius | -0.65    |
| Peter     | Exponent of flow velocity    |  4.15    |
| Peter     | Scaling coefficient          |  0.00025 |

Like scenario parameters, model configuration parameters can also be saved and exported for re-use in other scenarios via the *'Export...'* button at the bottom of the left-hand panel.

### Saving and Loading Projects

Now that all scenario and model parameters have been specified, we can save the current project to disk. This can be done via the *'File→Save'* menu option. To load this project later, simply select the *'File→Open project'* menu option after opening BASEbreach.

Note that project data only contains the scenario and model parameters, not the results of any simulations. These can be exported separately if desired as covered later in this tutorial.

### Running the Simulation

After configuring the scenario and required model parameters, the simulation can be run by clicking the *'Run all'* button at the bottom right of the application window. This action will run all the enabled models. Alternatively, you can run a specific model by switching to its tab and clicking the *'Run'* button.

In addition to the three selected progressive dam breach models, the *'Standard Breach'* model is always included in the results. This model provides a baseline peak discharge value for comparison with the other, more complex model implementations.

## Analysing the Results

<img src="images/tutorial1-04-overview_data.png" width="600"/>

*Figure 4: Overview panel after simulations were run.*

### Comparing Model Types

After running the simulation, the *'Overview'* panel's hydrograph (14) will be populated with hydrographs for any run models. You can use the mouse to zoom and pan the hydrograph to focus in on areas of interest within the plot.

At the top of the right-hand panel, you can choose the variable to plot (12), along with a button to reset the plot axes to their default auto-scaled values (13).

Below the hydrograph is a table of all models providing a read-out of the results at the given simulation time controlled by the below slider (15). In addition to the current (18) and peak value (17) of the currently selected plot variables, it also displays the total discharged volume up to the current simulation time (16).

Note that the *'Standard Breach'* model is always included in the results, but does not provide estimates for all plot variables. This is due to this not being a progressive dam breach model and only providing a baseline estimate for peak discharge and breach width.

### Visualizing per-Model Results

In addition to the overview hydrograph for model comparison, BASEbreach also generates a hydrograph for each model. These hydrographs are displayed next to the model parameters in the corresponding model tab.

<img src="images/tutorial1-05-modeltab_data.png" width="600"/>

*Figure 5: Model tab after simulation run with the hydrograph configured to plot breach flow velocity.*

They also provide a second drop-down allowing to customize the x-axis of the hydrograph to display (19), such as the change of breach flow velocity in relation to the current breach width.

### Exporting Model Runs

Finally, model runs can also be exported to a CSV file using the *'Export Model Run...'* button at the bottom of the model tab (20). This file can then be reimported into other software for further analysis or visualization.

Below is an example of the exported CSV file:

```csv
time [s], breach discharge [m3/s], flow velocity [m/s], ...
0,        0.682999,                1.84528,             ...
1,        0.686516,                1.84733,             ...
2,        0.690043,                1.84938,             ...
3,        0.693604,                1.85143,             ...
4,        0.697176,                1.85349,             ...
...
```

*Figure 6: Example for the CSV output format. Only the first few rows and columns are shown for brevity, but all available data is exported.*

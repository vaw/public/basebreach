# This script defines the "windeployqt_install" function, which allows the
# replication of Qt runtime libraries for a given executable into a temporary
# directory from which they are then installed by CMake.

function(windeployqt_install
    windeployqt_path
    target_executable_path
    windeployqt_temp_path
  )
  # Locate windeployqt
  if(NOT EXISTS "${windeployqt_path}")
    message(WARNING "given windeployqt path is not valid: ${windeployqt_path}")
    return()
  endif()
  message(STATUS "windeployqt found: ${windeployqt_path}")

  if(NOT EXISTS "${target_executable_path}")
    message(WARNING "Target executable not found, unable to install qt")
    return()
  endif()
 
  if(EXISTS "${windeployqt_temp_path}")
    message(STATUS "Clearing temporary directory: ${windeployqt_temp_path}")
    file(REMOVE "${windeployqt_temp_path}")
  endif()
  file(MAKE_DIRECTORY "${windeployqt_temp_path}")
  
  # Run windeployqt on the target executable
  execute_process(COMMAND ${windeployqt_path} --dir ${windeployqt_temp_path} ${target_executable_path}
    RESULT_VARIABLE windeployqt_result
  )
  if(NOT windeployqt_result EQUAL "0")
    message(WARNING "windeployqt exited with message: ${windeployqt_result}")
    return()
  endif()

endfunction()

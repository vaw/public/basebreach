# A helper script invoked by CMake's "install(SCRIPT ...)" to run windeployqt on
# the appropriate release executable.

set(project_root "${CMAKE_CURRENT_LIST_DIR}/..")
set(exe_name "BASEbreach")
set(temp_dir "${project_root}/build/windeployqt_output")

include("${project_root}/cmake/win_wrap_qt_targets.cmake")

set(target_exe_path "${project_root}/build/Release/${exe_name}.exe")
if(NOT EXISTS "${target_exe_path}")
  message(STATUS "Could not locate target binary for windeployqt")
  return()
endif()

# Read the location of the windeployqt utility from the temporary file
set(windeployqt_path_file "${project_root}/build/windeployqt_path.txt")
if(NOT EXISTS "${windeployqt_path_file}")
  message(WARNING "Could not locate windeployqt path file")
  return()
endif()
file(READ "${windeployqt_path_file}" windeployqt_path)

# Copy required Qt dependencies to temporary directory
windeployqt_install(
  "${windeployqt_path}"
  "${target_exe_path}"
  "${temp_dir}"
)

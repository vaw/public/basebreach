# 3rd Party Dependencies

This directory contains lightweight third-party dependencies that are either version-specific or customized for use with BASEbreach.

Large or shared dependencies like Qt or Boost should not be placed here; they are the build environment's concern.

Any libraries in this directory should be built with their own CMake targets and referenced directly (i.e. `<KDE.hpp>` rather than `<3rdparty/kernel-density/KDE.hpp>`). Use CMake's `target_include_directories()` command to ensure the headers are visible to any sources requiring them.

## `kernel-density/`

Core files of Tim Nugent's [Kernel Density Estimation](https://github.com/timnugent/kernel-density) (KDE) utility.

We are using the `KDE.hpp` and `KDE.cpp` sources directly without the command line.

> **Note:** The original repo by `@timnugent` uses `using namespace std;` it its header, which clutters the namespace of any files including this header.
>
> These files have been modified to keep the headers tidy.

## `qcustomplot/`

[QCustomPlot](https://www.qcustomplot.com/) (QCP) is a third-party Qt-based library for generating plots and graphs.

The latest version as of November 2021 is v2.1.0, which does not have support for Qt versions later than 6.0.

The following forum post covers the manual customization required to make it build under Qt 6.2: <https://www.qcustomplot.com/index.php/support/forum/2380>

This hack should be replaced with the latest version of QCP once a version supporting recent version of Qt is released.

classdef PostProcess < handle
    %%% TOOL TO ANALYZE A BASEbreach MODEL %%%
    % Juni 2014, Samuel J. Peter
    
    % private variables
    properties (Access = private)
        data;
        breach;
        Qpmax;
        volOut;
        ascInds;
        dQdt;
        timeInds;
        time;
        discharge;
        Z;
        Y;
        ritter;
        bAx;
        dAx;
    end
    
    % member functions
    methods (Access = public)
        % constructor
        function self = PostProcess (breachObject, dataFileName)
            self.breach = breachObject;
            if exist(dataFileName,'file')
                self.loadData(dataFileName);
            else
                error('PostProcess: filename ''%s'' not found!',dataFileName);
            end
        end
        % return peak discharge
        function out = getQpmax (self)
            out = self.Qpmax;
        end
        % return time to peak discharge
        function out = getFailureTime (self)
            [~, ind] = max(self.discharge);
            out = self.time(ind);
        end
        % return released volume
        function out = getVolOut (self)
            out = self.volOut;
        end
        % return change in discharge per time (between 5% and 95% of peak discharge)
        function out = getdQdt (self)
            out = self.dQdt;
        end
        % return the hydrograph [time,Q]
        function out = getHydrograph (self)
            out = [self.time, self.discharge];
        end
        % return the time when the breach has reached the bottom of the dam
        function out = getTimeAtBottom (self)
            out = self.time(sum(self.Y>0));
        end
        % return first moment with respect to the discharge
        function out = getDischargeMoment (self)
            out = sum(0.125*(self.time(2:end)+self.discharge(1:end-1)).^2 .* self.time(2)) ./ self.getVolOut();
        end
        % return the duration of the simulation (original data)
        function out = getDuration (self)
            out = self.data(end,1);
        end
        % return the hydrograph in a normalized way, described by the
        % maximal time 'tMax' and number of points on the time axis 'N'
        function out = getNormalizedHydrograph (self,tMax,N)
            xi = linspace(0,tMax,N)';
            t = self.data(:,1);
            q = self.data(:,2);
            if t(end)~=tMax
                t = [t; tMax];
                q = [q; 0];
            end
            % do linear interpolation
            yi = interp1(t,q,xi,'linear');
            out = [xi, yi];
        end
        % example of post processing tool
        function plotDischarge (self)
            figure('Color',[1.0 1.0 1.0]);
            plot(self.time./60,self.discharge,'b','LineWidth',1)
            hold on
            xlabel('time [min]')
            ylabel('breach discharge [m3/s]')
            grid on
            plot([self.time(self.ascInds(1)),self.time(self.ascInds(2))]./60,...
                [self.discharge(self.ascInds(1)),self.discharge(self.ascInds(2))],...
                'r--','LineWidth',1)
            annotation('textbox',[0.55 0.45 0.35 0.45],...
                'String',{'peak discharge:', sprintf('%1.1f [m^3/s]',self.Qpmax), [],...
                          'released volume:', sprintf('%1.3f [Mio m^3]',self.volOut/10^6), [],...
                          'change in discharge per time', '(ascending part, red):', sprintf('%1.3f [m^3/s^2]',self.dQdt)},...
                'FontSize',10,'Color',[0 0 0],...
                'LineStyle','-','EdgeColor',[0.2 0.2 0.2],'LineWidth',0.5,...
                'BackgroundColor',[1 1 1]);
            export_fig 'discharge' '-pdf' '-nocrop'
        end
        % to evaluate the development of the breach visually
        % 'tRate' is the rate of timesteps at which a plot update is done
        % to evaluate the development of the breach visually
        function plotBreach (self)
            % initialize
            [width, height] = self.initPlot();
            inds = 1:length(self.time);
            % create slider with callback function
            hSlider = uicontrol('Style', 'slider','Position',[width*0.8 height*0.97 width*0.2 height*0.03],...
                'Min',inds(2),'Max',inds(end),'Value',inds(2),'SliderStep',[1 10]./(inds(end)-inds(1)),...
                'Callback',@self.breachUpdate);
            self.breachUpdate(hSlider);
        end
        % save the breach development plots as .gif animation
        % 'tplot' is the delta time at which a plot is added to the animation
        function makeGIF (self, tplot)
            % initialize
            self.initPlot();
            outfile = 'dambreach.gif';
            % create discharge curve
            self.updateDischargePlot(2);
            % create the breach geometry
            self.updateBreachGeometry(2);
            % first frame, create the file
            drawnow
            [imind,cm] = rgb2ind(frame2im(getframe(1)),256);
            imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
            % loop over all time steps and plot each time 'tplot'
            fcount = 1;
            for ii=2:length(self.time)
                if self.time(ii)>=fcount*tplot
                    % update or create discharge curve
                    self.updateDischargePlot(ii);
                    % update the breach geometry
                    self.updateBreachGeometry(ii);
                    % add to gif file
                    drawnow
                    [imind,cm] = rgb2ind(frame2im(getframe(1)),256);
                    % append to the existing .gif-file
                    imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
                    fcount = fcount + 1;
                end
            end
            close all
        end
    end
    
    % private member functions
    methods (Access = private)
        % do some basic post processing
        function loadData (self, filename)
            try
                temp = importdata(filename);
                self.data = temp.data;
            catch
                error('something went wrong when loading the result file ...');
            end
            % find peak discharge
            self.Qpmax = max(self.data(:,2));
            % calculate total outflow
            self.volOut = sum((self.data(2:end,1)-self.data(1:end-1,1)) .* ...
                (self.data(1:end-1,2)+self.data(2:end,2))/2);
            % truncate
            ind1 = find(self.data(:,2) >= self.Qpmax/1000,1,'first');
            ind2 = find(self.data(:,2) >= self.Qpmax/100,1,'last');
            self.timeInds = [ind1 ind2];
            self.time = self.data(ind1:ind2,1)-self.data(ind1,1); % correct time, to begin with 0
            self.discharge = self.data(ind1:ind2,2);
            self.Z = self.data(ind1:ind2,3);
            self.Y = self.data(ind1:ind2,4);
            self.ritter = logical(self.data(ind1:ind2,5));
            % calculate change in discharge per time, ascending part
            ind1 = find(self.discharge >= self.Qpmax/20,1,'first');
            ind2 = find(self.discharge >= self.Qpmax/20*19,1,'first');
            self.ascInds = [ind1 ind2];
            if ind1 == 1
                q1 = 0;
            else
                q1 = self.discharge(ind1);
            end
            if ind2 == ind1
                [~, ind2] = max(self.discharge);
            end
            self.dQdt = (self.discharge(ind2)-q1)/(self.time(ind2)-self.time(ind1));
        end
        % initialize breach plot
        function [width, height] = initPlot (self)
            % some variables
            Hd = self.breach.Hd;
            self.breach.setLevel(self.Y(end));
            maxB = self.breach.B;
            self.breach.setLevel(self.breach.Y0);
            % build the figure
            hf = figure('Color','white','Toolbar','none','Menu','none');
            screenSize = get(0,'ScreenSize');
            height = screenSize(4)*0.5;
            width = height*1.2;
            set(hf,'Position',[screenSize(3)-100-width screenSize(4)-100-height width height]);
            self.bAx = subplot(2,1,1,'Units','normalized');
            hold on; box on;
            ylim([0 Hd*1.1])
            xlim(maxB*[-0.6 0.6]);
            xlabel('breach width [m]')
            ylabel('level [m]')
            axis(self.bAx,'equal')
            self.dAx = subplot(2,1,2,'Units','normalized');
            hold on; box on;
            xlabel('time [min]')
            ylabel('breach discharge [m^3/s]')
        end
        % called from function 'plotBreach', updates the plot
        function breachUpdate (self, hObj, varargin)
            ii = round(get(hObj,'Value'));
            % update or create discharge curve
            self.updateDischargePlot(ii);
            % update the breach geometry
            self.updateBreachGeometry(ii);
        end
        % update or create discharge curve
        function updateDischargePlot (self, ii)
            % update or create discharge curve
            tr = self.time(self.ritter)./60;
            qr = self.discharge(self.ritter);
            hr = findobj(get(self.dAx,'Children'),'Tag','discharge_ritter');
            if isempty(hr)
                hr = plot(self.dAx,tr,qr,'r-','linewidth',2);
                set(hr,'Tag','discharge_ritter');
            else
                set(hr,'Xdata',tr,'Ydata',qr);
            end
            tp = self.time(~self.ritter)./60;
            qp = self.discharge(~self.ritter);
            hp = findobj(get(self.dAx,'Children'),'Tag','discharge_poleni');
            if isempty(hp)
                hp = plot(self.dAx,tp,qp,'g-','linewidth',2);
                set(hp,'Tag','discharge_poleni');
            else
                set(hp,'Xdata',tp,'Ydata',qp);
            end
            % update actual position
            ph = findobj(get(self.dAx,'Children'),'Tag','dischargePoint');
            if isempty(ph)
                ph = plot(self.dAx,self.time(ii)/60,self.discharge(ii),'ko');
                set(ph,'Tag','dischargePoint');
            else
                set(ph,'Xdata',self.time(ii)/60,'Ydata',self.discharge(ii));
            end
            ph = findobj(get(self.dAx,'Children'),'Tag','dischargeLine');
            if isempty(ph)
                ph = plot(self.dAx,self.time(ii)/60.*[0 1 1],self.discharge(ii).*[1 1 0],'k-');
                set(ph,'Tag','dischargeLine');
            else
                set(ph,'Xdata',self.time(ii)/60.*[0 1 1],'Ydata',self.discharge(ii).*[1 1 0]);
            end
        end
        % update the breach geometry
        function updateBreachGeometry (self, ii)
            z = max(0,self.Z(ii));
            % update breach shape
            self.breach.setLevel(self.Y(ii));
            [xb,yb] = self.breach.getShape();
            Hd = self.breach.Hd;
            xlims = get(self.bAx,'xlim');
            xi = [xlims(1) xb' xlims(2) xlims(2) xlims(1) xlims(1)];
            yi = [Hd yb' Hd 0 0 Hd];
            ph = findobj(get(self.bAx,'Children'),'Tag','breachShape');
            if isempty(ph)
                ph = area(self.bAx,xi,yi,'EdgeColor','None','FaceColor',[0.95 0.85 0.6]);
                set(ph,'Tag','breachShape');
            else
                set(ph,'Xdata',xi,'Ydata',yi);   
            end
            % update water level
            [~,ind1] = min(abs(yb-z));
            ind2 = length(xb)+1-ind1;
            ph = findobj(get(self.bAx,'Children'),'Tag','waterLevel');
            if isempty(ph)
                ph = area(self.bAx,xb([ind1:ind2 ind1]),yb([ind1:ind2 ind1]),'EdgeColor','None','FaceColor',[0.7 0.78 1]);
                set(ph,'Tag','waterLevel');
            else
                set(ph,'Xdata',xb([ind1:ind2 ind1]),'Ydata',yb([ind1:ind2 ind1]));
            end
            % update critical water depth
            self.breach.setReservoirLevel(z,self.ritter(ii));
            hc = self.breach.getWaterDepth();
            ymin = min(yb);
            [~,ind] = min(abs(yb-(ymin+hc)));
            xh = xb(ind);
            ph = findobj(get(self.bAx,'Children'),'Tag','waterDepth');
            if isempty(ph)
                ph = plot(self.bAx,xh.*[-1 1],ymin+hc.*[1 1],'b--');
                set(ph,'Tag','waterDepth');
            else
                set(ph,'Xdata',xh.*[-1 1],'Ydata',ymin+hc.*[1 1]);
            end
        end
    end
end

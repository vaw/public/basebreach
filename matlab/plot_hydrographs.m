N = 5000;
rate = 5000;
outfile = 'calibration.gif';
paramhat = lognfit(Y);
qloglim = [1 3];
cloglim = [log10(0.002) log10(0.04)];
qphat = logspace(qloglim(1),qloglim(2),1000);
phat = pdf('logn',qphat,paramhat(1),paramhat(2));

for i=1:1%floor(N/rate)
    % gifure options
    hf = figure('Color','white','Toolbar','none','Menu','none');
    i1 = (i-1)*rate;
    i2 = i*rate;
    
    % axes options
    ax1 = axes('Position',[0.25 0.25 0.7 0.7]);
    grid on; hold on; box on;
    set(ax1,'XScale','log','YScale','log','YAxisLocation','right','XAxisLocation','top');
    %xlabel('Kalibrierungsparameter [-]');
    %ylabel('Spitzenabfluss [m^3/s]');
    xlim(10.^cloglim);
    ylim(10.^qloglim);
    %{
    % plot evaluations
    qp = zeros(i2,1);
    c = zeros(i2,1);
    for j=1:i1
        qp(j) = Ypp{j}.getQpmax();
        c(j) = X(12,j);
        plot(ax1,c(j),qp(j),'marker','.','Color',[0.7 0.7 0.7],'MarkerSize',15);
    end
    for j=i1+1:i2
        qp(j) = Ypp{j}.getQpmax();
        c(j) = X(12,j);
        %plot(ax1,c(j),qp(j),'marker','.','Color',[0.4 0.4 0.9],'MarkerSize',15);
        plot(ax1,c(j),qp(j),'marker','.','Color',[0.5 0.6 0.9],'MarkerSize',5);
    end
    %}
    % plot histograms
    % peak discharges
    ax2 = axes('Position',[0.05 0.25 0.2 0.7]);
    plot(ax2,phat,log10(qphat),'Color',[0.3 0.9 0.4],'linewidth',2);
    set(ax2,'Xdir','reverse','Ylim',qloglim);
    set(ax2,'visible','off');
    hold on
    [p,x] = ksdensity(qp);
    inds = x>0;
    %A = area(ax2,p(inds),log10(x(inds)),'EdgeColor','None','FaceColor',[0.7 0.7 0.7]);
    %set(get(A,'Children'),'facealpha',0.5);
    %{
    % calibration parameter
    ax3 = axes('Position',[0.25 0.05 0.7 0.2]);
    plot(ax3,phat,log10(qphat),'Color',[0.3 0.9 0.4],'linewidth',2);
    set(ax3,'Ydir','reverse','Xlim',cloglim);
    set(ax3,'visible','off');
    hold on
    [p,x]=ksdensity(c);
    inds = x>0;
    A = area(ax3,log10(x(inds)),p(inds),'EdgeColor','None','FaceColor',[0.9 0.4 0.4]);
    %}
    % print options
    screenSize = get(0,'ScreenSize');
    height = screenSize(4)*0.4;
    width = height*1.5;
    set(hf,'Position',[screenSize(3)-100-width screenSize(4)-100-height width height]);
    drawnow
    [imind,cm] = rgb2ind(frame2im(getframe(1)),256);
    % the first time create the .gif file, subsequently append
    if i==1
        imwrite(imind,cm,outfile,'gif','DelayTime',1,'loopcount',inf);
    else
        imwrite(imind,cm,outfile,'gif','DelayTime',1,'writemode','append');
    end
    close all
end
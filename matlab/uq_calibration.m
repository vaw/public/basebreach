clear all
uqlab

%% INPUT PARAMETERS
% height of the dam
InputMarginals(1).Name = 'Hd';
InputMarginals(1).Type = 'uniform';
InputMarginals(1).Parameters = [5,100];
% width of the crest
InputMarginals(2).Name = 'wc';
InputMarginals(2).Type = 'lognormal';
InputMarginals(2).Moments = [1.55 0.51];
% embankment slope
InputMarginals(3).Name = 's';
InputMarginals(3).Type = 'normal';
InputMarginals(3).Parameters = [2.16 0.66];
InputMarginals(3).Bounds = [1 10];
% initial breach level (relative to dam height and reservoir level)
InputMarginals(4).Name = 'y0';
InputMarginals(4).Type = 'uniform';
InputMarginals(4).Parameters = [0.0 0.9];
% breach side shape
InputMarginals(5).Name = 'shape';
InputMarginals(5).Type = 'uniform';
InputMarginals(5).Parameters = [0.1 0.9];
% initial breach width
InputMarginals(6).Name = 'width';
InputMarginals(6).Type = 'uniform';
InputMarginals(6).Parameters = [0 1];
% erosion/friction parameter, exp_velocity
InputMarginals(7).Name = 'A';
InputMarginals(7).Type = 'uniform';
InputMarginals(7).Parameters = [3 5];
% erosion/friction parameter, exp_rhy / exp_velocity
InputMarginals(8).Name = 'C';
InputMarginals(8).Type = 'uniform';
InputMarginals(8).Parameters = [-0.2 0.25];
% reservoir volume
InputMarginals(9).Name = 'V';
InputMarginals(9).Type = 'uniform';
InputMarginals(9).Parameters = [2e4, 1e8];
% initial water level (relative to dam height)
InputMarginals(10).Name = 'z0';
InputMarginals(10).Type = 'uniform';
InputMarginals(10).Parameters = [0.5,1.0];
% reservoir shape
InputMarginals(11).Name = 'alpha';
InputMarginals(11).Type = 'uniform';
InputMarginals(11).Parameters = [1 4];
% peak discharge
InputMarginals(12).Name = 'Qp';
InputMarginals(12).Type = 'constant';
InputMarginals(12).Parameters = 144;
% create uq_input
DBinput = uq_create_input(InputMarginals);
% get experimental design
N = 1000;
X = uq_getSample(N,'sobol');
X(:,189) = []; % this set has no solution
N = 999; % therefore, correction

% %% MODEL
% opts_model.Name = 'H4mr6';
% opts_model.Type = 'mfile';
% opts_model.Function = 'findCGLOBAL';
% DBmodel = uq_create_model(opts_model);
% already done ...
%% LOAD DATA
load BASEbreach999;

%% CONVERT DATA FOR HYDROGRAPH ANALYSIS
Np = 1000; % number of grid points (PCE models)
% get maximal time
tMax = 0.0;
for ii=1:N
    tMax = max(tMax,Ypp{ii}.postProcess.getDuration());
end
% get normalized hydrographs
for ii=1:N
    Ypp{ii}.hydrographNormalized = Ypp{ii}.postProcess.getNormalizedHydrograph(tMax,Np);
    Y(:,ii) = Ypp{ii}.hydrographNormalized(:,2);
end

%% PCE METAMODEL
opts_meta.Type = 'uq_metamodel';
opts_meta.MetaType = 'PCE';
opts_meta.Input = DBinput;
%opts_meta.FullModel = DBmodel;
opts_meta.Coeff.Truncation.Type = 'q-norm';
opts_meta.Coeff.Truncation.Value = 0.75;
opts_meta.Lars.early_stop = true;
opts_meta.Coeff.Degree = 2:10;
opts_meta.ExpDesign.Sampling = 'user';
opts_meta.ExpDesign.X = X;
opts_meta.ExpDesign.Y = Y;
opts_meta.Name = 'PCE, N=999';
DBpce = uq_create_model(opts_meta);
% calculate the metamodel
uq_calculate_metamodel;
%     
% 
% %% Sensitivity analysis
% AnalysisOpts.Name = 'Sensitivity Analysis';
% AnalysisOpts.Type = 'uq_sensitivity';
% AnalysisOpts.Method = 'Sobol';
% AnalysisOpts.Sobol.Order = 2;
% uq_create_analysis(AnalysisOpts);
% % run sensitivity analysis
% uq_runAnalysis;
% 
% %% Plots
% results = UQ_analysis.Results{1};
% varNames = results.VariableNames;
% varIds = results.VarIdx;
% sobol = results.SobolIndices;
% totsobol = results.TotalSobolIndices;

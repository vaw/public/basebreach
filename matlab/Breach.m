classdef Breach < handle
    
    % public variables
    properties (Access = public)
        % none
    end
    
    % protected variables
    properties (SetAccess = private, GetAccess = public)
        Hd; wc; s;
        Y0; B0; sigma;
        Y; B;
        H0; H;
        omega0; omega;
        lambda;
        Bd; r; dVbdY;
        alpha;
        hc; vc; Q, Rhy;
        ritter;
    end
    
    % private variables
    properties (Access = private)
        a;
        b;
        xi;
        yi;
    end
    
    % constants
    properties (Constant)
        delta = 0.1;
        g = 9.80665;
    end
    
    % public methods
    methods (Access = public)
        % constructor with the parameters that define the geometry
        function self = Breach (Hd,wc,s,y0,b0,sigma)
            self.Hd = Hd;
            self.wc = wc;
            self.s = s;
            self.Y0 = y0;
            self.B0 = b0;
            self.sigma = sigma;
            self.init();
        end
        % setting the breach level
        function setLevel (self,y)
            self.Y = y;
            % resetting h and B
            self.updateLevelAndWidth();
            % resetting all the parameters that describe the polynomials
            self.updateShape();
            % update for hydraulic calculations
            self.alpha = 2*self.lambda / (2*self.lambda+1);
            % resetting the output vectors xi and yi
            self.calcShape();
        end
        % returns the shape as two column vectors
        function [x,y] = getShape (self)
            x = [-self.xi(end:-1:2) self.xi]';
            y = max(0,self.Y)+[self.yi(end:-1:2) self.yi]';
        end
        % returns the flow area for given water depth
        function out = calcFlowArea(self,h)
            out = self.b*h^self.lambda;
        end
        % returns the wetted perimeter for given water depth
        function out = calcWettedPerimeter (self,h)
            if self.lambda==1
                out = self.B+2*h;
            else
                xh = (h/self.a)^(1/self.omega);
                x = linspace(0,xh,1e6);
                dx = x(2)-x(1);
                y = self.a*x.^self.omega;
                dy = y(2:end)-y(1:end-1);
                ds = sqrt(dy.^2+dx.^2);
                out = 2*sum(ds);
            end
        end
        % returns the erodible perimeter for given water depth
        function out = calcErodiblePerimeter (self,h)
            % the same for Y>0 and Y<0 ... ?!?
            out = self.calcWettedPerimeter(h);
        end
        % setting a new reservoir level and calculate all hydraulic
        % variables. if 'ritter' is set to ~= 0, then a non-steady solution
        % is given.
        function setReservoirLevel(self,z,opt)
            if z<self.Y
                error('reservoir level below breach level!');
            end
            if ~exist('opt','var')
                self.ritter = false;
            else
                if opt
                    self.ritter = true;
                else
                    self.ritter = false;
                end
            end
            self.calcHydraulicVariables(z);
        end
        % returns the water depth in the breach
        function out = getWaterDepth (self)
            out = self.hc;
        end
        % returns the flow velocity in the breach
        function out = getFlowVelocity (self)
            out = self.vc;
        end
        % returns the discharge through the breach
        function out = getDischarge (self)
            out = self.Q;
        end
        % returns the hydraulic radius
        function out = getHydraulicRadius (self)
            out = self.Rhy;
        end
    end
    
    % private methods
    methods (Access = private)
        % doing all the initialization stuff for a given geometry configuration
        function init (self)
            self.H0 = self.Hd-self.Y0;
            self.omega0 = log(self.delta) / log(self.sigma*(1.0-self.delta)+self.delta);
            self.Bd = self.B0 * (1+self.Y0/(self.H0*self.omega0));
            omegaBottom = (self.H0*self.omega0+self.Y0)/self.Hd;
            lambdaBottom = (omegaBottom+1.0)/omegaBottom;
            self.r = (-1.0) * self.Bd/self.Hd * (self.wc*lambdaBottom + 2*self.s*self.Hd) / (self.wc + 2*self.s*self.Bd);
            self.setLevel(self.Y0);
            self.setReservoirLevel(self.Hd);
        end
        % definition of the development of the width and height of the
        % breach, dependent on the breach level
        function updateLevelAndWidth (self)
            if self.Y>self.Y0
                self.Y = NaN;
                error('New level is above the initial level ...!')
            elseif self.Y>=0
                self.H = self.Hd-self.Y;
                self.B = self.B0 * (1.0+(self.Y0-self.Y)/(self.H0*self.omega0));
            else
                self.H = self.Hd;
                self.B = self.Bd + self.r*self.Y;
            end
        end
        % recalculating the polynomials a*x^omega and b*x^lambda
        function updateShape (self)
            self.omega = self.H0/self.H * self.B/self.B0 * self.omega0;
            self.a = self.H * (2.0/self.B)^self.omega;
            if self.a==0 || isinf(self.a)
                % switch to rectangle
                self.omega = 0;
                self.a = 0;
                self.lambda = 1;
                self.b = self.B;
            else
                % regular shape
                self.lambda = (self.omega+1.0)/self.omega;
                self.b = self.B / (self.lambda * self.H^(self.lambda-1));
            end
            % update volume change rate
            if self.Y>0
                self.dVbdY = (-1)*self.B/self.lambda*(self.wc*self.lambda+2*self.s*self.H);
            else
                self.dVbdY = self.r*self.Hd/self.lambda*(self.wc+2*self.s*self.B);
            end
        end
        % building the breach shape in form of xi and yi (1000 points)
        function calcShape (self)
            self.xi = linspace(0,0.5*self.B,1000);
            if self.omega==0
                % rectangle
                self.yi = [zeros(1,999) self.H];
            else
                % regular
                self.yi = self.a.*self.xi.^(self.omega);
            end
        end
        % calculating all hydraulic variables, given a reservoir level 'z'
        function calcHydraulicVariables (self,z)
            % parameter for discharge, dpendent only on geometric variables
            beta = self.B/self.H^(self.lambda-1)*sqrt(self.g/self.lambda^3);
            if self.ritter
                % water depth
                self.hc = self.alpha^2*z; % could be anything between ritter and poleni ... ?!
                % flow velocity
                self.vc = sqrt(self.g*self.alpha^2/self.lambda*z);
                % breach discharge
                self.Q = beta*(self.alpha^2*z)^(self.lambda/self.alpha);
            else
                h = z-max(0,self.Y);
                % water depth
                self.hc = self.alpha*h;
                % flow velocity
                self.vc = sqrt(self.g*self.alpha/self.lambda*h);
                % breach discharge
                self.Q = beta*(self.alpha*h)^(self.lambda/self.alpha);
            end
            % hydraulic radius
            A = self.calcFlowArea(self.hc);
            P = self.calcWettedPerimeter(self.hc);
            self.Rhy = A/P;
        end
    end
end
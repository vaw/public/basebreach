# Developer Notes

This document covers how the BASEbreach application is structured and how its components work together.

It is meant to help new contributors get a grasp at where to start when they are developing new features or components - please make sure to update it along any architectural changes made.

## Application

In Qt, general application functionality is implemented in `QCoreApplication`. `QApplication` then extends this base with widget-based GUI functionality.

For BASEbreach, both bases are used for the GUI and command line versions of the application. The resulting subclasses `BaseBreachGui` and `BaseBreachCli` are implemented in the independent modules `gui/app.cpp` and `cli/app.cpp` respectively.

All core functionality is then implemented in a standalone `QObject` subclass called `BaseBreachCore` within `core.cpp`. This includes model registration, validation, I/O, running and loading simulations, etc.

The `main.cpp` application entry point then switches between the GUI and CL variants of the application based on the number of command line arguments passed and instantiates the corresponding Qt application subclass.

### Graphical User Interface

The GUI implementation uses the CMake UIC (User Interface Compiler) to generate C++ versions of the UI-files defined via Qt Designer. These are generated as part of the Qt MOC run in the build directory. Ensure that CMake supports the `AUTOUIC` flag if you get errors about headers such as `ui_mainWindow.h` missing.

Model-specific GUI widgets are dynamically generated using generic model definitions and are realized by the `modelTab.h` frame, which includes a factory method for generating model-specific widgets.

#### MC-specific `ModelTab` subclass

For models supporting Monte Carlo, the `ModelTabMC` subclass provides extra hooks for starting and processing MC model runs. Currently, only the Peter and PeterCal models support MC, though this is arbitrarily set as part of the `MainWindow::addModelTab()` method.

There is nothing about the MC setup that would make other models incompatible, at least on the GUI side (model-specific quirks and stability issues are another matter).

### Legacy Modules

Some legacy interfaces were kept due to time constraints, with thin wrappers added to make the compatible with the rewrite.

One such interface is the `Parser` class, which is a hybrid of an input file parser, parameter validation, and a model factory. It is deeply tied to the `DBFactory`, command line interface and -output, and GUI parsing (which is done by injecting the GUI values into a file-less Parser instance) and could not be eliminated in the time available.

Similarly, the `model::Parameter` class is exclusively used to sample MC distributions, without being directly tied to the generic model-wide `modelDef::Parameter` type defined in `modelTypes.cpp`. These two are not related and could be merged/simplified to tidy things up.

## Defining and Altering Models

The progressive breach models are implemented by various modules in the `model/` directory: `reservoir.cpp`, `dam.cpp`, `erosion.cpp`, `geometry.cpp`, and so on. These model components are then combined by `GeometryWithNumerics.cpp` and `fullModel.cpp`, which is a wrapper class implementing the entire model.

Modifying this was beyond the scope of the 2021 rewrite, so this structure was kept as-is.

To separate this legacy design with the new multimodel GUI, the models were separated into distinct model types, which define the parameters and limits for each of these implementations. These model types are then handed to the GUI, which uses them to lay out the interface dynamically and run the models, regardless of the underlying model implementation.

The user-facing model types, parameters and limit classes are implemented in `model/modelDefinition.cpp`.

### Standard Breach Model

The standard breach model (`model/standardBreach.cpp`) is not considered a "model" in this documentation or in the code base as it is not progressive. It also gets special treatment in the GUI as it does not provide estimates for most fields and does not produce a time series or hydrograph.

### Model Types

Model types define the user-facing model data, the available parameters, any applicable value limits, etc., and converts these parameters into the format required to run the underlying model.

They are defined in `modelTypes.cpp`.

### Model Implementations

Model type instances provide an internal factory method that constructs a model of its type. This is effectively a wrapper around the `model::FullModel` class to avoid needing to reverse-engineer that inheritance and implementation tree.

### Parameters

Parameters are named fields that can be added to a model type. Parameters added to a model will show up in the GUI and may be re-used across models.

### Parameter Limits

Every parameter can have an arbitrary number of limits. These generally have a lower and upper bound for the value permissible for this parameter.

Parameter limits come in different types:

- CRITICAL: If this limit is exceeded, the model cannot run. This is generally used for geometric nonsense, such as dams with a negative height.

- WARNING: The model may be run, but the user will be prompted that warnings were raised and require interpretation. These should be used for geometrically valid inputs that may result in unstable or meaningless models (such a breach side angle of 5 degrees).

- INFORMATION: These are used for low-importance messages that should be logged, but don't necessarily invalidate the model run, such as a value being (barely) outside the calibration range of a model.

- NONE: Internal sentinel value used to signify that a limit was not exceeded. Do not define parameter limits using this limit type as they will be ignored.

## Creating Installer Packages

The CMake configuration and scripts provided in this repository can be used to create Windows installer packages for the BASEbreach application.

This is done by first building the BASEbreach package once using the "Release" configuration in CMake. This is necessary prior to creating the installer package as the package creation toolchain must run the `windeployqt` utility over this executable to determine which shared Qt libraries must be included.

The scripts in the `cmake/` directory of the repository automate this process by first analysing the created executable using `windeployqt` and then deploying the libraries it identified to a temporary directory. This directory is then registered via a CMake `install()` call, which ultimately informs CPack that these files must be included in the installer files.

To summarize, here are the steps required:

1. Reconfigure CMake for the "Release" configuration.
2. Perform a manual build of the executable using the Release configuration.
3. Navigate to the build directory and run CPack using the configuration generated by CMake.

The finished installer package will be generated in the current build directory.
